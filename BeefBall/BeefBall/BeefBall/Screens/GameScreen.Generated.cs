#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using BeefBall.Entities;
using FlatRedBall;
using FlatRedBall.Screens;
using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall.Math;
namespace BeefBall.Screens
{
    public partial class GameScreen : FlatRedBall.Screens.Screen
    {
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        
        private FlatRedBall.Math.PositionedObjectList<FlatRedBall.Math.Geometry.AxisAlignedRectangle> Walls;
        private FlatRedBall.Math.Geometry.AxisAlignedRectangle mWall1;
        public FlatRedBall.Math.Geometry.AxisAlignedRectangle Wall1
        {
            get
            {
                return mWall1;
            }
            private set
            {
                mWall1 = value;
            }
        }
        private FlatRedBall.Math.Geometry.AxisAlignedRectangle mWall2;
        public FlatRedBall.Math.Geometry.AxisAlignedRectangle Wall2
        {
            get
            {
                return mWall2;
            }
            private set
            {
                mWall2 = value;
            }
        }
        private FlatRedBall.Math.Geometry.AxisAlignedRectangle mWall3;
        public FlatRedBall.Math.Geometry.AxisAlignedRectangle Wall3
        {
            get
            {
                return mWall3;
            }
            private set
            {
                mWall3 = value;
            }
        }
        private FlatRedBall.Math.Geometry.AxisAlignedRectangle mWall4;
        public FlatRedBall.Math.Geometry.AxisAlignedRectangle Wall4
        {
            get
            {
                return mWall4;
            }
            private set
            {
                mWall4 = value;
            }
        }
        private FlatRedBall.Math.Geometry.AxisAlignedRectangle mWall5;
        public FlatRedBall.Math.Geometry.AxisAlignedRectangle Wall5
        {
            get
            {
                return mWall5;
            }
            private set
            {
                mWall5 = value;
            }
        }
        private FlatRedBall.Math.Geometry.AxisAlignedRectangle mWall6;
        public FlatRedBall.Math.Geometry.AxisAlignedRectangle Wall6
        {
            get
            {
                return mWall6;
            }
            private set
            {
                mWall6 = value;
            }
        }
        private BeefBall.Entities.Puck PuckInstance;
        private FlatRedBall.Math.PositionedObjectList<BeefBall.Entities.PlayerBall> PLayerBallList;
        private BeefBall.Entities.PlayerBall PlayerBallInstance;
        private BeefBall.Entities.PlayerBall PlayerBallInstance2;
        private FlatRedBall.Math.PositionedObjectList<FlatRedBall.Math.Geometry.AxisAlignedRectangle> Goals;
        private FlatRedBall.Math.Geometry.AxisAlignedRectangle mLeftGoal;
        public FlatRedBall.Math.Geometry.AxisAlignedRectangle LeftGoal
        {
            get
            {
                return mLeftGoal;
            }
            private set
            {
                mLeftGoal = value;
            }
        }
        private FlatRedBall.Math.Geometry.AxisAlignedRectangle mRigthGoal;
        public FlatRedBall.Math.Geometry.AxisAlignedRectangle RigthGoal
        {
            get
            {
                return mRigthGoal;
            }
            private set
            {
                mRigthGoal = value;
            }
        }
        private BeefBall.Entities.ScoreHud ScoreHudInstance;
        public GameScreen () 
        	: base ("GameScreen")
        {
        }
        public override void Initialize (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            Walls = new FlatRedBall.Math.PositionedObjectList<FlatRedBall.Math.Geometry.AxisAlignedRectangle>();
            Walls.Name = "Walls";
            mWall1 = new FlatRedBall.Math.Geometry.AxisAlignedRectangle();
            mWall1.Name = "mWall1";
            mWall2 = new FlatRedBall.Math.Geometry.AxisAlignedRectangle();
            mWall2.Name = "mWall2";
            mWall3 = new FlatRedBall.Math.Geometry.AxisAlignedRectangle();
            mWall3.Name = "mWall3";
            mWall4 = new FlatRedBall.Math.Geometry.AxisAlignedRectangle();
            mWall4.Name = "mWall4";
            mWall5 = new FlatRedBall.Math.Geometry.AxisAlignedRectangle();
            mWall5.Name = "mWall5";
            mWall6 = new FlatRedBall.Math.Geometry.AxisAlignedRectangle();
            mWall6.Name = "mWall6";
            PuckInstance = new BeefBall.Entities.Puck(ContentManagerName, false);
            PuckInstance.Name = "PuckInstance";
            PLayerBallList = new FlatRedBall.Math.PositionedObjectList<BeefBall.Entities.PlayerBall>();
            PLayerBallList.Name = "PLayerBallList";
            PlayerBallInstance = new BeefBall.Entities.PlayerBall(ContentManagerName, false);
            PlayerBallInstance.Name = "PlayerBallInstance";
            PlayerBallInstance2 = new BeefBall.Entities.PlayerBall(ContentManagerName, false);
            PlayerBallInstance2.Name = "PlayerBallInstance2";
            Goals = new FlatRedBall.Math.PositionedObjectList<FlatRedBall.Math.Geometry.AxisAlignedRectangle>();
            Goals.Name = "Goals";
            mLeftGoal = new FlatRedBall.Math.Geometry.AxisAlignedRectangle();
            mLeftGoal.Name = "mLeftGoal";
            mRigthGoal = new FlatRedBall.Math.Geometry.AxisAlignedRectangle();
            mRigthGoal.Name = "mRigthGoal";
            ScoreHudInstance = new BeefBall.Entities.ScoreHud(ContentManagerName, false);
            ScoreHudInstance.Name = "ScoreHudInstance";
            
            
            PostInitialize();
            base.Initialize(addToManagers);
            if (addToManagers)
            {
                AddToManagers();
            }
        }
        public override void AddToManagers () 
        {
            FlatRedBall.Math.Geometry.ShapeManager.AddAxisAlignedRectangle(mWall1);
            FlatRedBall.Math.Geometry.ShapeManager.AddAxisAlignedRectangle(mWall2);
            FlatRedBall.Math.Geometry.ShapeManager.AddAxisAlignedRectangle(mWall3);
            FlatRedBall.Math.Geometry.ShapeManager.AddAxisAlignedRectangle(mWall4);
            FlatRedBall.Math.Geometry.ShapeManager.AddAxisAlignedRectangle(mWall5);
            FlatRedBall.Math.Geometry.ShapeManager.AddAxisAlignedRectangle(mWall6);
            PuckInstance.AddToManagers(mLayer);
            PlayerBallInstance.AddToManagers(mLayer);
            PlayerBallInstance2.AddToManagers(mLayer);
            FlatRedBall.Math.Geometry.ShapeManager.AddAxisAlignedRectangle(mLeftGoal);
            FlatRedBall.Math.Geometry.ShapeManager.AddAxisAlignedRectangle(mRigthGoal);
            ScoreHudInstance.AddToManagers(mLayer);
            base.AddToManagers();
            AddToManagersBottomUp();
            CustomInitialize();
        }
        public override void Activity (bool firstTimeCalled) 
        {
            if (!IsPaused)
            {
                
                PuckInstance.Activity();
                for (int i = PLayerBallList.Count - 1; i > -1; i--)
                {
                    if (i < PLayerBallList.Count)
                    {
                        // We do the extra if-check because activity could destroy any number of entities
                        PLayerBallList[i].Activity();
                    }
                }
                ScoreHudInstance.Activity();
            }
            else
            {
            }
            base.Activity(firstTimeCalled);
            if (!IsActivityFinished)
            {
                CustomActivity(firstTimeCalled);
            }
        }
        public override void Destroy () 
        {
            base.Destroy();
            
            Walls.MakeOneWay();
            PLayerBallList.MakeOneWay();
            Goals.MakeOneWay();
            for (int i = Walls.Count - 1; i > -1; i--)
            {
                FlatRedBall.Math.Geometry.ShapeManager.Remove(Walls[i]);
            }
            if (PuckInstance != null)
            {
                PuckInstance.Destroy();
                PuckInstance.Detach();
            }
            for (int i = PLayerBallList.Count - 1; i > -1; i--)
            {
                PLayerBallList[i].Destroy();
            }
            for (int i = Goals.Count - 1; i > -1; i--)
            {
                FlatRedBall.Math.Geometry.ShapeManager.Remove(Goals[i]);
            }
            if (ScoreHudInstance != null)
            {
                ScoreHudInstance.Destroy();
                ScoreHudInstance.Detach();
            }
            Walls.MakeTwoWay();
            PLayerBallList.MakeTwoWay();
            Goals.MakeTwoWay();
            FlatRedBall.Math.Collision.CollisionManager.Self.Relationships.Clear();
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            Walls.Add(Wall1);
            if (Wall1.Parent == null)
            {
                Wall1.Y = 300f;
            }
            else
            {
                Wall1.RelativeY = 300f;
            }
            Wall1.Width = 800f;
            Wall1.Height = 30f;
            Walls.Add(Wall2);
            if (Wall2.Parent == null)
            {
                Wall2.X = 0f;
            }
            else
            {
                Wall2.RelativeX = 0f;
            }
            if (Wall2.Parent == null)
            {
                Wall2.Y = -300f;
            }
            else
            {
                Wall2.RelativeY = -300f;
            }
            Wall2.Width = 800f;
            Wall2.Height = 30f;
            Walls.Add(Wall3);
            if (Wall3.Parent == null)
            {
                Wall3.X = -400f;
            }
            else
            {
                Wall3.RelativeX = -400f;
            }
            if (Wall3.Parent == null)
            {
                Wall3.Y = 200f;
            }
            else
            {
                Wall3.RelativeY = 200f;
            }
            Wall3.Width = 30f;
            Wall3.Height = 200f;
            Walls.Add(Wall4);
            if (Wall4.Parent == null)
            {
                Wall4.X = 400f;
            }
            else
            {
                Wall4.RelativeX = 400f;
            }
            if (Wall4.Parent == null)
            {
                Wall4.Y = 200f;
            }
            else
            {
                Wall4.RelativeY = 200f;
            }
            Wall4.Width = 30f;
            Wall4.Height = 200f;
            Walls.Add(Wall5);
            if (Wall5.Parent == null)
            {
                Wall5.X = -400f;
            }
            else
            {
                Wall5.RelativeX = -400f;
            }
            if (Wall5.Parent == null)
            {
                Wall5.Y = -200f;
            }
            else
            {
                Wall5.RelativeY = -200f;
            }
            Wall5.Width = 30f;
            Wall5.Height = 200f;
            Walls.Add(Wall6);
            if (Wall6.Parent == null)
            {
                Wall6.X = 400f;
            }
            else
            {
                Wall6.RelativeX = 400f;
            }
            if (Wall6.Parent == null)
            {
                Wall6.Y = -200f;
            }
            else
            {
                Wall6.RelativeY = -200f;
            }
            Wall6.Width = 20f;
            Wall6.Height = 200f;
            PLayerBallList.Add(PlayerBallInstance);
            if (PlayerBallInstance.Parent == null)
            {
                PlayerBallInstance.X = 180f;
            }
            else
            {
                PlayerBallInstance.RelativeX = 180f;
            }
            if (PlayerBallInstance.Parent == null)
            {
                PlayerBallInstance.Y = 0f;
            }
            else
            {
                PlayerBallInstance.RelativeY = 0f;
            }
            PlayerBallInstance.MovementSpeed = 100f;
            PlayerBallInstance.Drag = 1f;
            PlayerBallInstance.CircleInstanceColor = Color.Aqua;
            PLayerBallList.Add(PlayerBallInstance2);
            if (PlayerBallInstance2.Parent == null)
            {
                PlayerBallInstance2.X = -180f;
            }
            else
            {
                PlayerBallInstance2.RelativeX = -180f;
            }
            PlayerBallInstance2.MovementSpeed = 100f;
            PlayerBallInstance2.Drag = 1f;
            Goals.Add(LeftGoal);
            if (LeftGoal.Parent == null)
            {
                LeftGoal.X = -400f;
            }
            else
            {
                LeftGoal.RelativeX = -400f;
            }
            LeftGoal.Width = 1f;
            LeftGoal.Height = 200f;
            Goals.Add(RigthGoal);
            if (RigthGoal.Parent == null)
            {
                RigthGoal.X = 400f;
            }
            else
            {
                RigthGoal.RelativeX = 400f;
            }
            RigthGoal.Width = 1f;
            RigthGoal.Height = 200f;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp () 
        {
            CameraSetup.ResetCamera(SpriteManager.Camera);
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            for (int i = Walls.Count - 1; i > -1; i--)
            {
                FlatRedBall.Math.Geometry.ShapeManager.Remove(Walls[i]);
            }
            PuckInstance.RemoveFromManagers();
            for (int i = PLayerBallList.Count - 1; i > -1; i--)
            {
                PLayerBallList[i].Destroy();
            }
            for (int i = Goals.Count - 1; i > -1; i--)
            {
                FlatRedBall.Math.Geometry.ShapeManager.Remove(Goals[i]);
            }
            ScoreHudInstance.RemoveFromManagers();
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
                PuckInstance.AssignCustomVariables(true);
                PlayerBallInstance.AssignCustomVariables(true);
                PlayerBallInstance2.AssignCustomVariables(true);
                ScoreHudInstance.AssignCustomVariables(true);
            }
            if (Wall1.Parent == null)
            {
                Wall1.Y = 300f;
            }
            else
            {
                Wall1.RelativeY = 300f;
            }
            Wall1.Width = 800f;
            Wall1.Height = 30f;
            if (Wall2.Parent == null)
            {
                Wall2.X = 0f;
            }
            else
            {
                Wall2.RelativeX = 0f;
            }
            if (Wall2.Parent == null)
            {
                Wall2.Y = -300f;
            }
            else
            {
                Wall2.RelativeY = -300f;
            }
            Wall2.Width = 800f;
            Wall2.Height = 30f;
            if (Wall3.Parent == null)
            {
                Wall3.X = -400f;
            }
            else
            {
                Wall3.RelativeX = -400f;
            }
            if (Wall3.Parent == null)
            {
                Wall3.Y = 200f;
            }
            else
            {
                Wall3.RelativeY = 200f;
            }
            Wall3.Width = 30f;
            Wall3.Height = 200f;
            if (Wall4.Parent == null)
            {
                Wall4.X = 400f;
            }
            else
            {
                Wall4.RelativeX = 400f;
            }
            if (Wall4.Parent == null)
            {
                Wall4.Y = 200f;
            }
            else
            {
                Wall4.RelativeY = 200f;
            }
            Wall4.Width = 30f;
            Wall4.Height = 200f;
            if (Wall5.Parent == null)
            {
                Wall5.X = -400f;
            }
            else
            {
                Wall5.RelativeX = -400f;
            }
            if (Wall5.Parent == null)
            {
                Wall5.Y = -200f;
            }
            else
            {
                Wall5.RelativeY = -200f;
            }
            Wall5.Width = 30f;
            Wall5.Height = 200f;
            if (Wall6.Parent == null)
            {
                Wall6.X = 400f;
            }
            else
            {
                Wall6.RelativeX = 400f;
            }
            if (Wall6.Parent == null)
            {
                Wall6.Y = -200f;
            }
            else
            {
                Wall6.RelativeY = -200f;
            }
            Wall6.Width = 20f;
            Wall6.Height = 200f;
            if (PlayerBallInstance.Parent == null)
            {
                PlayerBallInstance.X = 180f;
            }
            else
            {
                PlayerBallInstance.RelativeX = 180f;
            }
            if (PlayerBallInstance.Parent == null)
            {
                PlayerBallInstance.Y = 0f;
            }
            else
            {
                PlayerBallInstance.RelativeY = 0f;
            }
            PlayerBallInstance.MovementSpeed = 100f;
            PlayerBallInstance.Drag = 1f;
            PlayerBallInstance.CircleInstanceColor = Color.Aqua;
            if (PlayerBallInstance2.Parent == null)
            {
                PlayerBallInstance2.X = -180f;
            }
            else
            {
                PlayerBallInstance2.RelativeX = -180f;
            }
            PlayerBallInstance2.MovementSpeed = 100f;
            PlayerBallInstance2.Drag = 1f;
            if (LeftGoal.Parent == null)
            {
                LeftGoal.X = -400f;
            }
            else
            {
                LeftGoal.RelativeX = -400f;
            }
            LeftGoal.Width = 1f;
            LeftGoal.Height = 200f;
            if (RigthGoal.Parent == null)
            {
                RigthGoal.X = 400f;
            }
            else
            {
                RigthGoal.RelativeX = 400f;
            }
            RigthGoal.Width = 1f;
            RigthGoal.Height = 200f;
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            PuckInstance.ConvertToManuallyUpdated();
            for (int i = 0; i < PLayerBallList.Count; i++)
            {
                PLayerBallList[i].ConvertToManuallyUpdated();
            }
            ScoreHudInstance.ConvertToManuallyUpdated();
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            BeefBall.Entities.Puck.LoadStaticContent(contentManagerName);
            BeefBall.Entities.ScoreHud.LoadStaticContent(contentManagerName);
            CustomLoadStaticContent(contentManagerName);
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            return null;
        }
        public static object GetFile (string memberName) 
        {
            return null;
        }
        object GetMember (string memberName) 
        {
            return null;
        }
    }
}
