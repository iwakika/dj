using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;
using FlatRedBall.Localization;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace BeefBall.Screens
{
	public partial class GameScreen    
	{

        int player1Score = 0;
        int player2Score = 0;


        void CustomInitialize()
		{
            AssingInput();


        }

		void CustomActivity(bool firstTimeCalled)
		{

            ColisionActivity();
		}

		void CustomDestroy()
		{


		}

        static void CustomLoadStaticContent(string contentManagerName)
        {


        }


        private void AssingInput()
        {
            PlayerBallInstance.MovementInput = InputManager.Keyboard.Get2DInput(Keys.A, Keys.D, Keys.W, Keys.S);
            PlayerBallInstance.BoostInput = InputManager.Keyboard.GetKey(Keys.B);

            PlayerBallInstance2.MovementInput = InputManager.Keyboard.Get2DInput(Keys.Left, Keys.Right, Keys.Up, Keys.Down);
            PlayerBallInstance2.BoostInput = InputManager.Keyboard.GetKey(Keys.RightShift);
        }


        private void ColisionActivity()
        {



            //colision Puck vs Wall
            foreach (var wall in Walls)
            {
                float puckMass = 0;
                float wallMass = 1;
                float elasticity = 1;

                // PlayerBallInstance.CollideAgainstMove(wall, playerMass,wallMass); 
                PuckInstance.CollideAgainstBounce(wall, puckMass, wallMass, elasticity);
            }
            //Colision Player vs  Wall       
            foreach (var player in PLayerBallList)
            {
                foreach (var wall in Walls)
                {
                    float playerMass = 0;
                    float wallMass = 1;
                    float elasticity = 1;

                     PlayerBallInstance.CollideAgainstMove(wall, playerMass,wallMass); 
                 //   player.CollideAgainstBounce(wall, playerMass, wallMass, elasticity);

                }


                //colision player vs Goal
                foreach(var goal in Goals)
                {
                    float playerMass = 0;
                    float goalMass = 1;
                    float elasticity = 1;

                    player.CollideAgainstBounce(goal, playerMass, goalMass, elasticity);
                }

                //colision player vs player
                foreach (var otherPLayer in PLayerBallList)
                {

                    float playerMass = 1;
                    float elasticity = 1;

                    if (player != otherPLayer)
                    {

                        player.CollideAgainstBounce(otherPLayer, playerMass, playerMass, elasticity);

                    }
                }

                {    //Colision puck vs play
                    float puckMass = .3f;
                    float playMass = 1;
                    float elasticity = .8f;
                    PuckInstance.CollideAgainstBounce(player, puckMass, playMass, elasticity);

                }               
                                
            }

            //collision puck vs goals

            if (PuckInstance.CollideAgainst(LeftGoal))
            {
                player1Score++;
                ReacttoNewScore();
            }

            if (PuckInstance.CollideAgainst(RigthGoal))
            {
                player2Score++;
                ReacttoNewScore();
            }




        }

        //volta  para parāmetros iniciais
        private void ReacttoNewScore()
        {


            PlayerBallInstance.X = 180;
            PlayerBallInstance.Y = 0;
            PlayerBallInstance.Velocity = Vector3.Zero;
            PlayerBallInstance.Acceleration = Vector3.Zero;

            PlayerBallInstance2.X = -180;
            PlayerBallInstance2.Y = 0;
            PlayerBallInstance2.Velocity = Vector3.Zero;
            PlayerBallInstance2.Acceleration = Vector3.Zero;

            PuckInstance.X = 0;
            PuckInstance.Y = 0;
            PuckInstance.Velocity = Vector3.Zero;

            ScoreHudInstance.Score1 = player1Score;
            ScoreHudInstance.Score2 = player2Score;

        }



    }
}
