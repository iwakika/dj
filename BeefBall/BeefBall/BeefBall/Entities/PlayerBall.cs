using FlatRedBall;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.Math.Geometry;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Input;

namespace BeefBall.Entities
{
	public partial class PlayerBall
	{

        public I2DInput MovementInput { get; set; }// classe que le o movimento de entradas dos componetes
        public IPressableInput BoostInput { get; set; }
        private double lastTimeDashed = -1000;

        
        /// <summary>
        /// Initialization logic which is execute only one time for this Entity (unless the Entity is pooled).
        /// This method is called when the Entity is added to managers. Entities which are instantiated but not
        /// added to managers will not have this method called.
        /// </summary>
		private void CustomInitialize()
        {
		}

  
        private void CustomActivity()
		{
            MovementActivity();
            DashActivity();
    

        }

        private void CustomDestroy()
		{


		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        private void MovementActivity()
        {


            if (MovementInput != null)
            {
            /*    this.XVelocity = MovementInput.X * MovementSpeed;
                this.YVelocity = MovementInput.Y * MovementSpeed; */
                this.XAcceleration = MovementInput.X * MovementSpeed;
                this.YAcceleration = MovementInput.Y * MovementSpeed; 
            }


        }


        private void DashActivity()
        {
        
            if (BoostInput.WasJustPressed)
            {
                float magnitude = MovementInput.Magnitude;
                //bool isHoldingDirection = magnitude > 0;


                bool shouldBoost = BoostInput.WasJustPressed
                    && FlatRedBall.Screens.ScreenManager.CurrentScreen.PauseAdjustedSecondsSince(lastTimeDashed) > DashFrequency
                    && magnitude > 0;
                if (shouldBoost)
                {
                    // dividindo por magnitude nos diz o que X e Y
                    // seja se o usu�rio estivesse segurando a entrada completamente
                    // a dire��o atual.

                    lastTimeDashed =
                    FlatRedBall.Screens.ScreenManager.CurrentScreen.PauseAdjustedCurrentTime;

                    float nomalizedX = MovementInput.X / magnitude;
                    float nomalizedY = MovementInput.Y / magnitude;

                    XVelocity = nomalizedX * DashSpeed;
                    YVelocity = nomalizedY * DashSpeed;
                }
            }

        }
    }
}
