#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using BeefBall.Screens;
using FlatRedBall.Graphics;
using FlatRedBall.Math;
using BeefBall.Entities;
using FlatRedBall;
using FlatRedBall.Screens;
using System;
using System.Collections.Generic;
using System.Text;
namespace BeefBall.Entities
{
    public partial class ScoreHud : FlatRedBall.PositionedObject, FlatRedBall.Graphics.IDestroyable
    {
        // This is made static so that static lazy-loaded content can access it.
        public static string ContentManagerName { get; set; }
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        static object mLockObject = new object();
        static System.Collections.Generic.List<string> mRegisteredUnloads = new System.Collections.Generic.List<string>();
        static System.Collections.Generic.List<string> LoadedContentManagers = new System.Collections.Generic.List<string>();
        
        private FlatRedBall.Graphics.Text Team1ScoreLabel;
        private FlatRedBall.Graphics.Text Team1Score;
        private FlatRedBall.Graphics.Text Team2Score;
        private FlatRedBall.Graphics.Text Team2ScoreLabel;
        public int Score1
        {
            get
            {
                return int.Parse(Team1Score.DisplayText);
            }
            set
            {
                Team1Score.DisplayText = value.ToString();
            }
        }
        public int Score2
        {
            get
            {
                return int.Parse(Team2Score.DisplayText);
            }
            set
            {
                Team2Score.DisplayText = value.ToString();
            }
        }
        protected FlatRedBall.Graphics.Layer LayerProvidedByContainer = null;
        public ScoreHud () 
        	: this(FlatRedBall.Screens.ScreenManager.CurrentScreen.ContentManagerName, true)
        {
        }
        public ScoreHud (string contentManagerName) 
        	: this(contentManagerName, true)
        {
        }
        public ScoreHud (string contentManagerName, bool addToManagers) 
        	: base()
        {
            ContentManagerName = contentManagerName;
            InitializeEntity(addToManagers);
        }
        protected virtual void InitializeEntity (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            Team1ScoreLabel = new FlatRedBall.Graphics.Text();
            Team1ScoreLabel.Name = "Team1ScoreLabel";
            Team1Score = new FlatRedBall.Graphics.Text();
            Team1Score.Name = "Team1Score";
            Team2Score = new FlatRedBall.Graphics.Text();
            Team2Score.Name = "Team2Score";
            Team2ScoreLabel = new FlatRedBall.Graphics.Text();
            Team2ScoreLabel.Name = "Team2ScoreLabel";
            
            PostInitialize();
            if (addToManagers)
            {
                AddToManagers(null);
            }
        }
        public virtual void ReAddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.Graphics.TextManager.AddToLayer(Team1ScoreLabel, LayerProvidedByContainer);
            if (Team1ScoreLabel.Font != null)
            {
                Team1ScoreLabel.SetPixelPerfectScale(LayerProvidedByContainer);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(Team1Score, LayerProvidedByContainer);
            if (Team1Score.Font != null)
            {
                Team1Score.SetPixelPerfectScale(LayerProvidedByContainer);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(Team2Score, LayerProvidedByContainer);
            if (Team2Score.Font != null)
            {
                Team2Score.SetPixelPerfectScale(LayerProvidedByContainer);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(Team2ScoreLabel, LayerProvidedByContainer);
            if (Team2ScoreLabel.Font != null)
            {
                Team2ScoreLabel.SetPixelPerfectScale(LayerProvidedByContainer);
            }
        }
        public virtual void AddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.Graphics.TextManager.AddToLayer(Team1ScoreLabel, LayerProvidedByContainer);
            if (Team1ScoreLabel.Font != null)
            {
                Team1ScoreLabel.SetPixelPerfectScale(LayerProvidedByContainer);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(Team1Score, LayerProvidedByContainer);
            if (Team1Score.Font != null)
            {
                Team1Score.SetPixelPerfectScale(LayerProvidedByContainer);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(Team2Score, LayerProvidedByContainer);
            if (Team2Score.Font != null)
            {
                Team2Score.SetPixelPerfectScale(LayerProvidedByContainer);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(Team2ScoreLabel, LayerProvidedByContainer);
            if (Team2ScoreLabel.Font != null)
            {
                Team2ScoreLabel.SetPixelPerfectScale(LayerProvidedByContainer);
            }
            AddToManagersBottomUp(layerToAddTo);
            CustomInitialize();
        }
        public virtual void Activity () 
        {
            
            CustomActivity();
        }
        public virtual void Destroy () 
        {
            FlatRedBall.SpriteManager.RemovePositionedObject(this);
            
            if (Team1ScoreLabel != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveText(Team1ScoreLabel);
            }
            if (Team1Score != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveText(Team1Score);
            }
            if (Team2Score != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveText(Team2Score);
            }
            if (Team2ScoreLabel != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveText(Team2ScoreLabel);
            }
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            if (Team1ScoreLabel.Parent == null)
            {
                Team1ScoreLabel.CopyAbsoluteToRelative();
                Team1ScoreLabel.AttachTo(this, false);
            }
            Team1ScoreLabel.DisplayText = "Team 1:";
            if (Team1ScoreLabel.Parent == null)
            {
                Team1ScoreLabel.X = -205f;
            }
            else
            {
                Team1ScoreLabel.RelativeX = -205f;
            }
            if (Team1ScoreLabel.Parent == null)
            {
                Team1ScoreLabel.Y = 270f;
            }
            else
            {
                Team1ScoreLabel.RelativeY = 270f;
            }
            if (Team1Score.Parent == null)
            {
                Team1Score.CopyAbsoluteToRelative();
                Team1Score.AttachTo(this, false);
            }
            Team1Score.DisplayText = "99";
            if (Team1Score.Parent == null)
            {
                Team1Score.X = -150f;
            }
            else
            {
                Team1Score.RelativeX = -150f;
            }
            if (Team1Score.Parent == null)
            {
                Team1Score.Y = 270f;
            }
            else
            {
                Team1Score.RelativeY = 270f;
            }
            if (Team2Score.Parent == null)
            {
                Team2Score.CopyAbsoluteToRelative();
                Team2Score.AttachTo(this, false);
            }
            Team2Score.DisplayText = "99";
            if (Team2Score.Parent == null)
            {
                Team2Score.X = 180f;
            }
            else
            {
                Team2Score.RelativeX = 180f;
            }
            if (Team2Score.Parent == null)
            {
                Team2Score.Y = 270f;
            }
            else
            {
                Team2Score.RelativeY = 270f;
            }
            if (Team2ScoreLabel.Parent == null)
            {
                Team2ScoreLabel.CopyAbsoluteToRelative();
                Team2ScoreLabel.AttachTo(this, false);
            }
            Team2ScoreLabel.DisplayText = "Team 2:";
            if (Team2ScoreLabel.Parent == null)
            {
                Team2ScoreLabel.X = 124f;
            }
            else
            {
                Team2ScoreLabel.RelativeX = 124f;
            }
            if (Team2ScoreLabel.Parent == null)
            {
                Team2ScoreLabel.Y = 270f;
            }
            else
            {
                Team2ScoreLabel.RelativeY = 270f;
            }
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            if (Team1ScoreLabel != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveTextOneWay(Team1ScoreLabel);
            }
            if (Team1Score != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveTextOneWay(Team1Score);
            }
            if (Team2Score != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveTextOneWay(Team2Score);
            }
            if (Team2ScoreLabel != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveTextOneWay(Team2ScoreLabel);
            }
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
            }
            Team1ScoreLabel.DisplayText = "Team 1:";
            if (Team1ScoreLabel.Parent == null)
            {
                Team1ScoreLabel.X = -205f;
            }
            else
            {
                Team1ScoreLabel.RelativeX = -205f;
            }
            if (Team1ScoreLabel.Parent == null)
            {
                Team1ScoreLabel.Y = 270f;
            }
            else
            {
                Team1ScoreLabel.RelativeY = 270f;
            }
            Team1Score.DisplayText = "99";
            if (Team1Score.Parent == null)
            {
                Team1Score.X = -150f;
            }
            else
            {
                Team1Score.RelativeX = -150f;
            }
            if (Team1Score.Parent == null)
            {
                Team1Score.Y = 270f;
            }
            else
            {
                Team1Score.RelativeY = 270f;
            }
            Team2Score.DisplayText = "99";
            if (Team2Score.Parent == null)
            {
                Team2Score.X = 180f;
            }
            else
            {
                Team2Score.RelativeX = 180f;
            }
            if (Team2Score.Parent == null)
            {
                Team2Score.Y = 270f;
            }
            else
            {
                Team2Score.RelativeY = 270f;
            }
            Team2ScoreLabel.DisplayText = "Team 2:";
            if (Team2ScoreLabel.Parent == null)
            {
                Team2ScoreLabel.X = 124f;
            }
            else
            {
                Team2ScoreLabel.RelativeX = 124f;
            }
            if (Team2ScoreLabel.Parent == null)
            {
                Team2ScoreLabel.Y = 270f;
            }
            else
            {
                Team2ScoreLabel.RelativeY = 270f;
            }
            Score1 = 0;
            Score2 = 0;
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            this.ForceUpdateDependenciesDeep();
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            FlatRedBall.Graphics.TextManager.ConvertToManuallyUpdated(Team1ScoreLabel);
            FlatRedBall.Graphics.TextManager.ConvertToManuallyUpdated(Team1Score);
            FlatRedBall.Graphics.TextManager.ConvertToManuallyUpdated(Team2Score);
            FlatRedBall.Graphics.TextManager.ConvertToManuallyUpdated(Team2ScoreLabel);
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            ContentManagerName = contentManagerName;
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            bool registerUnload = false;
            if (LoadedContentManagers.Contains(contentManagerName) == false)
            {
                LoadedContentManagers.Add(contentManagerName);
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("ScoreHudStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            if (registerUnload && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("ScoreHudStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            CustomLoadStaticContent(contentManagerName);
        }
        public static void UnloadStaticContent () 
        {
            if (LoadedContentManagers.Count != 0)
            {
                LoadedContentManagers.RemoveAt(0);
                mRegisteredUnloads.RemoveAt(0);
            }
            if (LoadedContentManagers.Count == 0)
            {
            }
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            return null;
        }
        public static object GetFile (string memberName) 
        {
            return null;
        }
        object GetMember (string memberName) 
        {
            return null;
        }
        protected bool mIsPaused;
        public override void Pause (FlatRedBall.Instructions.InstructionList instructions) 
        {
            base.Pause(instructions);
            mIsPaused = true;
        }
        public virtual void SetToIgnorePausing () 
        {
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(this);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(Team1ScoreLabel);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(Team1Score);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(Team2Score);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(Team2ScoreLabel);
        }
        public virtual void MoveToLayer (FlatRedBall.Graphics.Layer layerToMoveTo) 
        {
            var layerToRemoveFrom = LayerProvidedByContainer;
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(Team1ScoreLabel);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(Team1ScoreLabel, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(Team1Score);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(Team1Score, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(Team2Score);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(Team2Score, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(Team2ScoreLabel);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(Team2ScoreLabel, layerToMoveTo);
            LayerProvidedByContainer = layerToMoveTo;
        }
    }
}
