    namespace FlatRedBall.Gum
    {
        public  class GumIdbExtensions
        {
            public static void RegisterTypes () 
            {
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("Circle", typeof(DungeonRun.GumRuntimes.CircleRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("ColoredRectangle", typeof(DungeonRun.GumRuntimes.ColoredRectangleRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("Container", typeof(DungeonRun.GumRuntimes.ContainerRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("NineSlice", typeof(DungeonRun.GumRuntimes.NineSliceRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("Rectangle", typeof(DungeonRun.GumRuntimes.RectangleRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("Sprite", typeof(DungeonRun.GumRuntimes.SpriteRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("Text", typeof(DungeonRun.GumRuntimes.TextRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("Button", typeof(DungeonRun.GumRuntimes.ButtonRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("DialogBox", typeof(DungeonRun.GumRuntimes.DialogBoxRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("EditTextBox", typeof(DungeonRun.GumRuntimes.EditTextBoxRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("GameOver_Display", typeof(DungeonRun.GumRuntimes.GameOver_DisplayRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("ItemDisplay", typeof(DungeonRun.GumRuntimes.ItemDisplayRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("ItemDisplay1", typeof(DungeonRun.GumRuntimes.ItemDisplay1Runtime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("LevelDisplay", typeof(DungeonRun.GumRuntimes.LevelDisplayRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("Name_Display", typeof(DungeonRun.GumRuntimes.Name_DisplayRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("Panel_Display", typeof(DungeonRun.GumRuntimes.Panel_DisplayRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("Panel2_Display", typeof(DungeonRun.GumRuntimes.Panel2_DisplayRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("PlayerPoints_display", typeof(DungeonRun.GumRuntimes.PlayerPoints_displayRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("TimeDisplay", typeof(DungeonRun.GumRuntimes.TimeDisplayRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("ComandScreen_HUD", typeof(DungeonRun.GumRuntimes.ComandScreen_HUDRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("Controle_HUD", typeof(DungeonRun.GumRuntimes.Controle_HUDRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("GameHudGum", typeof(DungeonRun.GumRuntimes.GameHudGumRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("MainMenuGum", typeof(DungeonRun.GumRuntimes.MainMenuGumRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("PlayPoints_HUD", typeof(DungeonRun.GumRuntimes.PlayPoints_HUDRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("RakingHUD", typeof(DungeonRun.GumRuntimes.RakingHUDRuntime));
            }
        }
    }
