    using System.Linq;
    namespace DungeonRun.GumRuntimes
    {
        public partial class GameOver_DisplayRuntime : DungeonRun.GumRuntimes.ContainerRuntime
        {
            #region State Enums
            public enum VariableState
            {
                Default
            }
            #endregion
            #region State Fields
            VariableState mCurrentVariableState;
            #endregion
            #region State Properties
            public VariableState CurrentVariableState
            {
                get
                {
                    return mCurrentVariableState;
                }
                set
                {
                    mCurrentVariableState = value;
                    switch(mCurrentVariableState)
                    {
                        case  VariableState.Default:
                            Height = 348f;
                            Width = 520f;
                            Painel.Height = 0f;
                            Painel.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            SetProperty("Painel.SourceFile", "panelInset_brown.png");
                            Painel.Width = 0f;
                            Painel.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            Painel.X = -2f;
                            Painel.Y = -1f;
                            RectangleInstance.Blue = 0;
                            RectangleInstance.Green = 0;
                            RectangleInstance.Height = -4f;
                            RectangleInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            RectangleInstance.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance1");
                            RectangleInstance.Red = 0;
                            RectangleInstance.Width = 3f;
                            RectangleInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            RectangleInstance.X = 0f;
                            RectangleInstance.Y = 1f;
                            TextNota.Font = "Impact";
                            TextNota.FontSize = 55;
                            TextNota.Height = 155f;
                            TextNota.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                            TextNota.OutlineThickness = 1;
                            TextNota.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                            TextNota.Text = "50";
                            TextNota.VerticalAlignment = RenderingLibrary.Graphics.VerticalAlignment.Center;
                            TextNota.Width = -2f;
                            TextNota.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            TextNota.X = 1f;
                            TextNota.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                            TextNota.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                            TextNota.Y = -3f;
                            TextNota.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Center;
                            TextNota.YUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                            TextResultado.Font = "Impact";
                            TextResultado.FontSize = 25;
                            TextResultado.Height = 35f;
                            TextResultado.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                            TextResultado.OutlineThickness = 2;
                            TextResultado.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                            TextResultado.Text = "RESULTADO";
                            TextResultado.VerticalAlignment = RenderingLibrary.Graphics.VerticalAlignment.Center;
                            TextResultado.Width = 157f;
                            TextResultado.X = 192f;
                            TextResultado.Y = 132f;
                            TextInstance.Blue = 0;
                            TextInstance.Font = "Arial";
                            TextInstance.FontSize = 35;
                            TextInstance.Green = 0;
                            TextInstance.Height = 41f;
                            TextInstance.OutlineThickness = 0;
                            TextInstance.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                            TextInstance.Red = 0;
                            TextInstance.Text = "Nota:";
                            TextInstance.Width = 128f;
                            TextInstance.X = 100f;
                            TextInstance.Y = 2f;
                            ContainerInstance.Height = 156f;
                            ContainerInstance.Width = 490f;
                            ContainerInstance.X = 7f;
                            ContainerInstance.Y = 11f;
                            GameOverMSG.Height = 121f;
                            GameOverMSG.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Left;
                            GameOverMSG.OutlineThickness = 1;
                            GameOverMSG.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "Avatar");
                            GameOverMSG.Text = "asdfasdfasdfasdfasdfasdfa";
                            GameOverMSG.VerticalAlignment = RenderingLibrary.Graphics.VerticalAlignment.Center;
                            GameOverMSG.Width = 354f;
                            GameOverMSG.X = 109f;
                            GameOverMSG.Y = -20f;
                            ContainerInstance1.Width = 475f;
                            ContainerInstance1.X = 13f;
                            ContainerInstance1.Y = 177f;
                            Avatar.Height = 187.2313f;
                            Avatar.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance1");
                            SetProperty("Avatar.SourceFile", "Galvisr.png");
                            Avatar.TextureAddress = Gum.Managers.TextureAddress.EntireTexture;
                            Avatar.TextureHeight = 57;
                            Avatar.TextureHeightScale = 1f;
                            Avatar.TextureLeft = 0;
                            Avatar.TextureTop = 0;
                            Avatar.TextureWidth = 55;
                            Avatar.TextureWidthScale = 1f;
                            Avatar.Width = 201.1433f;
                            Avatar.Wrap = false;
                            Avatar.X = 6f;
                            Avatar.Y = 33f;
                            RectangleInstance1.Height = -17f;
                            RectangleInstance1.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            RectangleInstance1.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance1");
                            RectangleInstance1.Width = -11f;
                            RectangleInstance1.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            RectangleInstance1.X = 8f;
                            RectangleInstance1.Y = 8f;
                            break;
                    }
                }
            }
            #endregion
            #region State Interpolation
            public void InterpolateBetween (VariableState firstState, VariableState secondState, float interpolationValue) 
            {
                #if DEBUG
                if (float.IsNaN(interpolationValue))
                {
                    throw new System.Exception("interpolationValue cannot be NaN");
                }
                #endif
                bool setAvatarHeightFirstValue = false;
                bool setAvatarHeightSecondValue = false;
                float AvatarHeightFirstValue= 0;
                float AvatarHeightSecondValue= 0;
                bool setAvatarTextureHeightFirstValue = false;
                bool setAvatarTextureHeightSecondValue = false;
                int AvatarTextureHeightFirstValue= 0;
                int AvatarTextureHeightSecondValue= 0;
                bool setAvatarTextureHeightScaleFirstValue = false;
                bool setAvatarTextureHeightScaleSecondValue = false;
                float AvatarTextureHeightScaleFirstValue= 0;
                float AvatarTextureHeightScaleSecondValue= 0;
                bool setAvatarTextureLeftFirstValue = false;
                bool setAvatarTextureLeftSecondValue = false;
                int AvatarTextureLeftFirstValue= 0;
                int AvatarTextureLeftSecondValue= 0;
                bool setAvatarTextureTopFirstValue = false;
                bool setAvatarTextureTopSecondValue = false;
                int AvatarTextureTopFirstValue= 0;
                int AvatarTextureTopSecondValue= 0;
                bool setAvatarTextureWidthFirstValue = false;
                bool setAvatarTextureWidthSecondValue = false;
                int AvatarTextureWidthFirstValue= 0;
                int AvatarTextureWidthSecondValue= 0;
                bool setAvatarTextureWidthScaleFirstValue = false;
                bool setAvatarTextureWidthScaleSecondValue = false;
                float AvatarTextureWidthScaleFirstValue= 0;
                float AvatarTextureWidthScaleSecondValue= 0;
                bool setAvatarWidthFirstValue = false;
                bool setAvatarWidthSecondValue = false;
                float AvatarWidthFirstValue= 0;
                float AvatarWidthSecondValue= 0;
                bool setAvatarXFirstValue = false;
                bool setAvatarXSecondValue = false;
                float AvatarXFirstValue= 0;
                float AvatarXSecondValue= 0;
                bool setAvatarYFirstValue = false;
                bool setAvatarYSecondValue = false;
                float AvatarYFirstValue= 0;
                float AvatarYSecondValue= 0;
                bool setContainerInstanceHeightFirstValue = false;
                bool setContainerInstanceHeightSecondValue = false;
                float ContainerInstanceHeightFirstValue= 0;
                float ContainerInstanceHeightSecondValue= 0;
                bool setContainerInstanceWidthFirstValue = false;
                bool setContainerInstanceWidthSecondValue = false;
                float ContainerInstanceWidthFirstValue= 0;
                float ContainerInstanceWidthSecondValue= 0;
                bool setContainerInstanceXFirstValue = false;
                bool setContainerInstanceXSecondValue = false;
                float ContainerInstanceXFirstValue= 0;
                float ContainerInstanceXSecondValue= 0;
                bool setContainerInstanceYFirstValue = false;
                bool setContainerInstanceYSecondValue = false;
                float ContainerInstanceYFirstValue= 0;
                float ContainerInstanceYSecondValue= 0;
                bool setContainerInstance1WidthFirstValue = false;
                bool setContainerInstance1WidthSecondValue = false;
                float ContainerInstance1WidthFirstValue= 0;
                float ContainerInstance1WidthSecondValue= 0;
                bool setContainerInstance1XFirstValue = false;
                bool setContainerInstance1XSecondValue = false;
                float ContainerInstance1XFirstValue= 0;
                float ContainerInstance1XSecondValue= 0;
                bool setContainerInstance1YFirstValue = false;
                bool setContainerInstance1YSecondValue = false;
                float ContainerInstance1YFirstValue= 0;
                float ContainerInstance1YSecondValue= 0;
                bool setGameOverMSGHeightFirstValue = false;
                bool setGameOverMSGHeightSecondValue = false;
                float GameOverMSGHeightFirstValue= 0;
                float GameOverMSGHeightSecondValue= 0;
                bool setGameOverMSGOutlineThicknessFirstValue = false;
                bool setGameOverMSGOutlineThicknessSecondValue = false;
                int GameOverMSGOutlineThicknessFirstValue= 0;
                int GameOverMSGOutlineThicknessSecondValue= 0;
                bool setGameOverMSGWidthFirstValue = false;
                bool setGameOverMSGWidthSecondValue = false;
                float GameOverMSGWidthFirstValue= 0;
                float GameOverMSGWidthSecondValue= 0;
                bool setGameOverMSGXFirstValue = false;
                bool setGameOverMSGXSecondValue = false;
                float GameOverMSGXFirstValue= 0;
                float GameOverMSGXSecondValue= 0;
                bool setGameOverMSGYFirstValue = false;
                bool setGameOverMSGYSecondValue = false;
                float GameOverMSGYFirstValue= 0;
                float GameOverMSGYSecondValue= 0;
                bool setHeightFirstValue = false;
                bool setHeightSecondValue = false;
                float HeightFirstValue= 0;
                float HeightSecondValue= 0;
                bool setPainelHeightFirstValue = false;
                bool setPainelHeightSecondValue = false;
                float PainelHeightFirstValue= 0;
                float PainelHeightSecondValue= 0;
                bool setPainelWidthFirstValue = false;
                bool setPainelWidthSecondValue = false;
                float PainelWidthFirstValue= 0;
                float PainelWidthSecondValue= 0;
                bool setPainelXFirstValue = false;
                bool setPainelXSecondValue = false;
                float PainelXFirstValue= 0;
                float PainelXSecondValue= 0;
                bool setPainelYFirstValue = false;
                bool setPainelYSecondValue = false;
                float PainelYFirstValue= 0;
                float PainelYSecondValue= 0;
                bool setRectangleInstanceBlueFirstValue = false;
                bool setRectangleInstanceBlueSecondValue = false;
                int RectangleInstanceBlueFirstValue= 0;
                int RectangleInstanceBlueSecondValue= 0;
                bool setRectangleInstanceGreenFirstValue = false;
                bool setRectangleInstanceGreenSecondValue = false;
                int RectangleInstanceGreenFirstValue= 0;
                int RectangleInstanceGreenSecondValue= 0;
                bool setRectangleInstanceHeightFirstValue = false;
                bool setRectangleInstanceHeightSecondValue = false;
                float RectangleInstanceHeightFirstValue= 0;
                float RectangleInstanceHeightSecondValue= 0;
                bool setRectangleInstanceRedFirstValue = false;
                bool setRectangleInstanceRedSecondValue = false;
                int RectangleInstanceRedFirstValue= 0;
                int RectangleInstanceRedSecondValue= 0;
                bool setRectangleInstanceWidthFirstValue = false;
                bool setRectangleInstanceWidthSecondValue = false;
                float RectangleInstanceWidthFirstValue= 0;
                float RectangleInstanceWidthSecondValue= 0;
                bool setRectangleInstanceXFirstValue = false;
                bool setRectangleInstanceXSecondValue = false;
                float RectangleInstanceXFirstValue= 0;
                float RectangleInstanceXSecondValue= 0;
                bool setRectangleInstanceYFirstValue = false;
                bool setRectangleInstanceYSecondValue = false;
                float RectangleInstanceYFirstValue= 0;
                float RectangleInstanceYSecondValue= 0;
                bool setRectangleInstance1HeightFirstValue = false;
                bool setRectangleInstance1HeightSecondValue = false;
                float RectangleInstance1HeightFirstValue= 0;
                float RectangleInstance1HeightSecondValue= 0;
                bool setRectangleInstance1WidthFirstValue = false;
                bool setRectangleInstance1WidthSecondValue = false;
                float RectangleInstance1WidthFirstValue= 0;
                float RectangleInstance1WidthSecondValue= 0;
                bool setRectangleInstance1XFirstValue = false;
                bool setRectangleInstance1XSecondValue = false;
                float RectangleInstance1XFirstValue= 0;
                float RectangleInstance1XSecondValue= 0;
                bool setRectangleInstance1YFirstValue = false;
                bool setRectangleInstance1YSecondValue = false;
                float RectangleInstance1YFirstValue= 0;
                float RectangleInstance1YSecondValue= 0;
                bool setTextInstanceBlueFirstValue = false;
                bool setTextInstanceBlueSecondValue = false;
                int TextInstanceBlueFirstValue= 0;
                int TextInstanceBlueSecondValue= 0;
                bool setTextInstanceFontSizeFirstValue = false;
                bool setTextInstanceFontSizeSecondValue = false;
                int TextInstanceFontSizeFirstValue= 0;
                int TextInstanceFontSizeSecondValue= 0;
                bool setTextInstanceGreenFirstValue = false;
                bool setTextInstanceGreenSecondValue = false;
                int TextInstanceGreenFirstValue= 0;
                int TextInstanceGreenSecondValue= 0;
                bool setTextInstanceHeightFirstValue = false;
                bool setTextInstanceHeightSecondValue = false;
                float TextInstanceHeightFirstValue= 0;
                float TextInstanceHeightSecondValue= 0;
                bool setTextInstanceOutlineThicknessFirstValue = false;
                bool setTextInstanceOutlineThicknessSecondValue = false;
                int TextInstanceOutlineThicknessFirstValue= 0;
                int TextInstanceOutlineThicknessSecondValue= 0;
                bool setTextInstanceRedFirstValue = false;
                bool setTextInstanceRedSecondValue = false;
                int TextInstanceRedFirstValue= 0;
                int TextInstanceRedSecondValue= 0;
                bool setTextInstanceWidthFirstValue = false;
                bool setTextInstanceWidthSecondValue = false;
                float TextInstanceWidthFirstValue= 0;
                float TextInstanceWidthSecondValue= 0;
                bool setTextInstanceXFirstValue = false;
                bool setTextInstanceXSecondValue = false;
                float TextInstanceXFirstValue= 0;
                float TextInstanceXSecondValue= 0;
                bool setTextInstanceYFirstValue = false;
                bool setTextInstanceYSecondValue = false;
                float TextInstanceYFirstValue= 0;
                float TextInstanceYSecondValue= 0;
                bool setTextNotaFontSizeFirstValue = false;
                bool setTextNotaFontSizeSecondValue = false;
                int TextNotaFontSizeFirstValue= 0;
                int TextNotaFontSizeSecondValue= 0;
                bool setTextNotaHeightFirstValue = false;
                bool setTextNotaHeightSecondValue = false;
                float TextNotaHeightFirstValue= 0;
                float TextNotaHeightSecondValue= 0;
                bool setTextNotaOutlineThicknessFirstValue = false;
                bool setTextNotaOutlineThicknessSecondValue = false;
                int TextNotaOutlineThicknessFirstValue= 0;
                int TextNotaOutlineThicknessSecondValue= 0;
                bool setTextNotaWidthFirstValue = false;
                bool setTextNotaWidthSecondValue = false;
                float TextNotaWidthFirstValue= 0;
                float TextNotaWidthSecondValue= 0;
                bool setTextNotaXFirstValue = false;
                bool setTextNotaXSecondValue = false;
                float TextNotaXFirstValue= 0;
                float TextNotaXSecondValue= 0;
                bool setTextNotaYFirstValue = false;
                bool setTextNotaYSecondValue = false;
                float TextNotaYFirstValue= 0;
                float TextNotaYSecondValue= 0;
                bool setTextResultadoFontSizeFirstValue = false;
                bool setTextResultadoFontSizeSecondValue = false;
                int TextResultadoFontSizeFirstValue= 0;
                int TextResultadoFontSizeSecondValue= 0;
                bool setTextResultadoHeightFirstValue = false;
                bool setTextResultadoHeightSecondValue = false;
                float TextResultadoHeightFirstValue= 0;
                float TextResultadoHeightSecondValue= 0;
                bool setTextResultadoOutlineThicknessFirstValue = false;
                bool setTextResultadoOutlineThicknessSecondValue = false;
                int TextResultadoOutlineThicknessFirstValue= 0;
                int TextResultadoOutlineThicknessSecondValue= 0;
                bool setTextResultadoWidthFirstValue = false;
                bool setTextResultadoWidthSecondValue = false;
                float TextResultadoWidthFirstValue= 0;
                float TextResultadoWidthSecondValue= 0;
                bool setTextResultadoXFirstValue = false;
                bool setTextResultadoXSecondValue = false;
                float TextResultadoXFirstValue= 0;
                float TextResultadoXSecondValue= 0;
                bool setTextResultadoYFirstValue = false;
                bool setTextResultadoYSecondValue = false;
                float TextResultadoYFirstValue= 0;
                float TextResultadoYSecondValue= 0;
                bool setWidthFirstValue = false;
                bool setWidthSecondValue = false;
                float WidthFirstValue= 0;
                float WidthSecondValue= 0;
                switch(firstState)
                {
                    case  VariableState.Default:
                        setAvatarHeightFirstValue = true;
                        AvatarHeightFirstValue = 187.2313f;
                        if (interpolationValue < 1)
                        {
                            this.Avatar.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance1");
                        }
                        if (interpolationValue < 1)
                        {
                            SetProperty("Avatar.SourceFile", "Galvisr.png");
                        }
                        if (interpolationValue < 1)
                        {
                            this.Avatar.TextureAddress = Gum.Managers.TextureAddress.EntireTexture;
                        }
                        setAvatarTextureHeightFirstValue = true;
                        AvatarTextureHeightFirstValue = 57;
                        setAvatarTextureHeightScaleFirstValue = true;
                        AvatarTextureHeightScaleFirstValue = 1f;
                        setAvatarTextureLeftFirstValue = true;
                        AvatarTextureLeftFirstValue = 0;
                        setAvatarTextureTopFirstValue = true;
                        AvatarTextureTopFirstValue = 0;
                        setAvatarTextureWidthFirstValue = true;
                        AvatarTextureWidthFirstValue = 55;
                        setAvatarTextureWidthScaleFirstValue = true;
                        AvatarTextureWidthScaleFirstValue = 1f;
                        setAvatarWidthFirstValue = true;
                        AvatarWidthFirstValue = 201.1433f;
                        if (interpolationValue < 1)
                        {
                            this.Avatar.Wrap = false;
                        }
                        setAvatarXFirstValue = true;
                        AvatarXFirstValue = 6f;
                        setAvatarYFirstValue = true;
                        AvatarYFirstValue = 33f;
                        setContainerInstanceHeightFirstValue = true;
                        ContainerInstanceHeightFirstValue = 156f;
                        setContainerInstanceWidthFirstValue = true;
                        ContainerInstanceWidthFirstValue = 490f;
                        setContainerInstanceXFirstValue = true;
                        ContainerInstanceXFirstValue = 7f;
                        setContainerInstanceYFirstValue = true;
                        ContainerInstanceYFirstValue = 11f;
                        setContainerInstance1WidthFirstValue = true;
                        ContainerInstance1WidthFirstValue = 475f;
                        setContainerInstance1XFirstValue = true;
                        ContainerInstance1XFirstValue = 13f;
                        setContainerInstance1YFirstValue = true;
                        ContainerInstance1YFirstValue = 177f;
                        setGameOverMSGHeightFirstValue = true;
                        GameOverMSGHeightFirstValue = 121f;
                        if (interpolationValue < 1)
                        {
                            this.GameOverMSG.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Left;
                        }
                        setGameOverMSGOutlineThicknessFirstValue = true;
                        GameOverMSGOutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.GameOverMSG.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "Avatar");
                        }
                        if (interpolationValue < 1)
                        {
                            this.GameOverMSG.Text = "asdfasdfasdfasdfasdfasdfa";
                        }
                        if (interpolationValue < 1)
                        {
                            this.GameOverMSG.VerticalAlignment = RenderingLibrary.Graphics.VerticalAlignment.Center;
                        }
                        setGameOverMSGWidthFirstValue = true;
                        GameOverMSGWidthFirstValue = 354f;
                        setGameOverMSGXFirstValue = true;
                        GameOverMSGXFirstValue = 109f;
                        setGameOverMSGYFirstValue = true;
                        GameOverMSGYFirstValue = -20f;
                        setHeightFirstValue = true;
                        HeightFirstValue = 348f;
                        setPainelHeightFirstValue = true;
                        PainelHeightFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.Painel.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue < 1)
                        {
                            SetProperty("Painel.SourceFile", "panelInset_brown.png");
                        }
                        setPainelWidthFirstValue = true;
                        PainelWidthFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.Painel.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setPainelXFirstValue = true;
                        PainelXFirstValue = -2f;
                        setPainelYFirstValue = true;
                        PainelYFirstValue = -1f;
                        setRectangleInstanceBlueFirstValue = true;
                        RectangleInstanceBlueFirstValue = 0;
                        setRectangleInstanceGreenFirstValue = true;
                        RectangleInstanceGreenFirstValue = 0;
                        setRectangleInstanceHeightFirstValue = true;
                        RectangleInstanceHeightFirstValue = -4f;
                        if (interpolationValue < 1)
                        {
                            this.RectangleInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue < 1)
                        {
                            this.RectangleInstance.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance1");
                        }
                        setRectangleInstanceRedFirstValue = true;
                        RectangleInstanceRedFirstValue = 0;
                        setRectangleInstanceWidthFirstValue = true;
                        RectangleInstanceWidthFirstValue = 3f;
                        if (interpolationValue < 1)
                        {
                            this.RectangleInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setRectangleInstanceXFirstValue = true;
                        RectangleInstanceXFirstValue = 0f;
                        setRectangleInstanceYFirstValue = true;
                        RectangleInstanceYFirstValue = 1f;
                        setRectangleInstance1HeightFirstValue = true;
                        RectangleInstance1HeightFirstValue = -17f;
                        if (interpolationValue < 1)
                        {
                            this.RectangleInstance1.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue < 1)
                        {
                            this.RectangleInstance1.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance1");
                        }
                        setRectangleInstance1WidthFirstValue = true;
                        RectangleInstance1WidthFirstValue = -11f;
                        if (interpolationValue < 1)
                        {
                            this.RectangleInstance1.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setRectangleInstance1XFirstValue = true;
                        RectangleInstance1XFirstValue = 8f;
                        setRectangleInstance1YFirstValue = true;
                        RectangleInstance1YFirstValue = 8f;
                        setTextInstanceBlueFirstValue = true;
                        TextInstanceBlueFirstValue = 0;
                        if (interpolationValue < 1)
                        {
                            this.TextInstance.Font = "Arial";
                        }
                        setTextInstanceFontSizeFirstValue = true;
                        TextInstanceFontSizeFirstValue = 35;
                        setTextInstanceGreenFirstValue = true;
                        TextInstanceGreenFirstValue = 0;
                        setTextInstanceHeightFirstValue = true;
                        TextInstanceHeightFirstValue = 41f;
                        setTextInstanceOutlineThicknessFirstValue = true;
                        TextInstanceOutlineThicknessFirstValue = 0;
                        if (interpolationValue < 1)
                        {
                            this.TextInstance.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        setTextInstanceRedFirstValue = true;
                        TextInstanceRedFirstValue = 0;
                        if (interpolationValue < 1)
                        {
                            this.TextInstance.Text = "Nota:";
                        }
                        setTextInstanceWidthFirstValue = true;
                        TextInstanceWidthFirstValue = 128f;
                        setTextInstanceXFirstValue = true;
                        TextInstanceXFirstValue = 100f;
                        setTextInstanceYFirstValue = true;
                        TextInstanceYFirstValue = 2f;
                        if (interpolationValue < 1)
                        {
                            this.TextNota.Font = "Impact";
                        }
                        setTextNotaFontSizeFirstValue = true;
                        TextNotaFontSizeFirstValue = 55;
                        setTextNotaHeightFirstValue = true;
                        TextNotaHeightFirstValue = 155f;
                        if (interpolationValue < 1)
                        {
                            this.TextNota.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        setTextNotaOutlineThicknessFirstValue = true;
                        TextNotaOutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.TextNota.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        if (interpolationValue < 1)
                        {
                            this.TextNota.Text = "50";
                        }
                        if (interpolationValue < 1)
                        {
                            this.TextNota.VerticalAlignment = RenderingLibrary.Graphics.VerticalAlignment.Center;
                        }
                        setTextNotaWidthFirstValue = true;
                        TextNotaWidthFirstValue = -2f;
                        if (interpolationValue < 1)
                        {
                            this.TextNota.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setTextNotaXFirstValue = true;
                        TextNotaXFirstValue = 1f;
                        if (interpolationValue < 1)
                        {
                            this.TextNota.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        if (interpolationValue < 1)
                        {
                            this.TextNota.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        setTextNotaYFirstValue = true;
                        TextNotaYFirstValue = -3f;
                        if (interpolationValue < 1)
                        {
                            this.TextNota.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Center;
                        }
                        if (interpolationValue < 1)
                        {
                            this.TextNota.YUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        if (interpolationValue < 1)
                        {
                            this.TextResultado.Font = "Impact";
                        }
                        setTextResultadoFontSizeFirstValue = true;
                        TextResultadoFontSizeFirstValue = 25;
                        setTextResultadoHeightFirstValue = true;
                        TextResultadoHeightFirstValue = 35f;
                        if (interpolationValue < 1)
                        {
                            this.TextResultado.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        setTextResultadoOutlineThicknessFirstValue = true;
                        TextResultadoOutlineThicknessFirstValue = 2;
                        if (interpolationValue < 1)
                        {
                            this.TextResultado.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        if (interpolationValue < 1)
                        {
                            this.TextResultado.Text = "RESULTADO";
                        }
                        if (interpolationValue < 1)
                        {
                            this.TextResultado.VerticalAlignment = RenderingLibrary.Graphics.VerticalAlignment.Center;
                        }
                        setTextResultadoWidthFirstValue = true;
                        TextResultadoWidthFirstValue = 157f;
                        setTextResultadoXFirstValue = true;
                        TextResultadoXFirstValue = 192f;
                        setTextResultadoYFirstValue = true;
                        TextResultadoYFirstValue = 132f;
                        setWidthFirstValue = true;
                        WidthFirstValue = 520f;
                        break;
                }
                switch(secondState)
                {
                    case  VariableState.Default:
                        setAvatarHeightSecondValue = true;
                        AvatarHeightSecondValue = 187.2313f;
                        if (interpolationValue >= 1)
                        {
                            this.Avatar.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance1");
                        }
                        if (interpolationValue >= 1)
                        {
                            SetProperty("Avatar.SourceFile", "Galvisr.png");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.Avatar.TextureAddress = Gum.Managers.TextureAddress.EntireTexture;
                        }
                        setAvatarTextureHeightSecondValue = true;
                        AvatarTextureHeightSecondValue = 57;
                        setAvatarTextureHeightScaleSecondValue = true;
                        AvatarTextureHeightScaleSecondValue = 1f;
                        setAvatarTextureLeftSecondValue = true;
                        AvatarTextureLeftSecondValue = 0;
                        setAvatarTextureTopSecondValue = true;
                        AvatarTextureTopSecondValue = 0;
                        setAvatarTextureWidthSecondValue = true;
                        AvatarTextureWidthSecondValue = 55;
                        setAvatarTextureWidthScaleSecondValue = true;
                        AvatarTextureWidthScaleSecondValue = 1f;
                        setAvatarWidthSecondValue = true;
                        AvatarWidthSecondValue = 201.1433f;
                        if (interpolationValue >= 1)
                        {
                            this.Avatar.Wrap = false;
                        }
                        setAvatarXSecondValue = true;
                        AvatarXSecondValue = 6f;
                        setAvatarYSecondValue = true;
                        AvatarYSecondValue = 33f;
                        setContainerInstanceHeightSecondValue = true;
                        ContainerInstanceHeightSecondValue = 156f;
                        setContainerInstanceWidthSecondValue = true;
                        ContainerInstanceWidthSecondValue = 490f;
                        setContainerInstanceXSecondValue = true;
                        ContainerInstanceXSecondValue = 7f;
                        setContainerInstanceYSecondValue = true;
                        ContainerInstanceYSecondValue = 11f;
                        setContainerInstance1WidthSecondValue = true;
                        ContainerInstance1WidthSecondValue = 475f;
                        setContainerInstance1XSecondValue = true;
                        ContainerInstance1XSecondValue = 13f;
                        setContainerInstance1YSecondValue = true;
                        ContainerInstance1YSecondValue = 177f;
                        setGameOverMSGHeightSecondValue = true;
                        GameOverMSGHeightSecondValue = 121f;
                        if (interpolationValue >= 1)
                        {
                            this.GameOverMSG.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Left;
                        }
                        setGameOverMSGOutlineThicknessSecondValue = true;
                        GameOverMSGOutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.GameOverMSG.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "Avatar");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.GameOverMSG.Text = "asdfasdfasdfasdfasdfasdfa";
                        }
                        if (interpolationValue >= 1)
                        {
                            this.GameOverMSG.VerticalAlignment = RenderingLibrary.Graphics.VerticalAlignment.Center;
                        }
                        setGameOverMSGWidthSecondValue = true;
                        GameOverMSGWidthSecondValue = 354f;
                        setGameOverMSGXSecondValue = true;
                        GameOverMSGXSecondValue = 109f;
                        setGameOverMSGYSecondValue = true;
                        GameOverMSGYSecondValue = -20f;
                        setHeightSecondValue = true;
                        HeightSecondValue = 348f;
                        setPainelHeightSecondValue = true;
                        PainelHeightSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.Painel.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue >= 1)
                        {
                            SetProperty("Painel.SourceFile", "panelInset_brown.png");
                        }
                        setPainelWidthSecondValue = true;
                        PainelWidthSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.Painel.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setPainelXSecondValue = true;
                        PainelXSecondValue = -2f;
                        setPainelYSecondValue = true;
                        PainelYSecondValue = -1f;
                        setRectangleInstanceBlueSecondValue = true;
                        RectangleInstanceBlueSecondValue = 0;
                        setRectangleInstanceGreenSecondValue = true;
                        RectangleInstanceGreenSecondValue = 0;
                        setRectangleInstanceHeightSecondValue = true;
                        RectangleInstanceHeightSecondValue = -4f;
                        if (interpolationValue >= 1)
                        {
                            this.RectangleInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.RectangleInstance.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance1");
                        }
                        setRectangleInstanceRedSecondValue = true;
                        RectangleInstanceRedSecondValue = 0;
                        setRectangleInstanceWidthSecondValue = true;
                        RectangleInstanceWidthSecondValue = 3f;
                        if (interpolationValue >= 1)
                        {
                            this.RectangleInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setRectangleInstanceXSecondValue = true;
                        RectangleInstanceXSecondValue = 0f;
                        setRectangleInstanceYSecondValue = true;
                        RectangleInstanceYSecondValue = 1f;
                        setRectangleInstance1HeightSecondValue = true;
                        RectangleInstance1HeightSecondValue = -17f;
                        if (interpolationValue >= 1)
                        {
                            this.RectangleInstance1.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.RectangleInstance1.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance1");
                        }
                        setRectangleInstance1WidthSecondValue = true;
                        RectangleInstance1WidthSecondValue = -11f;
                        if (interpolationValue >= 1)
                        {
                            this.RectangleInstance1.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setRectangleInstance1XSecondValue = true;
                        RectangleInstance1XSecondValue = 8f;
                        setRectangleInstance1YSecondValue = true;
                        RectangleInstance1YSecondValue = 8f;
                        setTextInstanceBlueSecondValue = true;
                        TextInstanceBlueSecondValue = 0;
                        if (interpolationValue >= 1)
                        {
                            this.TextInstance.Font = "Arial";
                        }
                        setTextInstanceFontSizeSecondValue = true;
                        TextInstanceFontSizeSecondValue = 35;
                        setTextInstanceGreenSecondValue = true;
                        TextInstanceGreenSecondValue = 0;
                        setTextInstanceHeightSecondValue = true;
                        TextInstanceHeightSecondValue = 41f;
                        setTextInstanceOutlineThicknessSecondValue = true;
                        TextInstanceOutlineThicknessSecondValue = 0;
                        if (interpolationValue >= 1)
                        {
                            this.TextInstance.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        setTextInstanceRedSecondValue = true;
                        TextInstanceRedSecondValue = 0;
                        if (interpolationValue >= 1)
                        {
                            this.TextInstance.Text = "Nota:";
                        }
                        setTextInstanceWidthSecondValue = true;
                        TextInstanceWidthSecondValue = 128f;
                        setTextInstanceXSecondValue = true;
                        TextInstanceXSecondValue = 100f;
                        setTextInstanceYSecondValue = true;
                        TextInstanceYSecondValue = 2f;
                        if (interpolationValue >= 1)
                        {
                            this.TextNota.Font = "Impact";
                        }
                        setTextNotaFontSizeSecondValue = true;
                        TextNotaFontSizeSecondValue = 55;
                        setTextNotaHeightSecondValue = true;
                        TextNotaHeightSecondValue = 155f;
                        if (interpolationValue >= 1)
                        {
                            this.TextNota.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        setTextNotaOutlineThicknessSecondValue = true;
                        TextNotaOutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.TextNota.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.TextNota.Text = "50";
                        }
                        if (interpolationValue >= 1)
                        {
                            this.TextNota.VerticalAlignment = RenderingLibrary.Graphics.VerticalAlignment.Center;
                        }
                        setTextNotaWidthSecondValue = true;
                        TextNotaWidthSecondValue = -2f;
                        if (interpolationValue >= 1)
                        {
                            this.TextNota.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setTextNotaXSecondValue = true;
                        TextNotaXSecondValue = 1f;
                        if (interpolationValue >= 1)
                        {
                            this.TextNota.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.TextNota.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        setTextNotaYSecondValue = true;
                        TextNotaYSecondValue = -3f;
                        if (interpolationValue >= 1)
                        {
                            this.TextNota.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Center;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.TextNota.YUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.TextResultado.Font = "Impact";
                        }
                        setTextResultadoFontSizeSecondValue = true;
                        TextResultadoFontSizeSecondValue = 25;
                        setTextResultadoHeightSecondValue = true;
                        TextResultadoHeightSecondValue = 35f;
                        if (interpolationValue >= 1)
                        {
                            this.TextResultado.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        setTextResultadoOutlineThicknessSecondValue = true;
                        TextResultadoOutlineThicknessSecondValue = 2;
                        if (interpolationValue >= 1)
                        {
                            this.TextResultado.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.TextResultado.Text = "RESULTADO";
                        }
                        if (interpolationValue >= 1)
                        {
                            this.TextResultado.VerticalAlignment = RenderingLibrary.Graphics.VerticalAlignment.Center;
                        }
                        setTextResultadoWidthSecondValue = true;
                        TextResultadoWidthSecondValue = 157f;
                        setTextResultadoXSecondValue = true;
                        TextResultadoXSecondValue = 192f;
                        setTextResultadoYSecondValue = true;
                        TextResultadoYSecondValue = 132f;
                        setWidthSecondValue = true;
                        WidthSecondValue = 520f;
                        break;
                }
                if (setAvatarHeightFirstValue && setAvatarHeightSecondValue)
                {
                    Avatar.Height = AvatarHeightFirstValue * (1 - interpolationValue) + AvatarHeightSecondValue * interpolationValue;
                }
                if (setAvatarTextureHeightFirstValue && setAvatarTextureHeightSecondValue)
                {
                    Avatar.TextureHeight = FlatRedBall.Math.MathFunctions.RoundToInt(AvatarTextureHeightFirstValue* (1 - interpolationValue) + AvatarTextureHeightSecondValue * interpolationValue);
                }
                if (setAvatarTextureHeightScaleFirstValue && setAvatarTextureHeightScaleSecondValue)
                {
                    Avatar.TextureHeightScale = AvatarTextureHeightScaleFirstValue * (1 - interpolationValue) + AvatarTextureHeightScaleSecondValue * interpolationValue;
                }
                if (setAvatarTextureLeftFirstValue && setAvatarTextureLeftSecondValue)
                {
                    Avatar.TextureLeft = FlatRedBall.Math.MathFunctions.RoundToInt(AvatarTextureLeftFirstValue* (1 - interpolationValue) + AvatarTextureLeftSecondValue * interpolationValue);
                }
                if (setAvatarTextureTopFirstValue && setAvatarTextureTopSecondValue)
                {
                    Avatar.TextureTop = FlatRedBall.Math.MathFunctions.RoundToInt(AvatarTextureTopFirstValue* (1 - interpolationValue) + AvatarTextureTopSecondValue * interpolationValue);
                }
                if (setAvatarTextureWidthFirstValue && setAvatarTextureWidthSecondValue)
                {
                    Avatar.TextureWidth = FlatRedBall.Math.MathFunctions.RoundToInt(AvatarTextureWidthFirstValue* (1 - interpolationValue) + AvatarTextureWidthSecondValue * interpolationValue);
                }
                if (setAvatarTextureWidthScaleFirstValue && setAvatarTextureWidthScaleSecondValue)
                {
                    Avatar.TextureWidthScale = AvatarTextureWidthScaleFirstValue * (1 - interpolationValue) + AvatarTextureWidthScaleSecondValue * interpolationValue;
                }
                if (setAvatarWidthFirstValue && setAvatarWidthSecondValue)
                {
                    Avatar.Width = AvatarWidthFirstValue * (1 - interpolationValue) + AvatarWidthSecondValue * interpolationValue;
                }
                if (setAvatarXFirstValue && setAvatarXSecondValue)
                {
                    Avatar.X = AvatarXFirstValue * (1 - interpolationValue) + AvatarXSecondValue * interpolationValue;
                }
                if (setAvatarYFirstValue && setAvatarYSecondValue)
                {
                    Avatar.Y = AvatarYFirstValue * (1 - interpolationValue) + AvatarYSecondValue * interpolationValue;
                }
                if (setContainerInstanceHeightFirstValue && setContainerInstanceHeightSecondValue)
                {
                    ContainerInstance.Height = ContainerInstanceHeightFirstValue * (1 - interpolationValue) + ContainerInstanceHeightSecondValue * interpolationValue;
                }
                if (setContainerInstanceWidthFirstValue && setContainerInstanceWidthSecondValue)
                {
                    ContainerInstance.Width = ContainerInstanceWidthFirstValue * (1 - interpolationValue) + ContainerInstanceWidthSecondValue * interpolationValue;
                }
                if (setContainerInstanceXFirstValue && setContainerInstanceXSecondValue)
                {
                    ContainerInstance.X = ContainerInstanceXFirstValue * (1 - interpolationValue) + ContainerInstanceXSecondValue * interpolationValue;
                }
                if (setContainerInstanceYFirstValue && setContainerInstanceYSecondValue)
                {
                    ContainerInstance.Y = ContainerInstanceYFirstValue * (1 - interpolationValue) + ContainerInstanceYSecondValue * interpolationValue;
                }
                if (setContainerInstance1WidthFirstValue && setContainerInstance1WidthSecondValue)
                {
                    ContainerInstance1.Width = ContainerInstance1WidthFirstValue * (1 - interpolationValue) + ContainerInstance1WidthSecondValue * interpolationValue;
                }
                if (setContainerInstance1XFirstValue && setContainerInstance1XSecondValue)
                {
                    ContainerInstance1.X = ContainerInstance1XFirstValue * (1 - interpolationValue) + ContainerInstance1XSecondValue * interpolationValue;
                }
                if (setContainerInstance1YFirstValue && setContainerInstance1YSecondValue)
                {
                    ContainerInstance1.Y = ContainerInstance1YFirstValue * (1 - interpolationValue) + ContainerInstance1YSecondValue * interpolationValue;
                }
                if (setGameOverMSGHeightFirstValue && setGameOverMSGHeightSecondValue)
                {
                    GameOverMSG.Height = GameOverMSGHeightFirstValue * (1 - interpolationValue) + GameOverMSGHeightSecondValue * interpolationValue;
                }
                if (setGameOverMSGOutlineThicknessFirstValue && setGameOverMSGOutlineThicknessSecondValue)
                {
                    GameOverMSG.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(GameOverMSGOutlineThicknessFirstValue* (1 - interpolationValue) + GameOverMSGOutlineThicknessSecondValue * interpolationValue);
                }
                if (setGameOverMSGWidthFirstValue && setGameOverMSGWidthSecondValue)
                {
                    GameOverMSG.Width = GameOverMSGWidthFirstValue * (1 - interpolationValue) + GameOverMSGWidthSecondValue * interpolationValue;
                }
                if (setGameOverMSGXFirstValue && setGameOverMSGXSecondValue)
                {
                    GameOverMSG.X = GameOverMSGXFirstValue * (1 - interpolationValue) + GameOverMSGXSecondValue * interpolationValue;
                }
                if (setGameOverMSGYFirstValue && setGameOverMSGYSecondValue)
                {
                    GameOverMSG.Y = GameOverMSGYFirstValue * (1 - interpolationValue) + GameOverMSGYSecondValue * interpolationValue;
                }
                if (setHeightFirstValue && setHeightSecondValue)
                {
                    Height = HeightFirstValue * (1 - interpolationValue) + HeightSecondValue * interpolationValue;
                }
                if (setPainelHeightFirstValue && setPainelHeightSecondValue)
                {
                    Painel.Height = PainelHeightFirstValue * (1 - interpolationValue) + PainelHeightSecondValue * interpolationValue;
                }
                if (setPainelWidthFirstValue && setPainelWidthSecondValue)
                {
                    Painel.Width = PainelWidthFirstValue * (1 - interpolationValue) + PainelWidthSecondValue * interpolationValue;
                }
                if (setPainelXFirstValue && setPainelXSecondValue)
                {
                    Painel.X = PainelXFirstValue * (1 - interpolationValue) + PainelXSecondValue * interpolationValue;
                }
                if (setPainelYFirstValue && setPainelYSecondValue)
                {
                    Painel.Y = PainelYFirstValue * (1 - interpolationValue) + PainelYSecondValue * interpolationValue;
                }
                if (setRectangleInstanceBlueFirstValue && setRectangleInstanceBlueSecondValue)
                {
                    RectangleInstance.Blue = FlatRedBall.Math.MathFunctions.RoundToInt(RectangleInstanceBlueFirstValue* (1 - interpolationValue) + RectangleInstanceBlueSecondValue * interpolationValue);
                }
                if (setRectangleInstanceGreenFirstValue && setRectangleInstanceGreenSecondValue)
                {
                    RectangleInstance.Green = FlatRedBall.Math.MathFunctions.RoundToInt(RectangleInstanceGreenFirstValue* (1 - interpolationValue) + RectangleInstanceGreenSecondValue * interpolationValue);
                }
                if (setRectangleInstanceHeightFirstValue && setRectangleInstanceHeightSecondValue)
                {
                    RectangleInstance.Height = RectangleInstanceHeightFirstValue * (1 - interpolationValue) + RectangleInstanceHeightSecondValue * interpolationValue;
                }
                if (setRectangleInstanceRedFirstValue && setRectangleInstanceRedSecondValue)
                {
                    RectangleInstance.Red = FlatRedBall.Math.MathFunctions.RoundToInt(RectangleInstanceRedFirstValue* (1 - interpolationValue) + RectangleInstanceRedSecondValue * interpolationValue);
                }
                if (setRectangleInstanceWidthFirstValue && setRectangleInstanceWidthSecondValue)
                {
                    RectangleInstance.Width = RectangleInstanceWidthFirstValue * (1 - interpolationValue) + RectangleInstanceWidthSecondValue * interpolationValue;
                }
                if (setRectangleInstanceXFirstValue && setRectangleInstanceXSecondValue)
                {
                    RectangleInstance.X = RectangleInstanceXFirstValue * (1 - interpolationValue) + RectangleInstanceXSecondValue * interpolationValue;
                }
                if (setRectangleInstanceYFirstValue && setRectangleInstanceYSecondValue)
                {
                    RectangleInstance.Y = RectangleInstanceYFirstValue * (1 - interpolationValue) + RectangleInstanceYSecondValue * interpolationValue;
                }
                if (setRectangleInstance1HeightFirstValue && setRectangleInstance1HeightSecondValue)
                {
                    RectangleInstance1.Height = RectangleInstance1HeightFirstValue * (1 - interpolationValue) + RectangleInstance1HeightSecondValue * interpolationValue;
                }
                if (setRectangleInstance1WidthFirstValue && setRectangleInstance1WidthSecondValue)
                {
                    RectangleInstance1.Width = RectangleInstance1WidthFirstValue * (1 - interpolationValue) + RectangleInstance1WidthSecondValue * interpolationValue;
                }
                if (setRectangleInstance1XFirstValue && setRectangleInstance1XSecondValue)
                {
                    RectangleInstance1.X = RectangleInstance1XFirstValue * (1 - interpolationValue) + RectangleInstance1XSecondValue * interpolationValue;
                }
                if (setRectangleInstance1YFirstValue && setRectangleInstance1YSecondValue)
                {
                    RectangleInstance1.Y = RectangleInstance1YFirstValue * (1 - interpolationValue) + RectangleInstance1YSecondValue * interpolationValue;
                }
                if (setTextInstanceBlueFirstValue && setTextInstanceBlueSecondValue)
                {
                    TextInstance.Blue = FlatRedBall.Math.MathFunctions.RoundToInt(TextInstanceBlueFirstValue* (1 - interpolationValue) + TextInstanceBlueSecondValue * interpolationValue);
                }
                if (setTextInstanceFontSizeFirstValue && setTextInstanceFontSizeSecondValue)
                {
                    TextInstance.FontSize = FlatRedBall.Math.MathFunctions.RoundToInt(TextInstanceFontSizeFirstValue* (1 - interpolationValue) + TextInstanceFontSizeSecondValue * interpolationValue);
                }
                if (setTextInstanceGreenFirstValue && setTextInstanceGreenSecondValue)
                {
                    TextInstance.Green = FlatRedBall.Math.MathFunctions.RoundToInt(TextInstanceGreenFirstValue* (1 - interpolationValue) + TextInstanceGreenSecondValue * interpolationValue);
                }
                if (setTextInstanceHeightFirstValue && setTextInstanceHeightSecondValue)
                {
                    TextInstance.Height = TextInstanceHeightFirstValue * (1 - interpolationValue) + TextInstanceHeightSecondValue * interpolationValue;
                }
                if (setTextInstanceOutlineThicknessFirstValue && setTextInstanceOutlineThicknessSecondValue)
                {
                    TextInstance.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(TextInstanceOutlineThicknessFirstValue* (1 - interpolationValue) + TextInstanceOutlineThicknessSecondValue * interpolationValue);
                }
                if (setTextInstanceRedFirstValue && setTextInstanceRedSecondValue)
                {
                    TextInstance.Red = FlatRedBall.Math.MathFunctions.RoundToInt(TextInstanceRedFirstValue* (1 - interpolationValue) + TextInstanceRedSecondValue * interpolationValue);
                }
                if (setTextInstanceWidthFirstValue && setTextInstanceWidthSecondValue)
                {
                    TextInstance.Width = TextInstanceWidthFirstValue * (1 - interpolationValue) + TextInstanceWidthSecondValue * interpolationValue;
                }
                if (setTextInstanceXFirstValue && setTextInstanceXSecondValue)
                {
                    TextInstance.X = TextInstanceXFirstValue * (1 - interpolationValue) + TextInstanceXSecondValue * interpolationValue;
                }
                if (setTextInstanceYFirstValue && setTextInstanceYSecondValue)
                {
                    TextInstance.Y = TextInstanceYFirstValue * (1 - interpolationValue) + TextInstanceYSecondValue * interpolationValue;
                }
                if (setTextNotaFontSizeFirstValue && setTextNotaFontSizeSecondValue)
                {
                    TextNota.FontSize = FlatRedBall.Math.MathFunctions.RoundToInt(TextNotaFontSizeFirstValue* (1 - interpolationValue) + TextNotaFontSizeSecondValue * interpolationValue);
                }
                if (setTextNotaHeightFirstValue && setTextNotaHeightSecondValue)
                {
                    TextNota.Height = TextNotaHeightFirstValue * (1 - interpolationValue) + TextNotaHeightSecondValue * interpolationValue;
                }
                if (setTextNotaOutlineThicknessFirstValue && setTextNotaOutlineThicknessSecondValue)
                {
                    TextNota.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(TextNotaOutlineThicknessFirstValue* (1 - interpolationValue) + TextNotaOutlineThicknessSecondValue * interpolationValue);
                }
                if (setTextNotaWidthFirstValue && setTextNotaWidthSecondValue)
                {
                    TextNota.Width = TextNotaWidthFirstValue * (1 - interpolationValue) + TextNotaWidthSecondValue * interpolationValue;
                }
                if (setTextNotaXFirstValue && setTextNotaXSecondValue)
                {
                    TextNota.X = TextNotaXFirstValue * (1 - interpolationValue) + TextNotaXSecondValue * interpolationValue;
                }
                if (setTextNotaYFirstValue && setTextNotaYSecondValue)
                {
                    TextNota.Y = TextNotaYFirstValue * (1 - interpolationValue) + TextNotaYSecondValue * interpolationValue;
                }
                if (setTextResultadoFontSizeFirstValue && setTextResultadoFontSizeSecondValue)
                {
                    TextResultado.FontSize = FlatRedBall.Math.MathFunctions.RoundToInt(TextResultadoFontSizeFirstValue* (1 - interpolationValue) + TextResultadoFontSizeSecondValue * interpolationValue);
                }
                if (setTextResultadoHeightFirstValue && setTextResultadoHeightSecondValue)
                {
                    TextResultado.Height = TextResultadoHeightFirstValue * (1 - interpolationValue) + TextResultadoHeightSecondValue * interpolationValue;
                }
                if (setTextResultadoOutlineThicknessFirstValue && setTextResultadoOutlineThicknessSecondValue)
                {
                    TextResultado.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(TextResultadoOutlineThicknessFirstValue* (1 - interpolationValue) + TextResultadoOutlineThicknessSecondValue * interpolationValue);
                }
                if (setTextResultadoWidthFirstValue && setTextResultadoWidthSecondValue)
                {
                    TextResultado.Width = TextResultadoWidthFirstValue * (1 - interpolationValue) + TextResultadoWidthSecondValue * interpolationValue;
                }
                if (setTextResultadoXFirstValue && setTextResultadoXSecondValue)
                {
                    TextResultado.X = TextResultadoXFirstValue * (1 - interpolationValue) + TextResultadoXSecondValue * interpolationValue;
                }
                if (setTextResultadoYFirstValue && setTextResultadoYSecondValue)
                {
                    TextResultado.Y = TextResultadoYFirstValue * (1 - interpolationValue) + TextResultadoYSecondValue * interpolationValue;
                }
                if (setWidthFirstValue && setWidthSecondValue)
                {
                    Width = WidthFirstValue * (1 - interpolationValue) + WidthSecondValue * interpolationValue;
                }
                if (interpolationValue < 1)
                {
                    mCurrentVariableState = firstState;
                }
                else
                {
                    mCurrentVariableState = secondState;
                }
            }
            #endregion
            #region State Interpolate To
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (DungeonRun.GumRuntimes.GameOver_DisplayRuntime.VariableState fromState,DungeonRun.GumRuntimes.GameOver_DisplayRuntime.VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null) 
            {
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from:0, to:1, duration:(float)secondsToTake, type:interpolationType, easing:easing );
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(fromState, toState, newPosition);
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = this.ElementSave.States.First(item => item.Name == toState.ToString());
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateToRelative (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = AddToCurrentValuesWithState(toState);
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            #endregion
            #region State Animations
            #endregion
            public override void StopAnimations () 
            {
                base.StopAnimations();
            }
            #region Get Current Values on State
            private Gum.DataTypes.Variables.StateSave GetCurrentValuesOnState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Height",
                            Type = "float",
                            Value = Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Width",
                            Type = "float",
                            Value = Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Painel.Height",
                            Type = "float",
                            Value = Painel.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Painel.Height Units",
                            Type = "DimensionUnitType",
                            Value = Painel.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Painel.SourceFile",
                            Type = "string",
                            Value = Painel.SourceFile
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Painel.Width",
                            Type = "float",
                            Value = Painel.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Painel.Width Units",
                            Type = "DimensionUnitType",
                            Value = Painel.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Painel.X",
                            Type = "float",
                            Value = Painel.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Painel.Y",
                            Type = "float",
                            Value = Painel.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Blue",
                            Type = "int",
                            Value = RectangleInstance.Blue
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Green",
                            Type = "int",
                            Value = RectangleInstance.Green
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Height",
                            Type = "float",
                            Value = RectangleInstance.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Height Units",
                            Type = "DimensionUnitType",
                            Value = RectangleInstance.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Parent",
                            Type = "string",
                            Value = RectangleInstance.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Red",
                            Type = "int",
                            Value = RectangleInstance.Red
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Width",
                            Type = "float",
                            Value = RectangleInstance.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Width Units",
                            Type = "DimensionUnitType",
                            Value = RectangleInstance.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.X",
                            Type = "float",
                            Value = RectangleInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Y",
                            Type = "float",
                            Value = RectangleInstance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.Font",
                            Type = "string",
                            Value = TextNota.Font
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.FontSize",
                            Type = "int",
                            Value = TextNota.FontSize
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.Height",
                            Type = "float",
                            Value = TextNota.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = TextNota.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.OutlineThickness",
                            Type = "int",
                            Value = TextNota.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.Parent",
                            Type = "string",
                            Value = TextNota.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.Text",
                            Type = "string",
                            Value = TextNota.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.VerticalAlignment",
                            Type = "VerticalAlignment",
                            Value = TextNota.VerticalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.Width",
                            Type = "float",
                            Value = TextNota.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.Width Units",
                            Type = "DimensionUnitType",
                            Value = TextNota.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.X",
                            Type = "float",
                            Value = TextNota.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.X Origin",
                            Type = "HorizontalAlignment",
                            Value = TextNota.XOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.X Units",
                            Type = "PositionUnitType",
                            Value = TextNota.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.Y",
                            Type = "float",
                            Value = TextNota.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.Y Origin",
                            Type = "VerticalAlignment",
                            Value = TextNota.YOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.Y Units",
                            Type = "PositionUnitType",
                            Value = TextNota.YUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextResultado.Font",
                            Type = "string",
                            Value = TextResultado.Font
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextResultado.FontSize",
                            Type = "int",
                            Value = TextResultado.FontSize
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextResultado.Height",
                            Type = "float",
                            Value = TextResultado.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextResultado.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = TextResultado.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextResultado.OutlineThickness",
                            Type = "int",
                            Value = TextResultado.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextResultado.Parent",
                            Type = "string",
                            Value = TextResultado.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextResultado.Text",
                            Type = "string",
                            Value = TextResultado.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextResultado.VerticalAlignment",
                            Type = "VerticalAlignment",
                            Value = TextResultado.VerticalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextResultado.Width",
                            Type = "float",
                            Value = TextResultado.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextResultado.X",
                            Type = "float",
                            Value = TextResultado.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextResultado.Y",
                            Type = "float",
                            Value = TextResultado.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Blue",
                            Type = "int",
                            Value = TextInstance.Blue
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Font",
                            Type = "string",
                            Value = TextInstance.Font
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.FontSize",
                            Type = "int",
                            Value = TextInstance.FontSize
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Green",
                            Type = "int",
                            Value = TextInstance.Green
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Height",
                            Type = "float",
                            Value = TextInstance.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.OutlineThickness",
                            Type = "int",
                            Value = TextInstance.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Parent",
                            Type = "string",
                            Value = TextInstance.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Red",
                            Type = "int",
                            Value = TextInstance.Red
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Text",
                            Type = "string",
                            Value = TextInstance.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Width",
                            Type = "float",
                            Value = TextInstance.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.X",
                            Type = "float",
                            Value = TextInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Y",
                            Type = "float",
                            Value = TextInstance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Height",
                            Type = "float",
                            Value = ContainerInstance.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Width",
                            Type = "float",
                            Value = ContainerInstance.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.X",
                            Type = "float",
                            Value = ContainerInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Y",
                            Type = "float",
                            Value = ContainerInstance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "GameOverMSG.Height",
                            Type = "float",
                            Value = GameOverMSG.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "GameOverMSG.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = GameOverMSG.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "GameOverMSG.OutlineThickness",
                            Type = "int",
                            Value = GameOverMSG.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "GameOverMSG.Parent",
                            Type = "string",
                            Value = GameOverMSG.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "GameOverMSG.Text",
                            Type = "string",
                            Value = GameOverMSG.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "GameOverMSG.VerticalAlignment",
                            Type = "VerticalAlignment",
                            Value = GameOverMSG.VerticalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "GameOverMSG.Width",
                            Type = "float",
                            Value = GameOverMSG.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "GameOverMSG.X",
                            Type = "float",
                            Value = GameOverMSG.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "GameOverMSG.Y",
                            Type = "float",
                            Value = GameOverMSG.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance1.Width",
                            Type = "float",
                            Value = ContainerInstance1.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance1.X",
                            Type = "float",
                            Value = ContainerInstance1.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance1.Y",
                            Type = "float",
                            Value = ContainerInstance1.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Height",
                            Type = "float",
                            Value = Avatar.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Parent",
                            Type = "string",
                            Value = Avatar.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.SourceFile",
                            Type = "string",
                            Value = Avatar.SourceFile
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Address",
                            Type = "TextureAddress",
                            Value = Avatar.TextureAddress
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Height",
                            Type = "int",
                            Value = Avatar.TextureHeight
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Height Scale",
                            Type = "float",
                            Value = Avatar.TextureHeightScale
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Left",
                            Type = "int",
                            Value = Avatar.TextureLeft
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Top",
                            Type = "int",
                            Value = Avatar.TextureTop
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Width",
                            Type = "int",
                            Value = Avatar.TextureWidth
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Width Scale",
                            Type = "float",
                            Value = Avatar.TextureWidthScale
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Width",
                            Type = "float",
                            Value = Avatar.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Wrap",
                            Type = "bool",
                            Value = Avatar.Wrap
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.X",
                            Type = "float",
                            Value = Avatar.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Y",
                            Type = "float",
                            Value = Avatar.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance1.Height",
                            Type = "float",
                            Value = RectangleInstance1.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance1.Height Units",
                            Type = "DimensionUnitType",
                            Value = RectangleInstance1.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance1.Parent",
                            Type = "string",
                            Value = RectangleInstance1.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance1.Width",
                            Type = "float",
                            Value = RectangleInstance1.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance1.Width Units",
                            Type = "DimensionUnitType",
                            Value = RectangleInstance1.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance1.X",
                            Type = "float",
                            Value = RectangleInstance1.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance1.Y",
                            Type = "float",
                            Value = RectangleInstance1.Y
                        }
                        );
                        break;
                }
                return newState;
            }
            private Gum.DataTypes.Variables.StateSave AddToCurrentValuesWithState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Height",
                            Type = "float",
                            Value = Height + 348f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Width",
                            Type = "float",
                            Value = Width + 520f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Painel.Height",
                            Type = "float",
                            Value = Painel.Height + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Painel.Height Units",
                            Type = "DimensionUnitType",
                            Value = Painel.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Painel.SourceFile",
                            Type = "string",
                            Value = Painel.SourceFile
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Painel.Width",
                            Type = "float",
                            Value = Painel.Width + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Painel.Width Units",
                            Type = "DimensionUnitType",
                            Value = Painel.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Painel.X",
                            Type = "float",
                            Value = Painel.X + -2f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Painel.Y",
                            Type = "float",
                            Value = Painel.Y + -1f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Blue",
                            Type = "int",
                            Value = RectangleInstance.Blue + 0
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Green",
                            Type = "int",
                            Value = RectangleInstance.Green + 0
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Height",
                            Type = "float",
                            Value = RectangleInstance.Height + -4f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Height Units",
                            Type = "DimensionUnitType",
                            Value = RectangleInstance.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Parent",
                            Type = "string",
                            Value = RectangleInstance.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Red",
                            Type = "int",
                            Value = RectangleInstance.Red + 0
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Width",
                            Type = "float",
                            Value = RectangleInstance.Width + 3f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Width Units",
                            Type = "DimensionUnitType",
                            Value = RectangleInstance.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.X",
                            Type = "float",
                            Value = RectangleInstance.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance.Y",
                            Type = "float",
                            Value = RectangleInstance.Y + 1f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.Font",
                            Type = "string",
                            Value = TextNota.Font
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.FontSize",
                            Type = "int",
                            Value = TextNota.FontSize + 55
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.Height",
                            Type = "float",
                            Value = TextNota.Height + 155f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = TextNota.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.OutlineThickness",
                            Type = "int",
                            Value = TextNota.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.Parent",
                            Type = "string",
                            Value = TextNota.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.Text",
                            Type = "string",
                            Value = TextNota.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.VerticalAlignment",
                            Type = "VerticalAlignment",
                            Value = TextNota.VerticalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.Width",
                            Type = "float",
                            Value = TextNota.Width + -2f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.Width Units",
                            Type = "DimensionUnitType",
                            Value = TextNota.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.X",
                            Type = "float",
                            Value = TextNota.X + 1f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.X Origin",
                            Type = "HorizontalAlignment",
                            Value = TextNota.XOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.X Units",
                            Type = "PositionUnitType",
                            Value = TextNota.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.Y",
                            Type = "float",
                            Value = TextNota.Y + -3f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.Y Origin",
                            Type = "VerticalAlignment",
                            Value = TextNota.YOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextNota.Y Units",
                            Type = "PositionUnitType",
                            Value = TextNota.YUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextResultado.Font",
                            Type = "string",
                            Value = TextResultado.Font
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextResultado.FontSize",
                            Type = "int",
                            Value = TextResultado.FontSize + 25
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextResultado.Height",
                            Type = "float",
                            Value = TextResultado.Height + 35f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextResultado.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = TextResultado.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextResultado.OutlineThickness",
                            Type = "int",
                            Value = TextResultado.OutlineThickness + 2
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextResultado.Parent",
                            Type = "string",
                            Value = TextResultado.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextResultado.Text",
                            Type = "string",
                            Value = TextResultado.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextResultado.VerticalAlignment",
                            Type = "VerticalAlignment",
                            Value = TextResultado.VerticalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextResultado.Width",
                            Type = "float",
                            Value = TextResultado.Width + 157f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextResultado.X",
                            Type = "float",
                            Value = TextResultado.X + 192f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextResultado.Y",
                            Type = "float",
                            Value = TextResultado.Y + 132f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Blue",
                            Type = "int",
                            Value = TextInstance.Blue + 0
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Font",
                            Type = "string",
                            Value = TextInstance.Font
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.FontSize",
                            Type = "int",
                            Value = TextInstance.FontSize + 35
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Green",
                            Type = "int",
                            Value = TextInstance.Green + 0
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Height",
                            Type = "float",
                            Value = TextInstance.Height + 41f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.OutlineThickness",
                            Type = "int",
                            Value = TextInstance.OutlineThickness + 0
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Parent",
                            Type = "string",
                            Value = TextInstance.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Red",
                            Type = "int",
                            Value = TextInstance.Red + 0
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Text",
                            Type = "string",
                            Value = TextInstance.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Width",
                            Type = "float",
                            Value = TextInstance.Width + 128f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.X",
                            Type = "float",
                            Value = TextInstance.X + 100f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "TextInstance.Y",
                            Type = "float",
                            Value = TextInstance.Y + 2f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Height",
                            Type = "float",
                            Value = ContainerInstance.Height + 156f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Width",
                            Type = "float",
                            Value = ContainerInstance.Width + 490f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.X",
                            Type = "float",
                            Value = ContainerInstance.X + 7f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Y",
                            Type = "float",
                            Value = ContainerInstance.Y + 11f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "GameOverMSG.Height",
                            Type = "float",
                            Value = GameOverMSG.Height + 121f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "GameOverMSG.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = GameOverMSG.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "GameOverMSG.OutlineThickness",
                            Type = "int",
                            Value = GameOverMSG.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "GameOverMSG.Parent",
                            Type = "string",
                            Value = GameOverMSG.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "GameOverMSG.Text",
                            Type = "string",
                            Value = GameOverMSG.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "GameOverMSG.VerticalAlignment",
                            Type = "VerticalAlignment",
                            Value = GameOverMSG.VerticalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "GameOverMSG.Width",
                            Type = "float",
                            Value = GameOverMSG.Width + 354f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "GameOverMSG.X",
                            Type = "float",
                            Value = GameOverMSG.X + 109f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "GameOverMSG.Y",
                            Type = "float",
                            Value = GameOverMSG.Y + -20f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance1.Width",
                            Type = "float",
                            Value = ContainerInstance1.Width + 475f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance1.X",
                            Type = "float",
                            Value = ContainerInstance1.X + 13f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance1.Y",
                            Type = "float",
                            Value = ContainerInstance1.Y + 177f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Height",
                            Type = "float",
                            Value = Avatar.Height + 187.2313f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Parent",
                            Type = "string",
                            Value = Avatar.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.SourceFile",
                            Type = "string",
                            Value = Avatar.SourceFile
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Address",
                            Type = "TextureAddress",
                            Value = Avatar.TextureAddress
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Height",
                            Type = "int",
                            Value = Avatar.TextureHeight + 57
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Height Scale",
                            Type = "float",
                            Value = Avatar.TextureHeightScale + 1f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Left",
                            Type = "int",
                            Value = Avatar.TextureLeft + 0
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Top",
                            Type = "int",
                            Value = Avatar.TextureTop + 0
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Width",
                            Type = "int",
                            Value = Avatar.TextureWidth + 55
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Texture Width Scale",
                            Type = "float",
                            Value = Avatar.TextureWidthScale + 1f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Width",
                            Type = "float",
                            Value = Avatar.Width + 201.1433f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Wrap",
                            Type = "bool",
                            Value = Avatar.Wrap
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.X",
                            Type = "float",
                            Value = Avatar.X + 6f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Avatar.Y",
                            Type = "float",
                            Value = Avatar.Y + 33f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance1.Height",
                            Type = "float",
                            Value = RectangleInstance1.Height + -17f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance1.Height Units",
                            Type = "DimensionUnitType",
                            Value = RectangleInstance1.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance1.Parent",
                            Type = "string",
                            Value = RectangleInstance1.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance1.Width",
                            Type = "float",
                            Value = RectangleInstance1.Width + -11f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance1.Width Units",
                            Type = "DimensionUnitType",
                            Value = RectangleInstance1.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance1.X",
                            Type = "float",
                            Value = RectangleInstance1.X + 8f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "RectangleInstance1.Y",
                            Type = "float",
                            Value = RectangleInstance1.Y + 8f
                        }
                        );
                        break;
                }
                return newState;
            }
            #endregion
            public override void ApplyState (Gum.DataTypes.Variables.StateSave state) 
            {
                bool matches = this.ElementSave.AllStates.Contains(state);
                if (matches)
                {
                    var category = this.ElementSave.Categories.FirstOrDefault(item => item.States.Contains(state));
                    if (category == null)
                    {
                        if (state.Name == "Default") this.mCurrentVariableState = VariableState.Default;
                    }
                }
                base.ApplyState(state);
            }
            private DungeonRun.GumRuntimes.SpriteRuntime Painel { get; set; }
            private DungeonRun.GumRuntimes.ColoredRectangleRuntime RectangleInstance { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime TextNota { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime TextResultado { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime TextInstance { get; set; }
            private DungeonRun.GumRuntimes.ContainerRuntime ContainerInstance { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime GameOverMSG { get; set; }
            private DungeonRun.GumRuntimes.ContainerRuntime ContainerInstance1 { get; set; }
            private DungeonRun.GumRuntimes.SpriteRuntime Avatar { get; set; }
            private DungeonRun.GumRuntimes.RectangleRuntime RectangleInstance1 { get; set; }
            public Microsoft.Xna.Framework.Graphics.Texture2D GameOverAvatarSourceFile
            {
                get
                {
                    return Avatar.SourceFile;
                }
                set
                {
                    if (Avatar.SourceFile != value)
                    {
                        Avatar.SourceFile = value;
                        GameOverAvatarSourceFileChanged?.Invoke(this, null);
                    }
                }
            }
            public string GameOverMSGText
            {
                get
                {
                    return GameOverMSG.Text;
                }
                set
                {
                    if (GameOverMSG.Text != value)
                    {
                        GameOverMSG.Text = value;
                        GameOverMSGTextChanged?.Invoke(this, null);
                    }
                }
            }
            public string TextNotaText
            {
                get
                {
                    return TextNota.Text;
                }
                set
                {
                    if (TextNota.Text != value)
                    {
                        TextNota.Text = value;
                        TextNotaTextChanged?.Invoke(this, null);
                    }
                }
            }
            public int TextResultadoBlue
            {
                get
                {
                    return TextResultado.Blue;
                }
                set
                {
                    if (TextResultado.Blue != value)
                    {
                        TextResultado.Blue = value;
                        TextResultadoBlueChanged?.Invoke(this, null);
                    }
                }
            }
            public int TextResultadoGreen
            {
                get
                {
                    return TextResultado.Green;
                }
                set
                {
                    if (TextResultado.Green != value)
                    {
                        TextResultado.Green = value;
                        TextResultadoGreenChanged?.Invoke(this, null);
                    }
                }
            }
            public int TextResultadoRed
            {
                get
                {
                    return TextResultado.Red;
                }
                set
                {
                    if (TextResultado.Red != value)
                    {
                        TextResultado.Red = value;
                        TextResultadoRedChanged?.Invoke(this, null);
                    }
                }
            }
            public string TextResultadoTex
            {
                get
                {
                    return TextResultado.Text;
                }
                set
                {
                    if (TextResultado.Text != value)
                    {
                        TextResultado.Text = value;
                        TextResultadoTexChanged?.Invoke(this, null);
                    }
                }
            }
            public event System.EventHandler GameOverAvatarSourceFileChanged;
            public event System.EventHandler GameOverMSGTextChanged;
            public event System.EventHandler TextNotaTextChanged;
            public event System.EventHandler TextResultadoBlueChanged;
            public event System.EventHandler TextResultadoGreenChanged;
            public event System.EventHandler TextResultadoRedChanged;
            public event System.EventHandler TextResultadoTexChanged;
            public GameOver_DisplayRuntime (bool fullInstantiation = true) 
            	: base(false)
            {
                if (fullInstantiation)
                {
                    Gum.DataTypes.ElementSave elementSave = Gum.Managers.ObjectFinder.Self.GumProjectSave.Components.First(item => item.Name == "GameOver_Display");
                    this.ElementSave = elementSave;
                    string oldDirectory = FlatRedBall.IO.FileManager.RelativeDirectory;
                    FlatRedBall.IO.FileManager.RelativeDirectory = FlatRedBall.IO.FileManager.GetDirectory(Gum.Managers.ObjectFinder.Self.GumProjectSave.FullFileName);
                    GumRuntime.ElementSaveExtensions.SetGraphicalUiElement(elementSave, this, RenderingLibrary.SystemManagers.Default);
                    FlatRedBall.IO.FileManager.RelativeDirectory = oldDirectory;
                }
            }
            public override void SetInitialState () 
            {
                base.SetInitialState();
                this.CurrentVariableState = VariableState.Default;
                CallCustomInitialize();
            }
            public override void CreateChildrenRecursively (Gum.DataTypes.ElementSave elementSave, RenderingLibrary.SystemManagers systemManagers) 
            {
                base.CreateChildrenRecursively(elementSave, systemManagers);
                this.AssignReferences();
            }
            private void AssignReferences () 
            {
                Painel = this.GetGraphicalUiElementByName("Painel") as DungeonRun.GumRuntimes.SpriteRuntime;
                RectangleInstance = this.GetGraphicalUiElementByName("RectangleInstance") as DungeonRun.GumRuntimes.ColoredRectangleRuntime;
                TextNota = this.GetGraphicalUiElementByName("TextNota") as DungeonRun.GumRuntimes.TextRuntime;
                TextResultado = this.GetGraphicalUiElementByName("TextResultado") as DungeonRun.GumRuntimes.TextRuntime;
                TextInstance = this.GetGraphicalUiElementByName("TextInstance") as DungeonRun.GumRuntimes.TextRuntime;
                ContainerInstance = this.GetGraphicalUiElementByName("ContainerInstance") as DungeonRun.GumRuntimes.ContainerRuntime;
                GameOverMSG = this.GetGraphicalUiElementByName("GameOverMSG") as DungeonRun.GumRuntimes.TextRuntime;
                ContainerInstance1 = this.GetGraphicalUiElementByName("ContainerInstance1") as DungeonRun.GumRuntimes.ContainerRuntime;
                Avatar = this.GetGraphicalUiElementByName("Avatar") as DungeonRun.GumRuntimes.SpriteRuntime;
                RectangleInstance1 = this.GetGraphicalUiElementByName("RectangleInstance1") as DungeonRun.GumRuntimes.RectangleRuntime;
            }
            public override void AddToManagers (RenderingLibrary.SystemManagers managers, RenderingLibrary.Graphics.Layer layer) 
            {
                base.AddToManagers(managers, layer);
            }
            private void CallCustomInitialize () 
            {
                CustomInitialize();
            }
            partial void CustomInitialize();
        }
    }
