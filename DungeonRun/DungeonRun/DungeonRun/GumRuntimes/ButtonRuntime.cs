﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonRun.GumRuntimes
{
    partial class ButtonRuntime
    {
       // private Microsoft.Xna.Framework.Audio.SoundEffectInstance SoundButton;

        partial void CustomInitialize()
        {
            this.Click += ButtonRuntime_Click;
            this.RollOn += ButtonRuntime_RollOn;
            this.RollOver += ButtonRuntime_RollOver;
            this.RollOff += ButtonRuntime_RollOff;
            this.Push += ButtonRuntime_Push;
            this.DragOver += ButtonRuntime_DragOver;

          
           // this.LosePush += ButtonRuntime_LosePush;

        }

        private void ButtonRuntime_DragOver(FlatRedBall.Gui.IWindow window)
        {
            this.CurrentButtonCategoryState = ButtonCategory.Enabled;
        }

        private void ButtonRuntime_LosePush(FlatRedBall.Gui.IWindow window)
        {
            this.CurrentButtonCategoryState = ButtonCategory.Enabled;
        }

        private void ButtonRuntime_RollOff(FlatRedBall.Gui.IWindow window)
        {
            this.CurrentButtonCategoryState = ButtonCategory.Enabled ;
        }

        private void ButtonRuntime_Push(FlatRedBall.Gui.IWindow window)
        {
            this.CurrentButtonCategoryState = ButtonCategory.Pushed;
        }

        private void ButtonRuntime_RollOver(FlatRedBall.Gui.IWindow window)
        {
           // this.CurrentButtonCategoryState = ButtonCategory.Enabled;
        }

        private void ButtonRuntime_RollOn(FlatRedBall.Gui.IWindow window)
        {
            this.CurrentButtonCategoryState = ButtonCategory.Highlighted;
        }

        private void ButtonRuntime_Click(FlatRedBall.Gui.IWindow window)
        {
            
           this.CurrentButtonCategoryState = ButtonCategory.Pushed;
        }
    }
}
