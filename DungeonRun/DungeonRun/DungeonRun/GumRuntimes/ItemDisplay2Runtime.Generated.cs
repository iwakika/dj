    using System.Linq;
    namespace DungeonRun.GumRuntimes
    {
        public partial class ItemDisplay2Runtime : DungeonRun.GumRuntimes.ContainerRuntime
        {
            #region State Enums
            public enum VariableState
            {
                Default
            }
            #endregion
            #region State Fields
            VariableState mCurrentVariableState;
            #endregion
            #region State Properties
            public VariableState CurrentVariableState
            {
                get
                {
                    return mCurrentVariableState;
                }
                set
                {
                    mCurrentVariableState = value;
                    switch(mCurrentVariableState)
                    {
                        case  VariableState.Default:
                            Height = 64f;
                            Width = 64f;
                            SpriteInstance.Height = 64f;
                            SpriteInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.Absolute;
                            SetProperty("SpriteInstance.SourceFile", "panelInset_brown_2.png");
                            SpriteInstance.Visible = false;
                            SpriteInstance.Width = 64f;
                            SpriteInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.Absolute;
                            SpriteInstance.X = 0f;
                            ItemText.FontSize = 12;
                            ItemText.Height = 24.67546f;
                            ItemText.HeightUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                            ItemText.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Left;
                            ItemText.OutlineThickness = 1;
                            ItemText.Text = "Nome do Bagulho";
                            ItemText.Width = 169.1172f;
                            ItemText.WidthUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                            ItemText.X = 74f;
                            ItemText.Y = 20f;
                            break;
                    }
                }
            }
            #endregion
            #region State Interpolation
            public void InterpolateBetween (VariableState firstState, VariableState secondState, float interpolationValue) 
            {
                #if DEBUG
                if (float.IsNaN(interpolationValue))
                {
                    throw new System.Exception("interpolationValue cannot be NaN");
                }
                #endif
                bool setHeightFirstValue = false;
                bool setHeightSecondValue = false;
                float HeightFirstValue= 0;
                float HeightSecondValue= 0;
                bool setItemTextFontSizeFirstValue = false;
                bool setItemTextFontSizeSecondValue = false;
                int ItemTextFontSizeFirstValue= 0;
                int ItemTextFontSizeSecondValue= 0;
                bool setItemTextHeightFirstValue = false;
                bool setItemTextHeightSecondValue = false;
                float ItemTextHeightFirstValue= 0;
                float ItemTextHeightSecondValue= 0;
                bool setItemTextOutlineThicknessFirstValue = false;
                bool setItemTextOutlineThicknessSecondValue = false;
                int ItemTextOutlineThicknessFirstValue= 0;
                int ItemTextOutlineThicknessSecondValue= 0;
                bool setItemTextWidthFirstValue = false;
                bool setItemTextWidthSecondValue = false;
                float ItemTextWidthFirstValue= 0;
                float ItemTextWidthSecondValue= 0;
                bool setItemTextXFirstValue = false;
                bool setItemTextXSecondValue = false;
                float ItemTextXFirstValue= 0;
                float ItemTextXSecondValue= 0;
                bool setItemTextYFirstValue = false;
                bool setItemTextYSecondValue = false;
                float ItemTextYFirstValue= 0;
                float ItemTextYSecondValue= 0;
                bool setSpriteInstanceHeightFirstValue = false;
                bool setSpriteInstanceHeightSecondValue = false;
                float SpriteInstanceHeightFirstValue= 0;
                float SpriteInstanceHeightSecondValue= 0;
                bool setSpriteInstanceWidthFirstValue = false;
                bool setSpriteInstanceWidthSecondValue = false;
                float SpriteInstanceWidthFirstValue= 0;
                float SpriteInstanceWidthSecondValue= 0;
                bool setSpriteInstanceXFirstValue = false;
                bool setSpriteInstanceXSecondValue = false;
                float SpriteInstanceXFirstValue= 0;
                float SpriteInstanceXSecondValue= 0;
                bool setWidthFirstValue = false;
                bool setWidthSecondValue = false;
                float WidthFirstValue= 0;
                float WidthSecondValue= 0;
                switch(firstState)
                {
                    case  VariableState.Default:
                        setHeightFirstValue = true;
                        HeightFirstValue = 64f;
                        setItemTextFontSizeFirstValue = true;
                        ItemTextFontSizeFirstValue = 12;
                        setItemTextHeightFirstValue = true;
                        ItemTextHeightFirstValue = 24.67546f;
                        if (interpolationValue < 1)
                        {
                            this.ItemText.HeightUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        if (interpolationValue < 1)
                        {
                            this.ItemText.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Left;
                        }
                        setItemTextOutlineThicknessFirstValue = true;
                        ItemTextOutlineThicknessFirstValue = 1;
                        if (interpolationValue < 1)
                        {
                            this.ItemText.Text = "Nome do Bagulho";
                        }
                        setItemTextWidthFirstValue = true;
                        ItemTextWidthFirstValue = 169.1172f;
                        if (interpolationValue < 1)
                        {
                            this.ItemText.WidthUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        setItemTextXFirstValue = true;
                        ItemTextXFirstValue = 74f;
                        setItemTextYFirstValue = true;
                        ItemTextYFirstValue = 20f;
                        setSpriteInstanceHeightFirstValue = true;
                        SpriteInstanceHeightFirstValue = 64f;
                        if (interpolationValue < 1)
                        {
                            this.SpriteInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.Absolute;
                        }
                        if (interpolationValue < 1)
                        {
                            SetProperty("SpriteInstance.SourceFile", "panelInset_brown_2.png");
                        }
                        if (interpolationValue < 1)
                        {
                            this.SpriteInstance.Visible = false;
                        }
                        setSpriteInstanceWidthFirstValue = true;
                        SpriteInstanceWidthFirstValue = 64f;
                        if (interpolationValue < 1)
                        {
                            this.SpriteInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.Absolute;
                        }
                        setSpriteInstanceXFirstValue = true;
                        SpriteInstanceXFirstValue = 0f;
                        setWidthFirstValue = true;
                        WidthFirstValue = 64f;
                        break;
                }
                switch(secondState)
                {
                    case  VariableState.Default:
                        setHeightSecondValue = true;
                        HeightSecondValue = 64f;
                        setItemTextFontSizeSecondValue = true;
                        ItemTextFontSizeSecondValue = 12;
                        setItemTextHeightSecondValue = true;
                        ItemTextHeightSecondValue = 24.67546f;
                        if (interpolationValue >= 1)
                        {
                            this.ItemText.HeightUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.ItemText.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Left;
                        }
                        setItemTextOutlineThicknessSecondValue = true;
                        ItemTextOutlineThicknessSecondValue = 1;
                        if (interpolationValue >= 1)
                        {
                            this.ItemText.Text = "Nome do Bagulho";
                        }
                        setItemTextWidthSecondValue = true;
                        ItemTextWidthSecondValue = 169.1172f;
                        if (interpolationValue >= 1)
                        {
                            this.ItemText.WidthUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        setItemTextXSecondValue = true;
                        ItemTextXSecondValue = 74f;
                        setItemTextYSecondValue = true;
                        ItemTextYSecondValue = 20f;
                        setSpriteInstanceHeightSecondValue = true;
                        SpriteInstanceHeightSecondValue = 64f;
                        if (interpolationValue >= 1)
                        {
                            this.SpriteInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.Absolute;
                        }
                        if (interpolationValue >= 1)
                        {
                            SetProperty("SpriteInstance.SourceFile", "panelInset_brown_2.png");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.SpriteInstance.Visible = false;
                        }
                        setSpriteInstanceWidthSecondValue = true;
                        SpriteInstanceWidthSecondValue = 64f;
                        if (interpolationValue >= 1)
                        {
                            this.SpriteInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.Absolute;
                        }
                        setSpriteInstanceXSecondValue = true;
                        SpriteInstanceXSecondValue = 0f;
                        setWidthSecondValue = true;
                        WidthSecondValue = 64f;
                        break;
                }
                if (setHeightFirstValue && setHeightSecondValue)
                {
                    Height = HeightFirstValue * (1 - interpolationValue) + HeightSecondValue * interpolationValue;
                }
                if (setItemTextFontSizeFirstValue && setItemTextFontSizeSecondValue)
                {
                    ItemText.FontSize = FlatRedBall.Math.MathFunctions.RoundToInt(ItemTextFontSizeFirstValue* (1 - interpolationValue) + ItemTextFontSizeSecondValue * interpolationValue);
                }
                if (setItemTextHeightFirstValue && setItemTextHeightSecondValue)
                {
                    ItemText.Height = ItemTextHeightFirstValue * (1 - interpolationValue) + ItemTextHeightSecondValue * interpolationValue;
                }
                if (setItemTextOutlineThicknessFirstValue && setItemTextOutlineThicknessSecondValue)
                {
                    ItemText.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(ItemTextOutlineThicknessFirstValue* (1 - interpolationValue) + ItemTextOutlineThicknessSecondValue * interpolationValue);
                }
                if (setItemTextWidthFirstValue && setItemTextWidthSecondValue)
                {
                    ItemText.Width = ItemTextWidthFirstValue * (1 - interpolationValue) + ItemTextWidthSecondValue * interpolationValue;
                }
                if (setItemTextXFirstValue && setItemTextXSecondValue)
                {
                    ItemText.X = ItemTextXFirstValue * (1 - interpolationValue) + ItemTextXSecondValue * interpolationValue;
                }
                if (setItemTextYFirstValue && setItemTextYSecondValue)
                {
                    ItemText.Y = ItemTextYFirstValue * (1 - interpolationValue) + ItemTextYSecondValue * interpolationValue;
                }
                if (setSpriteInstanceHeightFirstValue && setSpriteInstanceHeightSecondValue)
                {
                    SpriteInstance.Height = SpriteInstanceHeightFirstValue * (1 - interpolationValue) + SpriteInstanceHeightSecondValue * interpolationValue;
                }
                if (setSpriteInstanceWidthFirstValue && setSpriteInstanceWidthSecondValue)
                {
                    SpriteInstance.Width = SpriteInstanceWidthFirstValue * (1 - interpolationValue) + SpriteInstanceWidthSecondValue * interpolationValue;
                }
                if (setSpriteInstanceXFirstValue && setSpriteInstanceXSecondValue)
                {
                    SpriteInstance.X = SpriteInstanceXFirstValue * (1 - interpolationValue) + SpriteInstanceXSecondValue * interpolationValue;
                }
                if (setWidthFirstValue && setWidthSecondValue)
                {
                    Width = WidthFirstValue * (1 - interpolationValue) + WidthSecondValue * interpolationValue;
                }
                if (interpolationValue < 1)
                {
                    mCurrentVariableState = firstState;
                }
                else
                {
                    mCurrentVariableState = secondState;
                }
            }
            #endregion
            #region State Interpolate To
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (DungeonRun.GumRuntimes.ItemDisplay2Runtime.VariableState fromState,DungeonRun.GumRuntimes.ItemDisplay2Runtime.VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null) 
            {
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from:0, to:1, duration:(float)secondsToTake, type:interpolationType, easing:easing );
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(fromState, toState, newPosition);
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = this.ElementSave.States.First(item => item.Name == toState.ToString());
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateToRelative (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = AddToCurrentValuesWithState(toState);
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            #endregion
            #region State Animations
            #endregion
            public override void StopAnimations () 
            {
                base.StopAnimations();
            }
            #region Get Current Values on State
            private Gum.DataTypes.Variables.StateSave GetCurrentValuesOnState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Height",
                            Type = "float",
                            Value = Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Width",
                            Type = "float",
                            Value = Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Height",
                            Type = "float",
                            Value = SpriteInstance.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Height Units",
                            Type = "DimensionUnitType",
                            Value = SpriteInstance.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.SourceFile",
                            Type = "string",
                            Value = SpriteInstance.SourceFile
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Visible",
                            Type = "bool",
                            Value = SpriteInstance.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Width",
                            Type = "float",
                            Value = SpriteInstance.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Width Units",
                            Type = "DimensionUnitType",
                            Value = SpriteInstance.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.X",
                            Type = "float",
                            Value = SpriteInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.FontSize",
                            Type = "int",
                            Value = ItemText.FontSize
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Height",
                            Type = "float",
                            Value = ItemText.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Height Units",
                            Type = "DimensionUnitType",
                            Value = ItemText.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = ItemText.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.OutlineThickness",
                            Type = "int",
                            Value = ItemText.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Text",
                            Type = "string",
                            Value = ItemText.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Width",
                            Type = "float",
                            Value = ItemText.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Width Units",
                            Type = "DimensionUnitType",
                            Value = ItemText.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.X",
                            Type = "float",
                            Value = ItemText.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Y",
                            Type = "float",
                            Value = ItemText.Y
                        }
                        );
                        break;
                }
                return newState;
            }
            private Gum.DataTypes.Variables.StateSave AddToCurrentValuesWithState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Height",
                            Type = "float",
                            Value = Height + 64f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Width",
                            Type = "float",
                            Value = Width + 64f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Height",
                            Type = "float",
                            Value = SpriteInstance.Height + 64f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Height Units",
                            Type = "DimensionUnitType",
                            Value = SpriteInstance.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.SourceFile",
                            Type = "string",
                            Value = SpriteInstance.SourceFile
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Visible",
                            Type = "bool",
                            Value = SpriteInstance.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Width",
                            Type = "float",
                            Value = SpriteInstance.Width + 64f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Width Units",
                            Type = "DimensionUnitType",
                            Value = SpriteInstance.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.X",
                            Type = "float",
                            Value = SpriteInstance.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.FontSize",
                            Type = "int",
                            Value = ItemText.FontSize + 12
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Height",
                            Type = "float",
                            Value = ItemText.Height + 24.67546f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Height Units",
                            Type = "DimensionUnitType",
                            Value = ItemText.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = ItemText.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.OutlineThickness",
                            Type = "int",
                            Value = ItemText.OutlineThickness + 1
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Text",
                            Type = "string",
                            Value = ItemText.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Width",
                            Type = "float",
                            Value = ItemText.Width + 169.1172f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Width Units",
                            Type = "DimensionUnitType",
                            Value = ItemText.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.X",
                            Type = "float",
                            Value = ItemText.X + 74f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ItemText.Y",
                            Type = "float",
                            Value = ItemText.Y + 20f
                        }
                        );
                        break;
                }
                return newState;
            }
            #endregion
            public override void ApplyState (Gum.DataTypes.Variables.StateSave state) 
            {
                bool matches = this.ElementSave.AllStates.Contains(state);
                if (matches)
                {
                    var category = this.ElementSave.Categories.FirstOrDefault(item => item.States.Contains(state));
                    if (category == null)
                    {
                        if (state.Name == "Default") this.mCurrentVariableState = VariableState.Default;
                    }
                }
                base.ApplyState(state);
            }
            private DungeonRun.GumRuntimes.SpriteRuntime SpriteInstance { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime ItemText { get; set; }
            public string ItemDescText
            {
                get
                {
                    return ItemText.Text;
                }
                set
                {
                    if (ItemText.Text != value)
                    {
                        ItemText.Text = value;
                        ItemDescTextChanged?.Invoke(this, null);
                    }
                }
            }
            public Microsoft.Xna.Framework.Graphics.Texture2D ItemSprite_hud
            {
                get
                {
                    return SpriteInstance.SourceFile;
                }
                set
                {
                    if (SpriteInstance.SourceFile != value)
                    {
                        SpriteInstance.SourceFile = value;
                        ItemSprite_hudChanged?.Invoke(this, null);
                    }
                }
            }
            public event System.EventHandler ItemDescTextChanged;
            public event System.EventHandler ItemSprite_hudChanged;
            public ItemDisplay2Runtime (bool fullInstantiation = true) 
            	: base(false)
            {
                if (fullInstantiation)
                {
                    Gum.DataTypes.ElementSave elementSave = Gum.Managers.ObjectFinder.Self.GumProjectSave.Components.First(item => item.Name == "ItemDisplay2");
                    this.ElementSave = elementSave;
                    string oldDirectory = FlatRedBall.IO.FileManager.RelativeDirectory;
                    FlatRedBall.IO.FileManager.RelativeDirectory = FlatRedBall.IO.FileManager.GetDirectory(Gum.Managers.ObjectFinder.Self.GumProjectSave.FullFileName);
                    GumRuntime.ElementSaveExtensions.SetGraphicalUiElement(elementSave, this, RenderingLibrary.SystemManagers.Default);
                    FlatRedBall.IO.FileManager.RelativeDirectory = oldDirectory;
                }
            }
            public override void SetInitialState () 
            {
                base.SetInitialState();
                this.CurrentVariableState = VariableState.Default;
                CallCustomInitialize();
            }
            public override void CreateChildrenRecursively (Gum.DataTypes.ElementSave elementSave, RenderingLibrary.SystemManagers systemManagers) 
            {
                base.CreateChildrenRecursively(elementSave, systemManagers);
                this.AssignReferences();
            }
            private void AssignReferences () 
            {
                SpriteInstance = this.GetGraphicalUiElementByName("SpriteInstance") as DungeonRun.GumRuntimes.SpriteRuntime;
                ItemText = this.GetGraphicalUiElementByName("ItemText") as DungeonRun.GumRuntimes.TextRuntime;
            }
            public override void AddToManagers (RenderingLibrary.SystemManagers managers, RenderingLibrary.Graphics.Layer layer) 
            {
                base.AddToManagers(managers, layer);
            }
            private void CallCustomInitialize () 
            {
                CustomInitialize();
            }
            partial void CustomInitialize();
        }
    }
