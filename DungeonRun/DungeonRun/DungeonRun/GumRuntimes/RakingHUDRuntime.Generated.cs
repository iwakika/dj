    using System.Linq;
    namespace DungeonRun.GumRuntimes
    {
        public partial class RakingHUDRuntime : Gum.Wireframe.GraphicalUiElement
        {
            #region State Enums
            public enum VariableState
            {
                Default
            }
            #endregion
            #region State Fields
            VariableState mCurrentVariableState;
            #endregion
            #region State Properties
            public VariableState CurrentVariableState
            {
                get
                {
                    return mCurrentVariableState;
                }
                set
                {
                    mCurrentVariableState = value;
                    switch(mCurrentVariableState)
                    {
                        case  VariableState.Default:
                            Panel_DisplayInstance.Text2 = "Pontos\n";
                            Panel_DisplayInstance.Visible = true;
                            Panel_DisplayInstance.X = 160f;
                            Panel_DisplayInstance.Y = 5f;
                            ranking_VoltarInicio.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "Panel_DisplayInstance");
                            ranking_VoltarInicio.Text = "Voltar Inicio";
                            ranking_VoltarInicio.X = 0f;
                            ranking_VoltarInicio.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                            ranking_VoltarInicio.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                            ranking_VoltarInicio.Y = -20f;
                            ranking_VoltarInicio.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Bottom;
                            ranking_VoltarInicio.YUnits = Gum.Converters.GeneralUnitType.PixelsFromLarge;
                            break;
                    }
                }
            }
            #endregion
            #region State Interpolation
            public void InterpolateBetween (VariableState firstState, VariableState secondState, float interpolationValue) 
            {
                #if DEBUG
                if (float.IsNaN(interpolationValue))
                {
                    throw new System.Exception("interpolationValue cannot be NaN");
                }
                #endif
                bool setPanel_DisplayInstanceXFirstValue = false;
                bool setPanel_DisplayInstanceXSecondValue = false;
                float Panel_DisplayInstanceXFirstValue= 0;
                float Panel_DisplayInstanceXSecondValue= 0;
                bool setPanel_DisplayInstanceYFirstValue = false;
                bool setPanel_DisplayInstanceYSecondValue = false;
                float Panel_DisplayInstanceYFirstValue= 0;
                float Panel_DisplayInstanceYSecondValue= 0;
                bool setranking_VoltarInicioXFirstValue = false;
                bool setranking_VoltarInicioXSecondValue = false;
                float ranking_VoltarInicioXFirstValue= 0;
                float ranking_VoltarInicioXSecondValue= 0;
                bool setranking_VoltarInicioYFirstValue = false;
                bool setranking_VoltarInicioYSecondValue = false;
                float ranking_VoltarInicioYFirstValue= 0;
                float ranking_VoltarInicioYSecondValue= 0;
                switch(firstState)
                {
                    case  VariableState.Default:
                        if (interpolationValue < 1)
                        {
                            this.Panel_DisplayInstance.Text2 = "Pontos\n";
                        }
                        if (interpolationValue < 1)
                        {
                            this.Panel_DisplayInstance.Visible = true;
                        }
                        setPanel_DisplayInstanceXFirstValue = true;
                        Panel_DisplayInstanceXFirstValue = 160f;
                        setPanel_DisplayInstanceYFirstValue = true;
                        Panel_DisplayInstanceYFirstValue = 5f;
                        if (interpolationValue < 1)
                        {
                            this.ranking_VoltarInicio.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "Panel_DisplayInstance");
                        }
                        if (interpolationValue < 1)
                        {
                            this.ranking_VoltarInicio.Text = "Voltar Inicio";
                        }
                        setranking_VoltarInicioXFirstValue = true;
                        ranking_VoltarInicioXFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.ranking_VoltarInicio.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        if (interpolationValue < 1)
                        {
                            this.ranking_VoltarInicio.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        setranking_VoltarInicioYFirstValue = true;
                        ranking_VoltarInicioYFirstValue = -20f;
                        if (interpolationValue < 1)
                        {
                            this.ranking_VoltarInicio.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Bottom;
                        }
                        if (interpolationValue < 1)
                        {
                            this.ranking_VoltarInicio.YUnits = Gum.Converters.GeneralUnitType.PixelsFromLarge;
                        }
                        break;
                }
                switch(secondState)
                {
                    case  VariableState.Default:
                        if (interpolationValue >= 1)
                        {
                            this.Panel_DisplayInstance.Text2 = "Pontos\n";
                        }
                        if (interpolationValue >= 1)
                        {
                            this.Panel_DisplayInstance.Visible = true;
                        }
                        setPanel_DisplayInstanceXSecondValue = true;
                        Panel_DisplayInstanceXSecondValue = 160f;
                        setPanel_DisplayInstanceYSecondValue = true;
                        Panel_DisplayInstanceYSecondValue = 5f;
                        if (interpolationValue >= 1)
                        {
                            this.ranking_VoltarInicio.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "Panel_DisplayInstance");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.ranking_VoltarInicio.Text = "Voltar Inicio";
                        }
                        setranking_VoltarInicioXSecondValue = true;
                        ranking_VoltarInicioXSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.ranking_VoltarInicio.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.ranking_VoltarInicio.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        setranking_VoltarInicioYSecondValue = true;
                        ranking_VoltarInicioYSecondValue = -20f;
                        if (interpolationValue >= 1)
                        {
                            this.ranking_VoltarInicio.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Bottom;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.ranking_VoltarInicio.YUnits = Gum.Converters.GeneralUnitType.PixelsFromLarge;
                        }
                        break;
                }
                if (setPanel_DisplayInstanceXFirstValue && setPanel_DisplayInstanceXSecondValue)
                {
                    Panel_DisplayInstance.X = Panel_DisplayInstanceXFirstValue * (1 - interpolationValue) + Panel_DisplayInstanceXSecondValue * interpolationValue;
                }
                if (setPanel_DisplayInstanceYFirstValue && setPanel_DisplayInstanceYSecondValue)
                {
                    Panel_DisplayInstance.Y = Panel_DisplayInstanceYFirstValue * (1 - interpolationValue) + Panel_DisplayInstanceYSecondValue * interpolationValue;
                }
                if (setranking_VoltarInicioXFirstValue && setranking_VoltarInicioXSecondValue)
                {
                    ranking_VoltarInicio.X = ranking_VoltarInicioXFirstValue * (1 - interpolationValue) + ranking_VoltarInicioXSecondValue * interpolationValue;
                }
                if (setranking_VoltarInicioYFirstValue && setranking_VoltarInicioYSecondValue)
                {
                    ranking_VoltarInicio.Y = ranking_VoltarInicioYFirstValue * (1 - interpolationValue) + ranking_VoltarInicioYSecondValue * interpolationValue;
                }
                if (interpolationValue < 1)
                {
                    mCurrentVariableState = firstState;
                }
                else
                {
                    mCurrentVariableState = secondState;
                }
            }
            #endregion
            #region State Interpolate To
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (DungeonRun.GumRuntimes.RakingHUDRuntime.VariableState fromState,DungeonRun.GumRuntimes.RakingHUDRuntime.VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null) 
            {
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from:0, to:1, duration:(float)secondsToTake, type:interpolationType, easing:easing );
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(fromState, toState, newPosition);
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = this.ElementSave.States.First(item => item.Name == toState.ToString());
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateToRelative (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = AddToCurrentValuesWithState(toState);
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            #endregion
            #region State Animations
            #endregion
            public override void StopAnimations () 
            {
                base.StopAnimations();
                Panel_DisplayInstance.StopAnimations();
                ranking_VoltarInicio.StopAnimations();
            }
            #region Get Current Values on State
            private Gum.DataTypes.Variables.StateSave GetCurrentValuesOnState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Panel_DisplayInstance.Text2",
                            Type = "string",
                            Value = Panel_DisplayInstance.Text2
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Panel_DisplayInstance.Visible",
                            Type = "bool",
                            Value = Panel_DisplayInstance.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Panel_DisplayInstance.X",
                            Type = "float",
                            Value = Panel_DisplayInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Panel_DisplayInstance.Y",
                            Type = "float",
                            Value = Panel_DisplayInstance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ranking_VoltarInicio.Parent",
                            Type = "string",
                            Value = ranking_VoltarInicio.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ranking_VoltarInicio.Text",
                            Type = "string",
                            Value = ranking_VoltarInicio.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ranking_VoltarInicio.X",
                            Type = "float",
                            Value = ranking_VoltarInicio.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ranking_VoltarInicio.X Origin",
                            Type = "HorizontalAlignment",
                            Value = ranking_VoltarInicio.XOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ranking_VoltarInicio.X Units",
                            Type = "PositionUnitType",
                            Value = ranking_VoltarInicio.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ranking_VoltarInicio.Y",
                            Type = "float",
                            Value = ranking_VoltarInicio.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ranking_VoltarInicio.Y Origin",
                            Type = "VerticalAlignment",
                            Value = ranking_VoltarInicio.YOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ranking_VoltarInicio.Y Units",
                            Type = "PositionUnitType",
                            Value = ranking_VoltarInicio.YUnits
                        }
                        );
                        break;
                }
                return newState;
            }
            private Gum.DataTypes.Variables.StateSave AddToCurrentValuesWithState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Panel_DisplayInstance.Text2",
                            Type = "string",
                            Value = Panel_DisplayInstance.Text2
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Panel_DisplayInstance.Visible",
                            Type = "bool",
                            Value = Panel_DisplayInstance.Visible
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Panel_DisplayInstance.X",
                            Type = "float",
                            Value = Panel_DisplayInstance.X + 160f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Panel_DisplayInstance.Y",
                            Type = "float",
                            Value = Panel_DisplayInstance.Y + 5f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ranking_VoltarInicio.Parent",
                            Type = "string",
                            Value = ranking_VoltarInicio.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ranking_VoltarInicio.Text",
                            Type = "string",
                            Value = ranking_VoltarInicio.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ranking_VoltarInicio.X",
                            Type = "float",
                            Value = ranking_VoltarInicio.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ranking_VoltarInicio.X Origin",
                            Type = "HorizontalAlignment",
                            Value = ranking_VoltarInicio.XOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ranking_VoltarInicio.X Units",
                            Type = "PositionUnitType",
                            Value = ranking_VoltarInicio.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ranking_VoltarInicio.Y",
                            Type = "float",
                            Value = ranking_VoltarInicio.Y + -20f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ranking_VoltarInicio.Y Origin",
                            Type = "VerticalAlignment",
                            Value = ranking_VoltarInicio.YOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ranking_VoltarInicio.Y Units",
                            Type = "PositionUnitType",
                            Value = ranking_VoltarInicio.YUnits
                        }
                        );
                        break;
                }
                return newState;
            }
            #endregion
            public override void ApplyState (Gum.DataTypes.Variables.StateSave state) 
            {
                bool matches = this.ElementSave.AllStates.Contains(state);
                if (matches)
                {
                    var category = this.ElementSave.Categories.FirstOrDefault(item => item.States.Contains(state));
                    if (category == null)
                    {
                        if (state.Name == "Default") this.mCurrentVariableState = VariableState.Default;
                    }
                }
                base.ApplyState(state);
            }
            private DungeonRun.GumRuntimes.Panel_DisplayRuntime Panel_DisplayInstance { get; set; }
            private DungeonRun.GumRuntimes.ButtonRuntime ranking_VoltarInicio { get; set; }
            public RakingHUDRuntime (bool fullInstantiation = true) 
            {
                if (fullInstantiation)
                {
                    Gum.DataTypes.ElementSave elementSave = Gum.Managers.ObjectFinder.Self.GumProjectSave.Screens.First(item => item.Name == "RakingHUD");
                    this.ElementSave = elementSave;
                    string oldDirectory = FlatRedBall.IO.FileManager.RelativeDirectory;
                    FlatRedBall.IO.FileManager.RelativeDirectory = FlatRedBall.IO.FileManager.GetDirectory(Gum.Managers.ObjectFinder.Self.GumProjectSave.FullFileName);
                    GumRuntime.ElementSaveExtensions.SetGraphicalUiElement(elementSave, this, RenderingLibrary.SystemManagers.Default);
                    FlatRedBall.IO.FileManager.RelativeDirectory = oldDirectory;
                }
            }
            public override void SetInitialState () 
            {
                base.SetInitialState();
                this.CurrentVariableState = VariableState.Default;
                CallCustomInitialize();
            }
            public override void CreateChildrenRecursively (Gum.DataTypes.ElementSave elementSave, RenderingLibrary.SystemManagers systemManagers) 
            {
                base.CreateChildrenRecursively(elementSave, systemManagers);
                this.AssignReferences();
            }
            private void AssignReferences () 
            {
                Panel_DisplayInstance = this.GetGraphicalUiElementByName("Panel_DisplayInstance") as DungeonRun.GumRuntimes.Panel_DisplayRuntime;
                ranking_VoltarInicio = this.GetGraphicalUiElementByName("ranking_VoltarInicio") as DungeonRun.GumRuntimes.ButtonRuntime;
            }
            public override void AddToManagers (RenderingLibrary.SystemManagers managers, RenderingLibrary.Graphics.Layer layer) 
            {
                base.AddToManagers(managers, layer);
            }
            private void CallCustomInitialize () 
            {
                CustomInitialize();
            }
            partial void CustomInitialize();
        }
    }
