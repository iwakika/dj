    using System.Linq;
    namespace DungeonRun.GumRuntimes
    {
        public partial class Panel2_DisplayRuntime : DungeonRun.GumRuntimes.ContainerRuntime
        {
            #region State Enums
            public enum VariableState
            {
                Default
            }
            #endregion
            #region State Fields
            VariableState mCurrentVariableState;
            #endregion
            #region State Properties
            public VariableState CurrentVariableState
            {
                get
                {
                    return mCurrentVariableState;
                }
                set
                {
                    mCurrentVariableState = value;
                    switch(mCurrentVariableState)
                    {
                        case  VariableState.Default:
                            Height = 138f;
                            Width = 160f;
                            SpriteInstance.Height = 415f;
                            SpriteInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            SetProperty("SpriteInstance.SourceFile", "panelInset_brown.png");
                            SpriteInstance.Width = 507f;
                            SpriteInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            SpriteInstance.X = 74f;
                            SpriteInstance.Y = 17f;
                            HeaderInstance.Blue = 245;
                            HeaderInstance.Font = "Impact";
                            HeaderInstance.FontSize = 30;
                            HeaderInstance.Green = 240;
                            HeaderInstance.Height = -85f;
                            HeaderInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            HeaderInstance.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                            HeaderInstance.OutlineThickness = 2;
                            HeaderInstance.Red = 255;
                            HeaderInstance.Text = "CONTROLES\n";
                            HeaderInstance.VerticalAlignment = RenderingLibrary.Graphics.VerticalAlignment.Top;
                            HeaderInstance.Width = -321f;
                            HeaderInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            HeaderInstance.X = 339f;
                            HeaderInstance.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                            HeaderInstance.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                            HeaderInstance.Y = 34f;
                            HeaderInstance.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Top;
                            HeaderInstance.YUnits = Gum.Converters.GeneralUnitType.PixelsFromSmall;
                            key_Shot.Height = 128f;
                            key_Shot.ItemdescText = "Atirar";
                            SetProperty("key_Shot.ItemSprite_hud", "ArrowKeys.png");
                            key_Shot.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                            key_Shot.Width = 192f;
                            key_Shot.WidthUnits = Gum.DataTypes.DimensionUnitType.Absolute;
                            key_Shot.X = 0f;
                            key_Shot.XUnits = Gum.Converters.GeneralUnitType.Percentage;
                            key_Shot.Y = 0f;
                            ContainerInstance.Height = 329f;
                            ContainerInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            ContainerInstance.Width = 433f;
                            ContainerInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            ContainerInstance.X = 126f;
                            ContainerInstance.Y = 75f;
                            key_move.Height = 128f;
                            key_move.ItemdescText = "Mover\n";
                            SetProperty("key_move.ItemSprite_hud", "mOVEKeys.png");
                            key_move.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                            key_move.Width = 192f;
                            key_move.X = 0f;
                            key_move.Y = 150f;
                            key_Esc.Height = 64f;
                            key_Esc.ItemdescText = "Voltar para inicio\n";
                            SetProperty("key_Esc.ItemSprite_hud", "BlankKeys.png");
                            key_Esc.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                            key_Esc.Width = 64f;
                            key_Esc.X = 200f;
                            key_Esc.Y = 230f;
                            key_space.ItemdescText = "Desespero";
                            SetProperty("key_space.ItemSprite_hud", "Keys.png");
                            key_space.KeyText = "Space";
                            key_space.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                            key_space.Width = 192f;
                            key_space.X = 0f;
                            key_space.Y = 300f;
                            key_Pause.ItemdescText = "Pause";
                            SetProperty("key_Pause.ItemSprite_hud", "Keys.png");
                            key_Pause.KeyText = "P";
                            key_Pause.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                            key_Pause.Width = 64f;
                            key_Pause.X = 200f;
                            key_Pause.Y = 30f;
                            key_Ranking.ItemdescText = "Raking";
                            SetProperty("key_Ranking.ItemSprite_hud", "Keys.png");
                            key_Ranking.KeyText = "R";
                            key_Ranking.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                            key_Ranking.Width = 64f;
                            key_Ranking.X = 200f;
                            key_Ranking.Y = 130f;
                            key_Sangue.ItemdescText = "Sangue";
                            SetProperty("key_Sangue.ItemSprite_hud", "Keys.png");
                            key_Sangue.KeyText = "M";
                            key_Sangue.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                            key_Sangue.Width = 64f;
                            key_Sangue.X = 200f;
                            key_Sangue.Y = 330f;
                            key_Controle.ItemdescText = "Controle\\Ocultar Controle";
                            SetProperty("key_Controle.ItemSprite_hud", "Keys.png");
                            key_Controle.KeyText = "C";
                            key_Controle.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                            key_Controle.Width = 64f;
                            key_Controle.X = 0f;
                            key_Controle.Y = 390f;
                            break;
                    }
                }
            }
            #endregion
            #region State Interpolation
            public void InterpolateBetween (VariableState firstState, VariableState secondState, float interpolationValue) 
            {
                #if DEBUG
                if (float.IsNaN(interpolationValue))
                {
                    throw new System.Exception("interpolationValue cannot be NaN");
                }
                #endif
                bool setContainerInstanceHeightFirstValue = false;
                bool setContainerInstanceHeightSecondValue = false;
                float ContainerInstanceHeightFirstValue= 0;
                float ContainerInstanceHeightSecondValue= 0;
                bool setContainerInstanceWidthFirstValue = false;
                bool setContainerInstanceWidthSecondValue = false;
                float ContainerInstanceWidthFirstValue= 0;
                float ContainerInstanceWidthSecondValue= 0;
                bool setContainerInstanceXFirstValue = false;
                bool setContainerInstanceXSecondValue = false;
                float ContainerInstanceXFirstValue= 0;
                float ContainerInstanceXSecondValue= 0;
                bool setContainerInstanceYFirstValue = false;
                bool setContainerInstanceYSecondValue = false;
                float ContainerInstanceYFirstValue= 0;
                float ContainerInstanceYSecondValue= 0;
                bool setHeaderInstanceBlueFirstValue = false;
                bool setHeaderInstanceBlueSecondValue = false;
                int HeaderInstanceBlueFirstValue= 0;
                int HeaderInstanceBlueSecondValue= 0;
                bool setHeaderInstanceFontSizeFirstValue = false;
                bool setHeaderInstanceFontSizeSecondValue = false;
                int HeaderInstanceFontSizeFirstValue= 0;
                int HeaderInstanceFontSizeSecondValue= 0;
                bool setHeaderInstanceGreenFirstValue = false;
                bool setHeaderInstanceGreenSecondValue = false;
                int HeaderInstanceGreenFirstValue= 0;
                int HeaderInstanceGreenSecondValue= 0;
                bool setHeaderInstanceHeightFirstValue = false;
                bool setHeaderInstanceHeightSecondValue = false;
                float HeaderInstanceHeightFirstValue= 0;
                float HeaderInstanceHeightSecondValue= 0;
                bool setHeaderInstanceOutlineThicknessFirstValue = false;
                bool setHeaderInstanceOutlineThicknessSecondValue = false;
                int HeaderInstanceOutlineThicknessFirstValue= 0;
                int HeaderInstanceOutlineThicknessSecondValue= 0;
                bool setHeaderInstanceRedFirstValue = false;
                bool setHeaderInstanceRedSecondValue = false;
                int HeaderInstanceRedFirstValue= 0;
                int HeaderInstanceRedSecondValue= 0;
                bool setHeaderInstanceWidthFirstValue = false;
                bool setHeaderInstanceWidthSecondValue = false;
                float HeaderInstanceWidthFirstValue= 0;
                float HeaderInstanceWidthSecondValue= 0;
                bool setHeaderInstanceXFirstValue = false;
                bool setHeaderInstanceXSecondValue = false;
                float HeaderInstanceXFirstValue= 0;
                float HeaderInstanceXSecondValue= 0;
                bool setHeaderInstanceYFirstValue = false;
                bool setHeaderInstanceYSecondValue = false;
                float HeaderInstanceYFirstValue= 0;
                float HeaderInstanceYSecondValue= 0;
                bool setHeightFirstValue = false;
                bool setHeightSecondValue = false;
                float HeightFirstValue= 0;
                float HeightSecondValue= 0;
                bool setkey_ControleWidthFirstValue = false;
                bool setkey_ControleWidthSecondValue = false;
                float key_ControleWidthFirstValue= 0;
                float key_ControleWidthSecondValue= 0;
                bool setkey_ControleXFirstValue = false;
                bool setkey_ControleXSecondValue = false;
                float key_ControleXFirstValue= 0;
                float key_ControleXSecondValue= 0;
                bool setkey_ControleYFirstValue = false;
                bool setkey_ControleYSecondValue = false;
                float key_ControleYFirstValue= 0;
                float key_ControleYSecondValue= 0;
                bool setkey_EscHeightFirstValue = false;
                bool setkey_EscHeightSecondValue = false;
                float key_EscHeightFirstValue= 0;
                float key_EscHeightSecondValue= 0;
                bool setkey_EscWidthFirstValue = false;
                bool setkey_EscWidthSecondValue = false;
                float key_EscWidthFirstValue= 0;
                float key_EscWidthSecondValue= 0;
                bool setkey_EscXFirstValue = false;
                bool setkey_EscXSecondValue = false;
                float key_EscXFirstValue= 0;
                float key_EscXSecondValue= 0;
                bool setkey_EscYFirstValue = false;
                bool setkey_EscYSecondValue = false;
                float key_EscYFirstValue= 0;
                float key_EscYSecondValue= 0;
                bool setkey_moveHeightFirstValue = false;
                bool setkey_moveHeightSecondValue = false;
                float key_moveHeightFirstValue= 0;
                float key_moveHeightSecondValue= 0;
                bool setkey_moveWidthFirstValue = false;
                bool setkey_moveWidthSecondValue = false;
                float key_moveWidthFirstValue= 0;
                float key_moveWidthSecondValue= 0;
                bool setkey_moveXFirstValue = false;
                bool setkey_moveXSecondValue = false;
                float key_moveXFirstValue= 0;
                float key_moveXSecondValue= 0;
                bool setkey_moveYFirstValue = false;
                bool setkey_moveYSecondValue = false;
                float key_moveYFirstValue= 0;
                float key_moveYSecondValue= 0;
                bool setkey_PauseWidthFirstValue = false;
                bool setkey_PauseWidthSecondValue = false;
                float key_PauseWidthFirstValue= 0;
                float key_PauseWidthSecondValue= 0;
                bool setkey_PauseXFirstValue = false;
                bool setkey_PauseXSecondValue = false;
                float key_PauseXFirstValue= 0;
                float key_PauseXSecondValue= 0;
                bool setkey_PauseYFirstValue = false;
                bool setkey_PauseYSecondValue = false;
                float key_PauseYFirstValue= 0;
                float key_PauseYSecondValue= 0;
                bool setkey_RankingWidthFirstValue = false;
                bool setkey_RankingWidthSecondValue = false;
                float key_RankingWidthFirstValue= 0;
                float key_RankingWidthSecondValue= 0;
                bool setkey_RankingXFirstValue = false;
                bool setkey_RankingXSecondValue = false;
                float key_RankingXFirstValue= 0;
                float key_RankingXSecondValue= 0;
                bool setkey_RankingYFirstValue = false;
                bool setkey_RankingYSecondValue = false;
                float key_RankingYFirstValue= 0;
                float key_RankingYSecondValue= 0;
                bool setkey_SangueWidthFirstValue = false;
                bool setkey_SangueWidthSecondValue = false;
                float key_SangueWidthFirstValue= 0;
                float key_SangueWidthSecondValue= 0;
                bool setkey_SangueXFirstValue = false;
                bool setkey_SangueXSecondValue = false;
                float key_SangueXFirstValue= 0;
                float key_SangueXSecondValue= 0;
                bool setkey_SangueYFirstValue = false;
                bool setkey_SangueYSecondValue = false;
                float key_SangueYFirstValue= 0;
                float key_SangueYSecondValue= 0;
                bool setkey_ShotHeightFirstValue = false;
                bool setkey_ShotHeightSecondValue = false;
                float key_ShotHeightFirstValue= 0;
                float key_ShotHeightSecondValue= 0;
                bool setkey_ShotWidthFirstValue = false;
                bool setkey_ShotWidthSecondValue = false;
                float key_ShotWidthFirstValue= 0;
                float key_ShotWidthSecondValue= 0;
                bool setkey_ShotXFirstValue = false;
                bool setkey_ShotXSecondValue = false;
                float key_ShotXFirstValue= 0;
                float key_ShotXSecondValue= 0;
                bool setkey_ShotYFirstValue = false;
                bool setkey_ShotYSecondValue = false;
                float key_ShotYFirstValue= 0;
                float key_ShotYSecondValue= 0;
                bool setkey_spaceWidthFirstValue = false;
                bool setkey_spaceWidthSecondValue = false;
                float key_spaceWidthFirstValue= 0;
                float key_spaceWidthSecondValue= 0;
                bool setkey_spaceXFirstValue = false;
                bool setkey_spaceXSecondValue = false;
                float key_spaceXFirstValue= 0;
                float key_spaceXSecondValue= 0;
                bool setkey_spaceYFirstValue = false;
                bool setkey_spaceYSecondValue = false;
                float key_spaceYFirstValue= 0;
                float key_spaceYSecondValue= 0;
                bool setSpriteInstanceHeightFirstValue = false;
                bool setSpriteInstanceHeightSecondValue = false;
                float SpriteInstanceHeightFirstValue= 0;
                float SpriteInstanceHeightSecondValue= 0;
                bool setSpriteInstanceWidthFirstValue = false;
                bool setSpriteInstanceWidthSecondValue = false;
                float SpriteInstanceWidthFirstValue= 0;
                float SpriteInstanceWidthSecondValue= 0;
                bool setSpriteInstanceXFirstValue = false;
                bool setSpriteInstanceXSecondValue = false;
                float SpriteInstanceXFirstValue= 0;
                float SpriteInstanceXSecondValue= 0;
                bool setSpriteInstanceYFirstValue = false;
                bool setSpriteInstanceYSecondValue = false;
                float SpriteInstanceYFirstValue= 0;
                float SpriteInstanceYSecondValue= 0;
                bool setWidthFirstValue = false;
                bool setWidthSecondValue = false;
                float WidthFirstValue= 0;
                float WidthSecondValue= 0;
                switch(firstState)
                {
                    case  VariableState.Default:
                        setContainerInstanceHeightFirstValue = true;
                        ContainerInstanceHeightFirstValue = 329f;
                        if (interpolationValue < 1)
                        {
                            this.ContainerInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setContainerInstanceWidthFirstValue = true;
                        ContainerInstanceWidthFirstValue = 433f;
                        if (interpolationValue < 1)
                        {
                            this.ContainerInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setContainerInstanceXFirstValue = true;
                        ContainerInstanceXFirstValue = 126f;
                        setContainerInstanceYFirstValue = true;
                        ContainerInstanceYFirstValue = 75f;
                        setHeaderInstanceBlueFirstValue = true;
                        HeaderInstanceBlueFirstValue = 245;
                        if (interpolationValue < 1)
                        {
                            this.HeaderInstance.Font = "Impact";
                        }
                        setHeaderInstanceFontSizeFirstValue = true;
                        HeaderInstanceFontSizeFirstValue = 30;
                        setHeaderInstanceGreenFirstValue = true;
                        HeaderInstanceGreenFirstValue = 240;
                        setHeaderInstanceHeightFirstValue = true;
                        HeaderInstanceHeightFirstValue = -85f;
                        if (interpolationValue < 1)
                        {
                            this.HeaderInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue < 1)
                        {
                            this.HeaderInstance.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        setHeaderInstanceOutlineThicknessFirstValue = true;
                        HeaderInstanceOutlineThicknessFirstValue = 2;
                        setHeaderInstanceRedFirstValue = true;
                        HeaderInstanceRedFirstValue = 255;
                        if (interpolationValue < 1)
                        {
                            this.HeaderInstance.Text = "CONTROLES\n";
                        }
                        if (interpolationValue < 1)
                        {
                            this.HeaderInstance.VerticalAlignment = RenderingLibrary.Graphics.VerticalAlignment.Top;
                        }
                        setHeaderInstanceWidthFirstValue = true;
                        HeaderInstanceWidthFirstValue = -321f;
                        if (interpolationValue < 1)
                        {
                            this.HeaderInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setHeaderInstanceXFirstValue = true;
                        HeaderInstanceXFirstValue = 339f;
                        if (interpolationValue < 1)
                        {
                            this.HeaderInstance.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        if (interpolationValue < 1)
                        {
                            this.HeaderInstance.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        setHeaderInstanceYFirstValue = true;
                        HeaderInstanceYFirstValue = 34f;
                        if (interpolationValue < 1)
                        {
                            this.HeaderInstance.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Top;
                        }
                        if (interpolationValue < 1)
                        {
                            this.HeaderInstance.YUnits = Gum.Converters.GeneralUnitType.PixelsFromSmall;
                        }
                        setHeightFirstValue = true;
                        HeightFirstValue = 138f;
                        if (interpolationValue < 1)
                        {
                            this.key_Controle.ItemdescText = "Controle\\Ocultar Controle";
                        }
                        if (interpolationValue < 1)
                        {
                            SetProperty("key_Controle.ItemSprite_hud", "Keys.png");
                        }
                        if (interpolationValue < 1)
                        {
                            this.key_Controle.KeyText = "C";
                        }
                        if (interpolationValue < 1)
                        {
                            this.key_Controle.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        setkey_ControleWidthFirstValue = true;
                        key_ControleWidthFirstValue = 64f;
                        setkey_ControleXFirstValue = true;
                        key_ControleXFirstValue = 0f;
                        setkey_ControleYFirstValue = true;
                        key_ControleYFirstValue = 390f;
                        setkey_EscHeightFirstValue = true;
                        key_EscHeightFirstValue = 64f;
                        if (interpolationValue < 1)
                        {
                            this.key_Esc.ItemdescText = "Voltar para inicio\n";
                        }
                        if (interpolationValue < 1)
                        {
                            SetProperty("key_Esc.ItemSprite_hud", "BlankKeys.png");
                        }
                        if (interpolationValue < 1)
                        {
                            this.key_Esc.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        setkey_EscWidthFirstValue = true;
                        key_EscWidthFirstValue = 64f;
                        setkey_EscXFirstValue = true;
                        key_EscXFirstValue = 200f;
                        setkey_EscYFirstValue = true;
                        key_EscYFirstValue = 230f;
                        setkey_moveHeightFirstValue = true;
                        key_moveHeightFirstValue = 128f;
                        if (interpolationValue < 1)
                        {
                            this.key_move.ItemdescText = "Mover\n";
                        }
                        if (interpolationValue < 1)
                        {
                            SetProperty("key_move.ItemSprite_hud", "mOVEKeys.png");
                        }
                        if (interpolationValue < 1)
                        {
                            this.key_move.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        setkey_moveWidthFirstValue = true;
                        key_moveWidthFirstValue = 192f;
                        setkey_moveXFirstValue = true;
                        key_moveXFirstValue = 0f;
                        setkey_moveYFirstValue = true;
                        key_moveYFirstValue = 150f;
                        if (interpolationValue < 1)
                        {
                            this.key_Pause.ItemdescText = "Pause";
                        }
                        if (interpolationValue < 1)
                        {
                            SetProperty("key_Pause.ItemSprite_hud", "Keys.png");
                        }
                        if (interpolationValue < 1)
                        {
                            this.key_Pause.KeyText = "P";
                        }
                        if (interpolationValue < 1)
                        {
                            this.key_Pause.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        setkey_PauseWidthFirstValue = true;
                        key_PauseWidthFirstValue = 64f;
                        setkey_PauseXFirstValue = true;
                        key_PauseXFirstValue = 200f;
                        setkey_PauseYFirstValue = true;
                        key_PauseYFirstValue = 30f;
                        if (interpolationValue < 1)
                        {
                            this.key_Ranking.ItemdescText = "Raking";
                        }
                        if (interpolationValue < 1)
                        {
                            SetProperty("key_Ranking.ItemSprite_hud", "Keys.png");
                        }
                        if (interpolationValue < 1)
                        {
                            this.key_Ranking.KeyText = "R";
                        }
                        if (interpolationValue < 1)
                        {
                            this.key_Ranking.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        setkey_RankingWidthFirstValue = true;
                        key_RankingWidthFirstValue = 64f;
                        setkey_RankingXFirstValue = true;
                        key_RankingXFirstValue = 200f;
                        setkey_RankingYFirstValue = true;
                        key_RankingYFirstValue = 130f;
                        if (interpolationValue < 1)
                        {
                            this.key_Sangue.ItemdescText = "Sangue";
                        }
                        if (interpolationValue < 1)
                        {
                            SetProperty("key_Sangue.ItemSprite_hud", "Keys.png");
                        }
                        if (interpolationValue < 1)
                        {
                            this.key_Sangue.KeyText = "M";
                        }
                        if (interpolationValue < 1)
                        {
                            this.key_Sangue.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        setkey_SangueWidthFirstValue = true;
                        key_SangueWidthFirstValue = 64f;
                        setkey_SangueXFirstValue = true;
                        key_SangueXFirstValue = 200f;
                        setkey_SangueYFirstValue = true;
                        key_SangueYFirstValue = 330f;
                        setkey_ShotHeightFirstValue = true;
                        key_ShotHeightFirstValue = 128f;
                        if (interpolationValue < 1)
                        {
                            this.key_Shot.ItemdescText = "Atirar";
                        }
                        if (interpolationValue < 1)
                        {
                            SetProperty("key_Shot.ItemSprite_hud", "ArrowKeys.png");
                        }
                        if (interpolationValue < 1)
                        {
                            this.key_Shot.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        setkey_ShotWidthFirstValue = true;
                        key_ShotWidthFirstValue = 192f;
                        if (interpolationValue < 1)
                        {
                            this.key_Shot.WidthUnits = Gum.DataTypes.DimensionUnitType.Absolute;
                        }
                        setkey_ShotXFirstValue = true;
                        key_ShotXFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.key_Shot.XUnits = Gum.Converters.GeneralUnitType.Percentage;
                        }
                        setkey_ShotYFirstValue = true;
                        key_ShotYFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.key_space.ItemdescText = "Desespero";
                        }
                        if (interpolationValue < 1)
                        {
                            SetProperty("key_space.ItemSprite_hud", "Keys.png");
                        }
                        if (interpolationValue < 1)
                        {
                            this.key_space.KeyText = "Space";
                        }
                        if (interpolationValue < 1)
                        {
                            this.key_space.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        setkey_spaceWidthFirstValue = true;
                        key_spaceWidthFirstValue = 192f;
                        setkey_spaceXFirstValue = true;
                        key_spaceXFirstValue = 0f;
                        setkey_spaceYFirstValue = true;
                        key_spaceYFirstValue = 300f;
                        setSpriteInstanceHeightFirstValue = true;
                        SpriteInstanceHeightFirstValue = 415f;
                        if (interpolationValue < 1)
                        {
                            this.SpriteInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue < 1)
                        {
                            SetProperty("SpriteInstance.SourceFile", "panelInset_brown.png");
                        }
                        setSpriteInstanceWidthFirstValue = true;
                        SpriteInstanceWidthFirstValue = 507f;
                        if (interpolationValue < 1)
                        {
                            this.SpriteInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setSpriteInstanceXFirstValue = true;
                        SpriteInstanceXFirstValue = 74f;
                        setSpriteInstanceYFirstValue = true;
                        SpriteInstanceYFirstValue = 17f;
                        setWidthFirstValue = true;
                        WidthFirstValue = 160f;
                        break;
                }
                switch(secondState)
                {
                    case  VariableState.Default:
                        setContainerInstanceHeightSecondValue = true;
                        ContainerInstanceHeightSecondValue = 329f;
                        if (interpolationValue >= 1)
                        {
                            this.ContainerInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setContainerInstanceWidthSecondValue = true;
                        ContainerInstanceWidthSecondValue = 433f;
                        if (interpolationValue >= 1)
                        {
                            this.ContainerInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setContainerInstanceXSecondValue = true;
                        ContainerInstanceXSecondValue = 126f;
                        setContainerInstanceYSecondValue = true;
                        ContainerInstanceYSecondValue = 75f;
                        setHeaderInstanceBlueSecondValue = true;
                        HeaderInstanceBlueSecondValue = 245;
                        if (interpolationValue >= 1)
                        {
                            this.HeaderInstance.Font = "Impact";
                        }
                        setHeaderInstanceFontSizeSecondValue = true;
                        HeaderInstanceFontSizeSecondValue = 30;
                        setHeaderInstanceGreenSecondValue = true;
                        HeaderInstanceGreenSecondValue = 240;
                        setHeaderInstanceHeightSecondValue = true;
                        HeaderInstanceHeightSecondValue = -85f;
                        if (interpolationValue >= 1)
                        {
                            this.HeaderInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.HeaderInstance.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        setHeaderInstanceOutlineThicknessSecondValue = true;
                        HeaderInstanceOutlineThicknessSecondValue = 2;
                        setHeaderInstanceRedSecondValue = true;
                        HeaderInstanceRedSecondValue = 255;
                        if (interpolationValue >= 1)
                        {
                            this.HeaderInstance.Text = "CONTROLES\n";
                        }
                        if (interpolationValue >= 1)
                        {
                            this.HeaderInstance.VerticalAlignment = RenderingLibrary.Graphics.VerticalAlignment.Top;
                        }
                        setHeaderInstanceWidthSecondValue = true;
                        HeaderInstanceWidthSecondValue = -321f;
                        if (interpolationValue >= 1)
                        {
                            this.HeaderInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setHeaderInstanceXSecondValue = true;
                        HeaderInstanceXSecondValue = 339f;
                        if (interpolationValue >= 1)
                        {
                            this.HeaderInstance.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.HeaderInstance.XUnits = Gum.Converters.GeneralUnitType.PixelsFromMiddle;
                        }
                        setHeaderInstanceYSecondValue = true;
                        HeaderInstanceYSecondValue = 34f;
                        if (interpolationValue >= 1)
                        {
                            this.HeaderInstance.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Top;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.HeaderInstance.YUnits = Gum.Converters.GeneralUnitType.PixelsFromSmall;
                        }
                        setHeightSecondValue = true;
                        HeightSecondValue = 138f;
                        if (interpolationValue >= 1)
                        {
                            this.key_Controle.ItemdescText = "Controle\\Ocultar Controle";
                        }
                        if (interpolationValue >= 1)
                        {
                            SetProperty("key_Controle.ItemSprite_hud", "Keys.png");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.key_Controle.KeyText = "C";
                        }
                        if (interpolationValue >= 1)
                        {
                            this.key_Controle.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        setkey_ControleWidthSecondValue = true;
                        key_ControleWidthSecondValue = 64f;
                        setkey_ControleXSecondValue = true;
                        key_ControleXSecondValue = 0f;
                        setkey_ControleYSecondValue = true;
                        key_ControleYSecondValue = 390f;
                        setkey_EscHeightSecondValue = true;
                        key_EscHeightSecondValue = 64f;
                        if (interpolationValue >= 1)
                        {
                            this.key_Esc.ItemdescText = "Voltar para inicio\n";
                        }
                        if (interpolationValue >= 1)
                        {
                            SetProperty("key_Esc.ItemSprite_hud", "BlankKeys.png");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.key_Esc.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        setkey_EscWidthSecondValue = true;
                        key_EscWidthSecondValue = 64f;
                        setkey_EscXSecondValue = true;
                        key_EscXSecondValue = 200f;
                        setkey_EscYSecondValue = true;
                        key_EscYSecondValue = 230f;
                        setkey_moveHeightSecondValue = true;
                        key_moveHeightSecondValue = 128f;
                        if (interpolationValue >= 1)
                        {
                            this.key_move.ItemdescText = "Mover\n";
                        }
                        if (interpolationValue >= 1)
                        {
                            SetProperty("key_move.ItemSprite_hud", "mOVEKeys.png");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.key_move.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        setkey_moveWidthSecondValue = true;
                        key_moveWidthSecondValue = 192f;
                        setkey_moveXSecondValue = true;
                        key_moveXSecondValue = 0f;
                        setkey_moveYSecondValue = true;
                        key_moveYSecondValue = 150f;
                        if (interpolationValue >= 1)
                        {
                            this.key_Pause.ItemdescText = "Pause";
                        }
                        if (interpolationValue >= 1)
                        {
                            SetProperty("key_Pause.ItemSprite_hud", "Keys.png");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.key_Pause.KeyText = "P";
                        }
                        if (interpolationValue >= 1)
                        {
                            this.key_Pause.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        setkey_PauseWidthSecondValue = true;
                        key_PauseWidthSecondValue = 64f;
                        setkey_PauseXSecondValue = true;
                        key_PauseXSecondValue = 200f;
                        setkey_PauseYSecondValue = true;
                        key_PauseYSecondValue = 30f;
                        if (interpolationValue >= 1)
                        {
                            this.key_Ranking.ItemdescText = "Raking";
                        }
                        if (interpolationValue >= 1)
                        {
                            SetProperty("key_Ranking.ItemSprite_hud", "Keys.png");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.key_Ranking.KeyText = "R";
                        }
                        if (interpolationValue >= 1)
                        {
                            this.key_Ranking.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        setkey_RankingWidthSecondValue = true;
                        key_RankingWidthSecondValue = 64f;
                        setkey_RankingXSecondValue = true;
                        key_RankingXSecondValue = 200f;
                        setkey_RankingYSecondValue = true;
                        key_RankingYSecondValue = 130f;
                        if (interpolationValue >= 1)
                        {
                            this.key_Sangue.ItemdescText = "Sangue";
                        }
                        if (interpolationValue >= 1)
                        {
                            SetProperty("key_Sangue.ItemSprite_hud", "Keys.png");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.key_Sangue.KeyText = "M";
                        }
                        if (interpolationValue >= 1)
                        {
                            this.key_Sangue.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        setkey_SangueWidthSecondValue = true;
                        key_SangueWidthSecondValue = 64f;
                        setkey_SangueXSecondValue = true;
                        key_SangueXSecondValue = 200f;
                        setkey_SangueYSecondValue = true;
                        key_SangueYSecondValue = 330f;
                        setkey_ShotHeightSecondValue = true;
                        key_ShotHeightSecondValue = 128f;
                        if (interpolationValue >= 1)
                        {
                            this.key_Shot.ItemdescText = "Atirar";
                        }
                        if (interpolationValue >= 1)
                        {
                            SetProperty("key_Shot.ItemSprite_hud", "ArrowKeys.png");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.key_Shot.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        setkey_ShotWidthSecondValue = true;
                        key_ShotWidthSecondValue = 192f;
                        if (interpolationValue >= 1)
                        {
                            this.key_Shot.WidthUnits = Gum.DataTypes.DimensionUnitType.Absolute;
                        }
                        setkey_ShotXSecondValue = true;
                        key_ShotXSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.key_Shot.XUnits = Gum.Converters.GeneralUnitType.Percentage;
                        }
                        setkey_ShotYSecondValue = true;
                        key_ShotYSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.key_space.ItemdescText = "Desespero";
                        }
                        if (interpolationValue >= 1)
                        {
                            SetProperty("key_space.ItemSprite_hud", "Keys.png");
                        }
                        if (interpolationValue >= 1)
                        {
                            this.key_space.KeyText = "Space";
                        }
                        if (interpolationValue >= 1)
                        {
                            this.key_space.Parent = this.ContainedElements.FirstOrDefault(item =>item.Name == "ContainerInstance");
                        }
                        setkey_spaceWidthSecondValue = true;
                        key_spaceWidthSecondValue = 192f;
                        setkey_spaceXSecondValue = true;
                        key_spaceXSecondValue = 0f;
                        setkey_spaceYSecondValue = true;
                        key_spaceYSecondValue = 300f;
                        setSpriteInstanceHeightSecondValue = true;
                        SpriteInstanceHeightSecondValue = 415f;
                        if (interpolationValue >= 1)
                        {
                            this.SpriteInstance.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue >= 1)
                        {
                            SetProperty("SpriteInstance.SourceFile", "panelInset_brown.png");
                        }
                        setSpriteInstanceWidthSecondValue = true;
                        SpriteInstanceWidthSecondValue = 507f;
                        if (interpolationValue >= 1)
                        {
                            this.SpriteInstance.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        setSpriteInstanceXSecondValue = true;
                        SpriteInstanceXSecondValue = 74f;
                        setSpriteInstanceYSecondValue = true;
                        SpriteInstanceYSecondValue = 17f;
                        setWidthSecondValue = true;
                        WidthSecondValue = 160f;
                        break;
                }
                if (setContainerInstanceHeightFirstValue && setContainerInstanceHeightSecondValue)
                {
                    ContainerInstance.Height = ContainerInstanceHeightFirstValue * (1 - interpolationValue) + ContainerInstanceHeightSecondValue * interpolationValue;
                }
                if (setContainerInstanceWidthFirstValue && setContainerInstanceWidthSecondValue)
                {
                    ContainerInstance.Width = ContainerInstanceWidthFirstValue * (1 - interpolationValue) + ContainerInstanceWidthSecondValue * interpolationValue;
                }
                if (setContainerInstanceXFirstValue && setContainerInstanceXSecondValue)
                {
                    ContainerInstance.X = ContainerInstanceXFirstValue * (1 - interpolationValue) + ContainerInstanceXSecondValue * interpolationValue;
                }
                if (setContainerInstanceYFirstValue && setContainerInstanceYSecondValue)
                {
                    ContainerInstance.Y = ContainerInstanceYFirstValue * (1 - interpolationValue) + ContainerInstanceYSecondValue * interpolationValue;
                }
                if (setHeaderInstanceBlueFirstValue && setHeaderInstanceBlueSecondValue)
                {
                    HeaderInstance.Blue = FlatRedBall.Math.MathFunctions.RoundToInt(HeaderInstanceBlueFirstValue* (1 - interpolationValue) + HeaderInstanceBlueSecondValue * interpolationValue);
                }
                if (setHeaderInstanceFontSizeFirstValue && setHeaderInstanceFontSizeSecondValue)
                {
                    HeaderInstance.FontSize = FlatRedBall.Math.MathFunctions.RoundToInt(HeaderInstanceFontSizeFirstValue* (1 - interpolationValue) + HeaderInstanceFontSizeSecondValue * interpolationValue);
                }
                if (setHeaderInstanceGreenFirstValue && setHeaderInstanceGreenSecondValue)
                {
                    HeaderInstance.Green = FlatRedBall.Math.MathFunctions.RoundToInt(HeaderInstanceGreenFirstValue* (1 - interpolationValue) + HeaderInstanceGreenSecondValue * interpolationValue);
                }
                if (setHeaderInstanceHeightFirstValue && setHeaderInstanceHeightSecondValue)
                {
                    HeaderInstance.Height = HeaderInstanceHeightFirstValue * (1 - interpolationValue) + HeaderInstanceHeightSecondValue * interpolationValue;
                }
                if (setHeaderInstanceOutlineThicknessFirstValue && setHeaderInstanceOutlineThicknessSecondValue)
                {
                    HeaderInstance.OutlineThickness = FlatRedBall.Math.MathFunctions.RoundToInt(HeaderInstanceOutlineThicknessFirstValue* (1 - interpolationValue) + HeaderInstanceOutlineThicknessSecondValue * interpolationValue);
                }
                if (setHeaderInstanceRedFirstValue && setHeaderInstanceRedSecondValue)
                {
                    HeaderInstance.Red = FlatRedBall.Math.MathFunctions.RoundToInt(HeaderInstanceRedFirstValue* (1 - interpolationValue) + HeaderInstanceRedSecondValue * interpolationValue);
                }
                if (setHeaderInstanceWidthFirstValue && setHeaderInstanceWidthSecondValue)
                {
                    HeaderInstance.Width = HeaderInstanceWidthFirstValue * (1 - interpolationValue) + HeaderInstanceWidthSecondValue * interpolationValue;
                }
                if (setHeaderInstanceXFirstValue && setHeaderInstanceXSecondValue)
                {
                    HeaderInstance.X = HeaderInstanceXFirstValue * (1 - interpolationValue) + HeaderInstanceXSecondValue * interpolationValue;
                }
                if (setHeaderInstanceYFirstValue && setHeaderInstanceYSecondValue)
                {
                    HeaderInstance.Y = HeaderInstanceYFirstValue * (1 - interpolationValue) + HeaderInstanceYSecondValue * interpolationValue;
                }
                if (setHeightFirstValue && setHeightSecondValue)
                {
                    Height = HeightFirstValue * (1 - interpolationValue) + HeightSecondValue * interpolationValue;
                }
                if (setkey_ControleWidthFirstValue && setkey_ControleWidthSecondValue)
                {
                    key_Controle.Width = key_ControleWidthFirstValue * (1 - interpolationValue) + key_ControleWidthSecondValue * interpolationValue;
                }
                if (setkey_ControleXFirstValue && setkey_ControleXSecondValue)
                {
                    key_Controle.X = key_ControleXFirstValue * (1 - interpolationValue) + key_ControleXSecondValue * interpolationValue;
                }
                if (setkey_ControleYFirstValue && setkey_ControleYSecondValue)
                {
                    key_Controle.Y = key_ControleYFirstValue * (1 - interpolationValue) + key_ControleYSecondValue * interpolationValue;
                }
                if (setkey_EscHeightFirstValue && setkey_EscHeightSecondValue)
                {
                    key_Esc.Height = key_EscHeightFirstValue * (1 - interpolationValue) + key_EscHeightSecondValue * interpolationValue;
                }
                if (setkey_EscWidthFirstValue && setkey_EscWidthSecondValue)
                {
                    key_Esc.Width = key_EscWidthFirstValue * (1 - interpolationValue) + key_EscWidthSecondValue * interpolationValue;
                }
                if (setkey_EscXFirstValue && setkey_EscXSecondValue)
                {
                    key_Esc.X = key_EscXFirstValue * (1 - interpolationValue) + key_EscXSecondValue * interpolationValue;
                }
                if (setkey_EscYFirstValue && setkey_EscYSecondValue)
                {
                    key_Esc.Y = key_EscYFirstValue * (1 - interpolationValue) + key_EscYSecondValue * interpolationValue;
                }
                if (setkey_moveHeightFirstValue && setkey_moveHeightSecondValue)
                {
                    key_move.Height = key_moveHeightFirstValue * (1 - interpolationValue) + key_moveHeightSecondValue * interpolationValue;
                }
                if (setkey_moveWidthFirstValue && setkey_moveWidthSecondValue)
                {
                    key_move.Width = key_moveWidthFirstValue * (1 - interpolationValue) + key_moveWidthSecondValue * interpolationValue;
                }
                if (setkey_moveXFirstValue && setkey_moveXSecondValue)
                {
                    key_move.X = key_moveXFirstValue * (1 - interpolationValue) + key_moveXSecondValue * interpolationValue;
                }
                if (setkey_moveYFirstValue && setkey_moveYSecondValue)
                {
                    key_move.Y = key_moveYFirstValue * (1 - interpolationValue) + key_moveYSecondValue * interpolationValue;
                }
                if (setkey_PauseWidthFirstValue && setkey_PauseWidthSecondValue)
                {
                    key_Pause.Width = key_PauseWidthFirstValue * (1 - interpolationValue) + key_PauseWidthSecondValue * interpolationValue;
                }
                if (setkey_PauseXFirstValue && setkey_PauseXSecondValue)
                {
                    key_Pause.X = key_PauseXFirstValue * (1 - interpolationValue) + key_PauseXSecondValue * interpolationValue;
                }
                if (setkey_PauseYFirstValue && setkey_PauseYSecondValue)
                {
                    key_Pause.Y = key_PauseYFirstValue * (1 - interpolationValue) + key_PauseYSecondValue * interpolationValue;
                }
                if (setkey_RankingWidthFirstValue && setkey_RankingWidthSecondValue)
                {
                    key_Ranking.Width = key_RankingWidthFirstValue * (1 - interpolationValue) + key_RankingWidthSecondValue * interpolationValue;
                }
                if (setkey_RankingXFirstValue && setkey_RankingXSecondValue)
                {
                    key_Ranking.X = key_RankingXFirstValue * (1 - interpolationValue) + key_RankingXSecondValue * interpolationValue;
                }
                if (setkey_RankingYFirstValue && setkey_RankingYSecondValue)
                {
                    key_Ranking.Y = key_RankingYFirstValue * (1 - interpolationValue) + key_RankingYSecondValue * interpolationValue;
                }
                if (setkey_SangueWidthFirstValue && setkey_SangueWidthSecondValue)
                {
                    key_Sangue.Width = key_SangueWidthFirstValue * (1 - interpolationValue) + key_SangueWidthSecondValue * interpolationValue;
                }
                if (setkey_SangueXFirstValue && setkey_SangueXSecondValue)
                {
                    key_Sangue.X = key_SangueXFirstValue * (1 - interpolationValue) + key_SangueXSecondValue * interpolationValue;
                }
                if (setkey_SangueYFirstValue && setkey_SangueYSecondValue)
                {
                    key_Sangue.Y = key_SangueYFirstValue * (1 - interpolationValue) + key_SangueYSecondValue * interpolationValue;
                }
                if (setkey_ShotHeightFirstValue && setkey_ShotHeightSecondValue)
                {
                    key_Shot.Height = key_ShotHeightFirstValue * (1 - interpolationValue) + key_ShotHeightSecondValue * interpolationValue;
                }
                if (setkey_ShotWidthFirstValue && setkey_ShotWidthSecondValue)
                {
                    key_Shot.Width = key_ShotWidthFirstValue * (1 - interpolationValue) + key_ShotWidthSecondValue * interpolationValue;
                }
                if (setkey_ShotXFirstValue && setkey_ShotXSecondValue)
                {
                    key_Shot.X = key_ShotXFirstValue * (1 - interpolationValue) + key_ShotXSecondValue * interpolationValue;
                }
                if (setkey_ShotYFirstValue && setkey_ShotYSecondValue)
                {
                    key_Shot.Y = key_ShotYFirstValue * (1 - interpolationValue) + key_ShotYSecondValue * interpolationValue;
                }
                if (setkey_spaceWidthFirstValue && setkey_spaceWidthSecondValue)
                {
                    key_space.Width = key_spaceWidthFirstValue * (1 - interpolationValue) + key_spaceWidthSecondValue * interpolationValue;
                }
                if (setkey_spaceXFirstValue && setkey_spaceXSecondValue)
                {
                    key_space.X = key_spaceXFirstValue * (1 - interpolationValue) + key_spaceXSecondValue * interpolationValue;
                }
                if (setkey_spaceYFirstValue && setkey_spaceYSecondValue)
                {
                    key_space.Y = key_spaceYFirstValue * (1 - interpolationValue) + key_spaceYSecondValue * interpolationValue;
                }
                if (setSpriteInstanceHeightFirstValue && setSpriteInstanceHeightSecondValue)
                {
                    SpriteInstance.Height = SpriteInstanceHeightFirstValue * (1 - interpolationValue) + SpriteInstanceHeightSecondValue * interpolationValue;
                }
                if (setSpriteInstanceWidthFirstValue && setSpriteInstanceWidthSecondValue)
                {
                    SpriteInstance.Width = SpriteInstanceWidthFirstValue * (1 - interpolationValue) + SpriteInstanceWidthSecondValue * interpolationValue;
                }
                if (setSpriteInstanceXFirstValue && setSpriteInstanceXSecondValue)
                {
                    SpriteInstance.X = SpriteInstanceXFirstValue * (1 - interpolationValue) + SpriteInstanceXSecondValue * interpolationValue;
                }
                if (setSpriteInstanceYFirstValue && setSpriteInstanceYSecondValue)
                {
                    SpriteInstance.Y = SpriteInstanceYFirstValue * (1 - interpolationValue) + SpriteInstanceYSecondValue * interpolationValue;
                }
                if (setWidthFirstValue && setWidthSecondValue)
                {
                    Width = WidthFirstValue * (1 - interpolationValue) + WidthSecondValue * interpolationValue;
                }
                if (interpolationValue < 1)
                {
                    mCurrentVariableState = firstState;
                }
                else
                {
                    mCurrentVariableState = secondState;
                }
            }
            #endregion
            #region State Interpolate To
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (DungeonRun.GumRuntimes.Panel2_DisplayRuntime.VariableState fromState,DungeonRun.GumRuntimes.Panel2_DisplayRuntime.VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null) 
            {
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from:0, to:1, duration:(float)secondsToTake, type:interpolationType, easing:easing );
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(fromState, toState, newPosition);
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = this.ElementSave.States.First(item => item.Name == toState.ToString());
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateToRelative (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = AddToCurrentValuesWithState(toState);
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            #endregion
            #region State Animations
            #endregion
            public override void StopAnimations () 
            {
                base.StopAnimations();
                key_Shot.StopAnimations();
                key_move.StopAnimations();
                key_Esc.StopAnimations();
                key_space.StopAnimations();
                key_Pause.StopAnimations();
                key_Ranking.StopAnimations();
                key_Sangue.StopAnimations();
                key_Controle.StopAnimations();
            }
            #region Get Current Values on State
            private Gum.DataTypes.Variables.StateSave GetCurrentValuesOnState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Height",
                            Type = "float",
                            Value = Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Width",
                            Type = "float",
                            Value = Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Height",
                            Type = "float",
                            Value = SpriteInstance.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Height Units",
                            Type = "DimensionUnitType",
                            Value = SpriteInstance.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.SourceFile",
                            Type = "string",
                            Value = SpriteInstance.SourceFile
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Width",
                            Type = "float",
                            Value = SpriteInstance.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Width Units",
                            Type = "DimensionUnitType",
                            Value = SpriteInstance.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.X",
                            Type = "float",
                            Value = SpriteInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Y",
                            Type = "float",
                            Value = SpriteInstance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Blue",
                            Type = "int",
                            Value = HeaderInstance.Blue
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Font",
                            Type = "string",
                            Value = HeaderInstance.Font
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.FontSize",
                            Type = "int",
                            Value = HeaderInstance.FontSize
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Green",
                            Type = "int",
                            Value = HeaderInstance.Green
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Height",
                            Type = "float",
                            Value = HeaderInstance.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Height Units",
                            Type = "DimensionUnitType",
                            Value = HeaderInstance.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = HeaderInstance.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.OutlineThickness",
                            Type = "int",
                            Value = HeaderInstance.OutlineThickness
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Red",
                            Type = "int",
                            Value = HeaderInstance.Red
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Text",
                            Type = "string",
                            Value = HeaderInstance.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.VerticalAlignment",
                            Type = "VerticalAlignment",
                            Value = HeaderInstance.VerticalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Width",
                            Type = "float",
                            Value = HeaderInstance.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Width Units",
                            Type = "DimensionUnitType",
                            Value = HeaderInstance.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.X",
                            Type = "float",
                            Value = HeaderInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.X Origin",
                            Type = "HorizontalAlignment",
                            Value = HeaderInstance.XOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.X Units",
                            Type = "PositionUnitType",
                            Value = HeaderInstance.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Y",
                            Type = "float",
                            Value = HeaderInstance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Y Origin",
                            Type = "VerticalAlignment",
                            Value = HeaderInstance.YOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Y Units",
                            Type = "PositionUnitType",
                            Value = HeaderInstance.YUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Shot.Height",
                            Type = "float",
                            Value = key_Shot.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Shot.ItemdescText",
                            Type = "string",
                            Value = key_Shot.ItemdescText
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Shot.ItemSprite_hud",
                            Type = "string",
                            Value = key_Shot.ItemSprite_hud
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Shot.Parent",
                            Type = "string",
                            Value = key_Shot.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Shot.Width",
                            Type = "float",
                            Value = key_Shot.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Shot.Width Units",
                            Type = "DimensionUnitType",
                            Value = key_Shot.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Shot.X",
                            Type = "float",
                            Value = key_Shot.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Shot.X Units",
                            Type = "PositionUnitType",
                            Value = key_Shot.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Shot.Y",
                            Type = "float",
                            Value = key_Shot.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Height",
                            Type = "float",
                            Value = ContainerInstance.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Height Units",
                            Type = "DimensionUnitType",
                            Value = ContainerInstance.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Width",
                            Type = "float",
                            Value = ContainerInstance.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Width Units",
                            Type = "DimensionUnitType",
                            Value = ContainerInstance.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.X",
                            Type = "float",
                            Value = ContainerInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Y",
                            Type = "float",
                            Value = ContainerInstance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_move.Height",
                            Type = "float",
                            Value = key_move.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_move.ItemdescText",
                            Type = "string",
                            Value = key_move.ItemdescText
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_move.ItemSprite_hud",
                            Type = "string",
                            Value = key_move.ItemSprite_hud
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_move.Parent",
                            Type = "string",
                            Value = key_move.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_move.Width",
                            Type = "float",
                            Value = key_move.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_move.X",
                            Type = "float",
                            Value = key_move.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_move.Y",
                            Type = "float",
                            Value = key_move.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Esc.Height",
                            Type = "float",
                            Value = key_Esc.Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Esc.ItemdescText",
                            Type = "string",
                            Value = key_Esc.ItemdescText
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Esc.ItemSprite_hud",
                            Type = "string",
                            Value = key_Esc.ItemSprite_hud
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Esc.Parent",
                            Type = "string",
                            Value = key_Esc.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Esc.Width",
                            Type = "float",
                            Value = key_Esc.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Esc.X",
                            Type = "float",
                            Value = key_Esc.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Esc.Y",
                            Type = "float",
                            Value = key_Esc.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_space.ItemdescText",
                            Type = "string",
                            Value = key_space.ItemdescText
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_space.ItemSprite_hud",
                            Type = "string",
                            Value = key_space.ItemSprite_hud
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_space.KeyText",
                            Type = "string",
                            Value = key_space.KeyText
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_space.Parent",
                            Type = "string",
                            Value = key_space.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_space.Width",
                            Type = "float",
                            Value = key_space.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_space.X",
                            Type = "float",
                            Value = key_space.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_space.Y",
                            Type = "float",
                            Value = key_space.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Pause.ItemdescText",
                            Type = "string",
                            Value = key_Pause.ItemdescText
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Pause.ItemSprite_hud",
                            Type = "string",
                            Value = key_Pause.ItemSprite_hud
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Pause.KeyText",
                            Type = "string",
                            Value = key_Pause.KeyText
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Pause.Parent",
                            Type = "string",
                            Value = key_Pause.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Pause.Width",
                            Type = "float",
                            Value = key_Pause.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Pause.X",
                            Type = "float",
                            Value = key_Pause.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Pause.Y",
                            Type = "float",
                            Value = key_Pause.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Ranking.ItemdescText",
                            Type = "string",
                            Value = key_Ranking.ItemdescText
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Ranking.ItemSprite_hud",
                            Type = "string",
                            Value = key_Ranking.ItemSprite_hud
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Ranking.KeyText",
                            Type = "string",
                            Value = key_Ranking.KeyText
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Ranking.Parent",
                            Type = "string",
                            Value = key_Ranking.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Ranking.Width",
                            Type = "float",
                            Value = key_Ranking.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Ranking.X",
                            Type = "float",
                            Value = key_Ranking.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Ranking.Y",
                            Type = "float",
                            Value = key_Ranking.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Sangue.ItemdescText",
                            Type = "string",
                            Value = key_Sangue.ItemdescText
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Sangue.ItemSprite_hud",
                            Type = "string",
                            Value = key_Sangue.ItemSprite_hud
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Sangue.KeyText",
                            Type = "string",
                            Value = key_Sangue.KeyText
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Sangue.Parent",
                            Type = "string",
                            Value = key_Sangue.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Sangue.Width",
                            Type = "float",
                            Value = key_Sangue.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Sangue.X",
                            Type = "float",
                            Value = key_Sangue.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Sangue.Y",
                            Type = "float",
                            Value = key_Sangue.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Controle.ItemdescText",
                            Type = "string",
                            Value = key_Controle.ItemdescText
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Controle.ItemSprite_hud",
                            Type = "string",
                            Value = key_Controle.ItemSprite_hud
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Controle.KeyText",
                            Type = "string",
                            Value = key_Controle.KeyText
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Controle.Parent",
                            Type = "string",
                            Value = key_Controle.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Controle.Width",
                            Type = "float",
                            Value = key_Controle.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Controle.X",
                            Type = "float",
                            Value = key_Controle.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Controle.Y",
                            Type = "float",
                            Value = key_Controle.Y
                        }
                        );
                        break;
                }
                return newState;
            }
            private Gum.DataTypes.Variables.StateSave AddToCurrentValuesWithState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Height",
                            Type = "float",
                            Value = Height + 138f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Width",
                            Type = "float",
                            Value = Width + 160f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Height",
                            Type = "float",
                            Value = SpriteInstance.Height + 415f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Height Units",
                            Type = "DimensionUnitType",
                            Value = SpriteInstance.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.SourceFile",
                            Type = "string",
                            Value = SpriteInstance.SourceFile
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Width",
                            Type = "float",
                            Value = SpriteInstance.Width + 507f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Width Units",
                            Type = "DimensionUnitType",
                            Value = SpriteInstance.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.X",
                            Type = "float",
                            Value = SpriteInstance.X + 74f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "SpriteInstance.Y",
                            Type = "float",
                            Value = SpriteInstance.Y + 17f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Blue",
                            Type = "int",
                            Value = HeaderInstance.Blue + 245
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Font",
                            Type = "string",
                            Value = HeaderInstance.Font
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.FontSize",
                            Type = "int",
                            Value = HeaderInstance.FontSize + 30
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Green",
                            Type = "int",
                            Value = HeaderInstance.Green + 240
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Height",
                            Type = "float",
                            Value = HeaderInstance.Height + -85f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Height Units",
                            Type = "DimensionUnitType",
                            Value = HeaderInstance.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = HeaderInstance.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.OutlineThickness",
                            Type = "int",
                            Value = HeaderInstance.OutlineThickness + 2
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Red",
                            Type = "int",
                            Value = HeaderInstance.Red + 255
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Text",
                            Type = "string",
                            Value = HeaderInstance.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.VerticalAlignment",
                            Type = "VerticalAlignment",
                            Value = HeaderInstance.VerticalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Width",
                            Type = "float",
                            Value = HeaderInstance.Width + -321f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Width Units",
                            Type = "DimensionUnitType",
                            Value = HeaderInstance.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.X",
                            Type = "float",
                            Value = HeaderInstance.X + 339f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.X Origin",
                            Type = "HorizontalAlignment",
                            Value = HeaderInstance.XOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.X Units",
                            Type = "PositionUnitType",
                            Value = HeaderInstance.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Y",
                            Type = "float",
                            Value = HeaderInstance.Y + 34f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Y Origin",
                            Type = "VerticalAlignment",
                            Value = HeaderInstance.YOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "HeaderInstance.Y Units",
                            Type = "PositionUnitType",
                            Value = HeaderInstance.YUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Shot.Height",
                            Type = "float",
                            Value = key_Shot.Height + 128f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Shot.ItemdescText",
                            Type = "string",
                            Value = key_Shot.ItemdescText
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Shot.ItemSprite_hud",
                            Type = "string",
                            Value = key_Shot.ItemSprite_hud
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Shot.Parent",
                            Type = "string",
                            Value = key_Shot.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Shot.Width",
                            Type = "float",
                            Value = key_Shot.Width + 192f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Shot.Width Units",
                            Type = "DimensionUnitType",
                            Value = key_Shot.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Shot.X",
                            Type = "float",
                            Value = key_Shot.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Shot.X Units",
                            Type = "PositionUnitType",
                            Value = key_Shot.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Shot.Y",
                            Type = "float",
                            Value = key_Shot.Y + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Height",
                            Type = "float",
                            Value = ContainerInstance.Height + 329f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Height Units",
                            Type = "DimensionUnitType",
                            Value = ContainerInstance.HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Width",
                            Type = "float",
                            Value = ContainerInstance.Width + 433f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Width Units",
                            Type = "DimensionUnitType",
                            Value = ContainerInstance.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.X",
                            Type = "float",
                            Value = ContainerInstance.X + 126f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ContainerInstance.Y",
                            Type = "float",
                            Value = ContainerInstance.Y + 75f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_move.Height",
                            Type = "float",
                            Value = key_move.Height + 128f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_move.ItemdescText",
                            Type = "string",
                            Value = key_move.ItemdescText
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_move.ItemSprite_hud",
                            Type = "string",
                            Value = key_move.ItemSprite_hud
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_move.Parent",
                            Type = "string",
                            Value = key_move.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_move.Width",
                            Type = "float",
                            Value = key_move.Width + 192f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_move.X",
                            Type = "float",
                            Value = key_move.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_move.Y",
                            Type = "float",
                            Value = key_move.Y + 150f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Esc.Height",
                            Type = "float",
                            Value = key_Esc.Height + 64f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Esc.ItemdescText",
                            Type = "string",
                            Value = key_Esc.ItemdescText
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Esc.ItemSprite_hud",
                            Type = "string",
                            Value = key_Esc.ItemSprite_hud
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Esc.Parent",
                            Type = "string",
                            Value = key_Esc.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Esc.Width",
                            Type = "float",
                            Value = key_Esc.Width + 64f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Esc.X",
                            Type = "float",
                            Value = key_Esc.X + 200f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Esc.Y",
                            Type = "float",
                            Value = key_Esc.Y + 230f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_space.ItemdescText",
                            Type = "string",
                            Value = key_space.ItemdescText
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_space.ItemSprite_hud",
                            Type = "string",
                            Value = key_space.ItemSprite_hud
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_space.KeyText",
                            Type = "string",
                            Value = key_space.KeyText
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_space.Parent",
                            Type = "string",
                            Value = key_space.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_space.Width",
                            Type = "float",
                            Value = key_space.Width + 192f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_space.X",
                            Type = "float",
                            Value = key_space.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_space.Y",
                            Type = "float",
                            Value = key_space.Y + 300f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Pause.ItemdescText",
                            Type = "string",
                            Value = key_Pause.ItemdescText
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Pause.ItemSprite_hud",
                            Type = "string",
                            Value = key_Pause.ItemSprite_hud
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Pause.KeyText",
                            Type = "string",
                            Value = key_Pause.KeyText
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Pause.Parent",
                            Type = "string",
                            Value = key_Pause.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Pause.Width",
                            Type = "float",
                            Value = key_Pause.Width + 64f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Pause.X",
                            Type = "float",
                            Value = key_Pause.X + 200f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Pause.Y",
                            Type = "float",
                            Value = key_Pause.Y + 30f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Ranking.ItemdescText",
                            Type = "string",
                            Value = key_Ranking.ItemdescText
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Ranking.ItemSprite_hud",
                            Type = "string",
                            Value = key_Ranking.ItemSprite_hud
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Ranking.KeyText",
                            Type = "string",
                            Value = key_Ranking.KeyText
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Ranking.Parent",
                            Type = "string",
                            Value = key_Ranking.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Ranking.Width",
                            Type = "float",
                            Value = key_Ranking.Width + 64f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Ranking.X",
                            Type = "float",
                            Value = key_Ranking.X + 200f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Ranking.Y",
                            Type = "float",
                            Value = key_Ranking.Y + 130f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Sangue.ItemdescText",
                            Type = "string",
                            Value = key_Sangue.ItemdescText
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Sangue.ItemSprite_hud",
                            Type = "string",
                            Value = key_Sangue.ItemSprite_hud
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Sangue.KeyText",
                            Type = "string",
                            Value = key_Sangue.KeyText
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Sangue.Parent",
                            Type = "string",
                            Value = key_Sangue.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Sangue.Width",
                            Type = "float",
                            Value = key_Sangue.Width + 64f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Sangue.X",
                            Type = "float",
                            Value = key_Sangue.X + 200f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Sangue.Y",
                            Type = "float",
                            Value = key_Sangue.Y + 330f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Controle.ItemdescText",
                            Type = "string",
                            Value = key_Controle.ItemdescText
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Controle.ItemSprite_hud",
                            Type = "string",
                            Value = key_Controle.ItemSprite_hud
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Controle.KeyText",
                            Type = "string",
                            Value = key_Controle.KeyText
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Controle.Parent",
                            Type = "string",
                            Value = key_Controle.Parent
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Controle.Width",
                            Type = "float",
                            Value = key_Controle.Width + 64f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Controle.X",
                            Type = "float",
                            Value = key_Controle.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "key_Controle.Y",
                            Type = "float",
                            Value = key_Controle.Y + 390f
                        }
                        );
                        break;
                }
                return newState;
            }
            #endregion
            public override void ApplyState (Gum.DataTypes.Variables.StateSave state) 
            {
                bool matches = this.ElementSave.AllStates.Contains(state);
                if (matches)
                {
                    var category = this.ElementSave.Categories.FirstOrDefault(item => item.States.Contains(state));
                    if (category == null)
                    {
                        if (state.Name == "Default") this.mCurrentVariableState = VariableState.Default;
                    }
                }
                base.ApplyState(state);
            }
            private DungeonRun.GumRuntimes.SpriteRuntime SpriteInstance { get; set; }
            private DungeonRun.GumRuntimes.TextRuntime HeaderInstance { get; set; }
            private DungeonRun.GumRuntimes.ItemDisplay1Runtime key_Shot { get; set; }
            private DungeonRun.GumRuntimes.ContainerRuntime ContainerInstance { get; set; }
            private DungeonRun.GumRuntimes.ItemDisplay1Runtime key_move { get; set; }
            private DungeonRun.GumRuntimes.ItemDisplay1Runtime key_Esc { get; set; }
            private DungeonRun.GumRuntimes.ItemDisplay1Runtime key_space { get; set; }
            private DungeonRun.GumRuntimes.ItemDisplay1Runtime key_Pause { get; set; }
            private DungeonRun.GumRuntimes.ItemDisplay1Runtime key_Ranking { get; set; }
            private DungeonRun.GumRuntimes.ItemDisplay1Runtime key_Sangue { get; set; }
            private DungeonRun.GumRuntimes.ItemDisplay1Runtime key_Controle { get; set; }
            public Panel2_DisplayRuntime (bool fullInstantiation = true) 
            	: base(false)
            {
                if (fullInstantiation)
                {
                    Gum.DataTypes.ElementSave elementSave = Gum.Managers.ObjectFinder.Self.GumProjectSave.Components.First(item => item.Name == "Panel2_Display");
                    this.ElementSave = elementSave;
                    string oldDirectory = FlatRedBall.IO.FileManager.RelativeDirectory;
                    FlatRedBall.IO.FileManager.RelativeDirectory = FlatRedBall.IO.FileManager.GetDirectory(Gum.Managers.ObjectFinder.Self.GumProjectSave.FullFileName);
                    GumRuntime.ElementSaveExtensions.SetGraphicalUiElement(elementSave, this, RenderingLibrary.SystemManagers.Default);
                    FlatRedBall.IO.FileManager.RelativeDirectory = oldDirectory;
                }
            }
            public override void SetInitialState () 
            {
                base.SetInitialState();
                this.CurrentVariableState = VariableState.Default;
                CallCustomInitialize();
            }
            public override void CreateChildrenRecursively (Gum.DataTypes.ElementSave elementSave, RenderingLibrary.SystemManagers systemManagers) 
            {
                base.CreateChildrenRecursively(elementSave, systemManagers);
                this.AssignReferences();
            }
            private void AssignReferences () 
            {
                SpriteInstance = this.GetGraphicalUiElementByName("SpriteInstance") as DungeonRun.GumRuntimes.SpriteRuntime;
                HeaderInstance = this.GetGraphicalUiElementByName("HeaderInstance") as DungeonRun.GumRuntimes.TextRuntime;
                key_Shot = this.GetGraphicalUiElementByName("key_Shot") as DungeonRun.GumRuntimes.ItemDisplay1Runtime;
                ContainerInstance = this.GetGraphicalUiElementByName("ContainerInstance") as DungeonRun.GumRuntimes.ContainerRuntime;
                key_move = this.GetGraphicalUiElementByName("key_move") as DungeonRun.GumRuntimes.ItemDisplay1Runtime;
                key_Esc = this.GetGraphicalUiElementByName("key_Esc") as DungeonRun.GumRuntimes.ItemDisplay1Runtime;
                key_space = this.GetGraphicalUiElementByName("key_space") as DungeonRun.GumRuntimes.ItemDisplay1Runtime;
                key_Pause = this.GetGraphicalUiElementByName("key_Pause") as DungeonRun.GumRuntimes.ItemDisplay1Runtime;
                key_Ranking = this.GetGraphicalUiElementByName("key_Ranking") as DungeonRun.GumRuntimes.ItemDisplay1Runtime;
                key_Sangue = this.GetGraphicalUiElementByName("key_Sangue") as DungeonRun.GumRuntimes.ItemDisplay1Runtime;
                key_Controle = this.GetGraphicalUiElementByName("key_Controle") as DungeonRun.GumRuntimes.ItemDisplay1Runtime;
            }
            public override void AddToManagers (RenderingLibrary.SystemManagers managers, RenderingLibrary.Graphics.Layer layer) 
            {
                base.AddToManagers(managers, layer);
            }
            private void CallCustomInitialize () 
            {
                CustomInitialize();
            }
            partial void CustomInitialize();
        }
    }
