﻿using DungeonRun.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonRun.MinhasClasses.Data
{
    
    class PlayerData
    {

        // public static List<PlayerData> playList;
        private Player playerInstanceI { get; set; }
        public Player playerInstance
        {
            get
            {
                return playerInstanceI;
            }
            set
            {
                playerInstanceI = value;
                if (firstPlayLoad)
                {
                    health = playerInstanceI.StartingHealth;
                    firstPlayLoad = false;
                }
                GlobalData.hudInstance.LoadHealthToHUD();
                GlobalData.hudInstance.LoadNameDisplay();
            }
        }

        public String name { get; set; }
        public Dictionary<EnumItem,InvetoryItem> ItemList { get; set; } //inventario
        public int qtdkeys { get; set; }
        public bool firstPlayLoad = true;
        public int health { get; set; }
        public double totalPlayerTime { get; set; }
        public int totalLevelCompleted { get; set; }
        public int qtdMonsterKilled { get; set; }
        private int monsterKilled ;
        private int completedNivel;


        public PlayerData()
        {
            ItemList = new Dictionary<EnumItem, InvetoryItem>();

        }

        public void resetaVariaveis()
        {
            
           // name = "";
            qtdkeys = 0;
            firstPlayLoad = true;
            totalPlayerTime = 0;
            totalLevelCompleted= 0;
            qtdMonsterKilled = 0;
            completedNivel = 0;
            GlobalData.hudInstance.LoadItensToHUD();
            GlobalData.hudInstance.LoadLevelToHUD();
            ItemList = new Dictionary<EnumItem, InvetoryItem>();
        }

        public int Score
        {
            get;
            set;
        }
        
        public void PlaySave()
        {
            // int points = CalculatePoints.calculateTotalPoints(completedNivel, health, totalPlayerTime, monsterKilled);
            int points = CalculatePoints.Instance().calculateTotalPoints(completedNivel, health, totalPlayerTime, monsterKilled);
            GlobalData.Ranking.AtualizeRanking(new PlayerSave(this.name, points, totalPlayerTime));

        }



    }
}
