﻿using DungeonRun.Entities;
using FlatRedBall;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlatRedBall.IO;

namespace DungeonRun.MinhasClasses.Data
{
    class GlobalData
    {

        public static List<Entities.Level> LevelList
        {
            get;
            set;
        }

        public static int wallHeigth = 96;// altura de 2 blocos do tile
        public static int tileSize = 32;
        public static int halfTileSize = tileSize / 2;
        public static String LayerObjectsName = "Objects";
        public static String DoorInstanceName = "Door1";
        public static String MonsterSpawerName = "MonsterHole";
        public static String BossSpawerName = "BossHole";
        public static String nameInput;
        // public static String GroundInstanceName = "GroundGreen1";
        public static int Levelqtd = 5;//conta o level zero
        //public static int LevelRoomqtd = 6;

        public static int boosBatleInteval = 4;//
        public static int MonsterQtd = 2;

        public static Hud hudInstance { get; set; }
      //  public static Level currentLevel { get; set; }
        public static Entities.Level currentLevel { get; set; }
        public enum GameMode { NORMAL, SURVIE };
        public static GameMode currentGameMode = GameMode.NORMAL;
        public static bool gameOver = false;    
        public static bool isGamePause { get; set; }

        public static float camX = Camera.Main.OrthogonalWidth / 2.1f;
        public static float camY = -(Camera.Main.OrthogonalHeight / 2f);

        public static String myFilename = "date.txt";
       
        public static Ranking Ranking = new Ranking("raking.txt");
        
        public static PlayerData PlayerData = new PlayerData();
        public static double CurrentLevelTime;



        public static void Initialize()
        {
            isGamePause = false;
            teste();
           // PlayerData = new PlayerData();
            montaLevelList();

            currentLevel = LevelList.ElementAt(0);
           // Console.WriteLine("GlobalDataInitialize");


        }

        public static void resetarVariaveis()
        {
           
            isGamePause = false;
            teste();
            montaLevelList();
            currentLevel = LevelList.ElementAt(0);
            
            CurrentLevelTime = 0;
            PlayerData.resetaVariaveis();
            
            currentGameMode = GameMode.NORMAL;
            gameOver = false;
    }

        public static void teste()
        {
           
            
        }

        private static void montaLevelList()
        {
            
            LevelList = new List<Entities.Level>(); 
          

             for(int i = 0; i < Levelqtd;  i++)
              {
                
                Level lvx = new Entities.Level(i);
                //Console.WriteLine(); ;
                LevelList.Add(lvx);
             }

            

        }

        public static  void gameOverGlobal(bool venceuBoss)
        {
            //statusPlay = EnumPlay.ZERO_HP;
            GlobalData.PlayerData.health = 0;
            GlobalData.hudInstance.LoadHealthToHUD();
            GlobalData.isGamePause = true;
            string name = GlobalData.PlayerData.name;
            int point = CalculatePoints.Instance().calculateTotalPoints(GlobalData.PlayerData.totalLevelCompleted, GlobalData.PlayerData.health, GlobalData.PlayerData.totalPlayerTime, GlobalData.PlayerData.qtdMonsterKilled);
            double time = GlobalData.PlayerData.totalPlayerTime + GlobalData.currentLevel.levelTime;

            PlayerSave ps = new PlayerSave(name, point, time);

            GlobalData.hudInstance.LoadGameOver(venceuBoss);
            Console.WriteLine("LoadGameOver");
            GlobalData.Ranking.AtualizeRanking(ps);
            GlobalData.gameOver = true;

            //GlobalData.PlayerData = new PlayerData();            
           
            // GlobalData.PlayerData.


        }
        public static String NextLevel()
        {
            //Console.WriteLine("NExtLEvel|LevelID:" + currentLevel.id);
            int i = currentLevel.id + 1;
            if (i < LevelList.Count())
            {
                GlobalData.currentLevel = LevelList[i];
                return GlobalData.LevelList[i].name;
            }
            else
            {
                GlobalData.currentLevel = LevelList[i - 1];
                return GlobalData.LevelList[i - 1].name;
            }
        }
        

        public static String NextRoom()
        {

            //se  já passou por todas as salas do level vai para o próximo nivel
            // Console.WriteLine("Current" + currentLevel.RoomCurrent + "|Qtd:"+ currentLevel.RoomQtd);

            if (GlobalData.currentLevel.CurrentState != Level.VariableState.level0)
            {
                PlayerData.totalPlayerTime += currentLevel.levelTime;
                PlayerData.totalLevelCompleted++;
                CurrentLevelTime += currentLevel.levelTime;
            }
            //Console.WriteLine("PlayerTotaltime:" + PlayerData.totalPlayerTime + "; CurrentlevelTime:" + CurrentLevelTime);
            if (currentLevel.RoomCurrent == currentLevel.RoomQtd)
            {
                 Console.WriteLine("Global: Novo Nivel");
                CurrentLevelTime = 0;
                return NextLevel();
            }else
            {

                //Console.WriteLine("Global: Nova sala");                               
                return currentLevel.name;
            }
            

        }

    
        public static String RandomLevel()
        {
            int i = FlatRedBallServices.Random.Next(GlobalData.LevelList.Count);
            //Console.WriteLine(GlobalData.LevelList.Count);
            currentLevel = LevelList[i];
            return GlobalData.LevelList[i].name;
        }



    }
}
