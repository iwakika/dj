﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DungeonRun.MinhasClasses.Data;

namespace DungeonRun.MinhasClasses
{
    class CalculatePoints
    {

        
        public  int points { get; set; }
        private int qtdtotalSala = 11;
        private int minLife = 5;
        public int levelRoomToCompletedPoints = 50;
        public int completedInTimePoints = 35;

        public  int minLivePoints = 15;
        private int modalLivePoints = 2; // pontos para cada vida extra 

        private double maxTimeSecs = 120;
        private double modalTime = 10;

        public int extraPointsMax = 10;
       // private static int extrapoints;

        private static CalculatePoints instance;

        private CalculatePoints()
        {
            
            
        }

        public static CalculatePoints Instance()
        {
            if (instance == null)
            {
                instance = new CalculatePoints();
            }
            return instance;
        }

        public int calculateTotalPoints(int completedNivel, int qtdLife , double sec, int monsterKilled)
        {
            int points = 0;
            points += toLevelCompleted(completedNivel);
            points += completedWithMinLive(qtdLife);
            points += completedInTime(sec, completedNivel);
            points += extraPoints(qtdLife, monsterKilled);

            return points;
        }

        public static int calculateTotalPoints()
        {
            int points = CalculatePoints.Instance().calculateTotalPoints(GlobalData.PlayerData.totalLevelCompleted, GlobalData.PlayerData.health, GlobalData.PlayerData.totalPlayerTime, GlobalData.PlayerData.qtdMonsterKilled);
            Console.WriteLine(points);
            return points;
        }

        public int toLevelCompleted(int completedNivel)
        {
            int x;
            x = (levelRoomToCompletedPoints * completedNivel) / qtdtotalSala;
            return x;

        }
        public int completedWithMinLive(int qtdLife)
        {
            int x = 0;
            
            x = (minLivePoints * qtdLife/ minLife);     
            if(x > minLivePoints)
            {
                x = minLivePoints;
            }       
            
            return x ;

        }
        

        public  int completedInTime(double sec, int completedNivel)
        {
            int x;
            int lostPoints = 0;
            double faseCompletadasMod = Convert.ToDouble(completedNivel)/ qtdtotalSala;
            //Console.WriteLine("Completo:" + completedNivel + "/" + qtdtotalSala);
            if (sec > maxTimeSecs)
            {
                double extrapolatedTime = maxTimeSecs - sec;
                lostPoints = Convert.ToInt32(Math.Round(extrapolatedTime / modalTime));
                if (lostPoints > completedInTimePoints)
                {
                    lostPoints = completedInTimePoints;
                }

            }
            // se o jogo não foi completao perde 10 pontos
            if(completedNivel < qtdtotalSala-1)
            {
                Console.WriteLine(qtdtotalSala);
                Console.WriteLine(completedNivel);
                lostPoints = -10;

            }

            //Console.WriteLine("fassemod:"+ (completedInTimePoints - lostPoints) * faseCompletadasMod);
            x = Convert.ToInt32((completedInTimePoints - lostPoints) * faseCompletadasMod);
            return x;

        }

        public  int extraPoints(int qtdLife, int monsterKilled)
        {
            int extraPoints = 0;
            /*
            ///EXTRA LIFE POINTES
            if (qtdLife > minLife) { 
            
            int extraLife = qtdLife - minLife;

            if (extraLife > minLife)
            {
                    extraPoints = modalLivePoints * extraLife;
                }
            }
            */

            //EXTRA MONSTER KILL POINTES
            extraPoints += monsterKilled/2;

            if(extraPoints > extraPointsMax)
            {
                extraPoints = extraPointsMax;
            }
            
            return extraPoints;

        }

        
    }
}
