﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlatRedBall.IO;

namespace DungeonRun.MinhasClasses
{

    [Serializable]
    class Ranking
    {
        public List<PlayerSave> rankingList = new List<PlayerSave>();
        private String filePath;        
        private int rankingSizeMax = 15;

        public Ranking(String myFileName)
        {
           this.filePath = FileManager.UserApplicationDataForThisApplication + myFileName;
            Console.WriteLine(filePath);
        }

        public void orderRanking()
        {
            
            rankingList.Sort();
            Array arrayList = rankingList.ToArray();
            
            rankingList.Clear();
            for (int i = 0;  i <= rankingSizeMax; i ++  )
            {
                if (arrayList.Length - i > 0)
                {
                    PlayerSave p = (PlayerSave)arrayList.GetValue(arrayList.Length - i - 1);
                    rankingList.Add(p);
                }
            }
        }

        public void printRanking()
        {
            Console.WriteLine("-----PrintLine---");
            foreach(PlayerSave p in rankingList)
            {
                Console.WriteLine("Point-" + p.playerPoints + "| Name" + p.playerName);
            }
        }
        public string getRankingName()
        {
            String names = "";

            if(rankingList == null)
            {
                ReadRaking();
            }
            
            foreach (PlayerSave p in rankingList)
            {
                names += p.playerName + "\n";
            
            }
            return names;
        }

        public string getRankingPoints()
        {
            String points = "";

            if (rankingList == null)
            {
                ReadRaking();
            }

            foreach (PlayerSave p in rankingList)
            {
                points += p.playerPoints + "\n";

            }
            return points;
        }
        public string getRankingTime()
        {
            String time = "";
            

            if (rankingList == null)
            {
                ReadRaking();
            }

            foreach (PlayerSave p in rankingList)
            {

                TimeSpan t = TimeSpan.FromSeconds(p.playerTime);
                //Console.WriteLine("playertime:" + p.playerTime);
                string timeFormat = string.Format("{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds);
                time += timeFormat + "\n";

            }
            return time;
        }


        public void AtualizeRanking(PlayerSave save)
        {
            ReadRaking();
            rankingList.Add(save);
            writeRanking();

            
        } 

        public void writeRanking()
        {
            orderRanking();
            FileManager.BinarySerialize( rankingList, filePath);
        }

        public void ReadRaking()
        {
            try
            {
                if (FileManager.FileExists(filePath))
                {
                   // Console.WriteLine("Ranking - Lendo Arquivo");
                    rankingList = (List<PlayerSave>)FileManager.BinaryDeserialize(typeof(List<PlayerSave>), filePath);
                }
                else
                {
                    rankingList = new List<PlayerSave>();
                    Console.Error.WriteLine("Ranking - Não existe arquivo");
                }
            }
            catch(ArgumentException ae)
            {
                Console.Error.WriteLine("Raking - filePath não pode ser null:"+ ae.Message);
            }
        }

       

        

    }
}
