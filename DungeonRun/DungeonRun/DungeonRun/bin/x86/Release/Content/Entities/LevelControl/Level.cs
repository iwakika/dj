﻿using DungeonRun.Entities;
using DungeonRun.Factories;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Math;
using FlatRedBall.TileGraphics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System;
using FlatRedBall;
using DungeonRun.MinhasClasses.Data;

namespace DungeonRun.MinhasClasses
{
    class  Level
    {
        public TileNodeNetwork tileNodeNetwork { get; set; }
        private MapDrawableBatch currentLayerMap { get; set; }
        public int id { get; set; }
        public String name { get; set; }
        public AttachableList<MapDrawableBatch> layerList;
        //private AttachableList<MapDrawableBatch> templayerList;
        public LayeredTileMap mtileMap;
        double time = 0;        
        public Door currentDoor { get; set; }
        public List<Vector2> monsterSpawerPoints { get; set; }
        public Boolean bossBatle {
            get{
                
                if ((id + 1) % GlobalData.boosBatleInteval == 0)                {
                    return true;
                }else
                {
                    return false;
                }
            }
            set { bossBatle = value;
            }
        }
        public Boolean existMonsterSpawerPoints {
            get
            {
                if (monsterSpawerPoints.Count > 0) {
                    return true;
                } else
                {

                    return false;
                        }
            }
    }

        private int LayertoLoad = 0;
        public int RoomCurrent { get; set; }
        public int RoomQtd { get; set;}   

        private int keyMax = 2;
        private int keyMin = 1;
        private int keyPathMaxDistance = 10;
        private static List<int> layerListIndex = new List<int>();
        /// <summary>
        /// Carrega junto com o loadLevelLayerMap();  
        /// </summary>
        private MapDrawableBatch objectsMaps = new MapDrawableBatch();
             
        //public static int AlturaParede = 96;// altura de 2 blocos do tile
        public double levelTime        
        {
            get
            {
                time = FlatRedBall.Screens.ScreenManager.CurrentScreen.PauseAdjustedSecondsSince(time);
                return FlatRedBall.Screens.ScreenManager.CurrentScreen.PauseAdjustedSecondsSince(time);
            }
        }

        public LayeredTileMap tileMap {
        
            get
            {
               return mtileMap;

            }
            set
            {
                mtileMap = value;               
                initializeLevelMap();             


            }
        }

        public Level(int id)
        {
            this.id = id;
            this.name = "Level" + id;
            this.RoomCurrent = 0;
            //Console.WriteLine("level inicialize:" + id + "|Name:" + name);

        }
        public void initializeLevelMap()
        {
            //Console.WriteLine("level inicialize:" + id + "|Name:" + name);                      
            loadLevelLayerMap();            
            createNodeNetwork();
            time = 0;
            loadLevelDoor();
            loadMonsterSpawer();
            loadlevelItem();
            setPlayinDoorPosition();
           
            GlobalData.hudInstance.LoadLevelToHUD();



        }

        private void setPlayinDoorPosition()
        {
            Vector3 ajustedPosition;
            switch (currentDoor.CurrentState)
            {
                case Door.VariableState.DoorBotton:
                    ajustedPosition = currentDoor.Position;
                    ajustedPosition.Y += GlobalData.tileSize/2; 
                    setPlayPosition(ajustedPosition);
                    break;
                case Door.VariableState.DoorTop:
                    ajustedPosition = currentDoor.Position;
                    ajustedPosition.Y -= GlobalData.tileSize/2;
                    setPlayPosition(ajustedPosition);
                    break;
                case Door.VariableState.DoorNoTop:

                    ajustedPosition = currentDoor.Position;
                                        
                    float rigth = SpriteManager.Camera.AbsoluteRightXEdgeAt(0);
                   // Console.WriteLine("rigth" + rigth);
                   // Console.WriteLine("x" + ajustedPosition.X);
                    if (ajustedPosition.X + GlobalData.tileSize > rigth  )
                    ajustedPosition.X -= GlobalData.tileSize/2;
                    else
                    {
                        ajustedPosition.X += GlobalData.tileSize / 2;
                    }
                    setPlayPosition(ajustedPosition);
                    break;
            }

        }

        
        /// <summary>
        /// Altera as posições x e y do play, pasra alterar o z tera de ser feito manialmente
        /// </summary>
        /// <param name="position"></param>
        public void setPlayPosition(Vector3 position)
        {
            Vector3 playpositi = position;
            playpositi.Z = GlobalData.PlayerData.playerInstance.Position.Z;

            GlobalData.PlayerData.playerInstance.Position =  playpositi;
        }

        /// <summary>
        
        /// </summary>
        public Vector2 getRandomMonsterSpawerPoint()
        {
            Vector2 position = new Vector2();            
            position = monsterSpawerPoints[FlatRedBallServices.Random.Next(0, monsterSpawerPoints.Count)];
            
           
            return position;

        }


        /// <summary>
        /// Carrega item do nivel
        /// </summary>
        private void loadlevelItem()
        {
            createKey();
        }


        public Vector2 RandomGroundTilePosition(int distanceMax, Vector3 objectGoto) {

            Vector2 position;
            int distance = 0;                
            int loopcount = 0;

            /******ALERTA TRATAR LOOPINFINITO */
            do
            {

                position = RandomGroundTilePosition();
                PositionedNode goToNode = GlobalData.currentLevel.tileNodeNetwork.GetClosestNodeTo(objectGoto.X, objectGoto.Y);
                PositionedNode randomNode = GlobalData.currentLevel.tileNodeNetwork.GetClosestNodeTo(position.X, position.Y);
                List<PositionedNode> path = GlobalData.currentLevel.tileNodeNetwork.GetPath(goToNode, randomNode);
                distance = path.Count;
                /*  if (distancia < 20)
                  {
                     // Console.WriteLine("Caminho muito curto!" + distancia + ", Chave :" + i);

                  }
                  */
                loopcount++;
            } while (distance < distanceMax || loopcount < distanceMax);

            return position;
        }


        /// <summary>
        /// Cria um um numero aleatorio dechaves baseados na quantidade definida para o nivel
        /// e adiciona o numeros de chaves para serem usados na porta
        /// </summary>
        ///           
        
        private void createKey()
        {

            int keytoGenerate = FlatRedBallServices.Random.Next(keyMin, keyMax + 1);
            currentDoor.keyToOpen = keytoGenerate;

            //Console.WriteLine("Chaves para gerar" + keytoGenerate);
            for (int i = 0; i < keytoGenerate; i++)
            {
                /*
                int distancia = 0;
                Vector2 position;
                int loopcount = 0;

                ////*ALERTA TRATAR LOOPINFINITO 
                do
                {

                    position = RandomGroundTilePosition();
                    PositionedNode DoorNode = GlobalData.currentLevel.tileNodeNetwork.GetClosestNodeTo(currentDoor.X, currentDoor.Y);
                    PositionedNode KeyNode = GlobalData.currentLevel.tileNodeNetwork.GetClosestNodeTo(position.X, position.Y);
                    List<PositionedNode> path = GlobalData.currentLevel.tileNodeNetwork.GetPath(DoorNode, KeyNode);
                    distancia = path.Count;                 
                    loopcount++;
                } while (distancia < keyPathMaxDistance || loopcount < keyPathMaxDistance);

                createKeyEntities(position);*/
                createKeyEntities(RandomGroundTilePosition(keyPathMaxDistance, currentDoor.Position));

            }
        }     


        /// <summary>
        ///  cria uma chave a partir de uma posição informada
        /// </summary>
        /// <param name="position"></param>
        private void createKeyEntities(Vector2 position)
        {           

            Item key = ItemFactory.CreateNew();
            // key.X = tileCenterX;
            // key.Y = tileCenterY;
             key.X = position.X;
             key.Y = position.Y;
            key.itemType = EnumItem.KEY_SILVER;
            key.CurrentState = Item.VariableState.Key;

            //final da criação da chave


        }


        /// <summary>
        /// Randomiza um dos tile do currentLayerMap  que possuim o nome definido pelo o GlobalData.GroundInstanceName
        /// </summary>
        /// <returns></returns>
        public Vector2 RandomGroundTilePosition()
        {
            float tileLeft = 0;
            float tileBottom = 0;

            if (currentLayerMap != null)

            {

                //busca todos os tilenames com propriedade caminho
                /* var tileNamesForPath = tileMap.TileProperties
                 .Where(item => item.Value
                 .Any(customProperty => customProperty.Name == "TileType" && (string)customProperty.Value == "Caminho"))
                 .Select(item => item.Key)
                 .ToArray();*/

                //** PRecisa ser otimizado esse where para pegar apenas um com cada nome
                
                var tileNamesForPath = tileMap.TileProperties.Where(item => item.Value
                .Any(customProperty => customProperty.Name == "TileType" && (string)customProperty.Value == "Caminho"))
                .Select(item => item.Key).ToArray();                

                /* var tileNamesForPath = tileMap.TileProperties
                .Where(item => item.Value
                .Any(customProperty => customProperty.Name == "TileType" && (string)customProperty.Value == "Caminho"))
                .Select(item => item.Key)
                .ToArray();*/


                foreach (String tileName in tileNamesForPath)
                {
                    // if (currentLayerMap.NamedTileOrderedIndexes.ContainsKey(GlobalData.GroundInstanceName))
                    if (currentLayerMap.NamedTileOrderedIndexes.ContainsKey(tileName))                        
                    {

                        //Console.WriteLine("Tile encontrados" + tileName + "tilecount" +tileNamesForPath.Count());

                        // var groundtiles = currentLayerMap.NamedTileOrderedIndexes[GlobalData.GroundInstanceName];
                        var groundtiles = currentLayerMap.NamedTileOrderedIndexes[tileName];
                        //

                        int random = FlatRedBallServices.Random.Next(0, groundtiles.Count);
                        //random = 0;
                        int idx = groundtiles.ElementAt(random);

                        currentLayerMap.GetBottomLeftWorldCoordinateForOrderedTile(idx, out tileLeft, out tileBottom);

                        // Console.WriteLine("frond tilies in: " + currentLayerMap.Name + "- " + grundtiles.Count +", random:"+ random +", idx:"+idx);



                    }
                }
            }
            float tileCenterX = tileLeft + GlobalData.halfTileSize;
            float tileCenterY = tileBottom + GlobalData.halfTileSize;

            return new Vector2(tileCenterX, tileCenterY);
                    
        }

        
        /// <summary>
        /// Carrega monstros que estarão no nivel ao inicializar, e alimenta o index das posições
        /// </summary>
        private void loadMonsterSpawer()
        {
            //MapDrawableBatch objectsMaps = tileMap.MapLayers.FindByName(GlobalData.LayerObjectsName);
            monsterSpawerPoints = new List<Vector2>();
            objectsMaps.Visible = false;

            if (objectsMaps != null)
            {
                if (objectsMaps.NamedTileOrderedIndexes.ContainsKey(GlobalData.MonsterSpawerName))
                {

                    var monsterIndex = objectsMaps.NamedTileOrderedIndexes[GlobalData.MonsterSpawerName];
                    //
                    
                    foreach (int index in monsterIndex)
                    {
                        float tileLeft;
                        float tileBottom;
                        objectsMaps.GetBottomLeftWorldCoordinateForOrderedTile(index, out tileLeft, out tileBottom);

                        float tileCenterX = tileLeft + GlobalData.halfTileSize;
                        float tileCenterY = tileBottom + GlobalData.halfTileSize;

                        monsterSpawerPoints.Add(new Vector2(tileCenterX,tileCenterY));
                    }

                    }
            }
        }

        /// <summary>
        /// Carrega as instâncias de portas do nivel, e randomiza a posicao atual da porta
        /// </summary>
        private void loadLevelDoor()
        {                     

            //float halfTileDimension = 16f;
             //MapDrawableBatch objectsMaps = tileMap.MapLayers.FindByName(GlobalData.LayerObjectsName);

            objectsMaps = tileMap.MapLayers.FindByName(GlobalData.LayerObjectsName);
            objectsMaps.Visible = false;

            
            if (objectsMaps != null)
            {
                if (objectsMaps.NamedTileOrderedIndexes.ContainsKey(GlobalData.DoorInstanceName))
                {
                    
                    var doorIndex = objectsMaps.NamedTileOrderedIndexes[GlobalData.DoorInstanceName];
                    //

                    //randomiza uma posição da lista de index e acaptura o index da porta
                    int doorToLoad = doorIndex[FlatRedBallServices.Random.Next(0, doorIndex.Count)];
                    //Console.WriteLine("DoorToload" + doorToLoad);


                    foreach (int index in doorIndex)
                    {
                        float tileLeft;
                        float tileBottom;
                        objectsMaps.GetBottomLeftWorldCoordinateForOrderedTile(index, out tileLeft, out tileBottom);

                        float tileCenterX = tileLeft + GlobalData.halfTileSize;
                        float tileCenterY = tileBottom + GlobalData.halfTileSize;
                        
                       Door door = DoorFactory.CreateNew(tileCenterX, tileCenterY);    
                            
                           // door.X = tileCenterX;
                           // door.Y = tileCenterY;
                            door.Index = index;

                        float topEdge = SpriteManager.Camera.AbsoluteTopYEdgeAt(0);
                        float bottomEdge = SpriteManager.Camera.AbsoluteBottomYEdgeAt(0);

                        // Console.WriteLine("DoorToload" + doorToLoad + index);

                        if (doorToLoad == index)
                        {
                            
                            currentDoor = door;
                            door.setStatusDoor(true);                        
                        }
                        else
                        {//CarregaParede
                            door.setStatusDoor(false);
                         

                        }                       


                    }
                }
            }
            else
            {
                Console.WriteLine("Objects Maps is Null!");
            }

        }


        public Level(int id, List<Vector3> vDoorSpawPointList, List<Vector3> vMonsterSpawerList)
        {
            this.name = "Level" +id;
            this.id = id;
            //this.doorSpawPointList = vDoorSpawPointList;
           // this.monsterSpawerList = vMonsterSpawerList;
        }
     
        public void nextRoom()
        {
            LayertoLoad = 0;
            if (layerList.Count()< RoomQtd)
            {                
                layerList = tileMap.MapLayers.FindAllWithNameContaining("Mapaset");
            }
            if (GlobalData.currentGameMode == GlobalData.GameMode.NORMAL && layerList.Count() >=  1)
             {

                int index = FlatRedBall.FlatRedBallServices.Random.Next(0, layerListIndex.Count);
                if (layerListIndex.Count == 0)
                {
                    Console.WriteLine("Acabaram as SALAS");
                    LayertoLoad = FlatRedBall.FlatRedBallServices.Random.Next(1, layerList.Count) - 1;// ajustar
                    RoomCurrent  = 99;
                }
                else
                {
                    RoomCurrent += 1;
                    LayertoLoad = layerListIndex.ElementAt(index);                  
                    layerListIndex.RemoveAt(index);
                }
             }
             else
             {

            if (GlobalData.currentGameMode == GlobalData.GameMode.SURVIE)
                {
                if (layerList.Count() > 1)
                {
                    LayertoLoad = FlatRedBall.FlatRedBallServices.Random.Next(1, layerList.Count) - 1;// ajustar
                }
            }
                else
                {
                    //Console.WriteLine("Acabaram as salas");
                }
            }
                      
            for (int i = layerList.Count - 1; i >= 0; i--)
            {

                if (LayertoLoad != i)
                {

                    layerList.ElementAt(i).Visible = false;
                    layerList.ElementAt(i).Destroy();
                    
                }
                else
                {
                    layerList.ElementAt(i).Visible = true;
                    currentLayerMap = layerList.ElementAt(i);
                }
            }        
            

           // Console.WriteLine("Quartos restantes2:" + layerList.Count +"" + layerListIndex.Count()  );
            GlobalData.hudInstance.LoadLevelToHUD();
        }

        public void loadLevelLayerMap()
        {
            layerList = tileMap.MapLayers.FindAllWithNameContaining("Mapaset");
           // templayerList = tileMap.MapLayers.FindAllWithNameContaining("Mapaset");
           /* if (layerList.Count() == 0)
            {
                layerList = tileMap.MapLayers.FindAllWithNameContaining("Mapaset");

            } */     

           objectsMaps = tileMap.MapLayers.FindByName(GlobalData.LayerObjectsName);

            if (RoomCurrent == 0)
            {
                RoomCurrent = 0;
                RoomQtd = layerList.Count;
                loadLayerIndex();
            }
            
            nextRoom();

        }

        public void loadLayerIndex()
        {
            for (int i = 0; i < layerList.Count(); i++)
            {
                layerListIndex.Add(i);
            }
        }

        public  void loadLevelLayerMapX()
        {
            // Console.WriteLine("Tilemap- "+ tileMap.Name);
            // Console.WriteLine("listcont:" + layerList.Count);
           
            int LayertoLoad = 0;

            if (layerList.Count() == 0)
            {
                layerList = tileMap.MapLayers.FindAllWithNameContaining("Mapaset");
            }

            if (layerList.Count() > 1)
            {
                 LayertoLoad = FlatRedBall.FlatRedBallServices.Random.Next(1, layerList.Count) - 1;// ajustar
            }
            
            
                for (int i = layerList.Count-1 ; i >= 0; i--)
                {
                                  
                    if (LayertoLoad != i)
                    {

                        layerList.ElementAt(i).Visible = false;
                        layerList.ElementAt(i).Destroy();
                    }
                    else
                    {
                        layerList.ElementAt(i).Visible = true;
                        currentLayerMap = layerList.ElementAt(i);
                }

                objectsMaps = tileMap.MapLayers.FindByName(GlobalData.LayerObjectsName);

            }
            RoomCurrent = LayertoLoad;
            RoomQtd = layerList.Count;
            GlobalData.hudInstance.LoadLevelToHUD();
        }
   
        /// <summary>
        /// Cria Node de quatro direções baseado no layout da sala, definido como "node" todos os "Tiles" com o "TileType"  definido como Caminho
        /// 
        /// </summary>
        private void createNodeNetwork()
        {
            float GridWidth = 31;
            float xOrigin = 15; //1.5
            float yOrigin = -tileMap.Height + GridWidth / 2f;
            float gridSpacing = (float)tileMap.HeightPerTile;
            int numberOfXTiles = MathFunctions.RoundToInt(tileMap.Width / tileMap.WidthPerTile.Value);
            int numberOfYTiles = MathFunctions.RoundToInt(tileMap.Height / tileMap.HeightPerTile.Value);


            tileNodeNetwork = new TileNodeNetwork(xOrigin, yOrigin, gridSpacing, numberOfXTiles, numberOfYTiles, DirectionalType.Four);

            

            var tileNamesForPath = tileMap.TileProperties
            .Where(item => item.Value
            .Any(customProperty => customProperty.Name == "TileType" && (string)customProperty.Value == "Caminho"))
            .Select(item => item.Key)
            .ToArray();

            foreach (var layer in tileMap.MapLayers)
            {
                foreach (var nameToLookFor in tileNamesForPath)
                {
                    var indexes = layer.NamedTileOrderedIndexes.ContainsKey(nameToLookFor) ?
                        layer.NamedTileOrderedIndexes[nameToLookFor] : null;

                    if (indexes != null)
                    {

                        var count = indexes.Count;
                        for (int i = count - 1; i > -1; i--)
                        {
                            float x, y;

                            layer.GetBottomLeftWorldCoordinateForOrderedTile(indexes[i], out x, out y);
                            //Console.WriteLine("Indextile:" + i + "x:" + x + ", y:" + y + ": total:" + count);

                            tileNodeNetwork.AddAndLinkTiledNodeWorld(x, y);

                        }
                    }
                }
            }

          
        }

        public Vector2 getMonsterSpawerPoint(int i)
        {
            Vector2 position = new Vector2();
            try
            {
                position =  monsterSpawerPoints[i];
            }catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            


            return position;
        }

        /*public int countMonsterSpawerPoint()
        {
        }
        */


    }



   
}
