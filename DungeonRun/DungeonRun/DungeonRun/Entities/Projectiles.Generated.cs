#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using DungeonRun.Screens;
using FlatRedBall.Graphics;
using FlatRedBall.Math;
using DungeonRun.Performance;
using DungeonRun.Entities;
using DungeonRun.Factories;
using FlatRedBall;
using FlatRedBall.Screens;
using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall.Math.Geometry;
namespace DungeonRun.Entities
{
    public partial class Projectiles : FlatRedBall.PositionedObject, FlatRedBall.Graphics.IDestroyable, FlatRedBall.Performance.IPoolable, FlatRedBall.Math.Geometry.ICollidable
    {
        // This is made static so that static lazy-loaded content can access it.
        public static string ContentManagerName { get; set; }
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        public enum VariableState
        {
            Uninitialized = 0, //This exists so that the first set call actually does something
            Unknown = 1, //This exists so that if the entity is actually a child entity and has set a child state, you will get this
            FireBall = 2, 
            AtackWater1 = 3, 
            Arrow = 4, 
            SwordAtack = 5
        }
        protected int mCurrentState = 0;
        public Entities.Projectiles.VariableState CurrentState
        {
            get
            {
                if (mCurrentState >= 0 && mCurrentState <= 5)
                {
                    return (VariableState)mCurrentState;
                }
                else
                {
                    return VariableState.Unknown;
                }
            }
            set
            {
                mCurrentState = (int)value;
                switch(CurrentState)
                {
                    case  VariableState.Uninitialized:
                        break;
                    case  VariableState.Unknown:
                        break;
                    case  VariableState.FireBall:
                        SpriteInstanceTexture = Fire_1;
                        AnimationChains = AnimationChainListFile;
                        CurrentChainName = "FireBall_Default";
                        SpriteInstanceTextureScale = 0.5f;
                        CollisionInstanceRadius = 10f;
                        Atack = 1;
                        if (CollisionWallInstance.Parent == null)
                        {
                            CollisionWallInstanceY = -10f;
                        }
                        else
                        {
                            CollisionWallInstance.RelativeY = -10f;
                        }
                        break;
                    case  VariableState.AtackWater1:
                        if (this.Parent == null)
                        {
                            Y = 0f;
                        }
                        else
                        {
                            RelativeY = 0f;
                        }
                        AnimationChains = AnimationChainListFile;
                        CurrentChainName = "AtackWater1_Default";
                        SpriteInstanceTextureScale = 1f;
                        CollisionInstanceRadius = 30f;
                        Atack = 2;
                        FrameHitStart = 2;
                        break;
                    case  VariableState.Arrow:
                        if (this.Parent == null)
                        {
                            Y = -8f;
                        }
                        else
                        {
                            RelativeY = -8f;
                        }
                        MovementSpeed = 350;
                        SpriteInstanceTexture = arrow;
                        AnimationChains = AnimationChainListFile;
                        CurrentChainName = "Arrow_Default";
                        SpriteInstanceTextureScale = 0.5f;
                        isPlayAtack = false;
                        if (CollisionInstance.Parent == null)
                        {
                            CollisionInstanceY = 0f;
                        }
                        else
                        {
                            CollisionInstance.RelativeY = 0f;
                        }
                        CollisionInstanceRadius = 10f;
                        Atack = 1;
                        break;
                    case  VariableState.SwordAtack:
                        MovementSpeed = 500;
                        AnimationChains = AnimationChainListFile;
                        CurrentChainName = "Sword_Defaualt";
                        isPlayAtack = false;
                        if (CollisionInstance.Parent == null)
                        {
                            CollisionInstanceY = 10f;
                        }
                        else
                        {
                            CollisionInstance.RelativeY = 10f;
                        }
                        CollisionInstanceRadius = 16f;
                        Atack = 1;
                        FrameHitStart = 0;
                        if (CollisionWallInstance.Parent == null)
                        {
                            CollisionWallInstanceY = 0f;
                        }
                        else
                        {
                            CollisionWallInstance.RelativeY = 0f;
                        }
                        break;
                }
            }
        }
        static object mLockObject = new object();
        static System.Collections.Generic.List<string> mRegisteredUnloads = new System.Collections.Generic.List<string>();
        static System.Collections.Generic.List<string> LoadedContentManagers = new System.Collections.Generic.List<string>();
        protected static Microsoft.Xna.Framework.Graphics.Texture2D Fire_1;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D Fire_9;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D Blue1_b;
        protected static FlatRedBall.Graphics.Animation.AnimationChainList AnimationChainListFile;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D arrow;
        protected static Microsoft.Xna.Framework.Audio.SoundEffectInstance FireIgnite;
        protected static Microsoft.Xna.Framework.Audio.SoundEffect flaunch;
        protected static Microsoft.Xna.Framework.Audio.SoundEffect WeaponBlow;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D fxSword;
        
        private FlatRedBall.Sprite mSpriteInstance;
        public FlatRedBall.Sprite SpriteInstance
        {
            get
            {
                return mSpriteInstance;
            }
            private set
            {
                mSpriteInstance = value;
            }
        }
        static float SpriteInstanceXReset;
        static float SpriteInstanceYReset;
        static float SpriteInstanceZReset;
        static float SpriteInstanceXVelocityReset;
        static float SpriteInstanceYVelocityReset;
        static float SpriteInstanceZVelocityReset;
        static float SpriteInstanceRotationXReset;
        static float SpriteInstanceRotationYReset;
        static float SpriteInstanceRotationZReset;
        static float SpriteInstanceRotationXVelocityReset;
        static float SpriteInstanceRotationYVelocityReset;
        static float SpriteInstanceRotationZVelocityReset;
        static float SpriteInstanceAlphaReset;
        static float SpriteInstanceAlphaRateReset;
        private FlatRedBall.Math.Geometry.Circle mCollisionInstance;
        public FlatRedBall.Math.Geometry.Circle CollisionInstance
        {
            get
            {
                return mCollisionInstance;
            }
            private set
            {
                mCollisionInstance = value;
            }
        }
        static float CollisionInstanceXReset;
        static float CollisionInstanceYReset;
        static float CollisionInstanceZReset;
        static float CollisionInstanceXVelocityReset;
        static float CollisionInstanceYVelocityReset;
        static float CollisionInstanceZVelocityReset;
        static float CollisionInstanceRotationXReset;
        static float CollisionInstanceRotationYReset;
        static float CollisionInstanceRotationZReset;
        static float CollisionInstanceRotationXVelocityReset;
        static float CollisionInstanceRotationYVelocityReset;
        static float CollisionInstanceRotationZVelocityReset;
        private FlatRedBall.Math.Geometry.Circle mCollisionWallInstance;
        public FlatRedBall.Math.Geometry.Circle CollisionWallInstance
        {
            get
            {
                return mCollisionWallInstance;
            }
            private set
            {
                mCollisionWallInstance = value;
            }
        }
        static float CollisionWallInstanceXReset;
        static float CollisionWallInstanceYReset;
        static float CollisionWallInstanceZReset;
        static float CollisionWallInstanceXVelocityReset;
        static float CollisionWallInstanceYVelocityReset;
        static float CollisionWallInstanceZVelocityReset;
        static float CollisionWallInstanceRotationXReset;
        static float CollisionWallInstanceRotationYReset;
        static float CollisionWallInstanceRotationZReset;
        static float CollisionWallInstanceRotationXVelocityReset;
        static float CollisionWallInstanceRotationYVelocityReset;
        static float CollisionWallInstanceRotationZVelocityReset;
        private Microsoft.Xna.Framework.Audio.SoundEffectInstance SoundFire;
        private Microsoft.Xna.Framework.Audio.SoundEffectInstance SoundFlaunch;
        private Microsoft.Xna.Framework.Audio.SoundEffectInstance SoundArrow;
        public int MovementSpeed = 500;
        public Microsoft.Xna.Framework.Graphics.Texture2D SpriteInstanceTexture
        {
            get
            {
                return SpriteInstance.Texture;
            }
            set
            {
                SpriteInstance.Texture = value;
            }
        }
        public FlatRedBall.Graphics.Animation.AnimationChainList AnimationChains
        {
            get
            {
                return SpriteInstance.AnimationChains;
            }
            set
            {
                SpriteInstance.AnimationChains = value;
            }
        }
        public string CurrentChainName
        {
            get
            {
                return SpriteInstance.CurrentChainName;
            }
            set
            {
                SpriteInstance.CurrentChainName = value;
            }
        }
        public float SpriteInstanceTextureScale
        {
            get
            {
                return SpriteInstance.TextureScale;
            }
            set
            {
                SpriteInstance.TextureScale = value;
            }
        }
        public bool isPlayAtack;
        public float SpriteInstanceAnimationSpeed
        {
            get
            {
                return SpriteInstance.AnimationSpeed;
            }
            set
            {
                SpriteInstance.AnimationSpeed = value;
            }
        }
        public float CollisionInstanceX
        {
            get
            {
                if (CollisionInstance.Parent == null)
                {
                    return CollisionInstance.X;
                }
                else
                {
                    return CollisionInstance.RelativeX;
                }
            }
            set
            {
                if (CollisionInstance.Parent == null)
                {
                    CollisionInstance.X = value;
                }
                else
                {
                    CollisionInstance.RelativeX = value;
                }
            }
        }
        public float CollisionInstanceY
        {
            get
            {
                if (CollisionInstance.Parent == null)
                {
                    return CollisionInstance.Y;
                }
                else
                {
                    return CollisionInstance.RelativeY;
                }
            }
            set
            {
                if (CollisionInstance.Parent == null)
                {
                    CollisionInstance.Y = value;
                }
                else
                {
                    CollisionInstance.RelativeY = value;
                }
            }
        }
        public float CollisionInstanceRadius
        {
            get
            {
                return CollisionInstance.Radius;
            }
            set
            {
                CollisionInstance.Radius = value;
            }
        }
        public int Atack = 1;
        public int FrameHitStart = 0;
        public float CollisionWallInstanceY
        {
            get
            {
                if (CollisionWallInstance.Parent == null)
                {
                    return CollisionWallInstance.Y;
                }
                else
                {
                    return CollisionWallInstance.RelativeY;
                }
            }
            set
            {
                if (CollisionWallInstance.Parent == null)
                {
                    CollisionWallInstance.Y = value;
                }
                else
                {
                    CollisionWallInstance.RelativeY = value;
                }
            }
        }
        public int Index { get; set; }
        public bool Used { get; set; }
        private FlatRedBall.Math.Geometry.ShapeCollection mGeneratedCollision;
        public FlatRedBall.Math.Geometry.ShapeCollection Collision
        {
            get
            {
                return mGeneratedCollision;
            }
        }
        protected FlatRedBall.Graphics.Layer LayerProvidedByContainer = null;
        public Projectiles () 
        	: this(FlatRedBall.Screens.ScreenManager.CurrentScreen.ContentManagerName, true)
        {
        }
        public Projectiles (string contentManagerName) 
        	: this(contentManagerName, true)
        {
        }
        public Projectiles (string contentManagerName, bool addToManagers) 
        	: base()
        {
            ContentManagerName = contentManagerName;
            InitializeEntity(addToManagers);
        }
        protected virtual void InitializeEntity (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            SoundFire = FireIgnite;
            SoundFlaunch = flaunch.CreateInstance();
            SoundArrow = WeaponBlow.CreateInstance();
            mSpriteInstance = new FlatRedBall.Sprite();
            mSpriteInstance.Name = "mSpriteInstance";
            mCollisionInstance = new FlatRedBall.Math.Geometry.Circle();
            mCollisionInstance.Name = "mCollisionInstance";
            mCollisionWallInstance = new FlatRedBall.Math.Geometry.Circle();
            mCollisionWallInstance.Name = "mCollisionWallInstance";
            
            PostInitialize();
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceXReset = SpriteInstance.X;
            }
            else
            {
                SpriteInstanceXReset = SpriteInstance.RelativeX;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceYReset = SpriteInstance.Y;
            }
            else
            {
                SpriteInstanceYReset = SpriteInstance.RelativeY;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceZReset = SpriteInstance.Z;
            }
            else
            {
                SpriteInstanceZReset = SpriteInstance.RelativeZ;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceXVelocityReset = SpriteInstance.XVelocity;
            }
            else
            {
                SpriteInstanceXVelocityReset = SpriteInstance.RelativeXVelocity;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceYVelocityReset = SpriteInstance.YVelocity;
            }
            else
            {
                SpriteInstanceYVelocityReset = SpriteInstance.RelativeYVelocity;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceZVelocityReset = SpriteInstance.ZVelocity;
            }
            else
            {
                SpriteInstanceZVelocityReset = SpriteInstance.RelativeZVelocity;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceRotationXReset = SpriteInstance.RotationX;
            }
            else
            {
                SpriteInstanceRotationXReset = SpriteInstance.RelativeRotationX;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceRotationYReset = SpriteInstance.RotationY;
            }
            else
            {
                SpriteInstanceRotationYReset = SpriteInstance.RelativeRotationY;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceRotationZReset = SpriteInstance.RotationZ;
            }
            else
            {
                SpriteInstanceRotationZReset = SpriteInstance.RelativeRotationZ;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceRotationXVelocityReset = SpriteInstance.RotationXVelocity;
            }
            else
            {
                SpriteInstanceRotationXVelocityReset = SpriteInstance.RelativeRotationXVelocity;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceRotationYVelocityReset = SpriteInstance.RotationYVelocity;
            }
            else
            {
                SpriteInstanceRotationYVelocityReset = SpriteInstance.RelativeRotationYVelocity;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceRotationZVelocityReset = SpriteInstance.RotationZVelocity;
            }
            else
            {
                SpriteInstanceRotationZVelocityReset = SpriteInstance.RelativeRotationZVelocity;
            }
            SpriteInstanceAlphaReset = SpriteInstance.Alpha;
            SpriteInstanceAlphaRateReset = SpriteInstance.AlphaRate;
            if (CollisionInstance.Parent == null)
            {
                CollisionInstanceXReset = CollisionInstance.X;
            }
            else
            {
                CollisionInstanceXReset = CollisionInstance.RelativeX;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstanceYReset = CollisionInstance.Y;
            }
            else
            {
                CollisionInstanceYReset = CollisionInstance.RelativeY;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstanceZReset = CollisionInstance.Z;
            }
            else
            {
                CollisionInstanceZReset = CollisionInstance.RelativeZ;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstanceXVelocityReset = CollisionInstance.XVelocity;
            }
            else
            {
                CollisionInstanceXVelocityReset = CollisionInstance.RelativeXVelocity;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstanceYVelocityReset = CollisionInstance.YVelocity;
            }
            else
            {
                CollisionInstanceYVelocityReset = CollisionInstance.RelativeYVelocity;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstanceZVelocityReset = CollisionInstance.ZVelocity;
            }
            else
            {
                CollisionInstanceZVelocityReset = CollisionInstance.RelativeZVelocity;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstanceRotationXReset = CollisionInstance.RotationX;
            }
            else
            {
                CollisionInstanceRotationXReset = CollisionInstance.RelativeRotationX;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstanceRotationYReset = CollisionInstance.RotationY;
            }
            else
            {
                CollisionInstanceRotationYReset = CollisionInstance.RelativeRotationY;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstanceRotationZReset = CollisionInstance.RotationZ;
            }
            else
            {
                CollisionInstanceRotationZReset = CollisionInstance.RelativeRotationZ;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstanceRotationXVelocityReset = CollisionInstance.RotationXVelocity;
            }
            else
            {
                CollisionInstanceRotationXVelocityReset = CollisionInstance.RelativeRotationXVelocity;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstanceRotationYVelocityReset = CollisionInstance.RotationYVelocity;
            }
            else
            {
                CollisionInstanceRotationYVelocityReset = CollisionInstance.RelativeRotationYVelocity;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstanceRotationZVelocityReset = CollisionInstance.RotationZVelocity;
            }
            else
            {
                CollisionInstanceRotationZVelocityReset = CollisionInstance.RelativeRotationZVelocity;
            }
            if (CollisionWallInstance.Parent == null)
            {
                CollisionWallInstanceXReset = CollisionWallInstance.X;
            }
            else
            {
                CollisionWallInstanceXReset = CollisionWallInstance.RelativeX;
            }
            if (CollisionWallInstance.Parent == null)
            {
                CollisionWallInstanceYReset = CollisionWallInstance.Y;
            }
            else
            {
                CollisionWallInstanceYReset = CollisionWallInstance.RelativeY;
            }
            if (CollisionWallInstance.Parent == null)
            {
                CollisionWallInstanceZReset = CollisionWallInstance.Z;
            }
            else
            {
                CollisionWallInstanceZReset = CollisionWallInstance.RelativeZ;
            }
            if (CollisionWallInstance.Parent == null)
            {
                CollisionWallInstanceXVelocityReset = CollisionWallInstance.XVelocity;
            }
            else
            {
                CollisionWallInstanceXVelocityReset = CollisionWallInstance.RelativeXVelocity;
            }
            if (CollisionWallInstance.Parent == null)
            {
                CollisionWallInstanceYVelocityReset = CollisionWallInstance.YVelocity;
            }
            else
            {
                CollisionWallInstanceYVelocityReset = CollisionWallInstance.RelativeYVelocity;
            }
            if (CollisionWallInstance.Parent == null)
            {
                CollisionWallInstanceZVelocityReset = CollisionWallInstance.ZVelocity;
            }
            else
            {
                CollisionWallInstanceZVelocityReset = CollisionWallInstance.RelativeZVelocity;
            }
            if (CollisionWallInstance.Parent == null)
            {
                CollisionWallInstanceRotationXReset = CollisionWallInstance.RotationX;
            }
            else
            {
                CollisionWallInstanceRotationXReset = CollisionWallInstance.RelativeRotationX;
            }
            if (CollisionWallInstance.Parent == null)
            {
                CollisionWallInstanceRotationYReset = CollisionWallInstance.RotationY;
            }
            else
            {
                CollisionWallInstanceRotationYReset = CollisionWallInstance.RelativeRotationY;
            }
            if (CollisionWallInstance.Parent == null)
            {
                CollisionWallInstanceRotationZReset = CollisionWallInstance.RotationZ;
            }
            else
            {
                CollisionWallInstanceRotationZReset = CollisionWallInstance.RelativeRotationZ;
            }
            if (CollisionWallInstance.Parent == null)
            {
                CollisionWallInstanceRotationXVelocityReset = CollisionWallInstance.RotationXVelocity;
            }
            else
            {
                CollisionWallInstanceRotationXVelocityReset = CollisionWallInstance.RelativeRotationXVelocity;
            }
            if (CollisionWallInstance.Parent == null)
            {
                CollisionWallInstanceRotationYVelocityReset = CollisionWallInstance.RotationYVelocity;
            }
            else
            {
                CollisionWallInstanceRotationYVelocityReset = CollisionWallInstance.RelativeRotationYVelocity;
            }
            if (CollisionWallInstance.Parent == null)
            {
                CollisionWallInstanceRotationZVelocityReset = CollisionWallInstance.RotationZVelocity;
            }
            else
            {
                CollisionWallInstanceRotationZVelocityReset = CollisionWallInstance.RelativeRotationZVelocity;
            }
            if (addToManagers)
            {
                AddToManagers(null);
            }
        }
        public virtual void ReAddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.SpriteManager.AddToLayer(mSpriteInstance, LayerProvidedByContainer);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mCollisionInstance, LayerProvidedByContainer);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mCollisionWallInstance, LayerProvidedByContainer);
        }
        public virtual void AddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.SpriteManager.AddToLayer(mSpriteInstance, LayerProvidedByContainer);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mCollisionInstance, LayerProvidedByContainer);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mCollisionWallInstance, LayerProvidedByContainer);
            AddToManagersBottomUp(layerToAddTo);
            CustomInitialize();
        }
        public virtual void Activity () 
        {
            
            CustomActivity();
        }
        public virtual void Destroy () 
        {
            if (Used)
            {
                Factories.ProjectilesFactory.MakeUnused(this, false);
            }
            FlatRedBall.SpriteManager.RemovePositionedObject(this);
            
            if (SpriteInstance != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(SpriteInstance);
            }
            if (CollisionInstance != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.RemoveOneWay(CollisionInstance);
            }
            if (CollisionWallInstance != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.RemoveOneWay(CollisionWallInstance);
            }
            mGeneratedCollision.RemoveFromManagers(clearThis: false);
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            if (mSpriteInstance.Parent == null)
            {
                mSpriteInstance.CopyAbsoluteToRelative();
                mSpriteInstance.AttachTo(this, false);
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.X = -1f;
            }
            else
            {
                SpriteInstance.RelativeX = -1f;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.Y = 0f;
            }
            else
            {
                SpriteInstance.RelativeY = 0f;
            }
            SpriteInstance.Texture = null;
            SpriteInstance.TextureScale = 1f;
            SpriteInstance.AnimationSpeed = 0.5f;
            SpriteInstance.Visible = true;
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.RotationZ = 0f;
            }
            else
            {
                SpriteInstance.RelativeRotationZ = 0f;
            }
            if (mCollisionInstance.Parent == null)
            {
                mCollisionInstance.CopyAbsoluteToRelative();
                mCollisionInstance.AttachTo(this, false);
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.X = 1f;
            }
            else
            {
                CollisionInstance.RelativeX = 1f;
            }
            CollisionInstance.Radius = 32f;
            CollisionInstance.Visible = false;
            CollisionInstance.Color = Color.Violet;
            if (mCollisionWallInstance.Parent == null)
            {
                mCollisionWallInstance.CopyAbsoluteToRelative();
                mCollisionWallInstance.AttachTo(this, false);
            }
            if (CollisionWallInstance.Parent == null)
            {
                CollisionWallInstance.X = 0f;
            }
            else
            {
                CollisionWallInstance.RelativeX = 0f;
            }
            CollisionWallInstance.Radius = 3f;
            CollisionWallInstance.Visible = false;
            CollisionWallInstance.Color = Color.Tomato;
            CollisionWallInstance.ParentRotationChangesPosition = false;
            mGeneratedCollision = new FlatRedBall.Math.Geometry.ShapeCollection();
            mGeneratedCollision.Circles.AddOneWay(mCollisionInstance);
            mGeneratedCollision.Circles.AddOneWay(mCollisionWallInstance);
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            if (SpriteInstance != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(SpriteInstance);
            }
            if (CollisionInstance != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.RemoveOneWay(CollisionInstance);
            }
            if (CollisionWallInstance != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.RemoveOneWay(CollisionWallInstance);
            }
            mGeneratedCollision.RemoveFromManagers(clearThis: false);
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.X = -1f;
            }
            else
            {
                SpriteInstance.RelativeX = -1f;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.Y = 0f;
            }
            else
            {
                SpriteInstance.RelativeY = 0f;
            }
            SpriteInstance.Texture = null;
            SpriteInstance.TextureScale = 1f;
            SpriteInstance.AnimationSpeed = 0.5f;
            SpriteInstance.Visible = true;
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.RotationZ = 0f;
            }
            else
            {
                SpriteInstance.RelativeRotationZ = 0f;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.X = SpriteInstanceXReset;
            }
            else
            {
                SpriteInstance.RelativeX = SpriteInstanceXReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.Y = SpriteInstanceYReset;
            }
            else
            {
                SpriteInstance.RelativeY = SpriteInstanceYReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.Z = SpriteInstanceZReset;
            }
            else
            {
                SpriteInstance.RelativeZ = SpriteInstanceZReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.XVelocity = SpriteInstanceXVelocityReset;
            }
            else
            {
                SpriteInstance.RelativeXVelocity = SpriteInstanceXVelocityReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.YVelocity = SpriteInstanceYVelocityReset;
            }
            else
            {
                SpriteInstance.RelativeYVelocity = SpriteInstanceYVelocityReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.ZVelocity = SpriteInstanceZVelocityReset;
            }
            else
            {
                SpriteInstance.RelativeZVelocity = SpriteInstanceZVelocityReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.RotationX = SpriteInstanceRotationXReset;
            }
            else
            {
                SpriteInstance.RelativeRotationX = SpriteInstanceRotationXReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.RotationY = SpriteInstanceRotationYReset;
            }
            else
            {
                SpriteInstance.RelativeRotationY = SpriteInstanceRotationYReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.RotationZ = SpriteInstanceRotationZReset;
            }
            else
            {
                SpriteInstance.RelativeRotationZ = SpriteInstanceRotationZReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.RotationXVelocity = SpriteInstanceRotationXVelocityReset;
            }
            else
            {
                SpriteInstance.RelativeRotationXVelocity = SpriteInstanceRotationXVelocityReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.RotationYVelocity = SpriteInstanceRotationYVelocityReset;
            }
            else
            {
                SpriteInstance.RelativeRotationYVelocity = SpriteInstanceRotationYVelocityReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.RotationZVelocity = SpriteInstanceRotationZVelocityReset;
            }
            else
            {
                SpriteInstance.RelativeRotationZVelocity = SpriteInstanceRotationZVelocityReset;
            }
            SpriteInstance.Alpha = SpriteInstanceAlphaReset;
            SpriteInstance.AlphaRate = SpriteInstanceAlphaRateReset;
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.X = 1f;
            }
            else
            {
                CollisionInstance.RelativeX = 1f;
            }
            CollisionInstance.Radius = 32f;
            CollisionInstance.Visible = false;
            CollisionInstance.Color = Color.Violet;
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.X = CollisionInstanceXReset;
            }
            else
            {
                CollisionInstance.RelativeX = CollisionInstanceXReset;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.Y = CollisionInstanceYReset;
            }
            else
            {
                CollisionInstance.RelativeY = CollisionInstanceYReset;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.Z = CollisionInstanceZReset;
            }
            else
            {
                CollisionInstance.RelativeZ = CollisionInstanceZReset;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.XVelocity = CollisionInstanceXVelocityReset;
            }
            else
            {
                CollisionInstance.RelativeXVelocity = CollisionInstanceXVelocityReset;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.YVelocity = CollisionInstanceYVelocityReset;
            }
            else
            {
                CollisionInstance.RelativeYVelocity = CollisionInstanceYVelocityReset;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.ZVelocity = CollisionInstanceZVelocityReset;
            }
            else
            {
                CollisionInstance.RelativeZVelocity = CollisionInstanceZVelocityReset;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.RotationX = CollisionInstanceRotationXReset;
            }
            else
            {
                CollisionInstance.RelativeRotationX = CollisionInstanceRotationXReset;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.RotationY = CollisionInstanceRotationYReset;
            }
            else
            {
                CollisionInstance.RelativeRotationY = CollisionInstanceRotationYReset;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.RotationZ = CollisionInstanceRotationZReset;
            }
            else
            {
                CollisionInstance.RelativeRotationZ = CollisionInstanceRotationZReset;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.RotationXVelocity = CollisionInstanceRotationXVelocityReset;
            }
            else
            {
                CollisionInstance.RelativeRotationXVelocity = CollisionInstanceRotationXVelocityReset;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.RotationYVelocity = CollisionInstanceRotationYVelocityReset;
            }
            else
            {
                CollisionInstance.RelativeRotationYVelocity = CollisionInstanceRotationYVelocityReset;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.RotationZVelocity = CollisionInstanceRotationZVelocityReset;
            }
            else
            {
                CollisionInstance.RelativeRotationZVelocity = CollisionInstanceRotationZVelocityReset;
            }
            if (CollisionWallInstance.Parent == null)
            {
                CollisionWallInstance.X = 0f;
            }
            else
            {
                CollisionWallInstance.RelativeX = 0f;
            }
            CollisionWallInstance.Radius = 3f;
            CollisionWallInstance.Visible = false;
            CollisionWallInstance.Color = Color.Tomato;
            CollisionWallInstance.ParentRotationChangesPosition = false;
            if (CollisionWallInstance.Parent == null)
            {
                CollisionWallInstance.X = CollisionWallInstanceXReset;
            }
            else
            {
                CollisionWallInstance.RelativeX = CollisionWallInstanceXReset;
            }
            if (CollisionWallInstance.Parent == null)
            {
                CollisionWallInstance.Y = CollisionWallInstanceYReset;
            }
            else
            {
                CollisionWallInstance.RelativeY = CollisionWallInstanceYReset;
            }
            if (CollisionWallInstance.Parent == null)
            {
                CollisionWallInstance.Z = CollisionWallInstanceZReset;
            }
            else
            {
                CollisionWallInstance.RelativeZ = CollisionWallInstanceZReset;
            }
            if (CollisionWallInstance.Parent == null)
            {
                CollisionWallInstance.XVelocity = CollisionWallInstanceXVelocityReset;
            }
            else
            {
                CollisionWallInstance.RelativeXVelocity = CollisionWallInstanceXVelocityReset;
            }
            if (CollisionWallInstance.Parent == null)
            {
                CollisionWallInstance.YVelocity = CollisionWallInstanceYVelocityReset;
            }
            else
            {
                CollisionWallInstance.RelativeYVelocity = CollisionWallInstanceYVelocityReset;
            }
            if (CollisionWallInstance.Parent == null)
            {
                CollisionWallInstance.ZVelocity = CollisionWallInstanceZVelocityReset;
            }
            else
            {
                CollisionWallInstance.RelativeZVelocity = CollisionWallInstanceZVelocityReset;
            }
            if (CollisionWallInstance.Parent == null)
            {
                CollisionWallInstance.RotationX = CollisionWallInstanceRotationXReset;
            }
            else
            {
                CollisionWallInstance.RelativeRotationX = CollisionWallInstanceRotationXReset;
            }
            if (CollisionWallInstance.Parent == null)
            {
                CollisionWallInstance.RotationY = CollisionWallInstanceRotationYReset;
            }
            else
            {
                CollisionWallInstance.RelativeRotationY = CollisionWallInstanceRotationYReset;
            }
            if (CollisionWallInstance.Parent == null)
            {
                CollisionWallInstance.RotationZ = CollisionWallInstanceRotationZReset;
            }
            else
            {
                CollisionWallInstance.RelativeRotationZ = CollisionWallInstanceRotationZReset;
            }
            if (CollisionWallInstance.Parent == null)
            {
                CollisionWallInstance.RotationXVelocity = CollisionWallInstanceRotationXVelocityReset;
            }
            else
            {
                CollisionWallInstance.RelativeRotationXVelocity = CollisionWallInstanceRotationXVelocityReset;
            }
            if (CollisionWallInstance.Parent == null)
            {
                CollisionWallInstance.RotationYVelocity = CollisionWallInstanceRotationYVelocityReset;
            }
            else
            {
                CollisionWallInstance.RelativeRotationYVelocity = CollisionWallInstanceRotationYVelocityReset;
            }
            if (CollisionWallInstance.Parent == null)
            {
                CollisionWallInstance.RotationZVelocity = CollisionWallInstanceRotationZVelocityReset;
            }
            else
            {
                CollisionWallInstance.RelativeRotationZVelocity = CollisionWallInstanceRotationZVelocityReset;
            }
            if (Parent == null)
            {
                Z = 25f;
            }
            else if (Parent is FlatRedBall.Camera)
            {
                RelativeZ = 25f - 40.0f;
            }
            else
            {
                RelativeZ = 25f;
            }
            MovementSpeed = 500;
            SpriteInstanceTextureScale = 1f;
            SpriteInstanceAnimationSpeed = 0.5f;
            CollisionInstanceX = 0f;
            CollisionInstanceY = 0f;
            CollisionInstanceRadius = 32f;
            Atack = 1;
            FrameHitStart = 0;
            CollisionWallInstanceY = -8f;
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            this.ForceUpdateDependenciesDeep();
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(SpriteInstance);
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            ContentManagerName = contentManagerName;
            // Set the content manager for Gum
            var contentManagerWrapper = new FlatRedBall.Gum.ContentManagerWrapper();
            contentManagerWrapper.ContentManagerName = contentManagerName;
            RenderingLibrary.Content.LoaderManager.Self.ContentLoader = contentManagerWrapper;
            // Access the GumProject just in case it's async loaded
            var throwaway = GlobalContent.GumProject;
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            bool registerUnload = false;
            if (LoadedContentManagers.Contains(contentManagerName) == false)
            {
                LoadedContentManagers.Add(contentManagerName);
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("ProjectilesStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/item/fire_1.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                Fire_1 = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/item/fire_1.png", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/projectiles/fire_9.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                Fire_9 = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/projectiles/fire_9.png", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/projectiles/blue1_b.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                Blue1_b = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/projectiles/blue1_b.png", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/entities/projectiles/animationchainlistfile.achx", ContentManagerName))
                {
                    registerUnload = true;
                }
                AnimationChainListFile = FlatRedBall.FlatRedBallServices.Load<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/entities/projectiles/animationchainlistfile.achx", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/projectiles/arrow.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                arrow = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/projectiles/arrow.png", ContentManagerName);
                FireIgnite = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Audio.SoundEffect>(@"content/entities/projectiles/fireignite", ContentManagerName).CreateInstance();
                flaunch = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Audio.SoundEffect>(@"content/entities/projectiles/flaunch", ContentManagerName);
                WeaponBlow = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Audio.SoundEffect>(@"content/entities/projectiles/weaponblow", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/projectiles/fxsword.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                fxSword = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/projectiles/fxsword.png", ContentManagerName);
            }
            if (registerUnload && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("ProjectilesStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            CustomLoadStaticContent(contentManagerName);
        }
        public static void UnloadStaticContent () 
        {
            if (LoadedContentManagers.Count != 0)
            {
                LoadedContentManagers.RemoveAt(0);
                mRegisteredUnloads.RemoveAt(0);
            }
            if (LoadedContentManagers.Count == 0)
            {
                if (Fire_1 != null)
                {
                    Fire_1= null;
                }
                if (Fire_9 != null)
                {
                    Fire_9= null;
                }
                if (Blue1_b != null)
                {
                    Blue1_b= null;
                }
                if (AnimationChainListFile != null)
                {
                    AnimationChainListFile= null;
                }
                if (arrow != null)
                {
                    arrow= null;
                }
                if (FireIgnite != null)
                {
                    if(FireIgnite.IsDisposed == false) { FireIgnite.Stop();  FireIgnite.Dispose(); };
                    FireIgnite= null;
                }
                if (flaunch != null)
                {
                    flaunch= null;
                }
                if (WeaponBlow != null)
                {
                    WeaponBlow= null;
                }
                if (fxSword != null)
                {
                    fxSword= null;
                }
            }
        }
        static VariableState mLoadingState = VariableState.Uninitialized;
        public static VariableState LoadingState
        {
            get
            {
                return mLoadingState;
            }
            set
            {
                mLoadingState = value;
            }
        }
        public FlatRedBall.Instructions.Instruction InterpolateToState (VariableState stateToInterpolateTo, double secondsToTake) 
        {
            switch(stateToInterpolateTo)
            {
                case  VariableState.FireBall:
                    CollisionInstance.RadiusVelocity = (10f - CollisionInstance.Radius) / (float)secondsToTake;
                    if (CollisionWallInstance.Parent != null)
                    {
                        CollisionWallInstance.RelativeYVelocity = (-10f - CollisionWallInstance.RelativeY) / (float)secondsToTake;
                    }
                    else
                    {
                        CollisionWallInstance.YVelocity = (-10f - CollisionWallInstance.Y) / (float)secondsToTake;
                    }
                    break;
                case  VariableState.AtackWater1:
                    if (this.Parent != null)
                    {
                        RelativeYVelocity = (0f - RelativeY) / (float)secondsToTake;
                    }
                    else
                    {
                        YVelocity = (0f - Y) / (float)secondsToTake;
                    }
                    CollisionInstance.RadiusVelocity = (30f - CollisionInstance.Radius) / (float)secondsToTake;
                    break;
                case  VariableState.Arrow:
                    if (this.Parent != null)
                    {
                        RelativeYVelocity = (-8f - RelativeY) / (float)secondsToTake;
                    }
                    else
                    {
                        YVelocity = (-8f - Y) / (float)secondsToTake;
                    }
                    if (CollisionInstance.Parent != null)
                    {
                        CollisionInstance.RelativeYVelocity = (0f - CollisionInstance.RelativeY) / (float)secondsToTake;
                    }
                    else
                    {
                        CollisionInstance.YVelocity = (0f - CollisionInstance.Y) / (float)secondsToTake;
                    }
                    CollisionInstance.RadiusVelocity = (10f - CollisionInstance.Radius) / (float)secondsToTake;
                    break;
                case  VariableState.SwordAtack:
                    if (CollisionInstance.Parent != null)
                    {
                        CollisionInstance.RelativeYVelocity = (10f - CollisionInstance.RelativeY) / (float)secondsToTake;
                    }
                    else
                    {
                        CollisionInstance.YVelocity = (10f - CollisionInstance.Y) / (float)secondsToTake;
                    }
                    CollisionInstance.RadiusVelocity = (16f - CollisionInstance.Radius) / (float)secondsToTake;
                    if (CollisionWallInstance.Parent != null)
                    {
                        CollisionWallInstance.RelativeYVelocity = (0f - CollisionWallInstance.RelativeY) / (float)secondsToTake;
                    }
                    else
                    {
                        CollisionWallInstance.YVelocity = (0f - CollisionWallInstance.Y) / (float)secondsToTake;
                    }
                    break;
            }
            var instruction = new FlatRedBall.Instructions.DelegateInstruction<VariableState>(StopStateInterpolation, stateToInterpolateTo);
            instruction.TimeToExecute = FlatRedBall.TimeManager.CurrentTime + secondsToTake;
            this.Instructions.Add(instruction);
            return instruction;
        }
        public void StopStateInterpolation (VariableState stateToStop) 
        {
            switch(stateToStop)
            {
                case  VariableState.FireBall:
                    CollisionInstance.RadiusVelocity =  0;
                    if (CollisionWallInstance.Parent != null)
                    {
                        CollisionWallInstance.RelativeYVelocity =  0;
                    }
                    else
                    {
                        CollisionWallInstance.YVelocity =  0;
                    }
                    break;
                case  VariableState.AtackWater1:
                    if (this.Parent != null)
                    {
                        RelativeYVelocity =  0;
                    }
                    else
                    {
                        YVelocity =  0;
                    }
                    CollisionInstance.RadiusVelocity =  0;
                    break;
                case  VariableState.Arrow:
                    if (this.Parent != null)
                    {
                        RelativeYVelocity =  0;
                    }
                    else
                    {
                        YVelocity =  0;
                    }
                    if (CollisionInstance.Parent != null)
                    {
                        CollisionInstance.RelativeYVelocity =  0;
                    }
                    else
                    {
                        CollisionInstance.YVelocity =  0;
                    }
                    CollisionInstance.RadiusVelocity =  0;
                    break;
                case  VariableState.SwordAtack:
                    if (CollisionInstance.Parent != null)
                    {
                        CollisionInstance.RelativeYVelocity =  0;
                    }
                    else
                    {
                        CollisionInstance.YVelocity =  0;
                    }
                    CollisionInstance.RadiusVelocity =  0;
                    if (CollisionWallInstance.Parent != null)
                    {
                        CollisionWallInstance.RelativeYVelocity =  0;
                    }
                    else
                    {
                        CollisionWallInstance.YVelocity =  0;
                    }
                    break;
            }
            CurrentState = stateToStop;
        }
        public void InterpolateBetween (VariableState firstState, VariableState secondState, float interpolationValue) 
        {
            #if DEBUG
            if (float.IsNaN(interpolationValue))
            {
                throw new System.Exception("interpolationValue cannot be NaN");
            }
            #endif
            bool setSpriteInstanceTextureScale = true;
            float SpriteInstanceTextureScaleFirstValue= 0;
            float SpriteInstanceTextureScaleSecondValue= 0;
            bool setCollisionInstanceRadius = true;
            float CollisionInstanceRadiusFirstValue= 0;
            float CollisionInstanceRadiusSecondValue= 0;
            bool setAtack = true;
            int AtackFirstValue= 0;
            int AtackSecondValue= 0;
            bool setCollisionWallInstanceY = true;
            float CollisionWallInstanceYFirstValue= 0;
            float CollisionWallInstanceYSecondValue= 0;
            bool setY = true;
            float YFirstValue= 0;
            float YSecondValue= 0;
            bool setFrameHitStart = true;
            int FrameHitStartFirstValue= 0;
            int FrameHitStartSecondValue= 0;
            bool setMovementSpeed = true;
            int MovementSpeedFirstValue= 0;
            int MovementSpeedSecondValue= 0;
            bool setCollisionInstanceY = true;
            float CollisionInstanceYFirstValue= 0;
            float CollisionInstanceYSecondValue= 0;
            switch(firstState)
            {
                case  VariableState.FireBall:
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceTexture = Fire_1;
                    }
                    if (interpolationValue < 1)
                    {
                        this.AnimationChains = AnimationChainListFile;
                    }
                    if (interpolationValue < 1)
                    {
                        this.CurrentChainName = "FireBall_Default";
                    }
                    SpriteInstanceTextureScaleFirstValue = 0.5f;
                    CollisionInstanceRadiusFirstValue = 10f;
                    AtackFirstValue = 1;
                    CollisionWallInstanceYFirstValue = -10f;
                    break;
                case  VariableState.AtackWater1:
                    YFirstValue = 0f;
                    if (interpolationValue < 1)
                    {
                        this.AnimationChains = AnimationChainListFile;
                    }
                    if (interpolationValue < 1)
                    {
                        this.CurrentChainName = "AtackWater1_Default";
                    }
                    SpriteInstanceTextureScaleFirstValue = 1f;
                    CollisionInstanceRadiusFirstValue = 30f;
                    AtackFirstValue = 2;
                    FrameHitStartFirstValue = 2;
                    break;
                case  VariableState.Arrow:
                    YFirstValue = -8f;
                    MovementSpeedFirstValue = 350;
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceTexture = arrow;
                    }
                    if (interpolationValue < 1)
                    {
                        this.AnimationChains = AnimationChainListFile;
                    }
                    if (interpolationValue < 1)
                    {
                        this.CurrentChainName = "Arrow_Default";
                    }
                    SpriteInstanceTextureScaleFirstValue = 0.5f;
                    if (interpolationValue < 1)
                    {
                        this.isPlayAtack = false;
                    }
                    CollisionInstanceYFirstValue = 0f;
                    CollisionInstanceRadiusFirstValue = 10f;
                    AtackFirstValue = 1;
                    break;
                case  VariableState.SwordAtack:
                    MovementSpeedFirstValue = 500;
                    if (interpolationValue < 1)
                    {
                        this.AnimationChains = AnimationChainListFile;
                    }
                    if (interpolationValue < 1)
                    {
                        this.CurrentChainName = "Sword_Defaualt";
                    }
                    if (interpolationValue < 1)
                    {
                        this.isPlayAtack = false;
                    }
                    CollisionInstanceYFirstValue = 10f;
                    CollisionInstanceRadiusFirstValue = 16f;
                    AtackFirstValue = 1;
                    FrameHitStartFirstValue = 0;
                    CollisionWallInstanceYFirstValue = 0f;
                    break;
            }
            switch(secondState)
            {
                case  VariableState.FireBall:
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceTexture = Fire_1;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.AnimationChains = AnimationChainListFile;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.CurrentChainName = "FireBall_Default";
                    }
                    SpriteInstanceTextureScaleSecondValue = 0.5f;
                    CollisionInstanceRadiusSecondValue = 10f;
                    AtackSecondValue = 1;
                    CollisionWallInstanceYSecondValue = -10f;
                    break;
                case  VariableState.AtackWater1:
                    YSecondValue = 0f;
                    if (interpolationValue >= 1)
                    {
                        this.AnimationChains = AnimationChainListFile;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.CurrentChainName = "AtackWater1_Default";
                    }
                    SpriteInstanceTextureScaleSecondValue = 1f;
                    CollisionInstanceRadiusSecondValue = 30f;
                    AtackSecondValue = 2;
                    FrameHitStartSecondValue = 2;
                    break;
                case  VariableState.Arrow:
                    YSecondValue = -8f;
                    MovementSpeedSecondValue = 350;
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceTexture = arrow;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.AnimationChains = AnimationChainListFile;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.CurrentChainName = "Arrow_Default";
                    }
                    SpriteInstanceTextureScaleSecondValue = 0.5f;
                    if (interpolationValue >= 1)
                    {
                        this.isPlayAtack = false;
                    }
                    CollisionInstanceYSecondValue = 0f;
                    CollisionInstanceRadiusSecondValue = 10f;
                    AtackSecondValue = 1;
                    break;
                case  VariableState.SwordAtack:
                    MovementSpeedSecondValue = 500;
                    if (interpolationValue >= 1)
                    {
                        this.AnimationChains = AnimationChainListFile;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.CurrentChainName = "Sword_Defaualt";
                    }
                    if (interpolationValue >= 1)
                    {
                        this.isPlayAtack = false;
                    }
                    CollisionInstanceYSecondValue = 10f;
                    CollisionInstanceRadiusSecondValue = 16f;
                    AtackSecondValue = 1;
                    FrameHitStartSecondValue = 0;
                    CollisionWallInstanceYSecondValue = 0f;
                    break;
            }
            if (setSpriteInstanceTextureScale)
            {
                SpriteInstanceTextureScale = SpriteInstanceTextureScaleFirstValue * (1 - interpolationValue) + SpriteInstanceTextureScaleSecondValue * interpolationValue;
            }
            if (setCollisionInstanceRadius)
            {
                CollisionInstanceRadius = CollisionInstanceRadiusFirstValue * (1 - interpolationValue) + CollisionInstanceRadiusSecondValue * interpolationValue;
            }
            if (setAtack)
            {
                Atack = FlatRedBall.Math.MathFunctions.RoundToInt(AtackFirstValue* (1 - interpolationValue) + AtackSecondValue * interpolationValue);
            }
            if (setCollisionWallInstanceY)
            {
                if (CollisionWallInstance.Parent != null)
                {
                    CollisionWallInstance.RelativeY = CollisionWallInstanceYFirstValue * (1 - interpolationValue) + CollisionWallInstanceYSecondValue * interpolationValue;
                }
                else
                {
                    CollisionWallInstanceY = CollisionWallInstanceYFirstValue * (1 - interpolationValue) + CollisionWallInstanceYSecondValue * interpolationValue;
                }
                if (setY)
                {
                    if (this.Parent != null)
                    {
                        RelativeY = YFirstValue * (1 - interpolationValue) + YSecondValue * interpolationValue;
                    }
                    else
                    {
                        Y = YFirstValue * (1 - interpolationValue) + YSecondValue * interpolationValue;
                    }
                }
                if (setFrameHitStart)
                {
                    FrameHitStart = FlatRedBall.Math.MathFunctions.RoundToInt(FrameHitStartFirstValue* (1 - interpolationValue) + FrameHitStartSecondValue * interpolationValue);
                }
                if (setMovementSpeed)
                {
                    MovementSpeed = FlatRedBall.Math.MathFunctions.RoundToInt(MovementSpeedFirstValue* (1 - interpolationValue) + MovementSpeedSecondValue * interpolationValue);
                }
                if (setCollisionInstanceY)
                {
                    if (CollisionInstance.Parent != null)
                    {
                        CollisionInstance.RelativeY = CollisionInstanceYFirstValue * (1 - interpolationValue) + CollisionInstanceYSecondValue * interpolationValue;
                    }
                    else
                    {
                        CollisionInstanceY = CollisionInstanceYFirstValue * (1 - interpolationValue) + CollisionInstanceYSecondValue * interpolationValue;
                    }
                    if (interpolationValue < 1)
                    {
                        mCurrentState = (int)firstState;
                    }
                    else
                    {
                        mCurrentState = (int)secondState;
                    }
                }
            }
        }
        public static void PreloadStateContent (VariableState state, string contentManagerName) 
        {
            ContentManagerName = contentManagerName;
            switch(state)
            {
                case  VariableState.FireBall:
                    {
                        object throwaway = Fire_1;
                    }
                    {
                        object throwaway = AnimationChainListFile;
                    }
                    {
                        object throwaway = "FireBall_Default";
                    }
                    break;
                case  VariableState.AtackWater1:
                    {
                        object throwaway = AnimationChainListFile;
                    }
                    {
                        object throwaway = "AtackWater1_Default";
                    }
                    break;
                case  VariableState.Arrow:
                    {
                        object throwaway = arrow;
                    }
                    {
                        object throwaway = AnimationChainListFile;
                    }
                    {
                        object throwaway = "Arrow_Default";
                    }
                    break;
                case  VariableState.SwordAtack:
                    {
                        object throwaway = AnimationChainListFile;
                    }
                    {
                        object throwaway = "Sword_Defaualt";
                    }
                    break;
            }
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            switch(memberName)
            {
                case  "Fire_1":
                    return Fire_1;
                case  "Fire_9":
                    return Fire_9;
                case  "Blue1_b":
                    return Blue1_b;
                case  "AnimationChainListFile":
                    return AnimationChainListFile;
                case  "arrow":
                    return arrow;
                case  "FireIgnite":
                    return FireIgnite;
                case  "flaunch":
                    return flaunch;
                case  "WeaponBlow":
                    return WeaponBlow;
                case  "fxSword":
                    return fxSword;
            }
            return null;
        }
        public static object GetFile (string memberName) 
        {
            switch(memberName)
            {
                case  "Fire_1":
                    return Fire_1;
                case  "Fire_9":
                    return Fire_9;
                case  "Blue1_b":
                    return Blue1_b;
                case  "AnimationChainListFile":
                    return AnimationChainListFile;
                case  "arrow":
                    return arrow;
                case  "FireIgnite":
                    return FireIgnite;
                case  "flaunch":
                    return flaunch;
                case  "WeaponBlow":
                    return WeaponBlow;
                case  "fxSword":
                    return fxSword;
            }
            return null;
        }
        object GetMember (string memberName) 
        {
            switch(memberName)
            {
                case  "Fire_1":
                    return Fire_1;
                case  "Fire_9":
                    return Fire_9;
                case  "Blue1_b":
                    return Blue1_b;
                case  "AnimationChainListFile":
                    return AnimationChainListFile;
                case  "arrow":
                    return arrow;
                case  "FireIgnite":
                    return FireIgnite;
                case  "flaunch":
                    return flaunch;
                case  "WeaponBlow":
                    return WeaponBlow;
                case  "fxSword":
                    return fxSword;
            }
            return null;
        }
        protected bool mIsPaused;
        public override void Pause (FlatRedBall.Instructions.InstructionList instructions) 
        {
            base.Pause(instructions);
            mIsPaused = true;
            if (SoundFire.State == Microsoft.Xna.Framework.Audio.SoundState.Playing)
            {
                SoundFire.Pause();
                instructions.Add(new FlatRedBall.Instructions.DelegateInstruction(() => SoundFire.Resume()));
            }
            if (SoundFlaunch.State == Microsoft.Xna.Framework.Audio.SoundState.Playing)
            {
                SoundFlaunch.Pause();
                instructions.Add(new FlatRedBall.Instructions.DelegateInstruction(() => SoundFlaunch.Resume()));
            }
            if (SoundArrow.State == Microsoft.Xna.Framework.Audio.SoundState.Playing)
            {
                SoundArrow.Pause();
                instructions.Add(new FlatRedBall.Instructions.DelegateInstruction(() => SoundArrow.Resume()));
            }
        }
        public virtual void SetToIgnorePausing () 
        {
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(this);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(SpriteInstance);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(CollisionInstance);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(CollisionWallInstance);
        }
        public virtual void MoveToLayer (FlatRedBall.Graphics.Layer layerToMoveTo) 
        {
            var layerToRemoveFrom = LayerProvidedByContainer;
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(SpriteInstance);
            }
            FlatRedBall.SpriteManager.AddToLayer(SpriteInstance, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(CollisionInstance);
            }
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(CollisionInstance, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(CollisionWallInstance);
            }
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(CollisionWallInstance, layerToMoveTo);
            LayerProvidedByContainer = layerToMoveTo;
        }
    }
}
