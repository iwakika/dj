#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using DungeonRun.Screens;
using FlatRedBall.Graphics;
using FlatRedBall.Math;
using DungeonRun.Performance;
using DungeonRun.Entities;
using DungeonRun.Factories;
using FlatRedBall;
using FlatRedBall.Screens;
using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall.Math.Geometry;
namespace DungeonRun.Entities
{
    public partial class HitMelee : FlatRedBall.PositionedObject, FlatRedBall.Graphics.IDestroyable, FlatRedBall.Performance.IPoolable, FlatRedBall.Math.Geometry.ICollidable
    {
        // This is made static so that static lazy-loaded content can access it.
        public static string ContentManagerName { get; set; }
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        public enum VariableState
        {
            Uninitialized = 0, //This exists so that the first set call actually does something
            Unknown = 1, //This exists so that if the entity is actually a child entity and has set a child state, you will get this
            bite = 2, 
            simples = 3, 
            groundShock = 4, 
            sword = 5
        }
        protected int mCurrentState = 0;
        public Entities.HitMelee.VariableState CurrentState
        {
            get
            {
                if (mCurrentState >= 0 && mCurrentState <= 5)
                {
                    return (VariableState)mCurrentState;
                }
                else
                {
                    return VariableState.Unknown;
                }
            }
            set
            {
                mCurrentState = (int)value;
                switch(CurrentState)
                {
                    case  VariableState.Uninitialized:
                        break;
                    case  VariableState.Unknown:
                        break;
                    case  VariableState.bite:
                        SpriteInstanceAnimationChains = Animation;
                        isPlayAtack = false;
                        autoDestroy = true;
                        SpriteInstanceCurrentChainName = "AtackBite";
                        break;
                    case  VariableState.simples:
                        SpriteInstanceAnimationChains = null;
                        break;
                    case  VariableState.groundShock:
                        SpriteInstanceAnimationChains = Animation;
                        isPlayAtack = false;
                        autoDestroy = true;
                        SpriteInstanceCurrentChainName = "AtackShock";
                        SpriteInstanceTextureScale = 3f;
                        break;
                    case  VariableState.sword:
                        SpriteInstanceAnimationChains = Animation;
                        isPlayAtack = false;
                        autoDestroy = true;
                        SpriteInstanceCurrentChainName = "AtackSword";
                        SpriteInstanceTextureScale = 0.3f;
                        break;
                }
            }
        }
        static object mLockObject = new object();
        static System.Collections.Generic.List<string> mRegisteredUnloads = new System.Collections.Generic.List<string>();
        static System.Collections.Generic.List<string> LoadedContentManagers = new System.Collections.Generic.List<string>();
        protected static Microsoft.Xna.Framework.Graphics.Texture2D Fx16;
        protected static FlatRedBall.Graphics.Animation.AnimationChainList Animation;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D FX8;
        protected static Microsoft.Xna.Framework.Audio.SoundEffectInstance MonsterSound;
        protected static Microsoft.Xna.Framework.Audio.SoundEffectInstance swing;
        
        private FlatRedBall.Sprite mSpriteInstance;
        public FlatRedBall.Sprite SpriteInstance
        {
            get
            {
                return mSpriteInstance;
            }
            private set
            {
                mSpriteInstance = value;
            }
        }
        static float SpriteInstanceXReset;
        static float SpriteInstanceYReset;
        static float SpriteInstanceZReset;
        static float SpriteInstanceXVelocityReset;
        static float SpriteInstanceYVelocityReset;
        static float SpriteInstanceZVelocityReset;
        static float SpriteInstanceRotationXReset;
        static float SpriteInstanceRotationYReset;
        static float SpriteInstanceRotationZReset;
        static float SpriteInstanceRotationXVelocityReset;
        static float SpriteInstanceRotationYVelocityReset;
        static float SpriteInstanceRotationZVelocityReset;
        static float SpriteInstanceAlphaReset;
        static float SpriteInstanceAlphaRateReset;
        private FlatRedBall.Math.Geometry.AxisAlignedRectangle mHitCollision;
        public FlatRedBall.Math.Geometry.AxisAlignedRectangle HitCollision
        {
            get
            {
                return mHitCollision;
            }
            private set
            {
                mHitCollision = value;
            }
        }
        static float HitCollisionXReset;
        static float HitCollisionYReset;
        static float HitCollisionZReset;
        static float HitCollisionXVelocityReset;
        static float HitCollisionYVelocityReset;
        static float HitCollisionZVelocityReset;
        static float HitCollisionRotationXReset;
        static float HitCollisionRotationYReset;
        static float HitCollisionRotationZReset;
        static float HitCollisionRotationXVelocityReset;
        static float HitCollisionRotationYVelocityReset;
        static float HitCollisionRotationZVelocityReset;
        private Microsoft.Xna.Framework.Audio.SoundEffectInstance SoundMonster;
        private Microsoft.Xna.Framework.Audio.SoundEffectInstance SoundSword;
        public FlatRedBall.Graphics.Animation.AnimationChainList SpriteInstanceAnimationChains
        {
            get
            {
                return SpriteInstance.AnimationChains;
            }
            set
            {
                SpriteInstance.AnimationChains = value;
            }
        }
        public bool isPlayAtack;
        public bool autoDestroy;
        public string SpriteInstanceCurrentChainName
        {
            get
            {
                return SpriteInstance.CurrentChainName;
            }
            set
            {
                SpriteInstance.CurrentChainName = value;
            }
        }
        public float SpriteInstanceTextureScale
        {
            get
            {
                return SpriteInstance.TextureScale;
            }
            set
            {
                SpriteInstance.TextureScale = value;
            }
        }
        public int Index { get; set; }
        public bool Used { get; set; }
        private FlatRedBall.Math.Geometry.ShapeCollection mGeneratedCollision;
        public FlatRedBall.Math.Geometry.ShapeCollection Collision
        {
            get
            {
                return mGeneratedCollision;
            }
        }
        protected FlatRedBall.Graphics.Layer LayerProvidedByContainer = null;
        public HitMelee () 
        	: this(FlatRedBall.Screens.ScreenManager.CurrentScreen.ContentManagerName, true)
        {
        }
        public HitMelee (string contentManagerName) 
        	: this(contentManagerName, true)
        {
        }
        public HitMelee (string contentManagerName, bool addToManagers) 
        	: base()
        {
            ContentManagerName = contentManagerName;
            InitializeEntity(addToManagers);
        }
        protected virtual void InitializeEntity (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            SoundMonster = MonsterSound;
            SoundSword = swing;
            mSpriteInstance = new FlatRedBall.Sprite();
            mSpriteInstance.Name = "mSpriteInstance";
            mHitCollision = new FlatRedBall.Math.Geometry.AxisAlignedRectangle();
            mHitCollision.Name = "mHitCollision";
            
            PostInitialize();
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceXReset = SpriteInstance.X;
            }
            else
            {
                SpriteInstanceXReset = SpriteInstance.RelativeX;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceYReset = SpriteInstance.Y;
            }
            else
            {
                SpriteInstanceYReset = SpriteInstance.RelativeY;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceZReset = SpriteInstance.Z;
            }
            else
            {
                SpriteInstanceZReset = SpriteInstance.RelativeZ;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceXVelocityReset = SpriteInstance.XVelocity;
            }
            else
            {
                SpriteInstanceXVelocityReset = SpriteInstance.RelativeXVelocity;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceYVelocityReset = SpriteInstance.YVelocity;
            }
            else
            {
                SpriteInstanceYVelocityReset = SpriteInstance.RelativeYVelocity;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceZVelocityReset = SpriteInstance.ZVelocity;
            }
            else
            {
                SpriteInstanceZVelocityReset = SpriteInstance.RelativeZVelocity;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceRotationXReset = SpriteInstance.RotationX;
            }
            else
            {
                SpriteInstanceRotationXReset = SpriteInstance.RelativeRotationX;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceRotationYReset = SpriteInstance.RotationY;
            }
            else
            {
                SpriteInstanceRotationYReset = SpriteInstance.RelativeRotationY;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceRotationZReset = SpriteInstance.RotationZ;
            }
            else
            {
                SpriteInstanceRotationZReset = SpriteInstance.RelativeRotationZ;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceRotationXVelocityReset = SpriteInstance.RotationXVelocity;
            }
            else
            {
                SpriteInstanceRotationXVelocityReset = SpriteInstance.RelativeRotationXVelocity;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceRotationYVelocityReset = SpriteInstance.RotationYVelocity;
            }
            else
            {
                SpriteInstanceRotationYVelocityReset = SpriteInstance.RelativeRotationYVelocity;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstanceRotationZVelocityReset = SpriteInstance.RotationZVelocity;
            }
            else
            {
                SpriteInstanceRotationZVelocityReset = SpriteInstance.RelativeRotationZVelocity;
            }
            SpriteInstanceAlphaReset = SpriteInstance.Alpha;
            SpriteInstanceAlphaRateReset = SpriteInstance.AlphaRate;
            if (HitCollision.Parent == null)
            {
                HitCollisionXReset = HitCollision.X;
            }
            else
            {
                HitCollisionXReset = HitCollision.RelativeX;
            }
            if (HitCollision.Parent == null)
            {
                HitCollisionYReset = HitCollision.Y;
            }
            else
            {
                HitCollisionYReset = HitCollision.RelativeY;
            }
            if (HitCollision.Parent == null)
            {
                HitCollisionZReset = HitCollision.Z;
            }
            else
            {
                HitCollisionZReset = HitCollision.RelativeZ;
            }
            if (HitCollision.Parent == null)
            {
                HitCollisionXVelocityReset = HitCollision.XVelocity;
            }
            else
            {
                HitCollisionXVelocityReset = HitCollision.RelativeXVelocity;
            }
            if (HitCollision.Parent == null)
            {
                HitCollisionYVelocityReset = HitCollision.YVelocity;
            }
            else
            {
                HitCollisionYVelocityReset = HitCollision.RelativeYVelocity;
            }
            if (HitCollision.Parent == null)
            {
                HitCollisionZVelocityReset = HitCollision.ZVelocity;
            }
            else
            {
                HitCollisionZVelocityReset = HitCollision.RelativeZVelocity;
            }
            if (HitCollision.Parent == null)
            {
                HitCollisionRotationXReset = HitCollision.RotationX;
            }
            else
            {
                HitCollisionRotationXReset = HitCollision.RelativeRotationX;
            }
            if (HitCollision.Parent == null)
            {
                HitCollisionRotationYReset = HitCollision.RotationY;
            }
            else
            {
                HitCollisionRotationYReset = HitCollision.RelativeRotationY;
            }
            if (HitCollision.Parent == null)
            {
                HitCollisionRotationZReset = HitCollision.RotationZ;
            }
            else
            {
                HitCollisionRotationZReset = HitCollision.RelativeRotationZ;
            }
            if (HitCollision.Parent == null)
            {
                HitCollisionRotationXVelocityReset = HitCollision.RotationXVelocity;
            }
            else
            {
                HitCollisionRotationXVelocityReset = HitCollision.RelativeRotationXVelocity;
            }
            if (HitCollision.Parent == null)
            {
                HitCollisionRotationYVelocityReset = HitCollision.RotationYVelocity;
            }
            else
            {
                HitCollisionRotationYVelocityReset = HitCollision.RelativeRotationYVelocity;
            }
            if (HitCollision.Parent == null)
            {
                HitCollisionRotationZVelocityReset = HitCollision.RotationZVelocity;
            }
            else
            {
                HitCollisionRotationZVelocityReset = HitCollision.RelativeRotationZVelocity;
            }
            if (addToManagers)
            {
                AddToManagers(null);
            }
        }
        public virtual void ReAddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.SpriteManager.AddToLayer(mSpriteInstance, LayerProvidedByContainer);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mHitCollision, LayerProvidedByContainer);
        }
        public virtual void AddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.SpriteManager.AddToLayer(mSpriteInstance, LayerProvidedByContainer);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mHitCollision, LayerProvidedByContainer);
            AddToManagersBottomUp(layerToAddTo);
            CustomInitialize();
        }
        public virtual void Activity () 
        {
            
            CustomActivity();
        }
        public virtual void Destroy () 
        {
            if (Used)
            {
                Factories.HitMeleeFactory.MakeUnused(this, false);
            }
            FlatRedBall.SpriteManager.RemovePositionedObject(this);
            
            if (SpriteInstance != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(SpriteInstance);
            }
            if (HitCollision != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.RemoveOneWay(HitCollision);
            }
            mGeneratedCollision.RemoveFromManagers(clearThis: false);
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            if (mSpriteInstance.Parent == null)
            {
                mSpriteInstance.CopyAbsoluteToRelative();
                mSpriteInstance.AttachTo(this, false);
            }
            SpriteInstance.Texture = null;
            SpriteInstance.TextureScale = 1f;
            SpriteInstance.Animate = false;
            SpriteInstance.Visible = false;
            SpriteInstance.IgnoreParentPosition = true;
            if (mHitCollision.Parent == null)
            {
                mHitCollision.CopyAbsoluteToRelative();
                mHitCollision.AttachTo(this, false);
            }
            if (HitCollision.Parent == null)
            {
                HitCollision.Z = 20f;
            }
            else
            {
                HitCollision.RelativeZ = 20f;
            }
            HitCollision.Width = 32f;
            HitCollision.Height = 32f;
            HitCollision.Visible = false;
            HitCollision.Color = Color.Tomato;
            mGeneratedCollision = new FlatRedBall.Math.Geometry.ShapeCollection();
            mGeneratedCollision.AxisAlignedRectangles.AddOneWay(mHitCollision);
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            if (SpriteInstance != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(SpriteInstance);
            }
            if (HitCollision != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.RemoveOneWay(HitCollision);
            }
            mGeneratedCollision.RemoveFromManagers(clearThis: false);
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
            }
            SpriteInstance.Texture = null;
            SpriteInstance.TextureScale = 1f;
            SpriteInstance.Animate = false;
            SpriteInstance.Visible = false;
            SpriteInstance.IgnoreParentPosition = true;
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.X = SpriteInstanceXReset;
            }
            else
            {
                SpriteInstance.RelativeX = SpriteInstanceXReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.Y = SpriteInstanceYReset;
            }
            else
            {
                SpriteInstance.RelativeY = SpriteInstanceYReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.Z = SpriteInstanceZReset;
            }
            else
            {
                SpriteInstance.RelativeZ = SpriteInstanceZReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.XVelocity = SpriteInstanceXVelocityReset;
            }
            else
            {
                SpriteInstance.RelativeXVelocity = SpriteInstanceXVelocityReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.YVelocity = SpriteInstanceYVelocityReset;
            }
            else
            {
                SpriteInstance.RelativeYVelocity = SpriteInstanceYVelocityReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.ZVelocity = SpriteInstanceZVelocityReset;
            }
            else
            {
                SpriteInstance.RelativeZVelocity = SpriteInstanceZVelocityReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.RotationX = SpriteInstanceRotationXReset;
            }
            else
            {
                SpriteInstance.RelativeRotationX = SpriteInstanceRotationXReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.RotationY = SpriteInstanceRotationYReset;
            }
            else
            {
                SpriteInstance.RelativeRotationY = SpriteInstanceRotationYReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.RotationZ = SpriteInstanceRotationZReset;
            }
            else
            {
                SpriteInstance.RelativeRotationZ = SpriteInstanceRotationZReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.RotationXVelocity = SpriteInstanceRotationXVelocityReset;
            }
            else
            {
                SpriteInstance.RelativeRotationXVelocity = SpriteInstanceRotationXVelocityReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.RotationYVelocity = SpriteInstanceRotationYVelocityReset;
            }
            else
            {
                SpriteInstance.RelativeRotationYVelocity = SpriteInstanceRotationYVelocityReset;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.RotationZVelocity = SpriteInstanceRotationZVelocityReset;
            }
            else
            {
                SpriteInstance.RelativeRotationZVelocity = SpriteInstanceRotationZVelocityReset;
            }
            SpriteInstance.Alpha = SpriteInstanceAlphaReset;
            SpriteInstance.AlphaRate = SpriteInstanceAlphaRateReset;
            if (HitCollision.Parent == null)
            {
                HitCollision.Z = 20f;
            }
            else
            {
                HitCollision.RelativeZ = 20f;
            }
            HitCollision.Width = 32f;
            HitCollision.Height = 32f;
            HitCollision.Visible = false;
            HitCollision.Color = Color.Tomato;
            if (HitCollision.Parent == null)
            {
                HitCollision.X = HitCollisionXReset;
            }
            else
            {
                HitCollision.RelativeX = HitCollisionXReset;
            }
            if (HitCollision.Parent == null)
            {
                HitCollision.Y = HitCollisionYReset;
            }
            else
            {
                HitCollision.RelativeY = HitCollisionYReset;
            }
            if (HitCollision.Parent == null)
            {
                HitCollision.Z = HitCollisionZReset;
            }
            else
            {
                HitCollision.RelativeZ = HitCollisionZReset;
            }
            if (HitCollision.Parent == null)
            {
                HitCollision.XVelocity = HitCollisionXVelocityReset;
            }
            else
            {
                HitCollision.RelativeXVelocity = HitCollisionXVelocityReset;
            }
            if (HitCollision.Parent == null)
            {
                HitCollision.YVelocity = HitCollisionYVelocityReset;
            }
            else
            {
                HitCollision.RelativeYVelocity = HitCollisionYVelocityReset;
            }
            if (HitCollision.Parent == null)
            {
                HitCollision.ZVelocity = HitCollisionZVelocityReset;
            }
            else
            {
                HitCollision.RelativeZVelocity = HitCollisionZVelocityReset;
            }
            if (HitCollision.Parent == null)
            {
                HitCollision.RotationX = HitCollisionRotationXReset;
            }
            else
            {
                HitCollision.RelativeRotationX = HitCollisionRotationXReset;
            }
            if (HitCollision.Parent == null)
            {
                HitCollision.RotationY = HitCollisionRotationYReset;
            }
            else
            {
                HitCollision.RelativeRotationY = HitCollisionRotationYReset;
            }
            if (HitCollision.Parent == null)
            {
                HitCollision.RotationZ = HitCollisionRotationZReset;
            }
            else
            {
                HitCollision.RelativeRotationZ = HitCollisionRotationZReset;
            }
            if (HitCollision.Parent == null)
            {
                HitCollision.RotationXVelocity = HitCollisionRotationXVelocityReset;
            }
            else
            {
                HitCollision.RelativeRotationXVelocity = HitCollisionRotationXVelocityReset;
            }
            if (HitCollision.Parent == null)
            {
                HitCollision.RotationYVelocity = HitCollisionRotationYVelocityReset;
            }
            else
            {
                HitCollision.RelativeRotationYVelocity = HitCollisionRotationYVelocityReset;
            }
            if (HitCollision.Parent == null)
            {
                HitCollision.RotationZVelocity = HitCollisionRotationZVelocityReset;
            }
            else
            {
                HitCollision.RelativeRotationZVelocity = HitCollisionRotationZVelocityReset;
            }
            SpriteInstanceTextureScale = 1f;
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            this.ForceUpdateDependenciesDeep();
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(SpriteInstance);
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            ContentManagerName = contentManagerName;
            // Set the content manager for Gum
            var contentManagerWrapper = new FlatRedBall.Gum.ContentManagerWrapper();
            contentManagerWrapper.ContentManagerName = contentManagerName;
            RenderingLibrary.Content.LoaderManager.Self.ContentLoader = contentManagerWrapper;
            // Access the GumProject just in case it's async loaded
            var throwaway = GlobalContent.GumProject;
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            bool registerUnload = false;
            if (LoadedContentManagers.Contains(contentManagerName) == false)
            {
                LoadedContentManagers.Add(contentManagerName);
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("HitMeleeStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/hitmelee/fx16.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                Fx16 = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/hitmelee/fx16.png", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/entities/hitmelee/animation.achx", ContentManagerName))
                {
                    registerUnload = true;
                }
                Animation = FlatRedBall.FlatRedBallServices.Load<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/entities/hitmelee/animation.achx", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/hitmelee/fx8.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                FX8 = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/hitmelee/fx8.png", ContentManagerName);
                MonsterSound = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Audio.SoundEffect>(@"content/entities/hitmelee/monstersound", ContentManagerName).CreateInstance();
                swing = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Audio.SoundEffect>(@"content/entities/hitmelee/swing", ContentManagerName).CreateInstance();
            }
            if (registerUnload && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("HitMeleeStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            CustomLoadStaticContent(contentManagerName);
        }
        public static void UnloadStaticContent () 
        {
            if (LoadedContentManagers.Count != 0)
            {
                LoadedContentManagers.RemoveAt(0);
                mRegisteredUnloads.RemoveAt(0);
            }
            if (LoadedContentManagers.Count == 0)
            {
                if (Fx16 != null)
                {
                    Fx16= null;
                }
                if (Animation != null)
                {
                    Animation= null;
                }
                if (FX8 != null)
                {
                    FX8= null;
                }
                if (MonsterSound != null)
                {
                    if(MonsterSound.IsDisposed == false) { MonsterSound.Stop();  MonsterSound.Dispose(); };
                    MonsterSound= null;
                }
                if (swing != null)
                {
                    if(swing.IsDisposed == false) { swing.Stop();  swing.Dispose(); };
                    swing= null;
                }
            }
        }
        static VariableState mLoadingState = VariableState.Uninitialized;
        public static VariableState LoadingState
        {
            get
            {
                return mLoadingState;
            }
            set
            {
                mLoadingState = value;
            }
        }
        public FlatRedBall.Instructions.Instruction InterpolateToState (VariableState stateToInterpolateTo, double secondsToTake) 
        {
            switch(stateToInterpolateTo)
            {
                case  VariableState.bite:
                    break;
                case  VariableState.simples:
                    break;
                case  VariableState.groundShock:
                    break;
                case  VariableState.sword:
                    break;
            }
            var instruction = new FlatRedBall.Instructions.DelegateInstruction<VariableState>(StopStateInterpolation, stateToInterpolateTo);
            instruction.TimeToExecute = FlatRedBall.TimeManager.CurrentTime + secondsToTake;
            this.Instructions.Add(instruction);
            return instruction;
        }
        public void StopStateInterpolation (VariableState stateToStop) 
        {
            switch(stateToStop)
            {
                case  VariableState.bite:
                    break;
                case  VariableState.simples:
                    break;
                case  VariableState.groundShock:
                    break;
                case  VariableState.sword:
                    break;
            }
            CurrentState = stateToStop;
        }
        public void InterpolateBetween (VariableState firstState, VariableState secondState, float interpolationValue) 
        {
            #if DEBUG
            if (float.IsNaN(interpolationValue))
            {
                throw new System.Exception("interpolationValue cannot be NaN");
            }
            #endif
            bool setSpriteInstanceTextureScale = true;
            float SpriteInstanceTextureScaleFirstValue= 0;
            float SpriteInstanceTextureScaleSecondValue= 0;
            switch(firstState)
            {
                case  VariableState.bite:
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceAnimationChains = Animation;
                    }
                    if (interpolationValue < 1)
                    {
                        this.isPlayAtack = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.autoDestroy = true;
                    }
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceCurrentChainName = "AtackBite";
                    }
                    break;
                case  VariableState.simples:
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceAnimationChains = null;
                    }
                    break;
                case  VariableState.groundShock:
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceAnimationChains = Animation;
                    }
                    if (interpolationValue < 1)
                    {
                        this.isPlayAtack = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.autoDestroy = true;
                    }
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceCurrentChainName = "AtackShock";
                    }
                    SpriteInstanceTextureScaleFirstValue = 3f;
                    break;
                case  VariableState.sword:
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceAnimationChains = Animation;
                    }
                    if (interpolationValue < 1)
                    {
                        this.isPlayAtack = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.autoDestroy = true;
                    }
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceCurrentChainName = "AtackSword";
                    }
                    SpriteInstanceTextureScaleFirstValue = 0.3f;
                    break;
            }
            switch(secondState)
            {
                case  VariableState.bite:
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceAnimationChains = Animation;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.isPlayAtack = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.autoDestroy = true;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceCurrentChainName = "AtackBite";
                    }
                    break;
                case  VariableState.simples:
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceAnimationChains = null;
                    }
                    break;
                case  VariableState.groundShock:
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceAnimationChains = Animation;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.isPlayAtack = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.autoDestroy = true;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceCurrentChainName = "AtackShock";
                    }
                    SpriteInstanceTextureScaleSecondValue = 3f;
                    break;
                case  VariableState.sword:
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceAnimationChains = Animation;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.isPlayAtack = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.autoDestroy = true;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceCurrentChainName = "AtackSword";
                    }
                    SpriteInstanceTextureScaleSecondValue = 0.3f;
                    break;
            }
            if (setSpriteInstanceTextureScale)
            {
                SpriteInstanceTextureScale = SpriteInstanceTextureScaleFirstValue * (1 - interpolationValue) + SpriteInstanceTextureScaleSecondValue * interpolationValue;
            }
            if (interpolationValue < 1)
            {
                mCurrentState = (int)firstState;
            }
            else
            {
                mCurrentState = (int)secondState;
            }
        }
        public static void PreloadStateContent (VariableState state, string contentManagerName) 
        {
            ContentManagerName = contentManagerName;
            switch(state)
            {
                case  VariableState.bite:
                    {
                        object throwaway = Animation;
                    }
                    {
                        object throwaway = "AtackBite";
                    }
                    break;
                case  VariableState.simples:
                    {
                        object throwaway = null;
                    }
                    break;
                case  VariableState.groundShock:
                    {
                        object throwaway = Animation;
                    }
                    {
                        object throwaway = "AtackShock";
                    }
                    break;
                case  VariableState.sword:
                    {
                        object throwaway = Animation;
                    }
                    {
                        object throwaway = "AtackSword";
                    }
                    break;
            }
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            switch(memberName)
            {
                case  "Fx16":
                    return Fx16;
                case  "Animation":
                    return Animation;
                case  "FX8":
                    return FX8;
                case  "MonsterSound":
                    return MonsterSound;
                case  "swing":
                    return swing;
            }
            return null;
        }
        public static object GetFile (string memberName) 
        {
            switch(memberName)
            {
                case  "Fx16":
                    return Fx16;
                case  "Animation":
                    return Animation;
                case  "FX8":
                    return FX8;
                case  "MonsterSound":
                    return MonsterSound;
                case  "swing":
                    return swing;
            }
            return null;
        }
        object GetMember (string memberName) 
        {
            switch(memberName)
            {
                case  "Fx16":
                    return Fx16;
                case  "Animation":
                    return Animation;
                case  "FX8":
                    return FX8;
                case  "MonsterSound":
                    return MonsterSound;
                case  "swing":
                    return swing;
            }
            return null;
        }
        protected bool mIsPaused;
        public override void Pause (FlatRedBall.Instructions.InstructionList instructions) 
        {
            base.Pause(instructions);
            mIsPaused = true;
            if (SoundMonster.State == Microsoft.Xna.Framework.Audio.SoundState.Playing)
            {
                SoundMonster.Pause();
                instructions.Add(new FlatRedBall.Instructions.DelegateInstruction(() => SoundMonster.Resume()));
            }
            if (SoundSword.State == Microsoft.Xna.Framework.Audio.SoundState.Playing)
            {
                SoundSword.Pause();
                instructions.Add(new FlatRedBall.Instructions.DelegateInstruction(() => SoundSword.Resume()));
            }
        }
        public virtual void SetToIgnorePausing () 
        {
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(this);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(SpriteInstance);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(HitCollision);
        }
        public virtual void MoveToLayer (FlatRedBall.Graphics.Layer layerToMoveTo) 
        {
            var layerToRemoveFrom = LayerProvidedByContainer;
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(SpriteInstance);
            }
            FlatRedBall.SpriteManager.AddToLayer(SpriteInstance, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(HitCollision);
            }
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(HitCollision, layerToMoveTo);
            LayerProvidedByContainer = layerToMoveTo;
        }
    }
}
