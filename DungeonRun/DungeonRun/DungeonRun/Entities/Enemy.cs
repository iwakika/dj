﻿using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;
using Microsoft.Xna.Framework;
using DungeonRun.MinhasClasses.Data;
using DungeonRun.MinhasClasses;

using Point = FlatRedBall.Math.Geometry.Point;

namespace DungeonRun.Entities
{
	public partial class Enemy
	{
        
       // public float mass { get; set; }
        public Vector3 SpawerPoint { get; set; }
        public Vector3 goTo { get; set; }
        private Vector3 goToRandom;
        //private Vector3 collisioMod = new Vector3();
        public Polygon gotoLine ;
        public Boolean hasColisionGotoLine { get; set; }
        private double mLastPathTime;
       // private Boolean movementRandom = false;
        private Double mLastAtackTime;
        private bool  isInitialize = false;
        public int Health { get; set; }
        private Vector3 HitMelleePosition = new Vector3();

        //private bool pauseProject = false;
       // int cont = 0;

        
       
        
        public bool IsTimeToAtack
        {
            get
            {
                // double atackFrequency = 3;
                double segundosdesde = TimeManager.SecondsSince(mLastAtackTime);
                return (TimeManager.SecondsSince(mLastAtackTime)) > AtackFrequency;
                // return FlatRedBall.Screens.ScreenManager.CurrentScreen.PauseAdjustedSecondsSince(mLastSpawnTime) > spawnFrequency;
            }
            set { }
        }
        
        public bool isInAtack = false;

        double startAtackLoad = 0;
       // double timeAtackLoad = 3;
        bool IsAtackLoad
        {
            get
            {
              
                return (TimeManager.SecondsSince(startAtackLoad) > timeAtackLoad);
              
            }
        }

        private HitMelee hitMelee;
        //public Boolean isAtackMelee { get; set; }
       

        private void CustomInitialize()
		{
           
            LoadMeleeCollision();            
            hasColisionGotoLine = false;
            IsTimeToAtack = false;

        }

        private void initialize()
        {

            // Console.WriteLine("Enemy"+ cont++);
            
            loadHUD();
            goToLineActivity();
            mLastAtackTime = TimeManager.CurrentTime;
            //Console.WriteLine(mLastAtackTime);
            isInitialize = true;

        }

        private void CustomActivity()
        {

            if (!GlobalData.isGamePause){
                if (!isInitialize)
                {
                    initialize();
                }



                if (!isInAtack) {
                    movementActivity();
                    goToLineActivity();
                }
                atackActivity();
            }else
            {
                ActionStop();
            }
            




        }

        private void updateIsinAtack()
        {
            if (isAtackMelee) {
                if (hitMelee != null)
                {
                    if (!hitMelee.isAtackAnimate)
                    {
                        isInAtack = false;
                    }else
                    {
                        isInAtack = true;
                    }
                }else
                {
                    isInAtack = false;
                }
        }

        }

        private  void goToLineActivity()
        {
            if (isAtackProjectiles)
            { // apenas para quem tem ataque de projéteis
                if (gotoLine == null)
                {
                    gotoLine = new Polygon();
                    gotoLine.Visible = false;
                }
               
                Vector3 goTo = GlobalData.PlayerData.playerInstance.Position;
                // this.gotoLine = new Polygon();
                gotoLine.Position = Vector3.Zero;
                gotoLine.Position.Z = 30;

                Point[] points =
                {
                new Point(goTo.X , goTo.Y ),
                new Point(this.X , this.Y + GlobalData.halfTileSize),

                
               
                // repeat the last point to close the shape
              
            };

                gotoLine.Points = points;
                gotoLine.Visible = false;
            }
        
        }

        
        /// <summary>
        /// Executa o evento de atack de projeteis e carrega o hitbox do ataque meele
        /// </summary>
        private void atackActivity()
        {
             if (isAtackMelee) {
                //pauseProject = true;
                    LoadMeleeCollision();
               

            }

            if (isAtackProjectiles )
            {
                if (IsTimeToAtack && !hasColisionGotoLine)
                {
                    ActionAtack();
                    mLastAtackTime = TimeManager.CurrentTime;
                }
            }

        }

        private void movementActivity()
        {

            if (FlatRedBall.Screens.ScreenManager.CurrentScreen.PauseAdjustedSecondsSince(mLastPathTime) > 1)
            {            
                movementFollowObject(GlobalData.PlayerData.playerInstance.CollisionHitBox);      
                ActionWalk();

            }
        }


		private void CustomDestroy()
		{
            if (hitMelee != null)
            {
                hitMelee.Destroy();
            }

            if (gotoLine != null)
            {
                ShapeManager.Remove(gotoLine);
            }
            

		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        private void loadHUD()
        {
            
            if (StartingHealth > 1)
            {
                GlobalData.hudInstance.CreateHealthBarInstance(this);
            }
            
            
        }

       public void movementFollowPoint(Vector3 point, Boolean keepDistance)
        {
            PositionedNode objNode = GlobalData.currentLevel.tileNodeNetwork.GetClosestNodeTo(point.X, point.Y);
            PositionedNode enemyNode = GlobalData.currentLevel.tileNodeNetwork.GetClosestNodeTo(this.X, this.Y);


            try
            {

                List<PositionedNode> path = GlobalData.currentLevel.tileNodeNetwork.GetPath(enemyNode, objNode);


                if ((path.Count > 1 && hasColisionGotoLine) || (path.Count > PlayDistance && !hasColisionGotoLine && keepDistance))
                {

                    ///trata modificador de colisão com outro inimigo                 
                    goTo = path[1].Position;

                    Vector3 direction = getDiretion(goTo);

                    if (point != path[path.Count - 1].Position)

                    {
                        direction = getDiretion(goTo);

                        this.XVelocity = Speed * direction.X;
                        this.YVelocity = Speed * direction.Y;

                    }
                    else
                    {
                        ActionStop();
                    }

                }
                else
                {
                    this.XVelocity = 0;
                    this.YVelocity = 0;
                    ActionStop();

                }

            }
            catch (NullReferenceException nre)
            {
                Console.Error.WriteLine("enemy walking error");
            }
        }

        /// <summary>
        /// O inimigo vai seguir a posição do inimigo informado em obj DESVIANDO DE OBSTACULOS
        /// </summary>
        /// <param name="obj"></param>
        public void movementFollowObject(AxisAlignedRectangle obj) {

            movementFollowPoint(obj.Position, true);


        }


        public void movementSimpleFollowObject(PositionedObject obj)
        {
            Vector3 direction = getDiretion(obj.Position);

            this.XVelocity = Speed * direction.X;
            this.YVelocity = Speed * direction.Y;
            
        }

     /*   public void MoveToRandom(int tileToMove)
        {
            int modDirectionX = GlobalData.tileSize * tileToMove;
            int modDirectionY = GlobalData.tileSize * tileToMove;
           // Console.WriteLine("GoTo:" + this.Position);
            goToRandom = new Vector3(this.Position.X + modDirectionX, this.Position.Y + modDirectionY, 0);
         //   movementRandom = true;
        }
        */


        /// <summary>
        /// Caqlcula o vetor direcional de momimento
        /// </summary>
        /// <param name="goTo"> tile que o inimigo  vai se emcaminhar </param>
        public void movementeTo( Vector3 goTo)
        { 
            Vector3 direction = getDiretion(goTo);

            {
                this.XVelocity = Speed * direction.X;
                this.YVelocity = Speed * direction.Y;

            }          


           }


            /// <summary>
            /// Busca vetor de direção para ir
            /// </summary>
            /// <param name="Togo"></param>
        public Vector3 getDiretion(Vector3 Togo)
        {
            // 1.  Encontra caminho para ir            
            Vector3 centerOfGameScreen = new Vector3(Togo.X, Togo.Y, 0);

            // 2.  Get the direction towards the center of the screen
            Vector3 directionToCenter = Togo - this.Position;

            // 3.  Normalize the direction,
            directionToCenter.Normalize();
            return directionToCenter;

        }

        public void colission(AxisAlignedRectangle entityCollidable, int mass, String entidade)
        {

            if (this.CollisionInstance.CollideAgainstMove(entityCollidable, this.Mass,0.5f))
            {

                switch (entidade.ToUpper())
                {

                    case "PLAYER":
                                             
                        if (SpriteInstance.JustCycled)
                        {
                            SpriteInstance.Animate = false;
                            SpriteInstance.CurrentFrameIndex = 0;

                        }
                        break;

                    case "ENEMY":                     
                        
                        if (SpriteInstance.JustCycled)
                        {
                            SpriteInstance.Animate = false;
                            SpriteInstance.CurrentFrameIndex = 0;

                        }
                        break;
                }
            }
            else
            {
                SpriteInstance.Animate = true;
           
            }
            // GlobalData.PlayerData.playerInstance.Collision.CollideAgainstMove(entityCollidable, mass, this.Mass);


        }

        private void setAnimation(EnumAnimation enemyAnimation)
        {
            
            switch (enemyAnimation)
            {


                case EnumAnimation.PARADO_BAIXO:
                    SpriteInstance.Animate = true;

                    if(SpriteInstance.ContainsChainName("INATIVO"))
                    SpriteInstance.CurrentChainName = "INATIVO";
                    
                    break;
                case EnumAnimation.ANDAR_BAIXO:
                    SpriteInstance.Animate = true;
                    if (SpriteInstance.ContainsChainName("BAIXO"))
                        SpriteInstance.CurrentChainName = "BAIXO";
                    break;

                case EnumAnimation.ANDAR_CIMA:
                    SpriteInstance.Animate = true;
                    if (SpriteInstance.ContainsChainName("CIMA"))
                        SpriteInstance.CurrentChainName = "CIMA";
                    break;

                case EnumAnimation.ANDAR_DIREITA:
                    SpriteInstance.Animate = true;
                    if (SpriteInstance.ContainsChainName("DIREITA"))
                        SpriteInstance.CurrentChainName = "DIREITA";
                    break;

                case EnumAnimation.ANDAR_ESQUERDA:
                    SpriteInstance.Animate = true;
                    if(SpriteInstance.ContainsChainName("ESQUERDA"))
                    SpriteInstance.CurrentChainName = "ESQUERDA";
                    break;
                

            }

        }


        
         
        /// <summary>
        /// Cria projeteis do montro
        /// </summary>
        public void ActionAtack()
        {
         
            switch (this.CurrentState)
            {
                case Enemy.VariableState.Galvis:
                    Projectiles.createProjectiles(Projectiles.VariableState.AtackWater1, GlobalData.PlayerData.playerInstance.X, GlobalData.PlayerData.playerInstance.Y, null, false);
                    break;
                case Enemy.VariableState.Guiliandri:
                    Projectiles.createProjectiles(Projectiles.VariableState.SwordAtack, this.Position.X, this.Position.Y, null, false, GlobalData.PlayerData.playerInstance.X, GlobalData.PlayerData.playerInstance.Y);
                   // pauseProject = true;
                    break;

                case Enemy.VariableState.Spider:                    

                    break;
                case Enemy.VariableState.Ogro:

                    break;

                case Enemy.VariableState.Skeleton:
                    Projectiles.createProjectiles(Projectiles.VariableState.Arrow, this.Position.X, this.Position.Y, null, false, GlobalData.PlayerData.playerInstance.X, GlobalData.PlayerData.playerInstance.Y);
                    break;




            }

        }

        private void meleeAtack()
        {

            if (this.hasBiteAtack)
            {
                hitMelee.CurrentState = HitMelee.VariableState.bite;
            }else if(this.hasGroundShockAtack)
            {
                hitMelee.CurrentState = HitMelee.VariableState.groundShock;
            }
            else if (this.hasSwordAtack){
                hitMelee.CurrentState = HitMelee.VariableState.sword;
            }

        }



        private void LoadMeleeCollision()
        {
           
            if (isAtackMelee)
            {
                
                if (hitMelee == null)
                {

                    
                    hitMelee = Factories.HitMeleeFactory.CreateNew();
                    meleeAtack();                    
                    hitMelee.Position = this.Position;
                    hitMelee.HitCollision.Height = CollisionInstance.Height * (1 + MeleeRange);
                    hitMelee.HitCollision.Width = CollisionInstance.Width * (1 + MeleeRange);
                }
                else
                {

                    hitMelee.Position = this.Position;
                    hitMelee.HitCollision.Height = CollisionInstance.Height * (1 + MeleeRange);
                    hitMelee.HitCollision.Width = CollisionInstance.Width * (1 + MeleeRange);
                }
            }
       
        }
        

        /// <summary>
        /// Executado quando á colisão com  o player
        /// Se o Ataque acertou retorna true
        /// </summary>
        /// <param name="Player"></param>
        /// <returns></returns>
        public bool AtackHit(Player player)    // Retorna se acertou outro personagem ou não
        {
            bool hit = false;
            

            if (hitMelee.HitCollision.CollideAgainst(player.CollisionInstance))
            {
               
                if (!isInAtack && IsTimeToAtack) // se não tem nenhum atack inicializado E ESTA NA HORA DE ATACAR
                {
                    isInAtack = true;
                    startAtackLoad = TimeManager.CurrentTime;
                    
                    HitMelleePosition.X = player.Position.X;
                    HitMelleePosition.Y = player.Position.Y;
                    HitMelleePosition.Z = player.Position.Z;
                    ActionStop();
                    //  Console.WriteLine("creatHitMelee");                                   
                }
            }           


            if (IsAtackLoad && isInAtack)
            {
                mLastAtackTime = TimeManager.CurrentTime; 
                hit = hitMelee.HitCollision.CollideAgainst(player.CollisionHitBox);
                // hitMelee.createHitMeleeAnimation(player.Position.X, player.Position.Y, player.Position.Z +5);              
                hitMelee.createHitMeleeAnimation(HitMelleePosition.X, HitMelleePosition.Y, HitMelleePosition.Z + 5);
                isInAtack = false;


            }
            else
            {
               // Console.WriteLine("Carregando Atack");

            }
            //pauseProject = false;

            return hit;
        }
        public void ActionStop()
        {
            this.XVelocity = 0;
            this.YVelocity = 0;

        }

        public void ActionTakeHit(Projectiles projectile)
        {
       

                if ((Health - projectile.Atack) <= 0)
                {
                Health -= projectile.Atack;
                GlobalData.PlayerData.qtdMonsterKilled++;
                GlobalData.currentLevel.monsterKilled++;
                Destroy();
                SoundDeathMonster.Play();
                if (GlobalData.currentLevel.isLevelBoss)
                {
                    if (this.CurrentState == Enemy.VariableState.Galvis || this.CurrentState == Enemy.VariableState.Guiliandri)
                    {
                        GlobalData.PlayerData.totalLevelCompleted++;
                        GlobalData.gameOverGlobal(true);
                    }
                }



                }else
                {
                    Health -= projectile.Atack;
                }
                projectile.Destroy();       

        }

        public Boolean isNowallPath(){
            Boolean r = false;
            
            FlatRedBallServices.Game.IsMouseVisible = true;

          
       

                return r;
            }


        /// <summary>
        /// Defini a direção que a sprite do inimigo deve virar
        /// </summary>
        public void ActionWalk()
        {
            
            Vector3 direction = goTo - this.Position;
            // Console.WriteLine("position" + (goTo - this.Position));
            float abX = Math.Abs(direction.X);
            float abY = Math.Abs(direction.Y);

            if (GlobalData.isGamePause)
            {

                SpriteInstance.Animate = false;
                //Console.WriteLine("PausaAnimacao");
            }
            else
            {

                if (direction.X > 0 && (abX > abY))
                {

                    setAnimation(EnumAnimation.ANDAR_DIREITA);
                }
                else
                {
                    if (direction.X < 0 && (abX > abY))
                    {
                        setAnimation(EnumAnimation.ANDAR_ESQUERDA);
                    }
                    else
                    {
                        if (direction.Y > 0 && abY >= abX)
                        {
                            setAnimation(EnumAnimation.ANDAR_CIMA);
                        }
                        else
                        {
                            if (direction.Y < 0)
                            {
                                setAnimation(EnumAnimation.ANDAR_BAIXO);
                            }
                            else
                            {

                                if (SpriteInstance.JustCycled)
                                {
                                    SpriteInstance.Animate = false;
                                    SpriteInstance.CurrentFrameIndex = 0;

                                }
                            }


                        }

                    }
                }
            }
        }
        

    }
}
