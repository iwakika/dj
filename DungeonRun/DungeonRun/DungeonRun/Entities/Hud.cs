using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;
using DungeonRun.MinhasClasses.Data;
using DungeonRun.Screens;
using DungeonRun.MinhasClasses;
using FlatRedBall.Screens;

namespace DungeonRun.Entities

{
	public partial class Hud
	{
        /// <summary>
        /// 
        /// Interface do player atual
        /// </summary>
        /// 

       FlatRedBall.Math.PositionedObjectList<Player> mPlayerList;
        public FlatRedBall.Math.PositionedObjectList<Player> PLayerList
        {
            set
            {
                mPlayerList = value;
               // CreateHealthBarInstances();
                CreateDashBarInstance(GlobalData.PlayerData.playerInstance);
            }
        }
        FlatRedBall.Math.PositionedObjectList<Enemy> mEnemyList;
        public FlatRedBall.Math.PositionedObjectList<Enemy> EnemyList
        {
            set
            {
                mEnemyList= value;
                CreateHealthBarInstances();
            }
        }
        public bool RakingVisible = false;


        ///<summary> 
        ///Cria a "Barra de vida para o PLayer"
        ///</summary>
        public void CreateHealthBarInstances()
        {
            // First we instantiate them
            for (int i = 0; i < mPlayerList.Count; i++)
           {

                /*HealthBar healthBar = new HealthBar();             
                this.HealthBarList.Add(healthBar);
           // Console.WriteLine(GlobalData.PlayerData.playerInstance.DashSpeed+"player cliado ao executar hud");
                healthBar.player = GlobalData.PlayerData.playerInstance; //mPlayerList[i];               
                 */

            }

          

            int divisions = mPlayerList.Count + 1;

           // Console.WriteLine("Passou pelo o hud");

           
            Hud.GameHudGum.LoadFromFile("GumProject/Screens/GameHudGum.gusx");
            

        }

        public void CreateHealthBarInstance(Enemy enemy)
        {
            HealthBar healthBar = new HealthBar();            
            healthBar.enemy = enemy;
            this.HealthBarList.Add(healthBar);

        }

        public void CreateDashBarInstance(Player player)
        {
            DashBar dashBar = Factories.DashBarFactory.CreateNew();
            dashBar.player = player;
            //Console.WriteLine("Create DashBar");
            
            this.dashBarList.Add(dashBar);

        }



        private void CustomInitialize()
		{
            // CreateHealthBarInstances();
           
           
            if (ScreenManager.CurrentScreen.ContentManagerName == typeof(MainMenuScreen).Name)
            {
                LevelDisplay_HUD.Visible = false;                

            }
            else
            {
                LevelDisplay_HUD.Visible = true;
                //Hud.GameHudGum.Visible = true;
                LoadItensToHUD();
                LoadTimeDisplay(0);
            }

          
            Hud.RakingHUD.LoadFromFile("GumProject/Screens/RakingHUD.gusx");
            Hud.PlayPoints_HUD.LoadFromFile("GumProject/Screens/PlayPoints_HUD.gusx");
            //  Hud.ComandScreen_HUD.LoadFromFile("GumProject/Screens/ComandScreen_HUD.gusx");
            //Hud.ComandScreen_HUD.Visible = true;
            // ComandScreen_HUDInstance.Visible = true;
            Controle_DisplayInstance.Visible = false;
            PlayerPoints_displayInstance.Visible = false;
            Panel_DisplayInstance.Visible = false;
            GameOver_DisplayInstance.Visible = false;

          

            

        }

        public void loadPlayPoints()
        {
          
                //Hud.PlayPoints_HUD.LoadFromFile("GumProject/Screens/PlayPoints_HUD.gusx");
                //PlayPoints_HUD.Z= 30;
             
           
            if (GlobalData.PlayerData!= null) {
                TimeSpan t = TimeSpan.FromSeconds(GlobalData.PlayerData.totalPlayerTime + GlobalData.currentLevel.levelTime);

                string timeFormat = string.Format("{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds);
               // string lifePointsFormat = string.Format("{1:D2}/{2:D2}", CalculatePoints.completedWithMinLive(GlobalData.PlayerData.health), 25);

               // Console.WriteLine (lifePointsFormat +"lifeformat");
               CalculatePoints cp = CalculatePoints.Instance();

                PlayerPoints_displayInstance.Visible = true;
                PlayerPoints_displayInstance.PlayNameDisplay = GlobalData.PlayerData.name;
                PlayerPoints_displayInstance.PlayTime = timeFormat ;

                PlayerPoints_displayInstance.LifePointsText = Convert.ToString(cp.completedWithMinLive(GlobalData.PlayerData.health)) + "/" + cp.minLivePoints;
                PlayerPoints_displayInstance.TimePointsText = Convert.ToString(cp.completedInTime(GlobalData.PlayerData.totalPlayerTime, GlobalData.PlayerData.totalLevelCompleted)) + "/" + cp.completedInTimePoints;
                PlayerPoints_displayInstance.LevelPointsText = Convert.ToString(cp.toLevelCompleted(GlobalData.PlayerData.totalLevelCompleted)) + "/" + cp.levelRoomToCompletedPoints;
                PlayerPoints_displayInstance.ExtrasPointsText = Convert.ToString(cp.extraPoints(GlobalData.PlayerData.health, GlobalData.PlayerData.qtdMonsterKilled)) + "/" + cp.extraPointsMax;
                PlayerPoints_displayInstance.LifePointsText = Convert.ToString(cp.completedWithMinLive(GlobalData.PlayerData.health)) + "/" + cp.minLivePoints;
                PlayerPoints_displayInstance.TotalPointsText = Convert.ToString(cp.calculateTotalPoints(GlobalData.PlayerData.totalLevelCompleted, GlobalData.PlayerData.health, GlobalData.PlayerData.totalPlayerTime, GlobalData.PlayerData.qtdMonsterKilled)) + "/100";

                /*
                PlayerPoints_displayInstance.LifePointsText = Convert.ToString(CalculatePoints.completedWithMinLive(GlobalData.PlayerData.health))+ "/" + CalculatePoints.minLivePoints;
                PlayerPoints_displayInstance.TimePointsText= Convert.ToString(CalculatePoints.completedInTime(GlobalData.PlayerData.totalPlayerTime, GlobalData.PlayerData.totalLevelCompleted))+"/" + CalculatePoints.completedInTimePoints;
                PlayerPoints_displayInstance.LevelPointsText = Convert.ToString(CalculatePoints.toLevelCompleted(GlobalData.PlayerData.totalLevelCompleted))+"/" + CalculatePoints.levelRoomToCompletedPoints;
                PlayerPoints_displayInstance.ExtrasPointsText = Convert.ToString(CalculatePoints.extraPoints(GlobalData.PlayerData.health, GlobalData.PlayerData.qtdMonsterKilled))+"/";
                PlayerPoints_displayInstance.LifePointsText = Convert.ToString(CalculatePoints.completedWithMinLive(GlobalData.PlayerData.health))+"/" + CalculatePoints.extraPointsMax;
                PlayerPoints_displayInstance.TotalPointsText = Convert.ToString(CalculatePoints.calculateTotalPoints(GlobalData.PlayerData.totalLevelCompleted, GlobalData.PlayerData.health, GlobalData.PlayerData.totalPlayerTime, GlobalData.PlayerData.qtdMonsterKilled)) + "/100";
                */
            }

        }

        public bool loadControle()
        {

            Controle_DisplayInstance.Visible = !Controle_DisplayInstance.Visible;

            return Controle_DisplayInstance.Visible;
        }

        public bool LoadText(string text)
        {
            if (this.DialogBoxInstance.Visible == false) {
                this.DialogBoxInstance.Visible = true;
                this.DialogBoxInstance.Text = text;
            }
            return this.DialogBoxInstance.Visible;

        }
        public void desativaTexto()
        {
            if (this.DialogBoxInstance.Visible == true)
            {
                this.DialogBoxInstance.Visible = false;
            }
        }

        public void LoadNpcTex(Npc npc)
        {
            if (this.DialogBoxInstance.Visible)
            {
                switch (npc.ID)
                {
                    case 1:

                        this.DialogBoxInstance.Text = npc.buscaDialogo(0, 2);
                        this.DialogBoxInstance.Visible = false;
                        //this.NpcText.Visible = false;
                        break;
                }
            }else
            {
                this.DialogBoxInstance.Visible = true;
                
               // Console.WriteLine(this.texBox.X + "," +this.texBox.Y);
               // Console.WriteLine(npc.Position.X + "," + npc.Position.Y);

                this.SoundClick.Play();
                    // this.texBox.X = npc.Position.X ;
                //this.texBox.Y = npc.Position.Y+300;
                
            }

        }

        
        /// <summary>
        /// Monstra o Rud com o arnking
       
        /// </summary>
        /// <param name="monstrarApenasRankingSalvos"> se true apenas mostras os rankings salvos</param>
        /// <returns></returns>
        public bool loadRanking(bool mostrarApenasRankingsSalvos)
        {
            
            //Hud.RakingHUD.LoadFromFile("GumProject/Screens/RakingHUD.gusx");
           // Console.WriteLine("DISPLAY:" + Panel_DisplayInstance.Visible);
            if (!RakingVisible)
            {
                RakingVisible = true;
                Panel_DisplayInstance.Visible = true;
                // Console.WriteLine("rankingONN");
                GlobalData.Ranking.ReadRaking();
                if (!mostrarApenasRankingsSalvos)
                {
                    string name = GlobalData.PlayerData.name;
                    int point = CalculatePoints.Instance().calculateTotalPoints(GlobalData.PlayerData.totalLevelCompleted, GlobalData.PlayerData.health, GlobalData.PlayerData.totalPlayerTime, GlobalData.PlayerData.qtdMonsterKilled);
                    double time = GlobalData.PlayerData.totalPlayerTime + GlobalData.currentLevel.levelTime;

                    // Console.WriteLine("Time:" + time);
                   
                    GlobalData.Ranking.rankingList.Add(new PlayerSave(name, point, time));                   
                }
                
                GlobalData.Ranking.orderRanking();
                Panel_DisplayInstance.Text1 = GlobalData.Ranking.getRankingName();
                    Panel_DisplayInstance.Text2 = GlobalData.Ranking.getRankingPoints();                    

                    Panel_DisplayInstance.Text3 = GlobalData.Ranking.getRankingTime();
                
            }else
            {
                RakingVisible = false;
                Panel_DisplayInstance.Visible = false;
               // Console.WriteLine("rankingoff");
            }
            return RakingVisible;
        }
        public void UnloadPlayPoints()
        {
            
            PlayerPoints_displayInstance.Visible = false;
        }


        public void LoadBlood()
        {
            BloodIntance.Visible = !BloodIntance.Visible;
        }

        public  void LoadTimeDisplay(double sec)
        {
            TimeSpan t = TimeSpan.FromSeconds(sec);

            string timeFormat = string.Format("{1:D2}:{2:D2}",
                            t.Hours,
                            t.Minutes,
                            t.Seconds);
            
            TimeDisplay_HUD.Text = timeFormat;
                
        }

        public void LoadNameDisplay()
        {

            Name_Display_HUD.Text = GlobalData.PlayerData.name;

        }
    
        public void LoadHealthToHUD()
        {
            //Console.WriteLine("Vida" + GlobalData.PlayerData.pl);
            AtualizaItemHUD(0, GlobalData.PlayerData.health);

        }

        public void LoadItensToHUD()
        {
           AtualizaItemHUD(-1, 0);
                foreach (InvetoryItem i in GlobalData.PlayerData.ItemList.Values)
                {
                    //Console.WriteLine("item:" + i.type + '|' + i.qtd);
                    AtualizaItemHUD(i.iventoryIndex, i.qtd);
                }
            

            //  foreach( GlobalData.PlayerData.ItemList
            // GlobalData.hudInstance.AtualizaItemHUD(item.IndexInventory, GlobalData.PlayerData.ItemList[item.itemType]);
        }
        public void LoadGameOver(Boolean venceuBoss)
        {

            string name = GlobalData.PlayerData.name;
            int point = CalculatePoints.Instance().calculateTotalPoints(GlobalData.PlayerData.totalLevelCompleted, GlobalData.PlayerData.health, GlobalData.PlayerData.totalPlayerTime, GlobalData.PlayerData.qtdMonsterKilled);
            double time = GlobalData.PlayerData.totalPlayerTime + GlobalData.currentLevel.levelTime;
            GameOver_DisplayInstance.TextNotaText = Convert.ToString(point)+"/100";

            if (point > 65)
            {
                GameOver_DisplayInstance.TextResultadoTex = "APROVADO";
                GameOver_DisplayInstance.TextResultadoBlue= 0;
                GameOver_DisplayInstance.TextResultadoGreen=255;
                GameOver_DisplayInstance.TextResultadoRed = 0;
                if (GlobalData.currentLevel.isLevelBoss)
                {
                    if (venceuBoss)
                    {
                        GameOver_DisplayInstance.GameOverMSGText = "haha, voc� me venceu " + GlobalData.PlayerData.name + ", nada mais do que o esperado do meu pupilo favorito! Quem sabe na pr�xima voc� consiga provar o seu valar para a Rainha!";

                        if(point > 95)
                        {
                            GameOver_DisplayInstance.GameOverAvatarSourceFile = Hud.GuiliandriAvatar;
                            GameOver_DisplayInstance.GameOverMSGText = GlobalData.PlayerData.name + "querido, parece que voc� realmente consegue se defentes com seus truques magicos. Parab�ns voc� me provou o seu valor!";

                        }

                    }
                    else
                    {
                        GameOver_DisplayInstance.GameOverMSGText = "Muito bem " + GlobalData.PlayerData.name + "! Voc� passou, quem sabe na pr�xima consiga me vencer";
                    }
                }else
                {
                    GameOver_DisplayInstance.GameOverMSGText = GlobalData.PlayerData.name + " parece que voc� conseguiu o suficiente para passar, mas esperava te ver chegar ao fim da masmorra. Quem sabe na pr�xima!";
                }
            }else
            {
                
                GameOver_DisplayInstance.TextResultadoTex = "REPROVADO";
                GameOver_DisplayInstance.TextResultadoBlue = 0;
                GameOver_DisplayInstance.TextResultadoGreen = 0;
                GameOver_DisplayInstance.TextResultadoRed = 255;
                GameOver_DisplayInstance.GameOverMSGText = "Eu tinha bastante f� no seu potencial, mas parece que no final das contas nossa querida rainha estava certa. Voc� precisa de mais treinamento em combate!";
            }
            GameOver_DisplayInstance.Visible = true;
            //RESTAURA STATUS DO PLAY NO JOGO
            
        }

        public void LoadLevelToHUD()
        {
            LevelDisplay_HUD.LevelDisplay_txt_Hud = Convert.ToString(GlobalData.currentLevel.id);
            LevelDisplay_HUD.RomIn_HUD = Convert.ToString(GlobalData.currentLevel.RoomCurrent);
            LevelDisplay_HUD.RomOf_HUD = Convert.ToString(GlobalData.currentLevel.RoomQtd);
        }

        public void AtualizaItemHUD(int ItemHUd, int qtd)
        {
            switch (ItemHUd)
            {
                case 0:
                    Item_HUD_0.Item_qtd_hud = Convert.ToString(qtd);
                    break;
                case 1:
                    Item_HUD_1.Item_qtd_hud = Convert.ToString(qtd);
                break;
                case 2:
                    Item_HUD_2.Item_qtd_hud = Convert.ToString(qtd);
                    break;
                case 3:
                    //Item_HUD_3.Item_qtd_hud = Convert.ToString(qtd);
                    break;
                case 4:
                  //  Item_HUD_4.Item_qtd_hud = Convert.ToString(qtd);
                    break;

                default:
                //   Item_HUD_0.Item_qtd_hud = Convert.ToString(0);
                   Item_HUD_1.Item_qtd_hud = Convert.ToString(0);
                   Item_HUD_2.Item_qtd_hud = Convert.ToString(0);
                  // Item_HUD_3.Item_qtd_hud = Convert.ToString(0);
                   //Item_HUD_4.Item_qtd_hud = Convert.ToString(0);
                  break;
            }

        }

        private void CustomActivity()
		{
            if (GlobalData.currentLevel != null && GlobalData.currentLevel.CurrentState != Level.VariableState.level0)
            {
                LoadTimeDisplay(GlobalData.currentLevel.levelTime);
            }

        }

		private void CustomDestroy()
		{
            if (Hud.GameHudGum!= null) {
                Hud.GameHudGum.Destroy();
            }
            Hud.PlayPoints_HUD.Destroy();
        }

        private static void CustomLoadStaticContent(string contentManagerName)
        {
            
        }

      
    }
}
