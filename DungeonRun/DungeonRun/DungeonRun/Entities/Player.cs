using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;
using Microsoft.Xna.Framework.Input;
using DungeonRun.MinhasClasses;
using Microsoft.Xna.Framework;
using DungeonRun.MinhasClasses.Data;
using DungeonRun.Factories;

namespace DungeonRun.Entities
{
	public partial class Player
	{
        

        public I2DInput MovementInput { get; set; }
        public I2DInput ShotInput { get; set; }
        public IPressableInput ShotInput_UP { get; set; }
        public IPressableInput ShotInput_Dow { get; set; }
        public IPressableInput ShotInput_Rigth { get; set; }
        public IPressableInput ShotInput_Left { get; set; }
        public IPressableInput DashInput { get; set; }

        public Vector3 SpawerPoint { get; set; }



       public EnumPlay statusPlay {get; set;}
        private double startIntangibleTime;
        private double startAlfa;
        public bool interacaoNPC = false;
     

    /// ///////////dash varibles///       
    private float dashVelocityModify = 0;
        private double lastDashTime = 0;
        private Boolean shouldDash = false;
        public double startDashTime =0;
        int mHealth;


        private void CustomInitialize()
		{
            lastDashTime = 0;         
            Mass = 1;
            statusPlay = EnumPlay.DEFAULT;
          

        }

        private void CustomActivity()
		{
            MovementActivity();
            DashActivity();
            ActionShot();
            intangibleActivity();


        }
        public void intangibleActivity()
        {
            
            if (TimeManager.SecondsSince(startIntangibleTime) > intagibleDuration && statusPlay == EnumPlay.INTANGIBLE && shouldDash ==false)
            {

               // Console.WriteLine(TimeManager.SecondsSince(startIntangibleTime));
               // Console.WriteLine(TimeManager.CurrentTime);
                statusPlay = EnumPlay.DEFAULT;
                SpriteInstance.Alpha = 1;

            }
            else
            {
                if(statusPlay == EnumPlay.INTANGIBLE)
                {
                    //Console.WriteLine("time:" + (Math.Abs(TimeManager.SystemCurrentTime) % 0.2));
                    if (TimeManager.SecondsSince(startAlfa) > 0.15)
                    {
                        startAlfa = TimeManager.CurrentTime;
                        if (SpriteInstance.Alpha == 0)
                        {
                            SpriteInstance.Alpha = 1;
                            //Console.WriteLine("Alfa=1");
                        }
                        else
                        {
                            SpriteInstance.Alpha = 0;
                           // Console.WriteLine("Alfa=0");
                        }                    
                    }
                }
                else
                {
                    SpriteInstance.Alpha = 1;
                }
              
            }
        }

        private void MovementActivity()
        {

            ActionWalk();
            
         
        }

        public void ActionWalk()
        {
               
                    this.XVelocity = MovementInput.X * ( MovementSpeed + dashVelocityModify);
                    this.YVelocity = MovementInput.Y * (MovementSpeed + dashVelocityModify);


            if (MovementInput.X > 0) {

                setAnimation(EnumAnimation.ANDAR_DIREITA);
            } else {
                if (MovementInput.X < 0)
                {
                    setAnimation(EnumAnimation.ANDAR_ESQUERDA);
                }else
                {
                    if(MovementInput.Y > 0)
                    {
                        setAnimation(EnumAnimation.ANDAR_CIMA);
                    }else
                    {
                        if (MovementInput.Y < 0)
                        {
                            setAnimation(EnumAnimation.ANDAR_BAIXO);
                        }
                        else
                        {

                            if (SpriteInstance.JustCycled)
                            {
                                SpriteInstance.Animate = false;
                                SpriteInstance.CurrentFrameIndex = 0;                                
                                
                                // setAnimation(EnumAnimation.PARADO_BAIXO);   // Define a anima��o para IDLE caso n�o esteja sendo efetuada nenhuma movimenta��o
                            }
                        }

    
                    }

                }
            }

         

        }

        public void ActionShot()
        {
            //  Vector2 directionShot = new Vector2();
            //Console.WriteLine(ShotInput.Magnitude);
            EnumItem shotSelect = EnumItem.FIREBALL;
            if (GlobalData.PlayerData.ItemList.ContainsKey(shotSelect))
            {
                if (ShotInput_UP.WasJustPressed || ShotInput_Dow.WasJustPressed || ShotInput_Left.WasJustPressed || ShotInput_Rigth.WasJustPressed)
                {
                    if (ShotInput.Magnitude != 0)
                    {

                        Projectiles.createProjectiles(Projectiles.VariableState.FireBall, this.X, this.Y, ShotInput, true);
                     
                        removeToInventory(shotSelect, 1);
                    }

                }
            }
        }


        public void ActiontDie()
        {
            Destroy();
            //  Console.WriteLine(MovementSpeed + dashVelocityModify);

        }

        public bool ActionTakeHitMelee(Enemy enemy)
        {
            if (statusPlay != EnumPlay.INTANGIBLE)
            {
                SoundOof.Play();
                if (GlobalData.PlayerData.health - enemy.Atack > 0) { 
                    GlobalData.PlayerData.health -= enemy.Atack;
                    GlobalData.hudInstance.LoadHealthToHUD();
                    statusPlay = EnumPlay.INTANGIBLE;
                    startIntangibleTime = TimeManager.CurrentTime;
                }else
                {
                    
                    gameOverPLay();                   
                }
             
                return true;
            }else
            {
                return false;
            }


        }
        public void ActionTakeHit(Projectiles projectile)
        {
            if (statusPlay != EnumPlay.INTANGIBLE)
            {
                // Console.WriteLine("framecurente:" +projectile.SpriteInstance.CurrentFrameIndex);
                
                if (projectile.SpriteInstance.CurrentFrameIndex >= projectile.FrameHitStart)
                {
                    SoundOof.Play();
                    if (GlobalData.PlayerData.health - projectile.Atack > 0)
                    {
                        GlobalData.PlayerData.health -= projectile.Atack;
                        GlobalData.hudInstance.LoadHealthToHUD();
                        statusPlay = EnumPlay.INTANGIBLE;
                        startIntangibleTime = TimeManager.CurrentTime;

                    }else
                    {

                        gameOverPLay();
                       
                    }
                }
              
            }else
            {
              //  Console.WriteLine("playerIntangível");
            }


        }

      private void gameOverPLay()
        {
          GlobalData.gameOverGlobal(false);
          this.Destroy();
        }



        private void DashActivity()
        {
           
            if (interacaoNPC)
            {
                interacaoNPC = false;
            }
            {
                if (DashInput.WasJustPressed && MovementInput.Magnitude > 0)
                {
                    // Console.WriteLine("dash1"+ TimeManager.SecondsSince(lastDashTime));
                    if (TimeManager.SecondsSince(lastDashTime) > DashFrequency)
                    {

                        shouldDash = true;
                        startDashTime = TimeManager.CurrentTime;
                        lastDashTime = TimeManager.CurrentTime;
                        statusPlay = EnumPlay.INTANGIBLE;
                    }

                }
            }
            if (shouldDash)
            {

                //Console.WriteLine("dash2.5");
                if (TimeManager.SecondsSince(startDashTime) < DashDuraction)
                {
                   // Console.WriteLine("dash3");
                    dashVelocityModify = DashSpeed;
                    
                }else
                {
                   // Console.WriteLine("dash4");
                    dashVelocityModify = 0;
                    shouldDash = false;
                    statusPlay = EnumPlay.DEFAULT;
                }
            }
            

        }

        public void CollisionItem(Item item)
        {
            if (item.itemType == EnumItem.KEY_SILVER)
            {
                takeItem(item);
                
            }

           
        }
        public void takeItem(Item item)
        {

            item.SoundItem.Play();
            switch (item.itemType)
            {
                case EnumItem.KEY_SILVER:
                    addToInventory(item, 1);
                    GlobalData.PlayerData.qtdkeys++;
                    GlobalData.currentLevel.currentDoor.keyUsed++;
                    item.Destroy();
                    if(GlobalData.currentLevel.currentDoor.keyUsed == GlobalData.currentLevel.currentDoor.keyToOpen)
                    {
                        removeToInventory(EnumItem.KEY_SILVER, GlobalData.PlayerData.ItemList[EnumItem.KEY_SILVER].qtd);
                    }
                    break;

                case EnumItem.FIREBALL:
                    //Console.WriteLine(item.itemQtd);
                    addToInventory(item, item.itemQtd);                    
                    item.Destroy();
                    break;


            }

           



        }
       

        private void addToInventory(Item item, int qtd) {

            if (GlobalData.PlayerData.ItemList.ContainsKey(item.itemType))
            {
                GlobalData.PlayerData.ItemList[item.itemType].qtd += qtd ;
               // GlobalData.hudInstance.AtualizaItemHUD(item.IndexInventory, GlobalData.PlayerData.ItemList[item.itemType]);
            }
            else
            {
                InvetoryItem invItem = new InvetoryItem();
                invItem.type = item.itemType;
                invItem.qtd = qtd;
                invItem.iventoryIndex = item.IndexInventory;
                GlobalData.PlayerData.ItemList.Add(item.itemType, invItem);
            }

            GlobalData.hudInstance.LoadItensToHUD();
        }
        

        private void removeToInventory(EnumItem item, int qtd)
        {

            if (GlobalData.PlayerData.ItemList.ContainsKey(item))
            {
                GlobalData.PlayerData.ItemList[item].qtd -= qtd;
                if (GlobalData.PlayerData.ItemList[item].qtd == 0)
                {
                    GlobalData.PlayerData.ItemList.Remove(item);
                }
            }
            else
            {
               Console.WriteLine(" não existem item no inventário");
            }

            GlobalData.hudInstance.LoadItensToHUD();

        }

        private void  setAnimation(EnumAnimation playerAnimation)
        {
            switch(playerAnimation)
            {

                
                case EnumAnimation.PARADO_BAIXO:
                    SpriteInstance.Animate = true;
                    SpriteInstance.CurrentChainName = "INATIVO";
                    break;
                case EnumAnimation.ANDAR_BAIXO:
                    SpriteInstance.Animate = true;
                    SpriteInstance.CurrentChainName = "BAIXO";
                    break;

                case EnumAnimation.ANDAR_CIMA:
                    SpriteInstance.Animate = true;
                    SpriteInstance.CurrentChainName = "CIMA";
                    break;

                case EnumAnimation.ANDAR_DIREITA:
                    SpriteInstance.Animate = true;
                    SpriteInstance.CurrentChainName = "DIREITA";
                    break;

                case EnumAnimation.ANDAR_ESQUERDA:
                    SpriteInstance.Animate = true;
                    SpriteInstance.CurrentChainName = "ESQUERDA";
                    break;

            }

        }
        private void CustomDestroy()
		{

		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }
	}
}
