using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;
using Microsoft.Xna.Framework;
using DungeonRun.MinhasClasses.Data;
using DungeonRun.MinhasClasses;

namespace DungeonRun.Entities
{

   
    /// <summary>
    /// Deve ser definido o status da porta com o comando "SetStatus"
    /// </summary>
    public partial class Door
	{
       
        public int keyToOpen { get; set; }        
        public int keyUsed { get; set; }
        public Boolean isDoorOpen { get; set; }

        private Boolean isDoor = false; // s� eve saber se � porta depois de instaciado o status
        
        /// <summary>
        /// Apenas usa a regra se foram definidas chaves para a porta
        /// </summary>
        private bool IsToOpen {
            get
            {                
                return (keyToOpen == keyUsed && isDoor && keyToOpen >0);
            }
            }  
        
        
        private void CustomInitialize()
		{
            keyUsed = 0;
           
        }
        

		private void CustomActivity()
		{

            if (this.IsToOpen )
            {
                if (!isDoorOpen)
                {
                    openDoor();
                    isDoorOpen = true;
                }

            }else
            {
                closeDoor();
                isDoorOpen = false;
            }


		}

        private void closeDoor()
        {

            switch (this.CurrentState)
            {

                case VariableState.DoorNoTopOpen:
                    this.CurrentState = VariableState.DoorNoTop;
                    break;
                case VariableState.DoorBottonOpen:
                    this.CurrentState = VariableState.DoorBotton;
                    break;
                case VariableState.DoorTopOpen:
                    this.CurrentState = VariableState.DoorTop;
                    break;
                //default:
                   // this.CurrentState = VariableState.DoorNoTop;
                    break;
            }


        }

		
        public void openDoor()
        {
            switch (this.CurrentState) {

                case VariableState.DoorNoTop:
                    this.CurrentState = VariableState.DoorNoTopOpen;
                    break;
                case VariableState.DoorBotton:
                    this.CurrentState = VariableState.DoorBottonOpen;
                    break;
                case VariableState.DoorTop:
                    this.CurrentState = VariableState.DoorTopOpen;
                    break;
                    // default:
                    //    this.CurrentState = VariableState.DoorNoTopOpen;
                    break;
            }

            tocaSom();

        }

        /// <summary>
        /// Executa Audio da porta
        /// </summary>
        public void tocaSom()
        {
            //+  SoundOpenDoorIsLooped = false;
            // SoundOpenDoor.IsLooped = false;
            //SoundOpenDoor.Play();

            Console.WriteLine( SoundDoorOpen.IsLooped.ToString());
            SoundDoorOpen.Play();
            
        }
        
        private void CustomDestroy()
        {
            

        }

       
    

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        public void setStatusDoor(Boolean isdoor)

        {
            this.isDoor = isdoor;
            float topEdge = SpriteManager.Camera.AbsoluteTopYEdgeAt(0);
            float bottomEdge = SpriteManager.Camera.AbsoluteBottomYEdgeAt(0);
            float leftEdge = SpriteManager.Camera.AbsoluteLeftXEdgeAt(0);
            float rigtEdge = SpriteManager.Camera.AbsoluteRightXEdgeAt(0);
            // Console.WriteLine("top:" + topEdge + "|botom+ " + bottomEdge);

            float topCompara = Math.Abs(this.Y - topEdge);
            float bottomCompara = Math.Abs(this.Y - bottomEdge);
            float leftCompara = Math.Abs(this.X - leftEdge);
            float rigthCompara = Math.Abs(this.X - rigtEdge);

            //Console.WriteLine(topCompara + "|" + bottomCompara + "|" + leftCompara + "|" + rigthCompara);
            //Console.WriteLine(this.Y + "|" + this.X + "|door:"+ isdoor);



            if (isdoor)
            {

                //    topCompara maior
                if(topCompara < bottomCompara  && topCompara < leftCompara && topCompara < rigthCompara)
                {
                    this.CurrentState = Door.VariableState.DoorTop;
                }
                else if(bottomCompara < topCompara && bottomCompara < leftCompara && bottomCompara < rigthCompara) //baixo maior
                {
                    this.CurrentState = Door.VariableState.DoorBotton;
                }
                else // esquerda maior
                {
                    this.CurrentState = Door.VariableState.DoorNoTop;

                }
              
            }else////////////CARREGA PAREDE
            {

                if (topCompara < bottomCompara && topCompara < leftCompara && topCompara < rigthCompara)
                {
                    this.CurrentState = Door.VariableState.WallTop;
                    this.Y += 16;
                }
                else if (bottomCompara < topCompara && bottomCompara < leftCompara && bottomCompara < rigthCompara) //baixo maior
                {
                    this.CurrentState = Door.VariableState.WallBottom;
                }
                else // esquerda maior
                {
                    this.CurrentState = Door.VariableState.WallNotop;

                }

            }


           /*
            if (isdoor)
            { ///carrega porta
                //currentDoor = door;
               this.CurrentState = Door.VariableState.DoorTop;
                if (this.Y < topEdge - GlobalData.wallHeigth)
                {
                    this.CurrentState = Door.VariableState.DoorNoTop;

                    if (this.Y< bottomEdge + 32)
                    {
                        this.CurrentState = Door.VariableState.DoorBotton;

                    }

                }
            }
            else
            {//CarregaParede
                if (this.Y > topEdge - GlobalData.wallHeigth)
                {
                    this.CurrentState = Door.VariableState.WallTop;
                    this.Y += 16;
                }
                else
                {
                    if (this.Y < bottomEdge + 32)
                    {
                        // Console.WriteLine("entrou2");
                        this.CurrentState = Door.VariableState.WallBottom;

                    }
                }

            }*/

        }

       
    }
}
