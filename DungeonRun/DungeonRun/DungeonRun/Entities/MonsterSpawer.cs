using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;
using Microsoft.Xna.Framework;
using DungeonRun.Factories;
using DungeonRun.MinhasClasses.Data;

namespace DungeonRun.Entities
{
	public partial class MonsterSpawer
	{
        Boolean teste = true;
        public double  mLastSpawnTime { set; get; }
        public List<int> idListEnemyToSpawer = new List<int>();
        public FlatRedBall.Math.PositionedObjectList<Enemy> mEnemylist;
        public FlatRedBall.Math.PositionedObjectList<Enemy> Enemylist        

        {
            set
            {
                mEnemylist = value;

            }
            get
            {
                return mEnemylist;
            } 
            
               
        }
        public static  int currentMonterSpawned { get; set; }

        public String outroLevel = "";


        bool IsTimeToSpawn
        {
            get
            {
                float spawnFrequency = 1 / SpawerPerSecond;
               return  (GlobalData.currentLevel.levelTime - mLastSpawnTime) > spawnFrequency;
                // return FlatRedBall.Screens.ScreenManager.CurrentScreen.PauseAdjustedSecondsSince(mLastSpawnTime) > spawnFrequency;
            }
        }
        private bool inicializedMonster = false;





        private void CustomInitialize()        {

            loadMonsterToLevel();
 


        }
     

        private void CustomActivity()
		{

            if (!GlobalData.isGamePause)
            {
                if (!inicializedMonster)
                {
                  //  Console.WriteLine("Inicialize" + GlobalData.currentLevel.monsterInicialize);
                    inicializeMonters(GlobalData.currentLevel.monsterInicialize);
                    inicializedMonster = true;
                }
                //Console.WriteLine("CurrentSp:" + currentMonterSpawned + "| Killed:" + GlobalData.currentLevel.monsterKilled + "|Max: " + GlobalData.currentLevel.MonsterSpawerMax);

                if (IsTimeToSpawn && (GlobalData.currentLevel.EnableSpawer) && (currentMonterSpawned - GlobalData.currentLevel.monsterKilled)<= GlobalData.currentLevel.MonsterSpawerMax)
                {
                  
                    if (GlobalData.currentLevel.existMonsterSpawerPoints)
                    {
                        // createEnemy(Enemy.VariableState.Skeleton);
                        ramdomMonster(false);
                        currentMonterSpawned++;
                        //Console.WriteLine("CurrentonsterSpaw" + currentMonterSpawned);
                    }

                    // movementMonster();    
                }
            }

           

        }

        private void defineBoss()
        {
            int nota = DungeonRun.MinhasClasses.CalculatePoints.calculateTotalPoints();
            if (nota > 95)
            {
                createEnemy(Enemy.VariableState.Guiliandri, true);

            }
            else
            {
                createEnemy(Enemy.VariableState.Galvis, true);

            }
        }
        /// <summary>
        /// Cria monstros iniciais do mapa.
        /// </summary>
        private void inicializeMonters(int monsterQtd)
        {
            // inicializ a boim
            if (GlobalData.currentLevel.isLevelBoss)
            {
                defineBoss();
                currentMonterSpawned++;

            }
            

                for (int i = 0; i < monsterQtd; i++)
                {
               
                    ramdomMonster(true);
                currentMonterSpawned++;
                }
                            
        }


        /// <summary>
        /// Carrega uma lista com o id dos montros definido no level
        /// </summary>
        public void loadMonsterToLevel()
        {

            currentMonterSpawned = 0;
            if (GlobalData.currentLevel.hasSpider)
            {
                idListEnemyToSpawer.Add(0);
               // Console.WriteLine("addSpider");
            }

            if (GlobalData.currentLevel.hasSkeleton) {
                idListEnemyToSpawer.Add(1);
              //  Console.WriteLine("addSkeleton");
            };

            if (GlobalData.currentLevel.hasOgro)
            {
                idListEnemyToSpawer.Add(2);
               // Console.WriteLine("addTroll");
            }
            if (GlobalData.currentLevel.hasGalvis)
            {
                idListEnemyToSpawer.Add(3);
                // Console.WriteLine("addTroll");
            }
            if (GlobalData.currentLevel.hasGuiliandri)
            {
                idListEnemyToSpawer.Add(4);
                // Console.WriteLine("addTroll");
            }



        }

        /// <summary>
        /// cria a inst�ncia de um montro n�o Boss ALEAT�RIO
        /// </summary>

        public void ramdomMonster(Boolean randomSpawer)
        {
            //int monster = FlatRedBallServices.Random.Next(GlobalData.MonsterQtd);
            int monster = 99;

            if (idListEnemyToSpawer.Count != 0) {
                 monster = idListEnemyToSpawer[FlatRedBallServices.Random.Next((idListEnemyToSpawer.Count))];                 
               // Console.WriteLine("RandomMonsater:"+monster);
            }
            
            switch (monster)
            {

                case 0://spider
                    createEnemy(Enemy.VariableState.Spider, randomSpawer);
                    break;
                case 1://skeleton
                    createEnemy(Enemy.VariableState.Skeleton, randomSpawer);
                    break;
                case 2://troll
                    createEnemy(Enemy.VariableState.Ogro, randomSpawer);
                    break;
                case 3://galvis
                    createEnemy(Enemy.VariableState.Galvis, randomSpawer);
                    break;
                case 4:
                    createEnemy(Enemy.VariableState.Guiliandri, randomSpawer);
                    break;
                default:
                    //createEnemy(Enemy.VariableState.Spider, randomSpawer);
                    Console.WriteLine("Monstro fora da lista");
                    break;
            }

        }




        /// <summary>
        /// Cria um novo monstro  em uma posi��o fixa
        /// </summary>
        public void createEnemy(Enemy.VariableState statusMonster, bool randomPosition)
        {//
            Vector3 position = Vector3.Zero;

            if (randomPosition)
            {
                Vector2 random = GlobalData.currentLevel.RandomGroundTilePosition(15, GlobalData.currentLevel.currentDoor.Position);
                position.X = random.X;
                position.Y = random.Y;
                position.Z = GlobalData.PlayerData.playerInstance.Z -1;
            }
            else
            {
                position = GetRandomMonsterSpawerPosition();
            }
            
            
            //Vector3 velocity = GetRandomRockVelocity(position);
            Enemy enemy = EnemyFactory.CreateNew();
            enemy.Position = position;
            //Console.WriteLine(statusMonster);
            switch (statusMonster) { 
                case Enemy.VariableState.Spider:
                    enemy.CurrentState = Enemy.VariableState.Spider;
                break;
                case Enemy.VariableState.Skeleton:
                    enemy.CurrentState = Enemy.VariableState.Skeleton;
                break;
                case Enemy.VariableState.Galvis:
                    enemy.CurrentState = Enemy.VariableState.Galvis;
                break;
                case Enemy.VariableState.Guiliandri:
                    
                    enemy.CurrentState = Enemy.VariableState.Guiliandri;
                    break;
                case Enemy.VariableState.Ogro:
                    enemy.CurrentState = Enemy.VariableState.Ogro;
                    break;


            }
            enemy.Health = enemy.StartingHealth;
           
            
            //mLastSpawnTime = TimeManager.CurrentTime;
            mLastSpawnTime = GlobalData.currentLevel.levelTime;     
 

        }

      


        /// <summary>
        /// Busca no Levvel Atual os devidos pontos de respaw dos monstros
        /// </summary>
        private Vector3 GetRandomMonsterSpawerPosition()
        {
            Vector3 position = new Vector3();
            Vector2 randomPosition = GlobalData.currentLevel.getRandomMonsterSpawerPoint();
            Vector3 direction = getDiretion(new Vector3(randomPosition.X, randomPosition.Y, 0), Vector3.Zero);
            int mod = FlatRedBallServices.Random.Next(16);
            direction *= mod;

            position.X = randomPosition.X + direction.X;
            position.Y = randomPosition.Y + direction.X;
            position.Z = GlobalData.PlayerData.playerInstance.Z - 1;

            return position;

        }

        public Vector3 getDiretion(Vector3 position,Vector3 Togo)
        {
            // 1.  Encontra caminho para ir            
            Vector3 centerOfGameScreen = new Vector3(Togo.X, Togo.Y, 0);

            // 2.  Get the direction towards the center of the screen
            Vector3 directionToCenter = Togo - position;

            // 3.  Normalize the direction,
            directionToCenter.Normalize();
            return directionToCenter;

        }

        private void CustomDestroy()
		{

        }

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }

      
    }

   
}
