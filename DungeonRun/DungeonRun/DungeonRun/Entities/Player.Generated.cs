#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using DungeonRun.Screens;
using FlatRedBall.Graphics;
using FlatRedBall.Math;
using FlatRedBall.Gui;
using DungeonRun.Entities;
using DungeonRun.Factories;
using FlatRedBall;
using FlatRedBall.Screens;
using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall.Math.Geometry;
namespace DungeonRun.Entities
{
    public partial class Player : FlatRedBall.PositionedObject, FlatRedBall.Graphics.IDestroyable, FlatRedBall.Graphics.IVisible, FlatRedBall.Gui.IWindow, FlatRedBall.Math.Geometry.ICollidable
    {
        // This is made static so that static lazy-loaded content can access it.
        public static string ContentManagerName { get; set; }
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        static object mLockObject = new object();
        static System.Collections.Generic.List<string> mRegisteredUnloads = new System.Collections.Generic.List<string>();
        static System.Collections.Generic.List<string> LoadedContentManagers = new System.Collections.Generic.List<string>();
        protected static FlatRedBall.Graphics.Animation.AnimationChainList XalakomAnimation;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D Xalakom;
        protected static Microsoft.Xna.Framework.Audio.SoundEffect FemaleOof1;
        
        private FlatRedBall.Math.Geometry.AxisAlignedRectangle mCollisionInstance;
        public FlatRedBall.Math.Geometry.AxisAlignedRectangle CollisionInstance
        {
            get
            {
                return mCollisionInstance;
            }
            private set
            {
                mCollisionInstance = value;
            }
        }
        private FlatRedBall.Sprite SpriteInstance;
        private FlatRedBall.Math.PositionedObjectList<DungeonRun.Entities.Item> ItemList;
        private FlatRedBall.Math.Geometry.AxisAlignedRectangle mCollisionHitBox;
        public FlatRedBall.Math.Geometry.AxisAlignedRectangle CollisionHitBox
        {
            get
            {
                return mCollisionHitBox;
            }
            private set
            {
                mCollisionHitBox = value;
            }
        }
        private Microsoft.Xna.Framework.Audio.SoundEffectInstance SoundOof;
        public int MovementSpeed = 200;
        public int DashSpeed = 300;
        public float DashFrequency = 5f;
        public System.Double DashDuraction = 0.3;
        public int StartingHealth = 10;
        public float Mass = 1f;
        public int StartingFireBallQtd;
        public int intagibleDuration = 1;
        public event System.EventHandler BeforeVisibleSet;
        public event System.EventHandler AfterVisibleSet;
        protected bool mVisible = true;
        public virtual bool Visible
        {
            get
            {
                return mVisible;
            }
            set
            {
                if (BeforeVisibleSet != null)
                {
                    BeforeVisibleSet(this, null);
                }
                mVisible = value;
                if (AfterVisibleSet != null)
                {
                    AfterVisibleSet(this, null);
                }
            }
        }
        public bool IgnoresParentVisibility { get; set; }
        public bool AbsoluteVisible
        {
            get
            {
                return Visible && (Parent == null || IgnoresParentVisibility || Parent is FlatRedBall.Graphics.IVisible == false || (Parent as FlatRedBall.Graphics.IVisible).AbsoluteVisible);
            }
        }
        FlatRedBall.Graphics.IVisible FlatRedBall.Graphics.IVisible.Parent
        {
            get
            {
                if (this.Parent != null && this.Parent is FlatRedBall.Graphics.IVisible)
                {
                    return this.Parent as FlatRedBall.Graphics.IVisible;
                }
                else
                {
                    return null;
                }
            }
        }
        private FlatRedBall.Math.Geometry.ShapeCollection mGeneratedCollision;
        public FlatRedBall.Math.Geometry.ShapeCollection Collision
        {
            get
            {
                return mGeneratedCollision;
            }
        }
        protected FlatRedBall.Graphics.Layer LayerProvidedByContainer = null;
        public Player () 
        	: this(FlatRedBall.Screens.ScreenManager.CurrentScreen.ContentManagerName, true)
        {
        }
        public Player (string contentManagerName) 
        	: this(contentManagerName, true)
        {
        }
        public Player (string contentManagerName, bool addToManagers) 
        	: base()
        {
            ContentManagerName = contentManagerName;
            InitializeEntity(addToManagers);
        }
        protected virtual void InitializeEntity (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            SoundOof = FemaleOof1.CreateInstance();
            mCollisionInstance = new FlatRedBall.Math.Geometry.AxisAlignedRectangle();
            mCollisionInstance.Name = "mCollisionInstance";
            SpriteInstance = new FlatRedBall.Sprite();
            SpriteInstance.Name = "SpriteInstance";
            ItemList = new FlatRedBall.Math.PositionedObjectList<DungeonRun.Entities.Item>();
            ItemList.Name = "ItemList";
            mCollisionHitBox = new FlatRedBall.Math.Geometry.AxisAlignedRectangle();
            mCollisionHitBox.Name = "mCollisionHitBox";
            this.Click += CallLosePush;
            this.RollOff += CallLosePush;
            
            PostInitialize();
            if (addToManagers)
            {
                AddToManagers(null);
            }
        }
        public virtual void ReAddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.Gui.GuiManager.AddWindow(this);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mCollisionInstance, LayerProvidedByContainer);
            FlatRedBall.SpriteManager.AddToLayer(SpriteInstance, LayerProvidedByContainer);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mCollisionHitBox, LayerProvidedByContainer);
        }
        public virtual void AddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.Gui.GuiManager.AddWindow(this);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mCollisionInstance, LayerProvidedByContainer);
            FlatRedBall.SpriteManager.AddToLayer(SpriteInstance, LayerProvidedByContainer);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mCollisionHitBox, LayerProvidedByContainer);
            AddToManagersBottomUp(layerToAddTo);
            CustomInitialize();
        }
        public virtual void Activity () 
        {
            mIsPaused = false;
            
            for (int i = ItemList.Count - 1; i > -1; i--)
            {
                if (i < ItemList.Count)
                {
                    // We do the extra if-check because activity could destroy any number of entities
                    ItemList[i].Activity();
                }
            }
            CustomActivity();
        }
        public virtual void Destroy () 
        {
            FlatRedBall.SpriteManager.RemovePositionedObject(this);
            FlatRedBall.Gui.GuiManager.RemoveWindow(this);
            
            ItemList.MakeOneWay();
            if (CollisionInstance != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.Remove(CollisionInstance);
            }
            if (SpriteInstance != null)
            {
                FlatRedBall.SpriteManager.RemoveSprite(SpriteInstance);
            }
            for (int i = ItemList.Count - 1; i > -1; i--)
            {
                ItemList[i].Destroy();
            }
            if (CollisionHitBox != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.Remove(CollisionHitBox);
            }
            ItemList.MakeTwoWay();
            mGeneratedCollision.RemoveFromManagers(clearThis: false);
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            if (mCollisionInstance.Parent == null)
            {
                mCollisionInstance.CopyAbsoluteToRelative();
                mCollisionInstance.AttachTo(this, false);
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.Y = -20f;
            }
            else
            {
                CollisionInstance.RelativeY = -20f;
            }
            CollisionInstance.Width = 16f;
            CollisionInstance.Height = 16f;
            CollisionInstance.Visible = false;
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.CopyAbsoluteToRelative();
                SpriteInstance.AttachTo(this, false);
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.X = 0f;
            }
            else
            {
                SpriteInstance.RelativeX = 0f;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.Y = 0f;
            }
            else
            {
                SpriteInstance.RelativeY = 0f;
            }
            SpriteInstance.Texture = null;
            #if FRB_MDX
            SpriteInstance.TextureAddressMode = Microsoft.DirectX.Direct3D.TextureAddress.Clamp;
            #else
            SpriteInstance.TextureAddressMode = Microsoft.Xna.Framework.Graphics.TextureAddressMode.Clamp;
            #endif
            SpriteInstance.TextureScale = 0.7f;
            SpriteInstance.AnimationChains = XalakomAnimation;
            SpriteInstance.CurrentChainName = "INATIVO";
            SpriteInstance.Visible = true;
            if (mCollisionHitBox.Parent == null)
            {
                mCollisionHitBox.CopyAbsoluteToRelative();
                mCollisionHitBox.AttachTo(this, false);
            }
            if (CollisionHitBox.Parent == null)
            {
                CollisionHitBox.Y = -10f;
            }
            else
            {
                CollisionHitBox.RelativeY = -10f;
            }
            CollisionHitBox.Width = 32f;
            CollisionHitBox.Height = 32f;
            CollisionHitBox.Visible = false;
            CollisionHitBox.Color = Color.Violet;
            mGeneratedCollision = new FlatRedBall.Math.Geometry.ShapeCollection();
            mGeneratedCollision.AxisAlignedRectangles.AddOneWay(mCollisionInstance);
            mGeneratedCollision.AxisAlignedRectangles.AddOneWay(mCollisionHitBox);
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            FlatRedBall.Gui.GuiManager.RemoveWindow(this);
            if (CollisionInstance != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.RemoveOneWay(CollisionInstance);
            }
            if (SpriteInstance != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(SpriteInstance);
            }
            for (int i = ItemList.Count - 1; i > -1; i--)
            {
                ItemList[i].Destroy();
            }
            if (CollisionHitBox != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.RemoveOneWay(CollisionHitBox);
            }
            mGeneratedCollision.RemoveFromManagers(clearThis: false);
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.Y = -20f;
            }
            else
            {
                CollisionInstance.RelativeY = -20f;
            }
            CollisionInstance.Width = 16f;
            CollisionInstance.Height = 16f;
            CollisionInstance.Visible = false;
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.X = 0f;
            }
            else
            {
                SpriteInstance.RelativeX = 0f;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.Y = 0f;
            }
            else
            {
                SpriteInstance.RelativeY = 0f;
            }
            SpriteInstance.Texture = null;
            #if FRB_MDX
            SpriteInstance.TextureAddressMode = Microsoft.DirectX.Direct3D.TextureAddress.Clamp;
            #else
            SpriteInstance.TextureAddressMode = Microsoft.Xna.Framework.Graphics.TextureAddressMode.Clamp;
            #endif
            SpriteInstance.TextureScale = 0.7f;
            SpriteInstance.AnimationChains = XalakomAnimation;
            SpriteInstance.CurrentChainName = "INATIVO";
            SpriteInstance.Visible = true;
            if (CollisionHitBox.Parent == null)
            {
                CollisionHitBox.Y = -10f;
            }
            else
            {
                CollisionHitBox.RelativeY = -10f;
            }
            CollisionHitBox.Width = 32f;
            CollisionHitBox.Height = 32f;
            CollisionHitBox.Visible = false;
            CollisionHitBox.Color = Color.Violet;
            if (Parent == null)
            {
                X = 100f;
            }
            else
            {
                RelativeX = 100f;
            }
            if (Parent == null)
            {
                Y = -100f;
            }
            else
            {
                RelativeY = -100f;
            }
            if (Parent == null)
            {
                Z = 10f;
            }
            else if (Parent is FlatRedBall.Camera)
            {
                RelativeZ = 10f - 40.0f;
            }
            else
            {
                RelativeZ = 10f;
            }
            MovementSpeed = 200;
            DashSpeed = 300;
            DashFrequency = 5f;
            DashDuraction = 0.3;
            StartingHealth = 10;
            Mass = 1f;
            intagibleDuration = 1;
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            this.ForceUpdateDependenciesDeep();
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(SpriteInstance);
            for (int i = 0; i < ItemList.Count; i++)
            {
                ItemList[i].ConvertToManuallyUpdated();
            }
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            ContentManagerName = contentManagerName;
            // Set the content manager for Gum
            var contentManagerWrapper = new FlatRedBall.Gum.ContentManagerWrapper();
            contentManagerWrapper.ContentManagerName = contentManagerName;
            RenderingLibrary.Content.LoaderManager.Self.ContentLoader = contentManagerWrapper;
            // Access the GumProject just in case it's async loaded
            var throwaway = GlobalContent.GumProject;
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            bool registerUnload = false;
            if (LoadedContentManagers.Contains(contentManagerName) == false)
            {
                LoadedContentManagers.Add(contentManagerName);
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("PlayerStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/entities/player/xalakomanimation.achx", ContentManagerName))
                {
                    registerUnload = true;
                }
                XalakomAnimation = FlatRedBall.FlatRedBallServices.Load<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/entities/player/xalakomanimation.achx", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/player/xalakom.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                Xalakom = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/player/xalakom.png", ContentManagerName);
                FemaleOof1 = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Audio.SoundEffect>(@"content/entities/player/femaleoof1", ContentManagerName);
            }
            if (registerUnload && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("PlayerStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            CustomLoadStaticContent(contentManagerName);
        }
        public static void UnloadStaticContent () 
        {
            if (LoadedContentManagers.Count != 0)
            {
                LoadedContentManagers.RemoveAt(0);
                mRegisteredUnloads.RemoveAt(0);
            }
            if (LoadedContentManagers.Count == 0)
            {
                if (XalakomAnimation != null)
                {
                    XalakomAnimation= null;
                }
                if (Xalakom != null)
                {
                    Xalakom= null;
                }
                if (FemaleOof1 != null)
                {
                    FemaleOof1= null;
                }
            }
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            switch(memberName)
            {
                case  "XalakomAnimation":
                    return XalakomAnimation;
                case  "Xalakom":
                    return Xalakom;
                case  "FemaleOof1":
                    return FemaleOof1;
            }
            return null;
        }
        public static object GetFile (string memberName) 
        {
            switch(memberName)
            {
                case  "XalakomAnimation":
                    return XalakomAnimation;
                case  "Xalakom":
                    return Xalakom;
                case  "FemaleOof1":
                    return FemaleOof1;
            }
            return null;
        }
        object GetMember (string memberName) 
        {
            switch(memberName)
            {
                case  "XalakomAnimation":
                    return XalakomAnimation;
                case  "Xalakom":
                    return Xalakom;
                case  "FemaleOof1":
                    return FemaleOof1;
            }
            return null;
        }
        
    // DELEGATE START HERE
    

        #region IWindow methods and properties

        public event FlatRedBall.Gui.WindowEvent Click;
		public event FlatRedBall.Gui.WindowEvent ClickNoSlide;
		public event FlatRedBall.Gui.WindowEvent SlideOnClick;
        public event FlatRedBall.Gui.WindowEvent Push;
		public event FlatRedBall.Gui.WindowEvent DragOver;
		public event FlatRedBall.Gui.WindowEvent RollOn;
		public event FlatRedBall.Gui.WindowEvent RollOff;
		public event FlatRedBall.Gui.WindowEvent RollOver;
		public event FlatRedBall.Gui.WindowEvent LosePush;
		public event FlatRedBall.Gui.WindowEvent EnabledChange;

        System.Collections.ObjectModel.ReadOnlyCollection<FlatRedBall.Gui.IWindow> FlatRedBall.Gui.IWindow.Children
        {
            get { throw new System.NotImplementedException(); }
        }

        bool mEnabled = true;


		bool FlatRedBall.Graphics.IVisible.Visible
        {
            get
            {
                return this.AbsoluteVisible;
            }
			set
			{
				this.Visible = value;
			}
        }
		
		public bool MovesWhenGrabbed
        {
            get;
            set;
        }

        bool FlatRedBall.Gui.IWindow.GuiManagerDrawn
        {
            get
            {
                return false;
            }
            set
            {
                throw new System.NotImplementedException();
            }
        }

        public bool IgnoredByCursor
        {
            get
            {
                return false;
            }
            set
            {
                throw new System.NotImplementedException();
            }
        }



        public System.Collections.ObjectModel.ReadOnlyCollection<FlatRedBall.Gui.IWindow> FloatingChildren
        {
            get { return null; }
        }

        public FlatRedBall.ManagedSpriteGroups.SpriteFrame SpriteFrame
        {
            get
            {
                return null;
            }
            set
            {
                throw new System.NotImplementedException();
            }
        }

        float FlatRedBall.Gui.IWindow.WorldUnitX
        {
            get
            {
                return Position.X;
            }
            set
            {
                throw new System.NotImplementedException();
            }
        }

        float FlatRedBall.Gui.IWindow.WorldUnitY
        {
            get
            {
                return Position.Y;
            }
            set
            {
                throw new System.NotImplementedException();
            }
        }

        float FlatRedBall.Gui.IWindow.WorldUnitRelativeX
        {
            get
            {
                return RelativePosition.X;
            }
            set
            {
                throw new System.NotImplementedException();
            }
        }

        float FlatRedBall.Gui.IWindow.WorldUnitRelativeY
        {
            get
            {
                return RelativePosition.Y;
            }
            set
            {
                throw new System.NotImplementedException();
            }
        }

        float FlatRedBall.Gui.IWindow.ScaleX
        {
            get;
            set;
        }

        float FlatRedBall.Gui.IWindow.ScaleY
        {
            get;
            set;
        }

        FlatRedBall.Gui.IWindow FlatRedBall.Gui.IWindow.Parent
        {
            get
            {
                return null;
            }
            set
            {
                throw new System.NotImplementedException();
            }
        }

        void FlatRedBall.Gui.IWindow.Activity(FlatRedBall.Camera camera)
        {

        }

        void FlatRedBall.Gui.IWindow.CallRollOff()
        {
			if(RollOff != null)
			{
				RollOff(this);
			}
        }

        void FlatRedBall.Gui.IWindow.CallRollOn()
        {
			if(RollOn != null)
			{
				RollOn(this);
			}
        }

		
		void FlatRedBall.Gui.IWindow.CallRollOver()
		{
			if(RollOver != null)
			{
				RollOver(this);
			}	
		}
		
		void CallLosePush(FlatRedBall.Gui.IWindow instance)
		{
			if(LosePush != null)
			{
				LosePush(instance);
			}
		}

        void FlatRedBall.Gui.IWindow.CloseWindow()
        {
            throw new System.NotImplementedException();
        }

		void FlatRedBall.Gui.IWindow.CallClick()
		{
			if(Click != null)
			{
				Click(this);
			}
		}

        public bool GetParentVisibility()
        {
            throw new System.NotImplementedException();
        }

        bool FlatRedBall.Gui.IWindow.IsPointOnWindow(float x, float y)
        {
            throw new System.NotImplementedException();
        }

        public void OnDragging()
        {
			if(DragOver != null)
			{
				DragOver(this);
			}
        }

        public void OnResize()
        {
            throw new System.NotImplementedException();
        }

        public void OnResizeEnd()
        {
            throw new System.NotImplementedException();
        }

        public void OnLosingFocus()
        {
            // Do nothing
        }

        public bool OverlapsWindow(FlatRedBall.Gui.IWindow otherWindow)
        {
            return false; // we don't care about this.
        }

        public void SetScaleTL(float newScaleX, float newScaleY)
        {
            throw new System.NotImplementedException();
        }

        public void SetScaleTL(float newScaleX, float newScaleY, bool keepTopLeftStatic)
        {
            throw new System.NotImplementedException();
        }

        public virtual void TestCollision(FlatRedBall.Gui.Cursor cursor)
        {
            if (HasCursorOver(cursor))
            {
                cursor.WindowOver = this;

                if (cursor.PrimaryPush)
                {

                    cursor.WindowPushed = this;

                    if (Push != null)
                        Push(this);

						
					cursor.GrabWindow(this);

                }

                if (cursor.PrimaryClick) // both pushing and clicking can occur in one frame because of buffered input
                {
                    if (cursor.WindowPushed == this)
                    {
                        if (Click != null)
                        {
                            Click(this);
                        }
						if(cursor.PrimaryClickNoSlide && ClickNoSlide != null)
						{
							ClickNoSlide(this);
						}

                        // if (cursor.PrimaryDoubleClick && DoubleClick != null)
                        //   DoubleClick(this);
                    }
					else
					{
						if(SlideOnClick != null)
						{
							SlideOnClick(this);
						}
					}
                }
            }
        }

        void FlatRedBall.Gui.IWindow.UpdateDependencies()
        {
            // do nothing
        }

        FlatRedBall.Graphics.Layer FlatRedBall.Graphics.ILayered.Layer
        {
            get
            {
				return LayerProvidedByContainer;
            }
        }


        #endregion

        bool FlatRedBall.Gui.IWindow.Enabled
        {
            get
            {
                return mEnabled;
            }
            set
            {
                if (mEnabled != value)
                {
                    mEnabled = value;
                    EnabledChange?.Invoke(this);
                }
            }
        }
        public virtual bool HasCursorOver (FlatRedBall.Gui.Cursor cursor) 
        {
            if (mIsPaused)
            {
                return false;
            }
            if (!AbsoluteVisible)
            {
                return false;
            }
            if (LayerProvidedByContainer != null && LayerProvidedByContainer.Visible == false)
            {
                return false;
            }
            if (!cursor.IsOn(LayerProvidedByContainer))
            {
                return false;
            }
            if (cursor.IsOn3D(CollisionInstance, LayerProvidedByContainer))
            {
                return true;
            }
            if (SpriteInstance.Alpha != 0 && SpriteInstance.AbsoluteVisible && cursor.IsOn3D(SpriteInstance, LayerProvidedByContainer))
            {
                return true;
            }
            if (cursor.IsOn3D(CollisionHitBox, LayerProvidedByContainer))
            {
                return true;
            }
            return false;
        }
        public virtual bool WasClickedThisFrame (FlatRedBall.Gui.Cursor cursor) 
        {
            return cursor.PrimaryClick && HasCursorOver(cursor);
        }
        protected bool mIsPaused;
        public override void Pause (FlatRedBall.Instructions.InstructionList instructions) 
        {
            base.Pause(instructions);
            mIsPaused = true;
            if (SoundOof.State == Microsoft.Xna.Framework.Audio.SoundState.Playing)
            {
                SoundOof.Pause();
                instructions.Add(new FlatRedBall.Instructions.DelegateInstruction(() => SoundOof.Resume()));
            }
        }
        public virtual void SetToIgnorePausing () 
        {
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(this);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(CollisionInstance);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(SpriteInstance);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(CollisionHitBox);
        }
        public virtual void MoveToLayer (FlatRedBall.Graphics.Layer layerToMoveTo) 
        {
            var layerToRemoveFrom = LayerProvidedByContainer;
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(CollisionInstance);
            }
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(CollisionInstance, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(SpriteInstance);
            }
            FlatRedBall.SpriteManager.AddToLayer(SpriteInstance, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(CollisionHitBox);
            }
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(CollisionHitBox, layerToMoveTo);
            LayerProvidedByContainer = layerToMoveTo;
        }
    }
    public static class PlayerExtensionMethods
    {
        public static void SetVisible (this FlatRedBall.Math.PositionedObjectList<Player> list, bool value) 
        {
            int count = list.Count;
            for (int i = 0; i < count; i++)
            {
                list[i].Visible = value;
            }
        }
    }
}
