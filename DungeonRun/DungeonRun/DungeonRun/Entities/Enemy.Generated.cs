#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using DungeonRun.Screens;
using FlatRedBall.Graphics;
using FlatRedBall.Math;
using DungeonRun.Performance;
using DungeonRun.Entities;
using DungeonRun.Factories;
using FlatRedBall;
using FlatRedBall.Screens;
using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall.Math.Geometry;
namespace DungeonRun.Entities
{
    public partial class Enemy : FlatRedBall.PositionedObject, FlatRedBall.Graphics.IDestroyable, FlatRedBall.Performance.IPoolable, FlatRedBall.Math.Geometry.ICollidable
    {
        // This is made static so that static lazy-loaded content can access it.
        public static string ContentManagerName { get; set; }
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        public enum VariableState
        {
            Uninitialized = 0, //This exists so that the first set call actually does something
            Unknown = 1, //This exists so that if the entity is actually a child entity and has set a child state, you will get this
            Spider = 2, 
            Skeleton = 3, 
            Galvis = 4, 
            Guiliandri = 5, 
            Ogro = 6
        }
        protected int mCurrentState = 0;
        public Entities.Enemy.VariableState CurrentState
        {
            get
            {
                if (mCurrentState >= 0 && mCurrentState <= 6)
                {
                    return (VariableState)mCurrentState;
                }
                else
                {
                    return VariableState.Unknown;
                }
            }
            set
            {
                mCurrentState = (int)value;
                switch(CurrentState)
                {
                    case  VariableState.Uninitialized:
                        break;
                    case  VariableState.Unknown:
                        break;
                    case  VariableState.Spider:
                        Speed = 110;
                        SpriteInstanceAnimationChains = AnimationSpider;
                        SpriteInstanceCurrentChainName = "INATIVO";
                        SpriteScale = 1f;
                        AtackInterval = 0.3f;
                        isAtackMelee = true;
                        MeleeRange = 0.5f;
                        timeAtackLoad = 0.27f;
                        hasBiteAtack = true;
                        isAtackProjectiles = false;
                        hasWaterAtack1 = false;
                        CollisionHitBoxHeight = 32f;
                        CollisionHitBoxWidth = 32f;
                        if (CollisionHitBox.Parent == null)
                        {
                            CollisionHitBoxY = 0f;
                        }
                        else
                        {
                            CollisionHitBox.RelativeY = 0f;
                        }
                        break;
                    case  VariableState.Skeleton:
                        Speed = 100;
                        SpriteInstanceAnimationChains = AnimationSkeleton;
                        SpriteInstanceCurrentChainName = "INATIVO";
                        SpriteScale = 2f;
                        AtackFrequency = 3f;
                        isAtackMelee = false;
                        isAtackProjectiles = true;
                        CollisionHitBoxHeight = 64f;
                        CollisionHitBoxWidth = 32f;
                        if (CollisionHitBox.Parent == null)
                        {
                            CollisionHitBoxY = 16f;
                        }
                        else
                        {
                            CollisionHitBox.RelativeY = 16f;
                        }
                        PlayDistance = 10;
                        break;
                    case  VariableState.Galvis:
                        Speed = 130;
                        SpriteInstanceAnimationChains = AnimationGalvis;
                        SpriteInstanceCurrentChainName = "INATIVO";
                        SpriteScale = 0.7f;
                        AtackFrequency = 2f;
                        isAtackMelee = false;
                        isAtackProjectiles = true;
                        hasWaterAtack1 = true;
                        StartingHealth = 10;
                        CollisionHitBoxHeight = 64f;
                        CollisionHitBoxWidth = 32f;
                        if (CollisionHitBox.Parent == null)
                        {
                            CollisionHitBoxY = 26f;
                        }
                        else
                        {
                            CollisionHitBox.RelativeY = 26f;
                        }
                        PlayDistance = 10;
                        break;
                    case  VariableState.Guiliandri:
                        Speed = 195;
                        SpriteInstanceAnimationChains = AnimationGuiliandri;
                        SpriteInstanceCurrentChainName = "INATIVO";
                        SpriteScale = 0.7f;
                        AtackInterval = 0.3f;
                        AtackFrequency = 1f;
                        isAtackMelee = true;
                        MeleeRange = 0.7f;
                        timeAtackLoad = 0.2f;
                        hasBiteAtack = false;
                        hasSwordAtack = true;
                        hasGroundShockAtack = false;
                        isAtackProjectiles = true;
                        hasWaterAtack1 = false;
                        StartingHealth = 20;
                        CollisionHitBoxHeight = 64f;
                        CollisionHitBoxWidth = 32f;
                        if (CollisionHitBox.Parent == null)
                        {
                            CollisionHitBoxY = 26f;
                        }
                        else
                        {
                            CollisionHitBox.RelativeY = 26f;
                        }
                        break;
                    case  VariableState.Ogro:
                        Speed = 80;
                        SpriteInstanceAnimationChains = AnimationOgro;
                        SpriteInstanceCurrentChainName = "INATIVO";
                        SpriteScale = 1f;
                        AtackInterval = 0.3f;
                        Atack = 2;
                        isAtackMelee = true;
                        MeleeRange = 2f;
                        timeAtackLoad = 0.2f;
                        hasBiteAtack = false;
                        hasGroundShockAtack = true;
                        isAtackProjectiles = false;
                        hasWaterAtack1 = false;
                        CollisionHitBoxHeight = 64f;
                        CollisionHitBoxWidth = 32f;
                        if (CollisionHitBox.Parent == null)
                        {
                            CollisionHitBoxY = 16f;
                        }
                        else
                        {
                            CollisionHitBox.RelativeY = 16f;
                        }
                        break;
                }
            }
        }
        static object mLockObject = new object();
        static System.Collections.Generic.List<string> mRegisteredUnloads = new System.Collections.Generic.List<string>();
        static System.Collections.Generic.List<string> LoadedContentManagers = new System.Collections.Generic.List<string>();
        protected static Microsoft.Xna.Framework.Graphics.Texture2D MainShip1;
        protected static FlatRedBall.AI.Pathfinding.NodeNetwork NodeNetworkFile;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D spider;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D troll_1;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D Skele_War_bow;
        protected static FlatRedBall.Graphics.Animation.AnimationChainList AnimationSpider;
        protected static FlatRedBall.Graphics.Animation.AnimationChainList AnimationSkeleton;
        protected static FlatRedBall.Graphics.Animation.AnimationChainList AnimationGuiliandri;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D Galvisr;
        protected static FlatRedBall.Graphics.Animation.AnimationChainList AnimationGalvis;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D GuiliandriS;
        protected static FlatRedBall.Graphics.Animation.AnimationChainList AnimationOgro;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D ogroS;
        protected static Microsoft.Xna.Framework.Audio.SoundEffectInstance DeathMonster;
        protected static Microsoft.Xna.Framework.Audio.SoundEffectInstance monster;
        
        private FlatRedBall.Sprite SpriteInstance;
        private FlatRedBall.Math.Geometry.AxisAlignedRectangle mCollisionInstance;
        public FlatRedBall.Math.Geometry.AxisAlignedRectangle CollisionInstance
        {
            get
            {
                return mCollisionInstance;
            }
            private set
            {
                mCollisionInstance = value;
            }
        }
        private FlatRedBall.Math.Geometry.AxisAlignedRectangle mCollisionHitBox;
        public FlatRedBall.Math.Geometry.AxisAlignedRectangle CollisionHitBox
        {
            get
            {
                return mCollisionHitBox;
            }
            private set
            {
                mCollisionHitBox = value;
            }
        }
        private Microsoft.Xna.Framework.Audio.SoundEffectInstance SoundDeathMonster;
        private Microsoft.Xna.Framework.Audio.SoundEffectInstance SoundMonster;
        public int Speed = 90;
        public float Mass = 1f;
        public FlatRedBall.Graphics.Animation.AnimationChainList SpriteInstanceAnimationChains
        {
            get
            {
                return SpriteInstance.AnimationChains;
            }
            set
            {
                SpriteInstance.AnimationChains = value;
            }
        }
        public string SpriteInstanceCurrentChainName
        {
            get
            {
                return SpriteInstance.CurrentChainName;
            }
            set
            {
                SpriteInstance.CurrentChainName = value;
            }
        }
        public float SpriteScale
        {
            get
            {
                return SpriteInstance.TextureScale;
            }
            set
            {
                SpriteInstance.TextureScale = value;
            }
        }
        public float CollisionInstanceHeight
        {
            get
            {
                return CollisionInstance.Height;
            }
            set
            {
                CollisionInstance.Height = value;
            }
        }
        public float CollisionInstanceWidth
        {
            get
            {
                return CollisionInstance.Width;
            }
            set
            {
                CollisionInstance.Width = value;
            }
        }
        public float AtackInterval = 3f;
        public int Atack = 1;
        public float AtackFrequency = 2f;
        public bool isAtackMelee = false;
        public float MeleeRange = 0.5f;
        public float timeAtackLoad = 0.3f;
        public bool hasBiteAtack;
        public bool hasSwordAtack;
        public bool hasGroundShockAtack = false;
        public bool isAtackProjectiles;
        public bool hasWaterAtack1;
        public int StartingHealth = 1;
        public float CollisionHitBoxHeight
        {
            get
            {
                return CollisionHitBox.Height;
            }
            set
            {
                CollisionHitBox.Height = value;
            }
        }
        public float CollisionHitBoxWidth
        {
            get
            {
                return CollisionHitBox.Width;
            }
            set
            {
                CollisionHitBox.Width = value;
            }
        }
        public float CollisionHitBoxY
        {
            get
            {
                if (CollisionHitBox.Parent == null)
                {
                    return CollisionHitBox.Y;
                }
                else
                {
                    return CollisionHitBox.RelativeY;
                }
            }
            set
            {
                if (CollisionHitBox.Parent == null)
                {
                    CollisionHitBox.Y = value;
                }
                else
                {
                    CollisionHitBox.RelativeY = value;
                }
            }
        }
        public int PlayDistance = 1;
        public int Index { get; set; }
        public bool Used { get; set; }
        private FlatRedBall.Math.Geometry.ShapeCollection mGeneratedCollision;
        public FlatRedBall.Math.Geometry.ShapeCollection Collision
        {
            get
            {
                return mGeneratedCollision;
            }
        }
        protected FlatRedBall.Graphics.Layer LayerProvidedByContainer = null;
        public Enemy () 
        	: this(FlatRedBall.Screens.ScreenManager.CurrentScreen.ContentManagerName, true)
        {
        }
        public Enemy (string contentManagerName) 
        	: this(contentManagerName, true)
        {
        }
        public Enemy (string contentManagerName, bool addToManagers) 
        	: base()
        {
            ContentManagerName = contentManagerName;
            InitializeEntity(addToManagers);
        }
        protected virtual void InitializeEntity (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            SoundDeathMonster = DeathMonster;
            SoundMonster = monster;
            SpriteInstance = new FlatRedBall.Sprite();
            SpriteInstance.Name = "SpriteInstance";
            mCollisionInstance = new FlatRedBall.Math.Geometry.AxisAlignedRectangle();
            mCollisionInstance.Name = "mCollisionInstance";
            mCollisionHitBox = new FlatRedBall.Math.Geometry.AxisAlignedRectangle();
            mCollisionHitBox.Name = "mCollisionHitBox";
            
            PostInitialize();
            if (addToManagers)
            {
                AddToManagers(null);
            }
        }
        public virtual void ReAddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.SpriteManager.AddToLayer(SpriteInstance, LayerProvidedByContainer);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mCollisionInstance, LayerProvidedByContainer);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mCollisionHitBox, LayerProvidedByContainer);
        }
        public virtual void AddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.SpriteManager.AddToLayer(SpriteInstance, LayerProvidedByContainer);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mCollisionInstance, LayerProvidedByContainer);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mCollisionHitBox, LayerProvidedByContainer);
            AddToManagersBottomUp(layerToAddTo);
            CustomInitialize();
        }
        public virtual void Activity () 
        {
            
            CustomActivity();
        }
        public virtual void Destroy () 
        {
            if (Used)
            {
                Factories.EnemyFactory.MakeUnused(this, false);
            }
            FlatRedBall.SpriteManager.RemovePositionedObject(this);
            
            if (SpriteInstance != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(SpriteInstance);
            }
            if (CollisionInstance != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.RemoveOneWay(CollisionInstance);
            }
            if (CollisionHitBox != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.RemoveOneWay(CollisionHitBox);
            }
            mGeneratedCollision.RemoveFromManagers(clearThis: false);
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.CopyAbsoluteToRelative();
                SpriteInstance.AttachTo(this, false);
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.X = 1f;
            }
            else
            {
                SpriteInstance.RelativeX = 1f;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.Y = 42f;
            }
            else
            {
                SpriteInstance.RelativeY = 42f;
            }
            SpriteInstance.Texture = null;
            SpriteInstance.TextureScale = 2f;
            SpriteInstance.Visible = true;
            if (mCollisionInstance.Parent == null)
            {
                mCollisionInstance.CopyAbsoluteToRelative();
                mCollisionInstance.AttachTo(this, false);
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.X = 0f;
            }
            else
            {
                CollisionInstance.RelativeX = 0f;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.Y = 0f;
            }
            else
            {
                CollisionInstance.RelativeY = 0f;
            }
            CollisionInstance.Width = 26f;
            CollisionInstance.Height = 26f;
            CollisionInstance.Visible = false;
            if (mCollisionHitBox.Parent == null)
            {
                mCollisionHitBox.CopyAbsoluteToRelative();
                mCollisionHitBox.AttachTo(this, false);
            }
            if (CollisionHitBox.Parent == null)
            {
                CollisionHitBox.Y = 26f;
            }
            else
            {
                CollisionHitBox.RelativeY = 26f;
            }
            CollisionHitBox.Width = 36f;
            CollisionHitBox.Height = 64f;
            CollisionHitBox.Visible = false;
            CollisionHitBox.Color = Color.Violet;
            mGeneratedCollision = new FlatRedBall.Math.Geometry.ShapeCollection();
            mGeneratedCollision.AxisAlignedRectangles.AddOneWay(mCollisionInstance);
            mGeneratedCollision.AxisAlignedRectangles.AddOneWay(mCollisionHitBox);
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            if (SpriteInstance != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(SpriteInstance);
            }
            if (CollisionInstance != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.RemoveOneWay(CollisionInstance);
            }
            if (CollisionHitBox != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.RemoveOneWay(CollisionHitBox);
            }
            mGeneratedCollision.RemoveFromManagers(clearThis: false);
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.X = 1f;
            }
            else
            {
                SpriteInstance.RelativeX = 1f;
            }
            if (SpriteInstance.Parent == null)
            {
                SpriteInstance.Y = 42f;
            }
            else
            {
                SpriteInstance.RelativeY = 42f;
            }
            SpriteInstance.Texture = null;
            SpriteInstance.TextureScale = 2f;
            SpriteInstance.Visible = true;
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.X = 0f;
            }
            else
            {
                CollisionInstance.RelativeX = 0f;
            }
            if (CollisionInstance.Parent == null)
            {
                CollisionInstance.Y = 0f;
            }
            else
            {
                CollisionInstance.RelativeY = 0f;
            }
            CollisionInstance.Width = 26f;
            CollisionInstance.Height = 26f;
            CollisionInstance.Visible = false;
            if (CollisionHitBox.Parent == null)
            {
                CollisionHitBox.Y = 26f;
            }
            else
            {
                CollisionHitBox.RelativeY = 26f;
            }
            CollisionHitBox.Width = 36f;
            CollisionHitBox.Height = 64f;
            CollisionHitBox.Visible = false;
            CollisionHitBox.Color = Color.Violet;
            Speed = 90;
            Mass = 1f;
            SpriteInstanceAnimationChains = AnimationOgro;
            SpriteScale = 2f;
            CollisionInstanceHeight = 26f;
            CollisionInstanceWidth = 26f;
            AtackInterval = 3f;
            Atack = 1;
            AtackFrequency = 2f;
            isAtackMelee = false;
            MeleeRange = 0.5f;
            timeAtackLoad = 0.3f;
            hasGroundShockAtack = false;
            StartingHealth = 1;
            CollisionHitBoxHeight = 64f;
            CollisionHitBoxWidth = 36f;
            CollisionHitBoxY = 26f;
            PlayDistance = 1;
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            this.ForceUpdateDependenciesDeep();
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(SpriteInstance);
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            ContentManagerName = contentManagerName;
            // Set the content manager for Gum
            var contentManagerWrapper = new FlatRedBall.Gum.ContentManagerWrapper();
            contentManagerWrapper.ContentManagerName = contentManagerName;
            RenderingLibrary.Content.LoaderManager.Self.ContentLoader = contentManagerWrapper;
            // Access the GumProject just in case it's async loaded
            var throwaway = GlobalContent.GumProject;
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            bool registerUnload = false;
            if (LoadedContentManagers.Contains(contentManagerName) == false)
            {
                LoadedContentManagers.Add(contentManagerName);
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("EnemyStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/enemy/mainship1.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                MainShip1 = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/enemy/mainship1.png", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<FlatRedBall.AI.Pathfinding.NodeNetwork>(@"content/entities/enemy/nodenetworkfile.nntx", ContentManagerName))
                {
                    registerUnload = true;
                }
                NodeNetworkFile = FlatRedBall.FlatRedBallServices.Load<FlatRedBall.AI.Pathfinding.NodeNetwork>(@"content/entities/enemy/nodenetworkfile.nntx", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/enemy/spider.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                spider = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/enemy/spider.png", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/resources/troll_1.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                troll_1 = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/resources/troll_1.png", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/resources/skele_war_bow.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                Skele_War_bow = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/resources/skele_war_bow.png", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/entities/enemy/animationspider.achx", ContentManagerName))
                {
                    registerUnload = true;
                }
                AnimationSpider = FlatRedBall.FlatRedBallServices.Load<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/entities/enemy/animationspider.achx", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/entities/enemy/animationskeleton.achx", ContentManagerName))
                {
                    registerUnload = true;
                }
                AnimationSkeleton = FlatRedBall.FlatRedBallServices.Load<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/entities/enemy/animationskeleton.achx", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/entities/enemy/animationguiliandri.achx", ContentManagerName))
                {
                    registerUnload = true;
                }
                AnimationGuiliandri = FlatRedBall.FlatRedBallServices.Load<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/entities/enemy/animationguiliandri.achx", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/enemy/galvisr.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                Galvisr = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/enemy/galvisr.png", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/entities/enemy/animationgalvis.achx", ContentManagerName))
                {
                    registerUnload = true;
                }
                AnimationGalvis = FlatRedBall.FlatRedBallServices.Load<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/entities/enemy/animationgalvis.achx", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/enemy/guiliandris.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                GuiliandriS = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/enemy/guiliandris.png", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/entities/enemy/animationogro.achx", ContentManagerName))
                {
                    registerUnload = true;
                }
                AnimationOgro = FlatRedBall.FlatRedBallServices.Load<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/entities/enemy/animationogro.achx", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/enemy/ogros.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                ogroS = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/enemy/ogros.png", ContentManagerName);
                DeathMonster = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Audio.SoundEffect>(@"content/entities/enemy/deathmonster", ContentManagerName).CreateInstance();
                monster = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Audio.SoundEffect>(@"content/entities/enemy/monster", ContentManagerName).CreateInstance();
            }
            if (registerUnload && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("EnemyStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            CustomLoadStaticContent(contentManagerName);
        }
        public static void UnloadStaticContent () 
        {
            if (LoadedContentManagers.Count != 0)
            {
                LoadedContentManagers.RemoveAt(0);
                mRegisteredUnloads.RemoveAt(0);
            }
            if (LoadedContentManagers.Count == 0)
            {
                if (MainShip1 != null)
                {
                    MainShip1= null;
                }
                if (NodeNetworkFile != null)
                {
                    NodeNetworkFile.Visible = false;
                    NodeNetworkFile= null;
                }
                if (spider != null)
                {
                    spider= null;
                }
                if (troll_1 != null)
                {
                    troll_1= null;
                }
                if (Skele_War_bow != null)
                {
                    Skele_War_bow= null;
                }
                if (AnimationSpider != null)
                {
                    AnimationSpider= null;
                }
                if (AnimationSkeleton != null)
                {
                    AnimationSkeleton= null;
                }
                if (AnimationGuiliandri != null)
                {
                    AnimationGuiliandri= null;
                }
                if (Galvisr != null)
                {
                    Galvisr= null;
                }
                if (AnimationGalvis != null)
                {
                    AnimationGalvis= null;
                }
                if (GuiliandriS != null)
                {
                    GuiliandriS= null;
                }
                if (AnimationOgro != null)
                {
                    AnimationOgro= null;
                }
                if (ogroS != null)
                {
                    ogroS= null;
                }
                if (DeathMonster != null)
                {
                    if(DeathMonster.IsDisposed == false) { DeathMonster.Stop();  DeathMonster.Dispose(); };
                    DeathMonster= null;
                }
                if (monster != null)
                {
                    if(monster.IsDisposed == false) { monster.Stop();  monster.Dispose(); };
                    monster= null;
                }
            }
        }
        static VariableState mLoadingState = VariableState.Uninitialized;
        public static VariableState LoadingState
        {
            get
            {
                return mLoadingState;
            }
            set
            {
                mLoadingState = value;
            }
        }
        public FlatRedBall.Instructions.Instruction InterpolateToState (VariableState stateToInterpolateTo, double secondsToTake) 
        {
            switch(stateToInterpolateTo)
            {
                case  VariableState.Spider:
                    if (CollisionHitBox.Parent != null)
                    {
                        CollisionHitBox.RelativeYVelocity = (0f - CollisionHitBox.RelativeY) / (float)secondsToTake;
                    }
                    else
                    {
                        CollisionHitBox.YVelocity = (0f - CollisionHitBox.Y) / (float)secondsToTake;
                    }
                    break;
                case  VariableState.Skeleton:
                    if (CollisionHitBox.Parent != null)
                    {
                        CollisionHitBox.RelativeYVelocity = (16f - CollisionHitBox.RelativeY) / (float)secondsToTake;
                    }
                    else
                    {
                        CollisionHitBox.YVelocity = (16f - CollisionHitBox.Y) / (float)secondsToTake;
                    }
                    break;
                case  VariableState.Galvis:
                    if (CollisionHitBox.Parent != null)
                    {
                        CollisionHitBox.RelativeYVelocity = (26f - CollisionHitBox.RelativeY) / (float)secondsToTake;
                    }
                    else
                    {
                        CollisionHitBox.YVelocity = (26f - CollisionHitBox.Y) / (float)secondsToTake;
                    }
                    break;
                case  VariableState.Guiliandri:
                    if (CollisionHitBox.Parent != null)
                    {
                        CollisionHitBox.RelativeYVelocity = (26f - CollisionHitBox.RelativeY) / (float)secondsToTake;
                    }
                    else
                    {
                        CollisionHitBox.YVelocity = (26f - CollisionHitBox.Y) / (float)secondsToTake;
                    }
                    break;
                case  VariableState.Ogro:
                    if (CollisionHitBox.Parent != null)
                    {
                        CollisionHitBox.RelativeYVelocity = (16f - CollisionHitBox.RelativeY) / (float)secondsToTake;
                    }
                    else
                    {
                        CollisionHitBox.YVelocity = (16f - CollisionHitBox.Y) / (float)secondsToTake;
                    }
                    break;
            }
            var instruction = new FlatRedBall.Instructions.DelegateInstruction<VariableState>(StopStateInterpolation, stateToInterpolateTo);
            instruction.TimeToExecute = FlatRedBall.TimeManager.CurrentTime + secondsToTake;
            this.Instructions.Add(instruction);
            return instruction;
        }
        public void StopStateInterpolation (VariableState stateToStop) 
        {
            switch(stateToStop)
            {
                case  VariableState.Spider:
                    if (CollisionHitBox.Parent != null)
                    {
                        CollisionHitBox.RelativeYVelocity =  0;
                    }
                    else
                    {
                        CollisionHitBox.YVelocity =  0;
                    }
                    break;
                case  VariableState.Skeleton:
                    if (CollisionHitBox.Parent != null)
                    {
                        CollisionHitBox.RelativeYVelocity =  0;
                    }
                    else
                    {
                        CollisionHitBox.YVelocity =  0;
                    }
                    break;
                case  VariableState.Galvis:
                    if (CollisionHitBox.Parent != null)
                    {
                        CollisionHitBox.RelativeYVelocity =  0;
                    }
                    else
                    {
                        CollisionHitBox.YVelocity =  0;
                    }
                    break;
                case  VariableState.Guiliandri:
                    if (CollisionHitBox.Parent != null)
                    {
                        CollisionHitBox.RelativeYVelocity =  0;
                    }
                    else
                    {
                        CollisionHitBox.YVelocity =  0;
                    }
                    break;
                case  VariableState.Ogro:
                    if (CollisionHitBox.Parent != null)
                    {
                        CollisionHitBox.RelativeYVelocity =  0;
                    }
                    else
                    {
                        CollisionHitBox.YVelocity =  0;
                    }
                    break;
            }
            CurrentState = stateToStop;
        }
        public void InterpolateBetween (VariableState firstState, VariableState secondState, float interpolationValue) 
        {
            #if DEBUG
            if (float.IsNaN(interpolationValue))
            {
                throw new System.Exception("interpolationValue cannot be NaN");
            }
            #endif
            bool setSpeed = true;
            int SpeedFirstValue= 0;
            int SpeedSecondValue= 0;
            bool setSpriteScale = true;
            float SpriteScaleFirstValue= 0;
            float SpriteScaleSecondValue= 0;
            bool setAtackInterval = true;
            float AtackIntervalFirstValue= 0;
            float AtackIntervalSecondValue= 0;
            bool setMeleeRange = true;
            float MeleeRangeFirstValue= 0;
            float MeleeRangeSecondValue= 0;
            bool settimeAtackLoad = true;
            float timeAtackLoadFirstValue= 0;
            float timeAtackLoadSecondValue= 0;
            bool setCollisionHitBoxHeight = true;
            float CollisionHitBoxHeightFirstValue= 0;
            float CollisionHitBoxHeightSecondValue= 0;
            bool setCollisionHitBoxWidth = true;
            float CollisionHitBoxWidthFirstValue= 0;
            float CollisionHitBoxWidthSecondValue= 0;
            bool setCollisionHitBoxY = true;
            float CollisionHitBoxYFirstValue= 0;
            float CollisionHitBoxYSecondValue= 0;
            bool setAtackFrequency = true;
            float AtackFrequencyFirstValue= 0;
            float AtackFrequencySecondValue= 0;
            bool setPlayDistance = true;
            int PlayDistanceFirstValue= 0;
            int PlayDistanceSecondValue= 0;
            bool setStartingHealth = true;
            int StartingHealthFirstValue= 0;
            int StartingHealthSecondValue= 0;
            bool setAtack = true;
            int AtackFirstValue= 0;
            int AtackSecondValue= 0;
            switch(firstState)
            {
                case  VariableState.Spider:
                    SpeedFirstValue = 110;
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceAnimationChains = AnimationSpider;
                    }
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceCurrentChainName = "INATIVO";
                    }
                    SpriteScaleFirstValue = 1f;
                    AtackIntervalFirstValue = 0.3f;
                    if (interpolationValue < 1)
                    {
                        this.isAtackMelee = true;
                    }
                    MeleeRangeFirstValue = 0.5f;
                    timeAtackLoadFirstValue = 0.27f;
                    if (interpolationValue < 1)
                    {
                        this.hasBiteAtack = true;
                    }
                    if (interpolationValue < 1)
                    {
                        this.isAtackProjectiles = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasWaterAtack1 = false;
                    }
                    CollisionHitBoxHeightFirstValue = 32f;
                    CollisionHitBoxWidthFirstValue = 32f;
                    CollisionHitBoxYFirstValue = 0f;
                    break;
                case  VariableState.Skeleton:
                    SpeedFirstValue = 100;
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceAnimationChains = AnimationSkeleton;
                    }
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceCurrentChainName = "INATIVO";
                    }
                    SpriteScaleFirstValue = 2f;
                    AtackFrequencyFirstValue = 3f;
                    if (interpolationValue < 1)
                    {
                        this.isAtackMelee = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.isAtackProjectiles = true;
                    }
                    CollisionHitBoxHeightFirstValue = 64f;
                    CollisionHitBoxWidthFirstValue = 32f;
                    CollisionHitBoxYFirstValue = 16f;
                    PlayDistanceFirstValue = 10;
                    break;
                case  VariableState.Galvis:
                    SpeedFirstValue = 130;
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceAnimationChains = AnimationGalvis;
                    }
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceCurrentChainName = "INATIVO";
                    }
                    SpriteScaleFirstValue = 0.7f;
                    AtackFrequencyFirstValue = 2f;
                    if (interpolationValue < 1)
                    {
                        this.isAtackMelee = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.isAtackProjectiles = true;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasWaterAtack1 = true;
                    }
                    StartingHealthFirstValue = 10;
                    CollisionHitBoxHeightFirstValue = 64f;
                    CollisionHitBoxWidthFirstValue = 32f;
                    CollisionHitBoxYFirstValue = 26f;
                    PlayDistanceFirstValue = 10;
                    break;
                case  VariableState.Guiliandri:
                    SpeedFirstValue = 195;
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceAnimationChains = AnimationGuiliandri;
                    }
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceCurrentChainName = "INATIVO";
                    }
                    SpriteScaleFirstValue = 0.7f;
                    AtackIntervalFirstValue = 0.3f;
                    AtackFrequencyFirstValue = 1f;
                    if (interpolationValue < 1)
                    {
                        this.isAtackMelee = true;
                    }
                    MeleeRangeFirstValue = 0.7f;
                    timeAtackLoadFirstValue = 0.2f;
                    if (interpolationValue < 1)
                    {
                        this.hasBiteAtack = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasSwordAtack = true;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasGroundShockAtack = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.isAtackProjectiles = true;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasWaterAtack1 = false;
                    }
                    StartingHealthFirstValue = 20;
                    CollisionHitBoxHeightFirstValue = 64f;
                    CollisionHitBoxWidthFirstValue = 32f;
                    CollisionHitBoxYFirstValue = 26f;
                    break;
                case  VariableState.Ogro:
                    SpeedFirstValue = 80;
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceAnimationChains = AnimationOgro;
                    }
                    if (interpolationValue < 1)
                    {
                        this.SpriteInstanceCurrentChainName = "INATIVO";
                    }
                    SpriteScaleFirstValue = 1f;
                    AtackIntervalFirstValue = 0.3f;
                    AtackFirstValue = 2;
                    if (interpolationValue < 1)
                    {
                        this.isAtackMelee = true;
                    }
                    MeleeRangeFirstValue = 2f;
                    timeAtackLoadFirstValue = 0.2f;
                    if (interpolationValue < 1)
                    {
                        this.hasBiteAtack = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasGroundShockAtack = true;
                    }
                    if (interpolationValue < 1)
                    {
                        this.isAtackProjectiles = false;
                    }
                    if (interpolationValue < 1)
                    {
                        this.hasWaterAtack1 = false;
                    }
                    CollisionHitBoxHeightFirstValue = 64f;
                    CollisionHitBoxWidthFirstValue = 32f;
                    CollisionHitBoxYFirstValue = 16f;
                    break;
            }
            switch(secondState)
            {
                case  VariableState.Spider:
                    SpeedSecondValue = 110;
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceAnimationChains = AnimationSpider;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceCurrentChainName = "INATIVO";
                    }
                    SpriteScaleSecondValue = 1f;
                    AtackIntervalSecondValue = 0.3f;
                    if (interpolationValue >= 1)
                    {
                        this.isAtackMelee = true;
                    }
                    MeleeRangeSecondValue = 0.5f;
                    timeAtackLoadSecondValue = 0.27f;
                    if (interpolationValue >= 1)
                    {
                        this.hasBiteAtack = true;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.isAtackProjectiles = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasWaterAtack1 = false;
                    }
                    CollisionHitBoxHeightSecondValue = 32f;
                    CollisionHitBoxWidthSecondValue = 32f;
                    CollisionHitBoxYSecondValue = 0f;
                    break;
                case  VariableState.Skeleton:
                    SpeedSecondValue = 100;
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceAnimationChains = AnimationSkeleton;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceCurrentChainName = "INATIVO";
                    }
                    SpriteScaleSecondValue = 2f;
                    AtackFrequencySecondValue = 3f;
                    if (interpolationValue >= 1)
                    {
                        this.isAtackMelee = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.isAtackProjectiles = true;
                    }
                    CollisionHitBoxHeightSecondValue = 64f;
                    CollisionHitBoxWidthSecondValue = 32f;
                    CollisionHitBoxYSecondValue = 16f;
                    PlayDistanceSecondValue = 10;
                    break;
                case  VariableState.Galvis:
                    SpeedSecondValue = 130;
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceAnimationChains = AnimationGalvis;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceCurrentChainName = "INATIVO";
                    }
                    SpriteScaleSecondValue = 0.7f;
                    AtackFrequencySecondValue = 2f;
                    if (interpolationValue >= 1)
                    {
                        this.isAtackMelee = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.isAtackProjectiles = true;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasWaterAtack1 = true;
                    }
                    StartingHealthSecondValue = 10;
                    CollisionHitBoxHeightSecondValue = 64f;
                    CollisionHitBoxWidthSecondValue = 32f;
                    CollisionHitBoxYSecondValue = 26f;
                    PlayDistanceSecondValue = 10;
                    break;
                case  VariableState.Guiliandri:
                    SpeedSecondValue = 195;
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceAnimationChains = AnimationGuiliandri;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceCurrentChainName = "INATIVO";
                    }
                    SpriteScaleSecondValue = 0.7f;
                    AtackIntervalSecondValue = 0.3f;
                    AtackFrequencySecondValue = 1f;
                    if (interpolationValue >= 1)
                    {
                        this.isAtackMelee = true;
                    }
                    MeleeRangeSecondValue = 0.7f;
                    timeAtackLoadSecondValue = 0.2f;
                    if (interpolationValue >= 1)
                    {
                        this.hasBiteAtack = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasSwordAtack = true;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasGroundShockAtack = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.isAtackProjectiles = true;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasWaterAtack1 = false;
                    }
                    StartingHealthSecondValue = 20;
                    CollisionHitBoxHeightSecondValue = 64f;
                    CollisionHitBoxWidthSecondValue = 32f;
                    CollisionHitBoxYSecondValue = 26f;
                    break;
                case  VariableState.Ogro:
                    SpeedSecondValue = 80;
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceAnimationChains = AnimationOgro;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.SpriteInstanceCurrentChainName = "INATIVO";
                    }
                    SpriteScaleSecondValue = 1f;
                    AtackIntervalSecondValue = 0.3f;
                    AtackSecondValue = 2;
                    if (interpolationValue >= 1)
                    {
                        this.isAtackMelee = true;
                    }
                    MeleeRangeSecondValue = 2f;
                    timeAtackLoadSecondValue = 0.2f;
                    if (interpolationValue >= 1)
                    {
                        this.hasBiteAtack = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasGroundShockAtack = true;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.isAtackProjectiles = false;
                    }
                    if (interpolationValue >= 1)
                    {
                        this.hasWaterAtack1 = false;
                    }
                    CollisionHitBoxHeightSecondValue = 64f;
                    CollisionHitBoxWidthSecondValue = 32f;
                    CollisionHitBoxYSecondValue = 16f;
                    break;
            }
            if (setSpeed)
            {
                Speed = FlatRedBall.Math.MathFunctions.RoundToInt(SpeedFirstValue* (1 - interpolationValue) + SpeedSecondValue * interpolationValue);
            }
            if (setSpriteScale)
            {
                SpriteScale = SpriteScaleFirstValue * (1 - interpolationValue) + SpriteScaleSecondValue * interpolationValue;
            }
            if (setAtackInterval)
            {
                AtackInterval = AtackIntervalFirstValue * (1 - interpolationValue) + AtackIntervalSecondValue * interpolationValue;
            }
            if (setMeleeRange)
            {
                MeleeRange = MeleeRangeFirstValue * (1 - interpolationValue) + MeleeRangeSecondValue * interpolationValue;
            }
            if (settimeAtackLoad)
            {
                timeAtackLoad = timeAtackLoadFirstValue * (1 - interpolationValue) + timeAtackLoadSecondValue * interpolationValue;
            }
            if (setCollisionHitBoxHeight)
            {
                CollisionHitBoxHeight = CollisionHitBoxHeightFirstValue * (1 - interpolationValue) + CollisionHitBoxHeightSecondValue * interpolationValue;
            }
            if (setCollisionHitBoxWidth)
            {
                CollisionHitBoxWidth = CollisionHitBoxWidthFirstValue * (1 - interpolationValue) + CollisionHitBoxWidthSecondValue * interpolationValue;
            }
            if (setCollisionHitBoxY)
            {
                if (CollisionHitBox.Parent != null)
                {
                    CollisionHitBox.RelativeY = CollisionHitBoxYFirstValue * (1 - interpolationValue) + CollisionHitBoxYSecondValue * interpolationValue;
                }
                else
                {
                    CollisionHitBoxY = CollisionHitBoxYFirstValue * (1 - interpolationValue) + CollisionHitBoxYSecondValue * interpolationValue;
                }
                if (setAtackFrequency)
                {
                    AtackFrequency = AtackFrequencyFirstValue * (1 - interpolationValue) + AtackFrequencySecondValue * interpolationValue;
                }
                if (setPlayDistance)
                {
                    PlayDistance = FlatRedBall.Math.MathFunctions.RoundToInt(PlayDistanceFirstValue* (1 - interpolationValue) + PlayDistanceSecondValue * interpolationValue);
                }
                if (setStartingHealth)
                {
                    StartingHealth = FlatRedBall.Math.MathFunctions.RoundToInt(StartingHealthFirstValue* (1 - interpolationValue) + StartingHealthSecondValue * interpolationValue);
                }
                if (setAtack)
                {
                    Atack = FlatRedBall.Math.MathFunctions.RoundToInt(AtackFirstValue* (1 - interpolationValue) + AtackSecondValue * interpolationValue);
                }
                if (interpolationValue < 1)
                {
                    mCurrentState = (int)firstState;
                }
                else
                {
                    mCurrentState = (int)secondState;
                }
            }
        }
        public static void PreloadStateContent (VariableState state, string contentManagerName) 
        {
            ContentManagerName = contentManagerName;
            switch(state)
            {
                case  VariableState.Spider:
                    {
                        object throwaway = AnimationSpider;
                    }
                    {
                        object throwaway = "INATIVO";
                    }
                    break;
                case  VariableState.Skeleton:
                    {
                        object throwaway = AnimationSkeleton;
                    }
                    {
                        object throwaway = "INATIVO";
                    }
                    break;
                case  VariableState.Galvis:
                    {
                        object throwaway = AnimationGalvis;
                    }
                    {
                        object throwaway = "INATIVO";
                    }
                    break;
                case  VariableState.Guiliandri:
                    {
                        object throwaway = AnimationGuiliandri;
                    }
                    {
                        object throwaway = "INATIVO";
                    }
                    break;
                case  VariableState.Ogro:
                    {
                        object throwaway = AnimationOgro;
                    }
                    {
                        object throwaway = "INATIVO";
                    }
                    break;
            }
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            switch(memberName)
            {
                case  "MainShip1":
                    return MainShip1;
                case  "NodeNetworkFile":
                    return NodeNetworkFile;
                case  "spider":
                    return spider;
                case  "troll_1":
                    return troll_1;
                case  "Skele_War_bow":
                    return Skele_War_bow;
                case  "AnimationSpider":
                    return AnimationSpider;
                case  "AnimationSkeleton":
                    return AnimationSkeleton;
                case  "AnimationGuiliandri":
                    return AnimationGuiliandri;
                case  "Galvisr":
                    return Galvisr;
                case  "AnimationGalvis":
                    return AnimationGalvis;
                case  "GuiliandriS":
                    return GuiliandriS;
                case  "AnimationOgro":
                    return AnimationOgro;
                case  "ogroS":
                    return ogroS;
                case  "DeathMonster":
                    return DeathMonster;
                case  "monster":
                    return monster;
            }
            return null;
        }
        public static object GetFile (string memberName) 
        {
            switch(memberName)
            {
                case  "MainShip1":
                    return MainShip1;
                case  "NodeNetworkFile":
                    return NodeNetworkFile;
                case  "spider":
                    return spider;
                case  "troll_1":
                    return troll_1;
                case  "Skele_War_bow":
                    return Skele_War_bow;
                case  "AnimationSpider":
                    return AnimationSpider;
                case  "AnimationSkeleton":
                    return AnimationSkeleton;
                case  "AnimationGuiliandri":
                    return AnimationGuiliandri;
                case  "Galvisr":
                    return Galvisr;
                case  "AnimationGalvis":
                    return AnimationGalvis;
                case  "GuiliandriS":
                    return GuiliandriS;
                case  "AnimationOgro":
                    return AnimationOgro;
                case  "ogroS":
                    return ogroS;
                case  "DeathMonster":
                    return DeathMonster;
                case  "monster":
                    return monster;
            }
            return null;
        }
        object GetMember (string memberName) 
        {
            switch(memberName)
            {
                case  "MainShip1":
                    return MainShip1;
                case  "NodeNetworkFile":
                    return NodeNetworkFile;
                case  "spider":
                    return spider;
                case  "troll_1":
                    return troll_1;
                case  "Skele_War_bow":
                    return Skele_War_bow;
                case  "AnimationSpider":
                    return AnimationSpider;
                case  "AnimationSkeleton":
                    return AnimationSkeleton;
                case  "AnimationGuiliandri":
                    return AnimationGuiliandri;
                case  "Galvisr":
                    return Galvisr;
                case  "AnimationGalvis":
                    return AnimationGalvis;
                case  "GuiliandriS":
                    return GuiliandriS;
                case  "AnimationOgro":
                    return AnimationOgro;
                case  "ogroS":
                    return ogroS;
                case  "DeathMonster":
                    return DeathMonster;
                case  "monster":
                    return monster;
            }
            return null;
        }
        protected bool mIsPaused;
        public override void Pause (FlatRedBall.Instructions.InstructionList instructions) 
        {
            base.Pause(instructions);
            mIsPaused = true;
            if (SoundDeathMonster.State == Microsoft.Xna.Framework.Audio.SoundState.Playing)
            {
                SoundDeathMonster.Pause();
                instructions.Add(new FlatRedBall.Instructions.DelegateInstruction(() => SoundDeathMonster.Resume()));
            }
            if (SoundMonster.State == Microsoft.Xna.Framework.Audio.SoundState.Playing)
            {
                SoundMonster.Pause();
                instructions.Add(new FlatRedBall.Instructions.DelegateInstruction(() => SoundMonster.Resume()));
            }
        }
        public virtual void SetToIgnorePausing () 
        {
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(this);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(SpriteInstance);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(CollisionInstance);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(CollisionHitBox);
        }
        public virtual void MoveToLayer (FlatRedBall.Graphics.Layer layerToMoveTo) 
        {
            var layerToRemoveFrom = LayerProvidedByContainer;
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(SpriteInstance);
            }
            FlatRedBall.SpriteManager.AddToLayer(SpriteInstance, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(CollisionInstance);
            }
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(CollisionInstance, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(CollisionHitBox);
            }
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(CollisionHitBox, layerToMoveTo);
            LayerProvidedByContainer = layerToMoveTo;
        }
    }
}
