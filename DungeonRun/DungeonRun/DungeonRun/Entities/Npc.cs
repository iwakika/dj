using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;

namespace DungeonRun.Entities
{
	public partial class Npc
	{
        /// <summary>
        /// Initialization logic which is execute only one time for this Entity (unless the Entity is pooled).
        /// This method is called when the Entity is added to managers. Entities which are instantiated but not
        /// added to managers will not have this method called.
        /// </summary>
        /// 
        List<string> dialogosList = new List<string>();
        private int dialofoIdx = 0;

		private void CustomInitialize()
		{
            switch (this.ID)
            {
                case 1: // dialogo Galvis
                    dialogosList.Add("Hora da sua avalia��o, pegue as chaves para sair das salas,"
                        +"estarei esperando na ultima sala.");
                    dialogosList.Add("Deixei uma brecha para voc� usar magia, apenas colete as chamas. Mas seu uso vai ser limitado!");
                    dialogosList.Add("Lembre-se que voc� pode escorregar pelos monstros sem que eles te acertem!");
                    break;   
            }

		}

        
        /// <summary>
        /// Carrega uma margem de dialogos para o npc;
        /// </summary>
        /// <param name="inicio" id  inicial do Dialogo Ex: 0></param>
        /// <param name="fim"></param>
        /// <returns></returns>
        public String buscaDialogo(int inicio, int fim)
        {
            int dialogoAtual = inicio + dialofoIdx;
            if(dialogoAtual > fim)
            {
                dialogoAtual = inicio;
                dialofoIdx = 0;
            }
            
            String dialogo = dialogosList[dialogoAtual];
            dialofoIdx++;
            return dialogo;
        }

		private void CustomActivity()
		{


		}

        public bool collisionNpc(Player p)
        {
            this.CollisionInstance.CollideAgainstMove(p.CollisionInstance, 1, 0);

            return this.mCollisionDialog.CollideAgainst(p.CollisionInstance);   
            

        }

		private void CustomDestroy()
		{
          

		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }
	}
}
