using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Graphics;
using FlatRedBall.Math.Geometry;
using FlatRedBall.Localization;
using Microsoft.Xna.Framework.Input;
using DungeonRun.MinhasClasses.Data;



namespace DungeonRun.Screens
{
	public partial class MainMenuScreen
	{


		void CustomInitialize()
		{
            FlatRedBallServices.Game.IsMouseVisible = true;
        }

        void CustomUpdate()
        {
        
            if(InputManager.Keyboard.GetKey(Keys.Back).WasJustPressed){
                if (txt_NameInputText.Length > 0)
                {
                    txt_NameInputText = txt_NameInputText.Substring(0, txt_NameInputText.Length-1);
                }
            }

            if (!InputManager.Keyboard.GetKey(Keys.LeftControl).IsDown && !HudInstance.Controle_DisplayInstance.Visible)
            {
                //  txt_NameInputText += TextManager.AddText(InputManager.Keyboard.GetStringTyped());
                if (txt_NameInputText.Length <= 20)
                {
                    txt_NameInputText += InputManager.Keyboard.GetStringTyped();
                }
                
            }


            if (InputManager.Keyboard.GetKey(Keys.C).WasJustPressed && HudInstance.Controle_DisplayInstance.Visible)
            {
               

                key_C(); ;
            }


            if (InputManager.Keyboard.GetKey(Keys.Enter).WasJustPressed)
            {
                if (DialogBoxInstance.Visible)
                {
                    DialogBoxInstance.Visible = false;
                }
                else
                {
                    key_enter();
                }
            }

            if (InputManager.Keyboard.GetKey(Keys.Escape).WasJustPressed)
            {
                if(DialogBoxInstance.Visible)
                {
                    DialogBoxInstance.Visible = false;
                }
               
                
            }


        }

		void CustomActivity(bool firstTimeCalled)
		{
            CustomUpdate();

            
        }

		void CustomDestroy()
		{
           

        }

        static void CustomLoadStaticContent(string contentManagerName)
        {


        }

	}
}
