﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menuteste.GumRuntimes
{
    partial class btn_NineSliceRuntime
    {

    partial void CustomInitialize()
        {

            this.Click += Btn_NineSliceRuntime_Click;
            this.RollOn += Btn_NineSliceRuntime_RollOn;
            this.RollOver += Btn_NineSliceRuntime_RollOver;
            this.RollOff += Btn_NineSliceRuntime_RollOff;
            
        }

        private void Btn_NineSliceRuntime_RollOff(FlatRedBall.Gui.IWindow window)
        {
            this.CurrentButtonCategoryState = ButtonCategory.Enabled;
        }

       
        private void Btn_NineSliceRuntime_RollOver(FlatRedBall.Gui.IWindow window)
        {
            //this.CurrentButtonCategoryState = ButtonCategory.Enabled;
        }
    

        private void Btn_NineSliceRuntime_RollOn(FlatRedBall.Gui.IWindow window)
        {
            this.CurrentButtonCategoryState = ButtonCategory.Highlighted;
        }

        private void Btn_NineSliceRuntime_Click(FlatRedBall.Gui.IWindow window)
        {
            this.CurrentButtonCategoryState = ButtonCategory.Pushed;
        }

        
        

    }
}
