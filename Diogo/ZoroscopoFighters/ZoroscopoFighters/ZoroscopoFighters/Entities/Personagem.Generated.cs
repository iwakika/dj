#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using ZoroscopoFighters.Screens;
using FlatRedBall.Graphics;
using FlatRedBall.Math;
using ZoroscopoFighters.Entities;
using FlatRedBall;
using FlatRedBall.Screens;
using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall.Math.Geometry;
namespace ZoroscopoFighters.Entities
{
    public partial class Personagem : FlatRedBall.PositionedObject, FlatRedBall.Graphics.IDestroyable
    {
        // This is made static so that static lazy-loaded content can access it.
        public static string ContentManagerName { get; set; }
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        public enum VariableState
        {
            Uninitialized = 0, //This exists so that the first set call actually does something
            Unknown = 1, //This exists so that if the entity is actually a child entity and has set a child state, you will get this
            Ken = 2, 
            Sagitario = 3, 
            Peixes = 4
        }
        protected int mCurrentState = 0;
        public Entities.Personagem.VariableState CurrentState
        {
            get
            {
                if (mCurrentState >= 0 && mCurrentState <= 4)
                {
                    return (VariableState)mCurrentState;
                }
                else
                {
                    return VariableState.Unknown;
                }
            }
            set
            {
                mCurrentState = (int)value;
                switch(CurrentState)
                {
                    case  VariableState.Uninitialized:
                        break;
                    case  VariableState.Unknown:
                        break;
                    case  VariableState.Ken:
                        AnimationSprite = ken;
                        break;
                    case  VariableState.Sagitario:
                        AnimationSprite = Sagitario;
                        break;
                    case  VariableState.Peixes:
                        AnimationSprite = Peixes;
                        break;
                }
            }
        }
        static object mLockObject = new object();
        static System.Collections.Generic.List<string> mRegisteredUnloads = new System.Collections.Generic.List<string>();
        static System.Collections.Generic.List<string> LoadedContentManagers = new System.Collections.Generic.List<string>();
        protected static FlatRedBall.Graphics.Animation.AnimationChainList idle;
        protected static FlatRedBall.Graphics.Animation.AnimationChainList ken;
        protected static FlatRedBall.Graphics.Animation.AnimationChainList Sagitario;
        protected static FlatRedBall.Graphics.Animation.AnimationChainList Peixes;
        
        private FlatRedBall.Math.Geometry.AxisAlignedRectangle mHitbox;
        public FlatRedBall.Math.Geometry.AxisAlignedRectangle Hitbox
        {
            get
            {
                return mHitbox;
            }
            private set
            {
                mHitbox = value;
            }
        }
        private ZoroscopoFighters.Entities.Hit HitMelee;
        private FlatRedBall.Math.PositionedObjectList<ZoroscopoFighters.Entities.Hit> ListaAtaquesRanged;
        private FlatRedBall.Sprite SpritePersonagem;
        public float VelocidadeSalto = 250f;
        public float VelocidadeMovimento = 200f;
        public float MaxTempoSalto = 0.2f;
        public float TempoInvulnerabilidade = 0.7f;
        public int HPInicial = 100;
        public int DanoSoco = 20;
        public int DanoChute = 30;
        public FlatRedBall.Graphics.Animation.AnimationChainList AnimationSprite
        {
            get
            {
                return SpritePersonagem.AnimationChains;
            }
            set
            {
                SpritePersonagem.AnimationChains = value;
            }
        }
        public float ValorPercentualModificador = 4f;
        protected FlatRedBall.Graphics.Layer LayerProvidedByContainer = null;
        public Personagem () 
        	: this(FlatRedBall.Screens.ScreenManager.CurrentScreen.ContentManagerName, true)
        {
        }
        public Personagem (string contentManagerName) 
        	: this(contentManagerName, true)
        {
        }
        public Personagem (string contentManagerName, bool addToManagers) 
        	: base()
        {
            ContentManagerName = contentManagerName;
            InitializeEntity(addToManagers);
        }
        protected virtual void InitializeEntity (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            mHitbox = new FlatRedBall.Math.Geometry.AxisAlignedRectangle();
            mHitbox.Name = "mHitbox";
            HitMelee = new ZoroscopoFighters.Entities.Hit(ContentManagerName, false);
            HitMelee.Name = "HitMelee";
            ListaAtaquesRanged = new FlatRedBall.Math.PositionedObjectList<ZoroscopoFighters.Entities.Hit>();
            ListaAtaquesRanged.Name = "ListaAtaquesRanged";
            SpritePersonagem = new FlatRedBall.Sprite();
            SpritePersonagem.Name = "SpritePersonagem";
            
            PostInitialize();
            if (addToManagers)
            {
                AddToManagers(null);
            }
        }
        public virtual void ReAddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mHitbox, LayerProvidedByContainer);
            HitMelee.ReAddToManagers(LayerProvidedByContainer);
            FlatRedBall.SpriteManager.AddToLayer(SpritePersonagem, LayerProvidedByContainer);
        }
        public virtual void AddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mHitbox, LayerProvidedByContainer);
            HitMelee.AddToManagers(LayerProvidedByContainer);
            FlatRedBall.SpriteManager.AddToLayer(SpritePersonagem, LayerProvidedByContainer);
            AddToManagersBottomUp(layerToAddTo);
            CustomInitialize();
        }
        public virtual void Activity () 
        {
            
            HitMelee.Activity();
            for (int i = ListaAtaquesRanged.Count - 1; i > -1; i--)
            {
                if (i < ListaAtaquesRanged.Count)
                {
                    // We do the extra if-check because activity could destroy any number of entities
                    ListaAtaquesRanged[i].Activity();
                }
            }
            CustomActivity();
        }
        public virtual void Destroy () 
        {
            FlatRedBall.SpriteManager.RemovePositionedObject(this);
            
            ListaAtaquesRanged.MakeOneWay();
            if (Hitbox != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.Remove(Hitbox);
            }
            if (HitMelee != null)
            {
                HitMelee.Destroy();
                HitMelee.Detach();
            }
            for (int i = ListaAtaquesRanged.Count - 1; i > -1; i--)
            {
                ListaAtaquesRanged[i].Destroy();
            }
            if (SpritePersonagem != null)
            {
                FlatRedBall.SpriteManager.RemoveSprite(SpritePersonagem);
            }
            ListaAtaquesRanged.MakeTwoWay();
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            if (mHitbox.Parent == null)
            {
                mHitbox.CopyAbsoluteToRelative();
                mHitbox.AttachTo(this, false);
            }
            if (Hitbox.Parent == null)
            {
                Hitbox.X = 18f;
            }
            else
            {
                Hitbox.RelativeX = 18f;
            }
            if (Hitbox.Parent == null)
            {
                Hitbox.Y = -4f;
            }
            else
            {
                Hitbox.RelativeY = -4f;
            }
            Hitbox.Width = 25f;
            Hitbox.Height = 20f;
            Hitbox.Visible = false;
            if (HitMelee.Parent == null)
            {
                HitMelee.CopyAbsoluteToRelative();
                HitMelee.AttachTo(this, false);
            }
            if (SpritePersonagem.Parent == null)
            {
                SpritePersonagem.CopyAbsoluteToRelative();
                SpritePersonagem.AttachTo(this, false);
            }
            if (SpritePersonagem.Parent == null)
            {
                SpritePersonagem.Z = 15f;
            }
            else
            {
                SpritePersonagem.RelativeZ = 15f;
            }
            SpritePersonagem.TextureScale = 1f;
            SpritePersonagem.Animate = true;
            SpritePersonagem.CurrentFrameIndex = 0;
            SpritePersonagem.AnimationSpeed = 1f;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            if (Hitbox != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.RemoveOneWay(Hitbox);
            }
            HitMelee.RemoveFromManagers();
            for (int i = ListaAtaquesRanged.Count - 1; i > -1; i--)
            {
                ListaAtaquesRanged[i].Destroy();
            }
            if (SpritePersonagem != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(SpritePersonagem);
            }
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
                HitMelee.AssignCustomVariables(true);
            }
            if (Hitbox.Parent == null)
            {
                Hitbox.X = 18f;
            }
            else
            {
                Hitbox.RelativeX = 18f;
            }
            if (Hitbox.Parent == null)
            {
                Hitbox.Y = -4f;
            }
            else
            {
                Hitbox.RelativeY = -4f;
            }
            Hitbox.Width = 25f;
            Hitbox.Height = 20f;
            Hitbox.Visible = false;
            if (SpritePersonagem.Parent == null)
            {
                SpritePersonagem.Z = 15f;
            }
            else
            {
                SpritePersonagem.RelativeZ = 15f;
            }
            SpritePersonagem.TextureScale = 1f;
            SpritePersonagem.Animate = true;
            SpritePersonagem.CurrentFrameIndex = 0;
            SpritePersonagem.AnimationSpeed = 1f;
            VelocidadeSalto = 250f;
            VelocidadeMovimento = 200f;
            MaxTempoSalto = 0.2f;
            TempoInvulnerabilidade = 0.7f;
            HPInicial = 100;
            DanoSoco = 20;
            DanoChute = 30;
            ValorPercentualModificador = 4f;
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            this.ForceUpdateDependenciesDeep();
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            HitMelee.ConvertToManuallyUpdated();
            for (int i = 0; i < ListaAtaquesRanged.Count; i++)
            {
                ListaAtaquesRanged[i].ConvertToManuallyUpdated();
            }
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(SpritePersonagem);
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            ContentManagerName = contentManagerName;
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            bool registerUnload = false;
            if (LoadedContentManagers.Contains(contentManagerName) == false)
            {
                LoadedContentManagers.Add(contentManagerName);
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("PersonagemStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/entities/personagem/idle.achx", ContentManagerName))
                {
                    registerUnload = true;
                }
                idle = FlatRedBall.FlatRedBallServices.Load<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/entities/personagem/idle.achx", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/entities/personagem/ken.achx", ContentManagerName))
                {
                    registerUnload = true;
                }
                ken = FlatRedBall.FlatRedBallServices.Load<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/entities/personagem/ken.achx", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/entities/personagem/sagitario.achx", ContentManagerName))
                {
                    registerUnload = true;
                }
                Sagitario = FlatRedBall.FlatRedBallServices.Load<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/entities/personagem/sagitario.achx", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/entities/personagem/peixes.achx", ContentManagerName))
                {
                    registerUnload = true;
                }
                Peixes = FlatRedBall.FlatRedBallServices.Load<FlatRedBall.Graphics.Animation.AnimationChainList>(@"content/entities/personagem/peixes.achx", ContentManagerName);
            }
            ZoroscopoFighters.Entities.Hit.LoadStaticContent(contentManagerName);
            if (registerUnload && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("PersonagemStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            CustomLoadStaticContent(contentManagerName);
        }
        public static void UnloadStaticContent () 
        {
            if (LoadedContentManagers.Count != 0)
            {
                LoadedContentManagers.RemoveAt(0);
                mRegisteredUnloads.RemoveAt(0);
            }
            if (LoadedContentManagers.Count == 0)
            {
                if (idle != null)
                {
                    idle= null;
                }
                if (ken != null)
                {
                    ken= null;
                }
                if (Sagitario != null)
                {
                    Sagitario= null;
                }
                if (Peixes != null)
                {
                    Peixes= null;
                }
            }
        }
        static VariableState mLoadingState = VariableState.Uninitialized;
        public static VariableState LoadingState
        {
            get
            {
                return mLoadingState;
            }
            set
            {
                mLoadingState = value;
            }
        }
        public FlatRedBall.Instructions.Instruction InterpolateToState (VariableState stateToInterpolateTo, double secondsToTake) 
        {
            switch(stateToInterpolateTo)
            {
                case  VariableState.Ken:
                    break;
                case  VariableState.Sagitario:
                    break;
                case  VariableState.Peixes:
                    break;
            }
            var instruction = new FlatRedBall.Instructions.DelegateInstruction<VariableState>(StopStateInterpolation, stateToInterpolateTo);
            instruction.TimeToExecute = FlatRedBall.TimeManager.CurrentTime + secondsToTake;
            this.Instructions.Add(instruction);
            return instruction;
        }
        public void StopStateInterpolation (VariableState stateToStop) 
        {
            switch(stateToStop)
            {
                case  VariableState.Ken:
                    break;
                case  VariableState.Sagitario:
                    break;
                case  VariableState.Peixes:
                    break;
            }
            CurrentState = stateToStop;
        }
        public void InterpolateBetween (VariableState firstState, VariableState secondState, float interpolationValue) 
        {
            #if DEBUG
            if (float.IsNaN(interpolationValue))
            {
                throw new System.Exception("interpolationValue cannot be NaN");
            }
            #endif
            switch(firstState)
            {
                case  VariableState.Ken:
                    if (interpolationValue < 1)
                    {
                        this.AnimationSprite = ken;
                    }
                    break;
                case  VariableState.Sagitario:
                    if (interpolationValue < 1)
                    {
                        this.AnimationSprite = Sagitario;
                    }
                    break;
                case  VariableState.Peixes:
                    if (interpolationValue < 1)
                    {
                        this.AnimationSprite = Peixes;
                    }
                    break;
            }
            switch(secondState)
            {
                case  VariableState.Ken:
                    if (interpolationValue >= 1)
                    {
                        this.AnimationSprite = ken;
                    }
                    break;
                case  VariableState.Sagitario:
                    if (interpolationValue >= 1)
                    {
                        this.AnimationSprite = Sagitario;
                    }
                    break;
                case  VariableState.Peixes:
                    if (interpolationValue >= 1)
                    {
                        this.AnimationSprite = Peixes;
                    }
                    break;
            }
            if (interpolationValue < 1)
            {
                mCurrentState = (int)firstState;
            }
            else
            {
                mCurrentState = (int)secondState;
            }
        }
        public static void PreloadStateContent (VariableState state, string contentManagerName) 
        {
            ContentManagerName = contentManagerName;
            switch(state)
            {
                case  VariableState.Ken:
                    {
                        object throwaway = ken;
                    }
                    break;
                case  VariableState.Sagitario:
                    {
                        object throwaway = Sagitario;
                    }
                    break;
                case  VariableState.Peixes:
                    {
                        object throwaway = Peixes;
                    }
                    break;
            }
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            switch(memberName)
            {
                case  "idle":
                    return idle;
                case  "ken":
                    return ken;
                case  "Sagitario":
                    return Sagitario;
                case  "Peixes":
                    return Peixes;
            }
            return null;
        }
        public static object GetFile (string memberName) 
        {
            switch(memberName)
            {
                case  "idle":
                    return idle;
                case  "ken":
                    return ken;
                case  "Sagitario":
                    return Sagitario;
                case  "Peixes":
                    return Peixes;
            }
            return null;
        }
        object GetMember (string memberName) 
        {
            switch(memberName)
            {
                case  "idle":
                    return idle;
                case  "ken":
                    return ken;
                case  "Sagitario":
                    return Sagitario;
                case  "Peixes":
                    return Peixes;
            }
            return null;
        }
        protected bool mIsPaused;
        public override void Pause (FlatRedBall.Instructions.InstructionList instructions) 
        {
            base.Pause(instructions);
            mIsPaused = true;
        }
        public virtual void SetToIgnorePausing () 
        {
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(this);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(Hitbox);
            HitMelee.SetToIgnorePausing();
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(SpritePersonagem);
        }
        public virtual void MoveToLayer (FlatRedBall.Graphics.Layer layerToMoveTo) 
        {
            var layerToRemoveFrom = LayerProvidedByContainer;
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(Hitbox);
            }
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(Hitbox, layerToMoveTo);
            HitMelee.MoveToLayer(layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(SpritePersonagem);
            }
            FlatRedBall.SpriteManager.AddToLayer(SpritePersonagem, layerToMoveTo);
            LayerProvidedByContainer = layerToMoveTo;
        }
    }
}
