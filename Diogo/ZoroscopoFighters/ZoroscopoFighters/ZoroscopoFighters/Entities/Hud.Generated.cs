#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using ZoroscopoFighters.Screens;
using FlatRedBall.Graphics;
using FlatRedBall.Math;
using ZoroscopoFighters.Entities;
using FlatRedBall;
using FlatRedBall.Screens;
using System;
using System.Collections.Generic;
using System.Text;
namespace ZoroscopoFighters.Entities
{
    public partial class Hud : FlatRedBall.PositionedObject, FlatRedBall.Graphics.IDestroyable
    {
        // This is made static so that static lazy-loaded content can access it.
        public static string ContentManagerName { get; set; }
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        static object mLockObject = new object();
        static System.Collections.Generic.List<string> mRegisteredUnloads = new System.Collections.Generic.List<string>();
        static System.Collections.Generic.List<string> LoadedContentManagers = new System.Collections.Generic.List<string>();
        protected static FlatRedBall.Graphics.BitmapFont Bauhaus93;
        protected static FlatRedBall.Graphics.BitmapFont Bauhaus93_48;
        
        private FlatRedBall.Math.PositionedObjectList<ZoroscopoFighters.Entities.BarraHP> ListaBarrasHP;
        private FlatRedBall.Math.PositionedObjectList<FlatRedBall.Graphics.Text> ListaNomesPersonagens;
        private FlatRedBall.Graphics.Text TextTimer;
        private FlatRedBall.Math.PositionedObjectList<ZoroscopoFighters.Entities.ContadorRounds> ListaContagemRounds;
        public int Timer
        {
            get
            {
                return int.Parse(TextTimer.DisplayText);
            }
            set
            {
                TextTimer.DisplayText = value.ToString();
            }
        }
        public float TextTimerY
        {
            get
            {
                if (TextTimer.Parent == null)
                {
                    return TextTimer.Y;
                }
                else
                {
                    return TextTimer.RelativeY;
                }
            }
            set
            {
                if (TextTimer.Parent == null)
                {
                    TextTimer.Y = value;
                }
                else
                {
                    TextTimer.RelativeY = value;
                }
            }
        }
        protected FlatRedBall.Graphics.Layer LayerProvidedByContainer = null;
        public Hud () 
        	: this(FlatRedBall.Screens.ScreenManager.CurrentScreen.ContentManagerName, true)
        {
        }
        public Hud (string contentManagerName) 
        	: this(contentManagerName, true)
        {
        }
        public Hud (string contentManagerName, bool addToManagers) 
        	: base()
        {
            ContentManagerName = contentManagerName;
            InitializeEntity(addToManagers);
        }
        protected virtual void InitializeEntity (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            ListaBarrasHP = new FlatRedBall.Math.PositionedObjectList<ZoroscopoFighters.Entities.BarraHP>();
            ListaBarrasHP.Name = "ListaBarrasHP";
            ListaNomesPersonagens = new FlatRedBall.Math.PositionedObjectList<FlatRedBall.Graphics.Text>();
            ListaNomesPersonagens.Name = "ListaNomesPersonagens";
            TextTimer = new FlatRedBall.Graphics.Text();
            TextTimer.Name = "TextTimer";
            ListaContagemRounds = new FlatRedBall.Math.PositionedObjectList<ZoroscopoFighters.Entities.ContadorRounds>();
            ListaContagemRounds.Name = "ListaContagemRounds";
            
            PostInitialize();
            if (addToManagers)
            {
                AddToManagers(null);
            }
        }
        public virtual void ReAddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.Graphics.TextManager.AddToLayer(TextTimer, LayerProvidedByContainer);
            if (TextTimer.Font != null)
            {
                TextTimer.SetPixelPerfectScale(LayerProvidedByContainer);
            }
        }
        public virtual void AddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.Graphics.TextManager.AddToLayer(TextTimer, LayerProvidedByContainer);
            if (TextTimer.Font != null)
            {
                TextTimer.SetPixelPerfectScale(LayerProvidedByContainer);
            }
            AddToManagersBottomUp(layerToAddTo);
            CustomInitialize();
        }
        public virtual void Activity () 
        {
            
            for (int i = ListaBarrasHP.Count - 1; i > -1; i--)
            {
                if (i < ListaBarrasHP.Count)
                {
                    // We do the extra if-check because activity could destroy any number of entities
                    ListaBarrasHP[i].Activity();
                }
            }
            for (int i = ListaContagemRounds.Count - 1; i > -1; i--)
            {
                if (i < ListaContagemRounds.Count)
                {
                    // We do the extra if-check because activity could destroy any number of entities
                    ListaContagemRounds[i].Activity();
                }
            }
            CustomActivity();
        }
        public virtual void Destroy () 
        {
            FlatRedBall.SpriteManager.RemovePositionedObject(this);
            
            ListaBarrasHP.MakeOneWay();
            ListaNomesPersonagens.MakeOneWay();
            ListaContagemRounds.MakeOneWay();
            for (int i = ListaBarrasHP.Count - 1; i > -1; i--)
            {
                ListaBarrasHP[i].Destroy();
            }
            for (int i = ListaNomesPersonagens.Count - 1; i > -1; i--)
            {
                FlatRedBall.Graphics.TextManager.RemoveText(ListaNomesPersonagens[i]);
            }
            if (TextTimer != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveText(TextTimer);
            }
            for (int i = ListaContagemRounds.Count - 1; i > -1; i--)
            {
                ListaContagemRounds[i].Destroy();
            }
            ListaBarrasHP.MakeTwoWay();
            ListaNomesPersonagens.MakeTwoWay();
            ListaContagemRounds.MakeTwoWay();
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            if (TextTimer.Parent == null)
            {
                TextTimer.CopyAbsoluteToRelative();
                TextTimer.AttachTo(this, false);
            }
            TextTimer.Font = Bauhaus93_48;
            if (TextTimer.Parent == null)
            {
                TextTimer.Y = 0f;
            }
            else
            {
                TextTimer.RelativeY = 0f;
            }
            if (TextTimer.Parent == null)
            {
                TextTimer.Z = 16f;
            }
            else
            {
                TextTimer.RelativeZ = 16f;
            }
            TextTimer.TextureScale = 1f;
            TextTimer.Red = 0f;
            TextTimer.Green = 0f;
            TextTimer.Blue = 0f;
            TextTimer.AdjustPositionForPixelPerfectDrawing = true;
            TextTimer.IgnoreParentPosition = true;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            for (int i = ListaBarrasHP.Count - 1; i > -1; i--)
            {
                ListaBarrasHP[i].Destroy();
            }
            for (int i = ListaNomesPersonagens.Count - 1; i > -1; i--)
            {
                FlatRedBall.Graphics.TextManager.RemoveText(ListaNomesPersonagens[i]);
            }
            if (TextTimer != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveTextOneWay(TextTimer);
            }
            for (int i = ListaContagemRounds.Count - 1; i > -1; i--)
            {
                ListaContagemRounds[i].Destroy();
            }
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
            }
            TextTimer.Font = Bauhaus93_48;
            if (TextTimer.Parent == null)
            {
                TextTimer.Y = 0f;
            }
            else
            {
                TextTimer.RelativeY = 0f;
            }
            if (TextTimer.Parent == null)
            {
                TextTimer.Z = 16f;
            }
            else
            {
                TextTimer.RelativeZ = 16f;
            }
            TextTimer.TextureScale = 1f;
            TextTimer.Red = 0f;
            TextTimer.Green = 0f;
            TextTimer.Blue = 0f;
            TextTimer.AdjustPositionForPixelPerfectDrawing = true;
            TextTimer.IgnoreParentPosition = true;
            Timer = 0;
            TextTimerY = 0f;
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            this.ForceUpdateDependenciesDeep();
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            for (int i = 0; i < ListaBarrasHP.Count; i++)
            {
                ListaBarrasHP[i].ConvertToManuallyUpdated();
            }
            for (int i = 0; i < ListaNomesPersonagens.Count; i++)
            {
                FlatRedBall.Graphics.TextManager.ConvertToManuallyUpdated(ListaNomesPersonagens[i]);
            }
            FlatRedBall.Graphics.TextManager.ConvertToManuallyUpdated(TextTimer);
            for (int i = 0; i < ListaContagemRounds.Count; i++)
            {
                ListaContagemRounds[i].ConvertToManuallyUpdated();
            }
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            ContentManagerName = contentManagerName;
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            bool registerUnload = false;
            if (LoadedContentManagers.Contains(contentManagerName) == false)
            {
                LoadedContentManagers.Add(contentManagerName);
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("HudStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<FlatRedBall.Graphics.BitmapFont>(@"content/entities/hud/bauhaus93.fnt", ContentManagerName))
                {
                    registerUnload = true;
                }
                Bauhaus93 = FlatRedBall.FlatRedBallServices.Load<FlatRedBall.Graphics.BitmapFont>(@"content/entities/hud/bauhaus93.fnt", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<FlatRedBall.Graphics.BitmapFont>(@"content/entities/hud/bauhaus93_48.fnt", ContentManagerName))
                {
                    registerUnload = true;
                }
                Bauhaus93_48 = FlatRedBall.FlatRedBallServices.Load<FlatRedBall.Graphics.BitmapFont>(@"content/entities/hud/bauhaus93_48.fnt", ContentManagerName);
            }
            if (registerUnload && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("HudStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            CustomLoadStaticContent(contentManagerName);
        }
        public static void UnloadStaticContent () 
        {
            if (LoadedContentManagers.Count != 0)
            {
                LoadedContentManagers.RemoveAt(0);
                mRegisteredUnloads.RemoveAt(0);
            }
            if (LoadedContentManagers.Count == 0)
            {
                if (Bauhaus93 != null)
                {
                    Bauhaus93= null;
                }
                if (Bauhaus93_48 != null)
                {
                    Bauhaus93_48= null;
                }
            }
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            switch(memberName)
            {
                case  "Bauhaus93":
                    return Bauhaus93;
                case  "Bauhaus93_48":
                    return Bauhaus93_48;
            }
            return null;
        }
        public static object GetFile (string memberName) 
        {
            switch(memberName)
            {
                case  "Bauhaus93":
                    return Bauhaus93;
                case  "Bauhaus93_48":
                    return Bauhaus93_48;
            }
            return null;
        }
        object GetMember (string memberName) 
        {
            switch(memberName)
            {
                case  "Bauhaus93":
                    return Bauhaus93;
                case  "Bauhaus93_48":
                    return Bauhaus93_48;
            }
            return null;
        }
        protected bool mIsPaused;
        public override void Pause (FlatRedBall.Instructions.InstructionList instructions) 
        {
            base.Pause(instructions);
            mIsPaused = true;
        }
        public virtual void SetToIgnorePausing () 
        {
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(this);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(TextTimer);
        }
        public virtual void MoveToLayer (FlatRedBall.Graphics.Layer layerToMoveTo) 
        {
            var layerToRemoveFrom = LayerProvidedByContainer;
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(TextTimer);
            }
            FlatRedBall.Graphics.TextManager.AddToLayer(TextTimer, layerToMoveTo);
            LayerProvidedByContainer = layerToMoveTo;
        }
    }
}
