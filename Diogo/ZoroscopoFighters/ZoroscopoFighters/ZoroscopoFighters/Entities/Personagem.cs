#region Usings

using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using ZoroscopoFighters.MinhasClasses.Enumeracoes;

using FlatRedBall.Math.Geometry;
using FlatRedBall.Math.Splines;
using BitmapFont = FlatRedBall.Graphics.BitmapFont;
using Cursor = FlatRedBall.Gui.Cursor;
using GuiManager = FlatRedBall.Gui.GuiManager;
using ZoroscopoFighters.MinhasClasses.Organizadores;
using ZoroscopoFighters.MinhasClasses.Organizadores.Controle;

#if FRB_XNA || SILVERLIGHT
using Keys = Microsoft.Xna.Framework.Input.Keys;
using Vector3 = Microsoft.Xna.Framework.Vector3;
using Texture2D = Microsoft.Xna.Framework.Graphics.Texture2D;

#endif
#endregion

namespace ZoroscopoFighters.Entities
{
	public partial class Personagem
	{
        
        public bool IsAtacandoMelee
        {
            get;
            set;
        }

        public bool IsNoChao
        {
            get;
            set;
        }

        public bool IsAbaixado
        {
            get;
            set;
        }

        public bool IsAtacavel
        {
            get;
            set;
        }

        public int HPAtual
        {
            get;
            set;
        }

        public int EspecialAtual
        {
            get;
            set;
        }

        public int RoundsVencidos
        {
            get;
            set;
        }

        public Controlador Controle
        {
            get;
            set;
        }
        
        public bool IsMovel     // Esta vari�vel impede o personagem de se mover devido a ele ter levado um porra�o na cara.
        {
            get;
            set;
        }
        
        public bool IsDesimpedido  // Esta vari�vel impede o personagem de se mover devido a a��es da tela como "Inicio" e "Fim" de um round.
        {
            get;
            set;
        }

        private FichaPersonagem mFicha;
        public FichaPersonagem Ficha
        {
            get { return mFicha;  }
            set {
                mFicha = value;
                CurrentState = TextToAnimation.getVariableStateAnimation(Ficha.Nome);
                definirHitboxNormal();
            }
        }

        private int mValorLado;
        private ELado mLado;
        public ELado Lado
        {
            get { return mLado;  }
            set {
                mLado = value;
                if (mLado == ELado.ESQUERDO)
                {
                    mValorLado = -1;
                    SpritePersonagem.FlipHorizontal = true;
                } else
                {
                    mValorLado = 1;
                    SpritePersonagem.FlipHorizontal = false;
                }
            }
        }

        private double mTempoPuloPressionado = -1000;
        private double mTempoInvulneravel = -1000;

        /// <summary>
        /// Initialization logic which is execute only one time for this Entity (unless the Entity is pooled).
        /// This method is called when the Entity is added to managers. Entities which are instantiated but not
        /// added to managers will not have this method called.
        /// </summary>
		private void CustomInitialize()
		{
            IsAbaixado = false;
            IsAtacandoMelee = false;
            IsAtacavel = true;
            IsDesimpedido = false;

            HitMelee.Destroy();

            mValorLado = 1;

            HPAtual = HPInicial;
        }

		private void CustomActivity()
		{

            AplicarGravidade();

            AtualizaTimerMobilidadeEInvulneravel();

            Andar();

            Abaixar();

            Saltar();

            Socar();
            
        }

        private void CustomDestroy()
		{


		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        public bool AtaqueAcertou(Personagem outroPersonagem)    // Retorna se acertou outro personagem ou n�o
        {
            bool acertou = false;

            if (!HitMelee.IsDestroyed)
            {
                acertou = HitMelee.Hitbox.CollideAgainstBounce(outroPersonagem.Hitbox, 1, 0.9f, 1);
            }

            return acertou;
        }

        public void Apanhar(Personagem atacante)
        {
            if (IsAtacavel)
            {
                IsAtacavel = false;
                IsMovel = false;

                mTempoInvulneravel = TimeManager.CurrentTime;
                // TODO Sofrer dano
                int dano = (int) ((float)DanoSoco * atacante.Ficha.GetModificadorForca(ValorPercentualModificador));

                //System.Diagnostics.Debug.WriteLine("> antes = " + dano);

                dano = (int) ((float)dano * Ficha.GetModificadorResistencia(ValorPercentualModificador));

                //System.Diagnostics.Debug.WriteLine("> depois = " + dano);

                HPAtual -= dano;
            }
        }

        public void Nocautear()
        {
            definirAnimation(EAnimacao.IDLE);
            // TODO Nocautear
        }

        public void Vitoria()
        {
            definirAnimation(EAnimacao.IDLE);
        }

        public void TornarIdle()
        {
            definirAnimation(EAnimacao.IDLE);
        }

        private void definirHitboxNormal()
        {
            Hitbox.Width  = Ficha.TamanhoHitbox.Width;
            Hitbox.Height = Ficha.TamanhoHitbox.Height;
            Hitbox.RelativeX = Ficha.TamanhoHitbox.RelativeX * mValorLado;
            Hitbox.RelativeY = Ficha.TamanhoHitbox.RelativeY;
        }

        private void definirHitboxAbaixado()
        {
            Hitbox.Width  = Ficha.TamanhoHitboxAbaixado.Width;
            Hitbox.Height = Ficha.TamanhoHitboxAbaixado.Height;
            Hitbox.RelativeX = Ficha.TamanhoHitboxAbaixado.RelativeX * mValorLado;
            Hitbox.RelativeY = Ficha.TamanhoHitboxAbaixado.RelativeY;
            //Hitbox.RelativeY = - (((Ficha.TamanhoHitbox.Height - Ficha.TamanhoHitboxAbaixado.Height) / 2) );
        }

        private void definirAnimation(EAnimacao animacao)
        {
            switch(animacao)
            {
                case EAnimacao.IDLE:
                    SpritePersonagem.CurrentChainName = "Idle";
                    break;
                case EAnimacao.ANDAR:
                    SpritePersonagem.CurrentChainName = "Andar";
                    break;
                case EAnimacao.SALTAR:
                    SpritePersonagem.CurrentChainName = "Saltar";
                    break;
                case EAnimacao.ABAIXAR:
                    SpritePersonagem.CurrentChainName = "Abaixar";
                    break;
                case EAnimacao.SOCO_CHAO:
                    SpritePersonagem.CurrentChainName = "Soco";
                    break;
                case EAnimacao.SOCO_AR:
                    SpritePersonagem.CurrentChainName = "Soco_Ar";
                    break;
                case EAnimacao.SOCO_ABAIXADO:
                    SpritePersonagem.CurrentChainName = "Soco_Abaixado";
                    break;
            }
        }

        private void AplicarGravidade()
        {
            if (IsNoChao)
            {
                this.YAcceleration = 0;
                this.YVelocity = -VelocidadeSalto;
            }
            else
            {
                this.YAcceleration = -VelocidadeSalto * 5;
            }
        }

        private void AtualizaTimerMobilidadeEInvulneravel()
        {
            if (TimeManager.SecondsSince(mTempoInvulneravel) > TempoInvulnerabilidade)
            {
                IsAtacavel = true;
            }

            if (TimeManager.SecondsSince(mTempoInvulneravel) > (TempoInvulnerabilidade * 0.7))  // O personagem n�o pode se mover por 70% do tempo de invulnerabilidade
            {
                IsMovel = true;
            }
        }

        private void Andar()
        {

            this.XVelocity = 0; // Para de andar caso ele n�o esteja sendo movimentado para os lados
            //if (SpritePersonagem.JustCycled)
            
            if (!IsAbaixado && ((IsAtacandoMelee && !IsNoChao) || !IsAtacandoMelee) && IsDesimpedido && IsMovel)    // Verifica se o personagem n�o est� abaixado E (est� atacando no ar OU n�o est� atacando)
            {
                int mEsquerdaDireita = 0;
                if (InputManager.Keyboard.KeyDown(Controle.BotaoDireita))
                {
                    mEsquerdaDireita = 1;
                }
                else if (InputManager.Keyboard.KeyDown(Controle.BotaoEsquerda))
                {
                    mEsquerdaDireita = -1;
                }

                if (mEsquerdaDireita != 0)
                {
                    if (IsNoChao)
                    {
                        definirAnimation(EAnimacao.ANDAR);
                    }
                    
                    this.XVelocity = (VelocidadeMovimento * Ficha.GetModificadorAgilidade(ValorPercentualModificador)) * mEsquerdaDireita;    // Define a XVelocity como a Velocidade de movimento padr�o para esquerda (-1) ou direita (+1)
                }
                else
                {
                    if (SpritePersonagem.JustCycled)
                    {
                        definirAnimation(EAnimacao.IDLE);   // Define a anima��o para IDLE caso n�o esteja sendo efetuada nenhuma movimenta��o
                    } else if (IsNoChao)
                    {
                        definirAnimation(EAnimacao.IDLE);
                    }
                }
            }
        }

        private void Abaixar()
        {
            IsAbaixado = false;
            
            if (InputManager.Keyboard.KeyDown(Controle.BotaoAbaixar) && IsNoChao && !IsAtacandoMelee && IsDesimpedido && IsMovel)
            {
                //this.Hitbox.Height = hitboxHeightAbaixado;
                //this.Hitbox.RelativeY = yHitboxAbaixado;
                IsAbaixado = true;
                definirHitboxAbaixado();
                
                definirAnimation(EAnimacao.ABAIXAR);

                SpritePersonagem.AnimationSpeed = 2f * Ficha.GetModificadorAgilidade(ValorPercentualModificador);

                if (SpritePersonagem.JustCycled)
                {
                    SpritePersonagem.Animate = false;
                    SpritePersonagem.CurrentFrameIndex = SpritePersonagem.CurrentChain.Count - 1;
                }
            } else
            {
                if (!IsAtacandoMelee)
                {
                    SpritePersonagem.AnimationSpeed = 1f;
                    SpritePersonagem.Animate = true;
                    definirHitboxNormal();
                }
            }
        }

        private void Saltar()
        {
            if (TimeManager.SecondsSince(mTempoPuloPressionado) < MaxTempoSalto)
            {
                this.YVelocity = (VelocidadeSalto + this.Hitbox.Height) * Ficha.GetModificadorAgilidade(ValorPercentualModificador);  // A velocidade � a padr�o + a Altura do personagem. A gravidade � respons�vel por fazer o personagem descer de maneira "soft"
            }

            if (InputManager.Keyboard.KeyDown(Controle.BotaoSalto) && IsNoChao && !IsAtacandoMelee && IsDesimpedido && IsMovel)
            {
                mTempoPuloPressionado = TimeManager.CurrentTime;

                definirAnimation(EAnimacao.SALTAR);
                definirHitboxNormal();
            }
        }

        private void Socar()
        {
            if (!SpritePersonagem.JustCycled && IsAtacandoMelee)
            {
                if (SpritePersonagem.CurrentFrameIndex == SpritePersonagem.CurrentChain.Count - 1 && HitMelee.IsDestroyed)
                {
                    HitMelee = new Hit();
                    HitMelee.Hitbox.Width = Ficha.HitboxSoco.Width;
                    HitMelee.Hitbox.Height = Ficha.HitboxSoco.Height;
                }

                if (HitMelee != null)
                {
                    HitMelee.Position = this.Position;
                    HitMelee.X += Ficha.HitboxSoco.RelativeX * mValorLado;
                    HitMelee.Y += Ficha.HitboxSoco.RelativeY;
                }
            }
            else
            {
                IsAtacandoMelee = false;
                HitMelee.Destroy();
            }

            if (InputManager.Keyboard.KeyDown(Controle.BotaoSoco) && !IsAtacandoMelee && IsDesimpedido && IsMovel)
            {
                IsAtacandoMelee = true;
                SpritePersonagem.Animate = true;

                /*
                HitMelee = new Hit();
                HitMelee.Position = this.Position;
                HitMelee.Hitbox.Width     = Ficha.HitboxSoco.Width;
                HitMelee.Hitbox.Height    = Ficha.HitboxSoco.Height;
                HitMelee.X += Ficha.HitboxSoco.RelativeX * mValorLado;
                HitMelee.Y += Ficha.HitboxSoco.RelativeY;
                */

                SpritePersonagem.AnimationSpeed = 1.5f * Ficha.GetModificadorAgilidade(ValorPercentualModificador);

                definirAnimation(EAnimacao.SOCO_CHAO);
            }
        }

    }
}
