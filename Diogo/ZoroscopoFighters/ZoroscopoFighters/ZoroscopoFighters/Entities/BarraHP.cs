#region Usings

using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;

using FlatRedBall.Math.Geometry;
using FlatRedBall.Math.Splines;
using BitmapFont = FlatRedBall.Graphics.BitmapFont;
using Cursor = FlatRedBall.Gui.Cursor;
using GuiManager = FlatRedBall.Gui.GuiManager;
using ZoroscopoFighters.MinhasClasses.Organizadores;

#if FRB_XNA || SILVERLIGHT
using Keys = Microsoft.Xna.Framework.Input.Keys;
using Vector3 = Microsoft.Xna.Framework.Vector3;
using Texture2D = Microsoft.Xna.Framework.Graphics.Texture2D;

#endif
#endregion

namespace ZoroscopoFighters.Entities
{
	public partial class BarraHP
	{

        private Personagem mPersonagem;
        private float mScaleXInicialBarra;
        /// <summary>
        /// Initialization logic which is execute only one time for this Entity (unless the Entity is pooled).
        /// This method is called when the Entity is added to managers. Entities which are instantiated but not
        /// added to managers will not have this method called.
        /// </summary>
		private void CustomInitialize()
		{
            

		}

		private void CustomActivity()
		{
            AtualizaBarraHp();

		}

		private void CustomDestroy()
		{


		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        public void setPersonagem(Personagem personagem)
        {
            mPersonagem = personagem;
        }

        public void AtualizaTamanho()
        {
            Barra.ScaleX = ((GlobalData.ScreenWidth / 3) / 2);
            Barra.Height = BarraHeight;
                 
            mScaleXInicialBarra = Barra.ScaleX;
            

            ContornoMeio.Width = (GlobalData.ScreenWidth / 3) + 6;
            ContornoMeio.Height = BarraHeight + 4;

            ContornoEsquerdo.Height = BarraHeight;
            ContornoEsquerdo.RelativeX = - (mScaleXInicialBarra - 1);

            ContornoDireito.Height = BarraHeight;
            ContornoDireito.RelativeX = mScaleXInicialBarra;
        }

        private void AtualizaBarraHp()
        {
            float escala = (mScaleXInicialBarra * mPersonagem.HPAtual) / mPersonagem.HPInicial;
            if (escala >= 0) {
                Barra.ScaleX = escala;
            } else
            {
                Barra.ScaleX = 0;
            }

            if (X > 0)
            {
                Barra.RelativeX = Barra.ScaleX - mScaleXInicialBarra;
            } else
            {
                Barra.RelativeX = mScaleXInicialBarra - Barra.ScaleX;
            }
        }
	}
}
