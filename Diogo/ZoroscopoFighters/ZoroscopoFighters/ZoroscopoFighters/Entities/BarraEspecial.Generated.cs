#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using ZoroscopoFighters.Screens;
using FlatRedBall.Graphics;
using FlatRedBall.Math;
using ZoroscopoFighters.Entities;
using FlatRedBall;
using FlatRedBall.Screens;
using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall.Math.Geometry;
namespace ZoroscopoFighters.Entities
{
    public partial class BarraEspecial : FlatRedBall.PositionedObject, FlatRedBall.Graphics.IDestroyable
    {
        // This is made static so that static lazy-loaded content can access it.
        public static string ContentManagerName { get; set; }
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        static object mLockObject = new object();
        static System.Collections.Generic.List<string> mRegisteredUnloads = new System.Collections.Generic.List<string>();
        static System.Collections.Generic.List<string> LoadedContentManagers = new System.Collections.Generic.List<string>();
        
        private FlatRedBall.Sprite Barra;
        private FlatRedBall.Sprite Contorno;
        private FlatRedBall.Math.Geometry.AxisAlignedRectangle mContornoInternoTemp;
        public FlatRedBall.Math.Geometry.AxisAlignedRectangle ContornoInternoTemp
        {
            get
            {
                return mContornoInternoTemp;
            }
            private set
            {
                mContornoInternoTemp = value;
            }
        }
        protected FlatRedBall.Graphics.Layer LayerProvidedByContainer = null;
        public BarraEspecial () 
        	: this(FlatRedBall.Screens.ScreenManager.CurrentScreen.ContentManagerName, true)
        {
        }
        public BarraEspecial (string contentManagerName) 
        	: this(contentManagerName, true)
        {
        }
        public BarraEspecial (string contentManagerName, bool addToManagers) 
        	: base()
        {
            ContentManagerName = contentManagerName;
            InitializeEntity(addToManagers);
        }
        protected virtual void InitializeEntity (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            Barra = new FlatRedBall.Sprite();
            Barra.Name = "Barra";
            Contorno = new FlatRedBall.Sprite();
            Contorno.Name = "Contorno";
            mContornoInternoTemp = new FlatRedBall.Math.Geometry.AxisAlignedRectangle();
            mContornoInternoTemp.Name = "mContornoInternoTemp";
            
            PostInitialize();
            if (addToManagers)
            {
                AddToManagers(null);
            }
        }
        public virtual void ReAddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.SpriteManager.AddToLayer(Barra, LayerProvidedByContainer);
            FlatRedBall.SpriteManager.AddToLayer(Contorno, LayerProvidedByContainer);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mContornoInternoTemp, LayerProvidedByContainer);
        }
        public virtual void AddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.SpriteManager.AddToLayer(Barra, LayerProvidedByContainer);
            FlatRedBall.SpriteManager.AddToLayer(Contorno, LayerProvidedByContainer);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mContornoInternoTemp, LayerProvidedByContainer);
            AddToManagersBottomUp(layerToAddTo);
            CustomInitialize();
        }
        public virtual void Activity () 
        {
            
            CustomActivity();
        }
        public virtual void Destroy () 
        {
            FlatRedBall.SpriteManager.RemovePositionedObject(this);
            
            if (Barra != null)
            {
                FlatRedBall.SpriteManager.RemoveSprite(Barra);
            }
            if (Contorno != null)
            {
                FlatRedBall.SpriteManager.RemoveSprite(Contorno);
            }
            if (ContornoInternoTemp != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.Remove(ContornoInternoTemp);
            }
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            if (Barra.Parent == null)
            {
                Barra.CopyAbsoluteToRelative();
                Barra.AttachTo(this, false);
            }
            if (Barra.Parent == null)
            {
                Barra.Z = 16f;
            }
            else
            {
                Barra.RelativeZ = 16f;
            }
            Barra.TextureScale = 1f;
            if (Contorno.Parent == null)
            {
                Contorno.CopyAbsoluteToRelative();
                Contorno.AttachTo(this, false);
            }
            if (Contorno.Parent == null)
            {
                Contorno.Z = 15f;
            }
            else
            {
                Contorno.RelativeZ = 15f;
            }
            Contorno.TextureScale = 1f;
            if (mContornoInternoTemp.Parent == null)
            {
                mContornoInternoTemp.CopyAbsoluteToRelative();
                mContornoInternoTemp.AttachTo(this, false);
            }
            if (ContornoInternoTemp.Parent == null)
            {
                ContornoInternoTemp.Z = 15f;
            }
            else
            {
                ContornoInternoTemp.RelativeZ = 15f;
            }
            ContornoInternoTemp.Width = 32f;
            ContornoInternoTemp.Height = 32f;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            if (Barra != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(Barra);
            }
            if (Contorno != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(Contorno);
            }
            if (ContornoInternoTemp != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.RemoveOneWay(ContornoInternoTemp);
            }
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
            }
            if (Barra.Parent == null)
            {
                Barra.Z = 16f;
            }
            else
            {
                Barra.RelativeZ = 16f;
            }
            Barra.TextureScale = 1f;
            if (Contorno.Parent == null)
            {
                Contorno.Z = 15f;
            }
            else
            {
                Contorno.RelativeZ = 15f;
            }
            Contorno.TextureScale = 1f;
            if (ContornoInternoTemp.Parent == null)
            {
                ContornoInternoTemp.Z = 15f;
            }
            else
            {
                ContornoInternoTemp.RelativeZ = 15f;
            }
            ContornoInternoTemp.Width = 32f;
            ContornoInternoTemp.Height = 32f;
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            this.ForceUpdateDependenciesDeep();
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(Barra);
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(Contorno);
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            ContentManagerName = contentManagerName;
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            bool registerUnload = false;
            if (LoadedContentManagers.Contains(contentManagerName) == false)
            {
                LoadedContentManagers.Add(contentManagerName);
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("BarraEspecialStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            if (registerUnload && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("BarraEspecialStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            CustomLoadStaticContent(contentManagerName);
        }
        public static void UnloadStaticContent () 
        {
            if (LoadedContentManagers.Count != 0)
            {
                LoadedContentManagers.RemoveAt(0);
                mRegisteredUnloads.RemoveAt(0);
            }
            if (LoadedContentManagers.Count == 0)
            {
            }
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            return null;
        }
        public static object GetFile (string memberName) 
        {
            return null;
        }
        object GetMember (string memberName) 
        {
            return null;
        }
        protected bool mIsPaused;
        public override void Pause (FlatRedBall.Instructions.InstructionList instructions) 
        {
            base.Pause(instructions);
            mIsPaused = true;
        }
        public virtual void SetToIgnorePausing () 
        {
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(this);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(Barra);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(Contorno);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(ContornoInternoTemp);
        }
        public virtual void MoveToLayer (FlatRedBall.Graphics.Layer layerToMoveTo) 
        {
            var layerToRemoveFrom = LayerProvidedByContainer;
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(Barra);
            }
            FlatRedBall.SpriteManager.AddToLayer(Barra, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(Contorno);
            }
            FlatRedBall.SpriteManager.AddToLayer(Contorno, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(ContornoInternoTemp);
            }
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(ContornoInternoTemp, layerToMoveTo);
            LayerProvidedByContainer = layerToMoveTo;
        }
    }
}
