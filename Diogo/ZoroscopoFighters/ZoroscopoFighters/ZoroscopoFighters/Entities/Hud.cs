#region Usings

using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;


using FlatRedBall.Math.Geometry;
using FlatRedBall.Math.Splines;
using BitmapFont = FlatRedBall.Graphics.BitmapFont;
using Cursor = FlatRedBall.Gui.Cursor;
using GuiManager = FlatRedBall.Gui.GuiManager;
using ZoroscopoFighters.MinhasClasses.Organizadores;

#if FRB_XNA || SILVERLIGHT
using Keys = Microsoft.Xna.Framework.Input.Keys;
using Vector3 = Microsoft.Xna.Framework.Vector3;
using Texture2D = Microsoft.Xna.Framework.Graphics.Texture2D;

#endif
#endregion

namespace ZoroscopoFighters.Entities
{
	public partial class Hud
	{

        private double mTempoInicioLuta;
        private BitmapFont mFonte;

        private bool mTimerAtivo;
        public bool IsTimerAtivo
        {
            get { return mTimerAtivo;  }
            set
            {
                mTimerAtivo = value;
                if (mTimerAtivo)
                {
                    mTempoInicioLuta = TimeManager.CurrentTime;
                }
            }
        }

        /// <summary>
        /// Initialization logic which is execute only one time for this Entity (unless the Entity is pooled).
        /// This method is called when the Entity is added to managers. Entities which are instantiated but not
        /// added to managers will not have this method called.
        /// </summary>
		private void CustomInitialize()
		{
            IsTimerAtivo = true;
            
            mFonte = new BitmapFont("Content/Entities/Hud/Bauhaus93.fnt", "Global");
        }

		private void CustomActivity()
		{
            AtualizaTimer();

		}

		private void CustomDestroy()
		{


		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        public void CriarBarraDeHp(Personagem personagem)
        {
            BarraHP barra = new BarraHP();
            barra.setPersonagem(personagem);
            ListaBarrasHP.Add(barra);
        }

        public void CriarTextNome(string nomePersonagem)
        {
            FlatRedBall.Graphics.Text texto = FlatRedBall.Graphics.TextManager.AddText(nomePersonagem, mFonte);
            texto.TextureScale = 0.6f;
            texto.Red = 0;
            texto.Green = 0;
            texto.Blue = 0;
            ListaNomesPersonagens.Add(texto);
        }

        public void CriarTextJogador(int nroJogador)
        {
            FlatRedBall.Graphics.Text texto = FlatRedBall.Graphics.TextManager.AddText("Jogador " + nroJogador, mFonte);
            texto.TextureScale = 0.6f;
            texto.Red = 0;
            texto.Green = 0;
            texto.Blue = 0;
            ListaNomesPersonagens.Add(texto);
        }

        public void CriarTextRoundsVencidos(Personagem personagem)
        {
            ContadorRounds contador = new ContadorRounds();
            contador.setPersonagem(personagem);
            ListaContagemRounds.Add(contador);
        }

        public void DefinePosicaoDosObjetos()
        {
            DefinePosicaoDasBarrasDeHp();

            DefinePosicaoDoTimer();

            DefinePosicaoDosNomes();

            DefinePosicaoDoContadorDeRoundsVencidos();
        }

        public bool AcabouTempo()
        {
            return Timer <= 0;
        }

        private void DefinePosicaoDasBarrasDeHp()
        {

            int i = -1;
            foreach (var barra in ListaBarrasHP)
            {
                barra.X = (GlobalData.ScreenWidth / 4) * i;
                barra.Y = (GlobalData.ScreenHeight / 2) - barra.BarraHeight;

                barra.AtualizaTamanho();

                i = i * -1;
            }
        }

        private void DefinePosicaoDoTimer()
        {
            /*
            TextTimer.RelativeX -= TextTimer.Width;
            TextTimer.RelativeY = (GlobalData.ScreenHeight / 2) - TextTimer.Height;
            */
            TextTimer.X = - (TextTimer.Width / 2);
            TextTimer.Y = (GlobalData.ScreenHeight / 2) - (TextTimer.Height * 0.8f);
        }

        private void DefinePosicaoDosNomes()
        {
            int i = -1;
            int qtdNomes = 0;
            int qtdNomesPorLinha = 2;
            int linha = 1;

            foreach (var texto in ListaNomesPersonagens)
            {
                //texto.Scale = 5;
                //texto.Spacing += 5;

                texto.X = (GlobalData.ScreenWidth / 4) * i;
                texto.X = texto.X - ((((GlobalData.ScreenWidth / 3) / 2) - texto.Width) * i);
                if (i > 0)
                {
                    texto.X -= texto.Width;
                }
                
                texto.Y = (GlobalData.ScreenHeight / 2) - (texto.Height * linha) - ListaBarrasHP.Last.BarraHeight - 10;
                
                i = i * -1;
                qtdNomes++;

                if (qtdNomes % qtdNomesPorLinha == 0)
                {
                    linha++;
                }
            }
        }

        private void DefinePosicaoDoContadorDeRoundsVencidos()
        {
            int i = -1;
            foreach (var contador in ListaContagemRounds)
            {
                contador.X = (GlobalData.ScreenWidth / 4) * i;
                contador.X = contador.X + ((((GlobalData.ScreenWidth / 3) / 2) + contador.Width) * i) + (20 * i);
                if (i > 0)
                {
                    contador.X -= contador.Width;
                }

                contador.Y = (GlobalData.ScreenHeight / 2) - ListaBarrasHP.Last.BarraHeight;
                
                i = i * -1;
            }
        }

        private void AtualizaTimer()
        {
            if (IsTimerAtivo)
            {
                int tempo = GlobalData.TempoMaxLuta - (int)TimeManager.SecondsSince(mTempoInicioLuta);
                if (tempo >= 0)
                {
                    Timer = tempo;
                }
                else
                {
                    Timer = 0;
                }
                DefinePosicaoDoTimer();
            }
        }
	}
}
