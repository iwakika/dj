#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using ZoroscopoFighters.Screens;
using FlatRedBall.Graphics;
using FlatRedBall.Math;
using ZoroscopoFighters.Entities;
using FlatRedBall;
using FlatRedBall.Screens;
using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall.ManagedSpriteGroups;
namespace ZoroscopoFighters.Entities
{
    public partial class BarraHP : FlatRedBall.PositionedObject, FlatRedBall.Graphics.IDestroyable
    {
        // This is made static so that static lazy-loaded content can access it.
        public static string ContentManagerName { get; set; }
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        static object mLockObject = new object();
        static System.Collections.Generic.List<string> mRegisteredUnloads = new System.Collections.Generic.List<string>();
        static System.Collections.Generic.List<string> LoadedContentManagers = new System.Collections.Generic.List<string>();
        protected static Microsoft.Xna.Framework.Graphics.Texture2D BarraFull;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D BarraContornoEdge;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D BarraContornoMeio;
        
        private FlatRedBall.ManagedSpriteGroups.SpriteFrame Barra;
        private FlatRedBall.Sprite ContornoMeio;
        private FlatRedBall.Sprite ContornoEsquerdo;
        private FlatRedBall.Sprite ContornoDireito;
        public float BarraHeight = 25f;
        protected FlatRedBall.Graphics.Layer LayerProvidedByContainer = null;
        public BarraHP () 
        	: this(FlatRedBall.Screens.ScreenManager.CurrentScreen.ContentManagerName, true)
        {
        }
        public BarraHP (string contentManagerName) 
        	: this(contentManagerName, true)
        {
        }
        public BarraHP (string contentManagerName, bool addToManagers) 
        	: base()
        {
            ContentManagerName = contentManagerName;
            InitializeEntity(addToManagers);
        }
        protected virtual void InitializeEntity (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            Barra = new FlatRedBall.ManagedSpriteGroups.SpriteFrame();
            Barra.Name = "Barra";
            ContornoMeio = new FlatRedBall.Sprite();
            ContornoMeio.Name = "ContornoMeio";
            ContornoEsquerdo = new FlatRedBall.Sprite();
            ContornoEsquerdo.Name = "ContornoEsquerdo";
            ContornoDireito = new FlatRedBall.Sprite();
            ContornoDireito.Name = "ContornoDireito";
            
            PostInitialize();
            if (addToManagers)
            {
                AddToManagers(null);
            }
        }
        public virtual void ReAddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.SpriteManager.AddToLayer(Barra, LayerProvidedByContainer);
            FlatRedBall.SpriteManager.AddToLayer(ContornoMeio, LayerProvidedByContainer);
            FlatRedBall.SpriteManager.AddToLayer(ContornoEsquerdo, LayerProvidedByContainer);
            FlatRedBall.SpriteManager.AddToLayer(ContornoDireito, LayerProvidedByContainer);
        }
        public virtual void AddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.SpriteManager.AddToLayer(Barra, LayerProvidedByContainer);
            FlatRedBall.SpriteManager.AddToLayer(ContornoMeio, LayerProvidedByContainer);
            FlatRedBall.SpriteManager.AddToLayer(ContornoEsquerdo, LayerProvidedByContainer);
            FlatRedBall.SpriteManager.AddToLayer(ContornoDireito, LayerProvidedByContainer);
            AddToManagersBottomUp(layerToAddTo);
            CustomInitialize();
        }
        public virtual void Activity () 
        {
            
            CustomActivity();
        }
        public virtual void Destroy () 
        {
            FlatRedBall.SpriteManager.RemovePositionedObject(this);
            
            if (Barra != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteFrame(Barra);
            }
            if (ContornoMeio != null)
            {
                FlatRedBall.SpriteManager.RemoveSprite(ContornoMeio);
            }
            if (ContornoEsquerdo != null)
            {
                FlatRedBall.SpriteManager.RemoveSprite(ContornoEsquerdo);
            }
            if (ContornoDireito != null)
            {
                FlatRedBall.SpriteManager.RemoveSprite(ContornoDireito);
            }
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            if (Barra.Parent == null)
            {
                Barra.CopyAbsoluteToRelative();
                Barra.AttachTo(this, false);
            }
            Barra.Texture = BarraFull;
            if (Barra.Parent == null)
            {
                Barra.Z = 16f;
            }
            else
            {
                Barra.RelativeZ = 16f;
            }
            if (ContornoMeio.Parent == null)
            {
                ContornoMeio.CopyAbsoluteToRelative();
                ContornoMeio.AttachTo(this, false);
            }
            if (ContornoMeio.Parent == null)
            {
                ContornoMeio.Z = 14f;
            }
            else
            {
                ContornoMeio.RelativeZ = 14f;
            }
            ContornoMeio.Texture = BarraContornoMeio;
            #if FRB_MDX
            ContornoMeio.TextureAddressMode = Microsoft.DirectX.Direct3D.TextureAddress.Wrap;
            #else
            ContornoMeio.TextureAddressMode = Microsoft.Xna.Framework.Graphics.TextureAddressMode.Wrap;
            #endif
            ContornoMeio.TextureScale = 1f;
            if (ContornoMeio.Parent == null)
            {
                ContornoMeio.RotationZ = 0f;
            }
            else
            {
                ContornoMeio.RelativeRotationZ = 0f;
            }
            if (ContornoEsquerdo.Parent == null)
            {
                ContornoEsquerdo.CopyAbsoluteToRelative();
                ContornoEsquerdo.AttachTo(this, false);
            }
            if (ContornoEsquerdo.Parent == null)
            {
                ContornoEsquerdo.Z = 15f;
            }
            else
            {
                ContornoEsquerdo.RelativeZ = 15f;
            }
            ContornoEsquerdo.Texture = BarraContornoEdge;
            ContornoEsquerdo.TextureScale = 1f;
            ContornoEsquerdo.Width = 7f;
            if (ContornoEsquerdo.Parent == null)
            {
                ContornoEsquerdo.RotationZ = 0f;
            }
            else
            {
                ContornoEsquerdo.RelativeRotationZ = 0f;
            }
            if (ContornoDireito.Parent == null)
            {
                ContornoDireito.CopyAbsoluteToRelative();
                ContornoDireito.AttachTo(this, false);
            }
            if (ContornoDireito.Parent == null)
            {
                ContornoDireito.Z = 15f;
            }
            else
            {
                ContornoDireito.RelativeZ = 15f;
            }
            ContornoDireito.Texture = BarraContornoEdge;
            ContornoDireito.TextureScale = 1f;
            ContornoDireito.Width = 7f;
            if (ContornoDireito.Parent == null)
            {
                ContornoDireito.RotationZ = 0f;
            }
            else
            {
                ContornoDireito.RelativeRotationZ = 0f;
            }
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            if (Barra != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteFrame(Barra);
            }
            if (ContornoMeio != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(ContornoMeio);
            }
            if (ContornoEsquerdo != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(ContornoEsquerdo);
            }
            if (ContornoDireito != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(ContornoDireito);
            }
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
            }
            Barra.Texture = BarraFull;
            if (Barra.Parent == null)
            {
                Barra.Z = 16f;
            }
            else
            {
                Barra.RelativeZ = 16f;
            }
            if (ContornoMeio.Parent == null)
            {
                ContornoMeio.Z = 14f;
            }
            else
            {
                ContornoMeio.RelativeZ = 14f;
            }
            ContornoMeio.Texture = BarraContornoMeio;
            #if FRB_MDX
            ContornoMeio.TextureAddressMode = Microsoft.DirectX.Direct3D.TextureAddress.Wrap;
            #else
            ContornoMeio.TextureAddressMode = Microsoft.Xna.Framework.Graphics.TextureAddressMode.Wrap;
            #endif
            ContornoMeio.TextureScale = 1f;
            if (ContornoMeio.Parent == null)
            {
                ContornoMeio.RotationZ = 0f;
            }
            else
            {
                ContornoMeio.RelativeRotationZ = 0f;
            }
            if (ContornoEsquerdo.Parent == null)
            {
                ContornoEsquerdo.Z = 15f;
            }
            else
            {
                ContornoEsquerdo.RelativeZ = 15f;
            }
            ContornoEsquerdo.Texture = BarraContornoEdge;
            ContornoEsquerdo.TextureScale = 1f;
            ContornoEsquerdo.Width = 7f;
            if (ContornoEsquerdo.Parent == null)
            {
                ContornoEsquerdo.RotationZ = 0f;
            }
            else
            {
                ContornoEsquerdo.RelativeRotationZ = 0f;
            }
            if (ContornoDireito.Parent == null)
            {
                ContornoDireito.Z = 15f;
            }
            else
            {
                ContornoDireito.RelativeZ = 15f;
            }
            ContornoDireito.Texture = BarraContornoEdge;
            ContornoDireito.TextureScale = 1f;
            ContornoDireito.Width = 7f;
            if (ContornoDireito.Parent == null)
            {
                ContornoDireito.RotationZ = 0f;
            }
            else
            {
                ContornoDireito.RelativeRotationZ = 0f;
            }
            BarraHeight = 25f;
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            this.ForceUpdateDependenciesDeep();
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(Barra);
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(ContornoMeio);
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(ContornoEsquerdo);
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(ContornoDireito);
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            ContentManagerName = contentManagerName;
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            bool registerUnload = false;
            if (LoadedContentManagers.Contains(contentManagerName) == false)
            {
                LoadedContentManagers.Add(contentManagerName);
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("BarraHPStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/barrahp/barrafull.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                BarraFull = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/barrahp/barrafull.png", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/barrahp/barracontornoedge.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                BarraContornoEdge = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/barrahp/barracontornoedge.png", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/barrahp/barracontornomeio.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                BarraContornoMeio = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/barrahp/barracontornomeio.png", ContentManagerName);
            }
            if (registerUnload && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("BarraHPStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            CustomLoadStaticContent(contentManagerName);
        }
        public static void UnloadStaticContent () 
        {
            if (LoadedContentManagers.Count != 0)
            {
                LoadedContentManagers.RemoveAt(0);
                mRegisteredUnloads.RemoveAt(0);
            }
            if (LoadedContentManagers.Count == 0)
            {
                if (BarraFull != null)
                {
                    BarraFull= null;
                }
                if (BarraContornoEdge != null)
                {
                    BarraContornoEdge= null;
                }
                if (BarraContornoMeio != null)
                {
                    BarraContornoMeio= null;
                }
            }
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            switch(memberName)
            {
                case  "BarraFull":
                    return BarraFull;
                case  "BarraContornoEdge":
                    return BarraContornoEdge;
                case  "BarraContornoMeio":
                    return BarraContornoMeio;
            }
            return null;
        }
        public static object GetFile (string memberName) 
        {
            switch(memberName)
            {
                case  "BarraFull":
                    return BarraFull;
                case  "BarraContornoEdge":
                    return BarraContornoEdge;
                case  "BarraContornoMeio":
                    return BarraContornoMeio;
            }
            return null;
        }
        object GetMember (string memberName) 
        {
            switch(memberName)
            {
                case  "BarraFull":
                    return BarraFull;
                case  "BarraContornoEdge":
                    return BarraContornoEdge;
                case  "BarraContornoMeio":
                    return BarraContornoMeio;
            }
            return null;
        }
        protected bool mIsPaused;
        public override void Pause (FlatRedBall.Instructions.InstructionList instructions) 
        {
            base.Pause(instructions);
            mIsPaused = true;
        }
        public virtual void SetToIgnorePausing () 
        {
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(this);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(Barra);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(ContornoMeio);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(ContornoEsquerdo);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(ContornoDireito);
        }
        public virtual void MoveToLayer (FlatRedBall.Graphics.Layer layerToMoveTo) 
        {
            var layerToRemoveFrom = LayerProvidedByContainer;
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(Barra);
            }
            FlatRedBall.SpriteManager.AddToLayer(Barra, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(ContornoMeio);
            }
            FlatRedBall.SpriteManager.AddToLayer(ContornoMeio, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(ContornoEsquerdo);
            }
            FlatRedBall.SpriteManager.AddToLayer(ContornoEsquerdo, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(ContornoDireito);
            }
            FlatRedBall.SpriteManager.AddToLayer(ContornoDireito, layerToMoveTo);
            LayerProvidedByContainer = layerToMoveTo;
        }
    }
}
