﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZoroscopoFighters.MinhasClasses.Organizadores.Controle;

namespace ZoroscopoFighters.MinhasClasses.Organizadores

{
    class GlobalData
    {
        public static Controlador ControlePlayer1
        {
            get;
            private set;
        }

        public static Controlador ControlePlayer2
        {
            get;
            private set;
        }

        //int mWidth;
        public static int ScreenWidth {
            get;
            set;
        }

        //int mHeight;
        public static int ScreenHeight
        {
            get;
            set;
        }

        public static int TempoMaxLuta
        {
            get;
            set;
        }

        public static int MaxVitoriasPartida
        {
            get;
            set;
        }

        public static void Initialize()
        {
            ControlePlayer1 = new Controlador();
            ControlePlayer2 = new Controlador();
            TempoMaxLuta = 60;
            MaxVitoriasPartida = 2;
        }

    }
}
