﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZoroscopoFighters.MinhasClasses.Organizadores.Personagens;
using ZoroscopoFighters.MinhasClasses.Enumeracoes;

namespace ZoroscopoFighters.MinhasClasses.Organizadores
{
    public class FichaPersonagem
    {

        //FichaPersonagem() {}

        public string Nome
        {
            get;
            set;
        }

        public int Forca
        {
            get;
            set;
        }

        public int Resistencia
        {
            get;
            set;
        }

        public int Agilidade
        {
            get;
            set;
        }

        public Hitbox TamanhoHitbox
        {
            get;
            set;
        }

        public Hitbox TamanhoHitboxAbaixado
        {
            get;
            set;
        }

        public Hitbox HitboxSoco
        {
            get;
            set;
        }

        public float GetModificadorForca(float valorPercentual)
        {
            return GetModificador(EModificador.FORCA, valorPercentual);
        }

        public float GetModificadorResistencia(float valorPercentual)
        {
            return GetModificador(EModificador.RESISTENCIA, valorPercentual);
        }

        public float GetModificadorAgilidade(float valorPercentual)
        {
            return GetModificador(EModificador.AGILIDADE, valorPercentual);
        }

        private float GetModificador(EModificador tipoModificador, float valorPercentual)
        {
            float modificador = 1f;

            switch (tipoModificador)
            {
                case EModificador.FORCA:
                    modificador += (float)Forca * (valorPercentual / 100f);
                    break;
                case EModificador.RESISTENCIA:
                    modificador -= (float)Resistencia * (valorPercentual / 100f);
                    break;
                case EModificador.AGILIDADE:
                    modificador += (float)Agilidade * (valorPercentual / 100f);
                    break;
            }

            return modificador;
        }

    }
}
