﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZoroscopoFighters.MinhasClasses.Organizadores.Personagens
{
    class Sagitario : FichaPersonagem
    {
        
        public Sagitario()
        {
            Nome        = "Sagitario";
            Forca       = 7;
            Resistencia = 5;
            Agilidade   = 3;
            TamanhoHitbox = new Hitbox(45, 107, -5, -1);
            TamanhoHitboxAbaixado = new Hitbox(55, 88, -3, -8);
            HitboxSoco = new Hitbox(40, 15, 18, 8);
        }

    }
}
