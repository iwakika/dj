﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZoroscopoFighters.MinhasClasses.Organizadores.Personagens
{
    public class Hitbox
    {

        public Hitbox()
        {
        }

        public Hitbox(int width, int height, int x, int y)
        {
            Width = width;
            Height = height;
            RelativeX = x;
            RelativeY = y;
        }

        private int mWidth;
        public int Width
        {
            get { return mWidth; }
            set
            {
                if (value > 0)
                {
                    mWidth = value;
                }
                else
                {
                    mWidth = 1;
                }
            }
        }

        private int mHeight;
        public int Height
        {
            get { return mHeight; }
            set
            {
                if (value > 0)
                {
                    mHeight = value;
                }
                else
                {
                    mHeight = 1;
                }
            }
        }

        private int mRelativeY;
        public int RelativeY
        {
            get { return mRelativeY; }
            set { mRelativeY = value; }
        }

        private int mRelativeX;
        public int RelativeX
        {
            get { return mRelativeX; }
            set { mRelativeX = value; }
        }
    }
}
