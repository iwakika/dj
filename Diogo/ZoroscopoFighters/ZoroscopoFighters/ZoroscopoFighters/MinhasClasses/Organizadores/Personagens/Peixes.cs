﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZoroscopoFighters.MinhasClasses.Organizadores.Personagens
{
    class Peixes : FichaPersonagem
    {
        public Peixes()
        {
            Nome = "Peixes";
            Forca = 3;
            Resistencia = 4;
            Agilidade = 8;
            TamanhoHitbox = new Hitbox(30, 77, -3, -15);
            TamanhoHitboxAbaixado = new Hitbox(30, 69, -3, -17);
            HitboxSoco = new Hitbox(25, 22, 18, -1);
        }
    }
}
