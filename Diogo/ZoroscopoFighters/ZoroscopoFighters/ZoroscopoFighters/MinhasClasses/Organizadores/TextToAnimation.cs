﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZoroscopoFighters.Entities;

namespace ZoroscopoFighters.MinhasClasses.Organizadores
{
    public class TextToAnimation
    {
        public static Personagem.VariableState getVariableStateAnimation(String nome)
        {
            Personagem.VariableState estadoRetorno = Personagem.VariableState.Unknown;

            if (nome.ToUpper().Equals("KEN"))
            {
                estadoRetorno = Personagem.VariableState.Ken;
            }
            else if (nome.ToUpper().Equals("SAGITARIO"))
            {
                estadoRetorno = Personagem.VariableState.Sagitario;
            }
            else if (nome.ToUpper().Equals("PEIXES"))
            {
                estadoRetorno = Personagem.VariableState.Peixes;
            }

            return estadoRetorno;
        }
    }
    
}
