﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZoroscopoFighters.MinhasClasses.Organizadores.Controle
{
    public class Controlador
    {

        public Keys BotaoSalto
        {
            get;
            set;
        }

        public Keys BotaoAbaixar
        {
            get;
            set;
        }

        public Keys BotaoDireita
        {
            get;
            set;
        }

        public Keys BotaoEsquerda
        {
            get;
            set;
        }

        public Keys BotaoSoco
        {
            get;
            set;
        }

        public Keys BotaoChute
        {
            get;
            set;
        }

    }
}
