﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZoroscopoFighters.MinhasClasses.Enumeracoes
{
    public enum EModificador
    {
        FORCA,
        RESISTENCIA,
        AGILIDADE
    }
}
