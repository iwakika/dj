﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZoroscopoFighters.MinhasClasses.Enumeracoes
{
    enum EAnimacao
    {
        IDLE,
        ANDAR,
        SALTAR,
        ABAIXAR,
        SOCO_CHAO,
        SOCO_AR,
        SOCO_ABAIXADO
    }
}
