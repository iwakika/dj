﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZoroscopoFighters.MinhasClasses.Enumeracoes
{
    public enum EPersonagem
    {
        ARIES,
        TOURO,
        GEMEOS,
        CANCER,
        LEAO,
        VIRGEM,
        LIBRA,
        ESCORPIAO,
        SAGITARIO,
        CAPRICORNIO,
        AQUARIO,
        PEIXES
    }
}
