﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZoroscopoFighters.MinhasClasses.Organizadores.Personagens;
using ZoroscopoFighters.MinhasClasses.Enumeracoes;
using ZoroscopoFighters.MinhasClasses.Organizadores;

namespace ZoroscopoFighters.MinhasClasses.Factories
{
    class FactoryFichaPersonagem
    {

        public static FichaPersonagem createFichaPersonagem(EPersonagem personagem)
        {
            FichaPersonagem ficha = null;

            switch (personagem)
            {
                case EPersonagem.SAGITARIO:
                    ficha = new Sagitario();
                    break;
                case EPersonagem.PEIXES:
                    ficha = new Peixes();
                    break;
            }


            return ficha;
        }

    }
}
