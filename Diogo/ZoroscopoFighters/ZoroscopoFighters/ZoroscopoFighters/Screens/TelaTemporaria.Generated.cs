#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using ZoroscopoFighters.Entities;
using FlatRedBall;
using FlatRedBall.Screens;
using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall.Graphics;
namespace ZoroscopoFighters.Screens
{
    public partial class TelaTemporaria : FlatRedBall.Screens.Screen
    {
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        protected static Microsoft.Xna.Framework.Graphics.Texture2D Fade;
        protected static FlatRedBall.Graphics.BitmapFont Bauhaus93_160;
        
        private FlatRedBall.Graphics.Text TextPressStart;
        private FlatRedBall.Sprite SpriteFundo;
        public TelaTemporaria () 
        	: base ("TelaTemporaria")
        {
        }
        public override void Initialize (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            TextPressStart = new FlatRedBall.Graphics.Text();
            TextPressStart.Name = "TextPressStart";
            SpriteFundo = new FlatRedBall.Sprite();
            SpriteFundo.Name = "SpriteFundo";
            
            
            PostInitialize();
            base.Initialize(addToManagers);
            if (addToManagers)
            {
                AddToManagers();
            }
        }
        public override void AddToManagers () 
        {
            FlatRedBall.Graphics.TextManager.AddText(TextPressStart); if(TextPressStart.Font != null) TextPressStart.SetPixelPerfectScale(SpriteManager.Camera);
            if (TextPressStart.Font != null)
            {
                TextPressStart.SetPixelPerfectScale(mLayer);
            }
            FlatRedBall.SpriteManager.AddSprite(SpriteFundo);
            base.AddToManagers();
            AddToManagersBottomUp();
            CustomInitialize();
        }
        public override void Activity (bool firstTimeCalled) 
        {
            if (!IsPaused)
            {
                
            }
            else
            {
            }
            base.Activity(firstTimeCalled);
            if (!IsActivityFinished)
            {
                CustomActivity(firstTimeCalled);
            }
        }
        public override void Destroy () 
        {
            base.Destroy();
            Fade = null;
            Bauhaus93_160 = null;
            
            if (TextPressStart != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveText(TextPressStart);
            }
            if (SpriteFundo != null)
            {
                FlatRedBall.SpriteManager.RemoveSprite(SpriteFundo);
            }
            FlatRedBall.Math.Collision.CollisionManager.Self.Relationships.Clear();
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            TextPressStart.DisplayText = "Press Enter";
            TextPressStart.Font = Bauhaus93_160;
            if (TextPressStart.Parent == null)
            {
                TextPressStart.Z = 2f;
            }
            else
            {
                TextPressStart.RelativeZ = 2f;
            }
            TextPressStart.TextureScale = 0.6f;
            TextPressStart.Green = 0.2f;
            TextPressStart.Blue = 0.2f;
            SpriteFundo.Texture = Fade;
            SpriteFundo.TextureScale = 1f;
            SpriteFundo.Red = 1f;
            SpriteFundo.Green = 1f;
            SpriteFundo.Blue = 1f;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp () 
        {
            CameraSetup.ResetCamera(SpriteManager.Camera);
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            if (TextPressStart != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveTextOneWay(TextPressStart);
            }
            if (SpriteFundo != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(SpriteFundo);
            }
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
            }
            TextPressStart.DisplayText = "Press Enter";
            TextPressStart.Font = Bauhaus93_160;
            if (TextPressStart.Parent == null)
            {
                TextPressStart.Z = 2f;
            }
            else
            {
                TextPressStart.RelativeZ = 2f;
            }
            TextPressStart.TextureScale = 0.6f;
            TextPressStart.Green = 0.2f;
            TextPressStart.Blue = 0.2f;
            SpriteFundo.Texture = Fade;
            SpriteFundo.TextureScale = 1f;
            SpriteFundo.Red = 1f;
            SpriteFundo.Green = 1f;
            SpriteFundo.Blue = 1f;
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            FlatRedBall.Graphics.TextManager.ConvertToManuallyUpdated(TextPressStart);
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(SpriteFundo);
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            Fade = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/screens/telatemporaria/fade.png", contentManagerName);
            Bauhaus93_160 = FlatRedBall.FlatRedBallServices.Load<FlatRedBall.Graphics.BitmapFont>(@"content/screens/telatemporaria/bauhaus93_160.fnt", contentManagerName);
            CustomLoadStaticContent(contentManagerName);
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            switch(memberName)
            {
                case  "Fade":
                    return Fade;
                case  "Bauhaus93_160":
                    return Bauhaus93_160;
            }
            return null;
        }
        public static object GetFile (string memberName) 
        {
            switch(memberName)
            {
                case  "Fade":
                    return Fade;
                case  "Bauhaus93_160":
                    return Bauhaus93_160;
            }
            return null;
        }
        object GetMember (string memberName) 
        {
            switch(memberName)
            {
                case  "Fade":
                    return Fade;
                case  "Bauhaus93_160":
                    return Bauhaus93_160;
            }
            return null;
        }
    }
}
