
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using ZoroscopoFighters.MinhasClasses.Organizadores;
using ZoroscopoFighters.MinhasClasses.Factories;
using ZoroscopoFighters.MinhasClasses.Enumeracoes;

using FlatRedBall.Math.Geometry;
using FlatRedBall.Math.Splines;

using Cursor = FlatRedBall.Gui.Cursor;
using GuiManager = FlatRedBall.Gui.GuiManager;
using FlatRedBall.Localization;

using Keys = Microsoft.Xna.Framework.Input.Keys;
using Vector3 = Microsoft.Xna.Framework.Vector3;
using Texture2D = Microsoft.Xna.Framework.Graphics.Texture2D;
using FlatRedBall.Graphics;

namespace ZoroscopoFighters.Screens
{
    public partial class TelaLuta
    {

        private int mRoundAtual = 1;

        //private bool mInicioDeRound;
        private double mTempoInicioRound;
        private int mInicioFimRound; // 0 = Em luta; 1 = Inicio; 2 = Fim
        
        //private bool mFimDeRound;
        private double mTempoFinalRound;

        private int JogadorVencedor;
        private bool mPartidaEncerrada;

        void CustomInitialize()
        {
            
            // Define cenario
            PegaTamanhoTela();

            PosicionaChao();

            PosicionaParedes();

            DefinirFundo();

            RedefineTextoCentral("");

            // Define personagens
            DefineSpawnPoints();

            DefinePersonagens();

            DefineControladores();

            // Define o HUD
            CriarHUD();

            // Reseta o Cenario para um inicio de partida
            ResetarCenario();
                        
        }

        void CustomActivity(bool firstTimeCalled)
        {

            ValidacaoInicioFimPartida();

            ColisaoComChao();

            ColisaoComParedes();

            VirarPersonagens();

            ChecaAcertos();

            ChecaPersonagensSobrepostos();

            ChecarVencedor();
        }

        void CustomDestroy()
        {


        }

        static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        private void PegaTamanhoTela()
        {
            GlobalData.ScreenWidth = FlatRedBallServices.Game.Window.ClientBounds.Width;
            GlobalData.ScreenHeight = FlatRedBallServices.Game.Window.ClientBounds.Height;
        }

        private void DefineSpawnPoints()
        {
            Spawn1.X = -((GlobalData.ScreenWidth / 2) - (GlobalData.ScreenWidth / 4));

            Spawn2.X = ((GlobalData.ScreenWidth / 2) - (GlobalData.ScreenWidth / 4));

        }

        private void DefineControladores()
        {
            int i = 1;
            foreach (var personagem in ListaPersonagens)
            {
                if (i == 1)
                {
                    personagem.Controle = GlobalData.ControlePlayer1;
                }
                else if (i == 2)
                {
                    personagem.Controle = GlobalData.ControlePlayer2;
                }
                i++;
            }
        }

        private void ResetarCenario()
        {
            //mInicioDeRound = true; // Inicio de Round
            mInicioFimRound = 1;
            mTempoInicioRound = TimeManager.CurrentTime;

            SpriteFade.Alpha = 1f;

            RedefineTextoCentral("");

            HudInstance.IsTimerAtivo = true; // Faz com que o tempo resete

            mPartidaEncerrada = false;

            int i = 0;
            foreach (var personagem in ListaPersonagens)
            {
                ColocarPersonagemNoSpawm(personagem, i);
                personagem.HPAtual = personagem.HPInicial;
                i++;
            }
        }


        private void DefinirFundo()
        {
            SpriteFundo.Width  = GlobalData.ScreenWidth;
            SpriteFundo.Height = GlobalData.ScreenHeight;

            SpriteFade.Width = GlobalData.ScreenWidth;
            SpriteFade.Height = GlobalData.ScreenHeight;
            SpriteFade.Alpha = 0f;
            //SpriteFundo.Y = 
        }

        private void PosicionaChao()
        {
            Chao.Width = GlobalData.ScreenWidth + 1;
            Chao.X = 0;
            Chao.Y = -((GlobalData.ScreenHeight / 2) - (Chao.Height / 2));

            SpriteChao.Width = Chao.Width;
            SpriteChao.Height = Chao.Height + 20;
            SpriteChao.X = Chao.X;
            SpriteChao.Y = Chao.Y;

            SpriteGramaChao.Width = Chao.Width; // - (ParedeDireita.Width);
            SpriteGramaChao.Height = 40;
            SpriteGramaChao.X = Chao.X;
            SpriteGramaChao.Y = Chao.Y + (SpriteChao.Height / 2) - 4;
        }

        private void PosicionaParedes()
        {
            ParedeDireita.Height = GlobalData.ScreenHeight;
            ParedeDireita.X = (GlobalData.ScreenWidth / 2);

            SpriteParedeDireita.Height = GlobalData.ScreenHeight;
            SpriteParedeDireita.X = (GlobalData.ScreenWidth / 2);

            //---

            ParedeEsquerda.Height = GlobalData.ScreenHeight;
            ParedeEsquerda.X = -(GlobalData.ScreenWidth / 2);

            SpriteParedeEsquerda.Height = GlobalData.ScreenHeight;
            SpriteParedeEsquerda.X = -(GlobalData.ScreenWidth / 2);
        }

        private void RedefineTextoCentral(string texto)
        {
            RedefineTextoCentral(texto, 1);
        }

        private void RedefineTextoCentral(string texto, float escala)
        {
            TextContagem.TextureScale = escala;
            TextContagem.DisplayText = texto;
            //TextContagem.X = 0 - (TextContagem.Width  / 2);
            TextContagem.Y = 0 + (TextContagem.Height / 4);
            TextContagem.Visible = true;
        }

        private void DefinePersonagens()
        {
            ListaPersonagens.Add(new Entities.Personagem());
            ListaPersonagens.Add(new Entities.Personagem());

            int i = 1;
            foreach (var personagem in ListaPersonagens)
            {
                if (i % 2 == 0)
                {
                    personagem.Ficha = FactoryFichaPersonagem.createFichaPersonagem(EPersonagem.PEIXES);
                }
                else
                {
                    personagem.Ficha = FactoryFichaPersonagem.createFichaPersonagem(EPersonagem.SAGITARIO);
                }

                HudInstance.CriarTextJogador(i);

                //ColocarPersonagemNoSpawm(personagem, i);
                i++;
                // TODO
            }
        }

        private void ColocarPersonagemNoSpawm(Entities.Personagem personagem, int indiceSpawn)
        {
            personagem.X = ListaSpawnPoint.ElementAt(indiceSpawn).X;
            personagem.Y = (Chao.Y + Chao.Height / 2) + (personagem.Hitbox.Height / 2);
        }

        private void ValidacaoInicioFimPartida()
        {
            //if (mInicioDeRound)
            if (mInicioFimRound == 1)
            {
                HudInstance.IsTimerAtivo = false;
                //System.Diagnostics.Debug.WriteLine("> " + TimeManager.SecondsSince(mTempoFinalRound));

                if (TimeManager.SecondsSince(mTempoInicioRound) < TempoFading)
                {
                    SpriteFade.Alpha = 1f - ((float)TimeManager.SecondsSince(mTempoInicioRound) / TempoFading);
                } else if (TimeManager.SecondsSince(mTempoInicioRound) < TempoFading + 1)
                {
                    foreach (var personagem in ListaPersonagens)
                    {
                        personagem.TornarIdle();
                        personagem.IsDesimpedido = false;
                    }
                    RedefineTextoCentral("ROUND " + mRoundAtual, 0.6f);
                    SpriteFade.Alpha = 0f;
                } else if (TimeManager.SecondsSince(mTempoInicioRound) < TempoFading + 2)
                {
                    RedefineTextoCentral("3");
                } else if (TimeManager.SecondsSince(mTempoInicioRound) < TempoFading + 3)
                {
                    RedefineTextoCentral("2");
                } else if (TimeManager.SecondsSince(mTempoInicioRound) < TempoFading + 4)
                {
                    RedefineTextoCentral("1");
                } else if (TimeManager.SecondsSince(mTempoInicioRound) < TempoFading + 5)
                {
                    RedefineTextoCentral("FIGHT!");
                } else if (TimeManager.SecondsSince(mTempoInicioRound) < TempoFading + 6)
                {
                    //mInicioDeRound = false;
                    //mFimDeRound = false;
                    mInicioFimRound = 0;

                    HudInstance.IsTimerAtivo = true;
                    foreach (var personagem in ListaPersonagens)
                    {
                        personagem.IsDesimpedido = true;
                    }
                }
            }

            //if (mFimDeRound)
            if (mInicioFimRound == 2)
            {
                HudInstance.IsTimerAtivo = false;
                //System.Diagnostics.Debug.WriteLine("> " + TimeManager.SecondsSince(mTempoFinalRound));
                
                if (TimeManager.SecondsSince(mTempoFinalRound) < 3)
                {
                    if (mPartidaEncerrada)
                    {
                        RedefineTextoCentral("Jogador " + JogadorVencedor + "\nVenceu a Partida!", 0.6f);
                    }
                    else
                    {
                        RedefineTextoCentral("Jogador " + JogadorVencedor + "\nVenceu o Round!", 0.6f);
                    }
                }
                /*
                else if (TimeManager.SecondsSince(mTempoFinalRound) < 2)
                {
                    RedefineTextoCentral("2");
                }
                */
                else if (TimeManager.SecondsSince(mTempoFinalRound) < TempoFading + 3)
                {
                    SpriteFade.Alpha = (float)TimeManager.SecondsSince(mTempoFinalRound) / (TempoFading + 3);
                }
                else if (TimeManager.SecondsSince(mTempoFinalRound) < TempoFading + 4)
                {
                    if (mPartidaEncerrada)
                    {
                        this.MoveToScreen(typeof(TelaTemporaria).FullName);
                    }
                    else
                    {
                        ResetarCenario();

                        //mFimDeRound = false;

                        mRoundAtual++;

                        //HudInstance.IsTimerAtivo = true;
                        /*
                        foreach (var personagem in ListaPersonagens)
                        {
                            personagem.IsMovel = false;
                        }
                        */
                    }
                }
            }
            
            //if (!mInicioDeRound && !mFimDeRound)
            if (mInicioFimRound == 0)
            {
                TextContagem.Visible = false;
            }
            
            // TODO
        }

        private void ColisaoComChao()
        {
            foreach (var personagem in ListaPersonagens)
            {
                if (personagem.Hitbox.CollideAgainstMove(Chao, 0, 1))
                {
                    if (!personagem.IsNoChao && personagem.IsAtacandoMelee)
                    {
                        personagem.IsAtacandoMelee = false;
                    }
                    personagem.IsNoChao = true;
                }
                else
                {
                    personagem.IsNoChao = false;
                }
            }
        }

        private void ColisaoComParedes()
        {
            foreach (var personagem in ListaPersonagens)
            {

                foreach (var parede in ListaParedes)
                {
                    personagem.Hitbox.CollideAgainstMove(parede, 0, 1);
                }
            }
        }

        private void VirarPersonagens()
        {
            foreach (var personagem in ListaPersonagens)
            {
                foreach (var outroPersonagem in ListaPersonagens)
                {
                    if (personagem != outroPersonagem)
                    {
                        if (personagem.Hitbox.X - outroPersonagem.Hitbox.X > 0)
                        {
                            personagem.Lado = ELado.ESQUERDO;
                        }
                        else
                        {
                            personagem.Lado = ELado.DIREITO;
                        }
                    }
                }
            }
        }

        private void ChecaAcertos()
        {
            foreach (var personagem in ListaPersonagens)
            {
                foreach (var outroPersonagem in ListaPersonagens)
                {
                    if (personagem != outroPersonagem)
                    {
                        if (personagem.IsAtacandoMelee)
                        {
                            if (personagem.AtaqueAcertou(outroPersonagem)) // Se o ataque acertar
                            {
                                outroPersonagem.Apanhar(personagem);
                            }
                        }
                    }
                }
            }
        }

        private void ChecaPersonagensSobrepostos()
        {

            foreach (var personagem in ListaPersonagens)
            {
                foreach (var outroPersonagem in ListaPersonagens)
                {
                    if (personagem != outroPersonagem)
                    {
                        if (personagem.Hitbox.CollideAgainst(outroPersonagem.Hitbox))
                        {
                            if (personagem.Lado == ELado.ESQUERDO)
                            {
                                personagem.Velocity += personagem.RotationMatrix.Right * 200;
                                outroPersonagem.Velocity += personagem.RotationMatrix.Left * 200;
                            }
                            else
                            {
                                personagem.Velocity += personagem.RotationMatrix.Left * 200;
                                outroPersonagem.Velocity += outroPersonagem.RotationMatrix.Right * 200;
                            }
                        }
                    }
                }
            }

        }

        private void CriarHUD()
        {
            foreach (var personagem in ListaPersonagens)
            {
                HudInstance.CriarBarraDeHp(personagem);
                HudInstance.CriarTextNome(personagem.Ficha.Nome);
                HudInstance.CriarTextRoundsVencidos(personagem);
            }

            HudInstance.DefinePosicaoDosObjetos();

        }

        private void ChecarVencedor()
        {
            Entities.Personagem perdedor = null;
            perdedor = VerificarHpDosPersonagens();

            if (perdedor == null) {
                perdedor = VerificarTempo();
            }

            //if (perdedor != null && !mFimDeRound)
            if (perdedor != null && mInicioFimRound == 0)  // mInicioFimRound != 2)
            {
                int i = 1;
                foreach (var personagem in ListaPersonagens)
                {
                    if (personagem != perdedor)
                    {
                        personagem.RoundsVencidos++;
                        JogadorVencedor = i;
                        personagem.Vitoria();

                        if (personagem.RoundsVencidos == GlobalData.MaxVitoriasPartida)
                        {
                            mPartidaEncerrada = true;
                        }
                        // TODO Resetar tudo, fazer a paradinha "fancy" de vit�ria (delegar para o "Hud" fazer isso)
                        
                    }

                    personagem.IsDesimpedido = false;
                    i++;
                }
                mTempoFinalRound = TimeManager.CurrentTime;
                //mFimDeRound = true;
                mInicioFimRound = 2;
                SpriteFade.Alpha = 0f;

                perdedor.Nocautear();
            }
        }

        private Entities.Personagem VerificarHpDosPersonagens()
        {
            Entities.Personagem perdedor = null;

            foreach (var personagem in ListaPersonagens)
            {
                if (personagem.HPAtual <= 0)
                {
                    perdedor = personagem;
                    break;
                }
            }

            return perdedor;
        }

        private Entities.Personagem VerificarTempo()
        {
            Entities.Personagem perdedor = null;

            if (HudInstance.AcabouTempo())
            {
                int menorHP = int.MaxValue;

                foreach (var personagem in ListaPersonagens)
                {
                    if (personagem.HPAtual < menorHP)
                    {
                        perdedor = personagem;

                        menorHP = personagem.HPAtual;
                        // TODO sistema de Draw (ou n�o, foda-se)

                    }
                }
            }

            return perdedor;
        }
    }
}
