#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using ZoroscopoFighters.Entities;
using FlatRedBall;
using FlatRedBall.Screens;
using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall.Math;
using FlatRedBall.Math.Geometry;
using FlatRedBall.Graphics;
namespace ZoroscopoFighters.Screens
{
    public partial class TelaLuta : FlatRedBall.Screens.Screen
    {
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        protected static Microsoft.Xna.Framework.Graphics.Texture2D ground;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D Paredes;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D Grama;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D GramaMenor;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D Fundo;
        protected static FlatRedBall.Graphics.BitmapFont Bauhaus93_160;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D Fade;
        
        private FlatRedBall.Math.PositionedObjectList<ZoroscopoFighters.Entities.Personagem> ListaPersonagens;
        private FlatRedBall.Math.Geometry.AxisAlignedRectangle mChao;
        public FlatRedBall.Math.Geometry.AxisAlignedRectangle Chao
        {
            get
            {
                return mChao;
            }
            private set
            {
                mChao = value;
            }
        }
        private FlatRedBall.Math.PositionedObjectList<FlatRedBall.Math.Geometry.AxisAlignedRectangle> ListaParedes;
        private FlatRedBall.Math.Geometry.AxisAlignedRectangle mParedeDireita;
        public FlatRedBall.Math.Geometry.AxisAlignedRectangle ParedeDireita
        {
            get
            {
                return mParedeDireita;
            }
            private set
            {
                mParedeDireita = value;
            }
        }
        private FlatRedBall.Math.Geometry.AxisAlignedRectangle mParedeEsquerda;
        public FlatRedBall.Math.Geometry.AxisAlignedRectangle ParedeEsquerda
        {
            get
            {
                return mParedeEsquerda;
            }
            private set
            {
                mParedeEsquerda = value;
            }
        }
        private FlatRedBall.Math.PositionedObjectList<FlatRedBall.PositionedObject> ListaSpawnPoint;
        private FlatRedBall.PositionedObject Spawn1;
        private FlatRedBall.PositionedObject Spawn2;
        private FlatRedBall.Sprite SpriteChao;
        private ZoroscopoFighters.Entities.Hud HudInstance;
        private FlatRedBall.Graphics.Text TextContagem;
        private FlatRedBall.Sprite SpriteGramaChao;
        private FlatRedBall.Sprite SpriteParedeEsquerda;
        private FlatRedBall.Sprite SpriteParedeDireita;
        private FlatRedBall.Sprite SpriteFundo;
        private FlatRedBall.Sprite SpriteFade;
        public float TempoFading = 2f;
        public TelaLuta () 
        	: base ("TelaLuta")
        {
        }
        public override void Initialize (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            ListaPersonagens = new FlatRedBall.Math.PositionedObjectList<ZoroscopoFighters.Entities.Personagem>();
            ListaPersonagens.Name = "ListaPersonagens";
            mChao = new FlatRedBall.Math.Geometry.AxisAlignedRectangle();
            mChao.Name = "mChao";
            ListaParedes = new FlatRedBall.Math.PositionedObjectList<FlatRedBall.Math.Geometry.AxisAlignedRectangle>();
            ListaParedes.Name = "ListaParedes";
            mParedeDireita = new FlatRedBall.Math.Geometry.AxisAlignedRectangle();
            mParedeDireita.Name = "mParedeDireita";
            mParedeEsquerda = new FlatRedBall.Math.Geometry.AxisAlignedRectangle();
            mParedeEsquerda.Name = "mParedeEsquerda";
            ListaSpawnPoint = new FlatRedBall.Math.PositionedObjectList<FlatRedBall.PositionedObject>();
            ListaSpawnPoint.Name = "ListaSpawnPoint";
            Spawn1 = new FlatRedBall.PositionedObject();
            Spawn1.Name = "Spawn1";
            Spawn2 = new FlatRedBall.PositionedObject();
            Spawn2.Name = "Spawn2";
            SpriteChao = new FlatRedBall.Sprite();
            SpriteChao.Name = "SpriteChao";
            HudInstance = new ZoroscopoFighters.Entities.Hud(ContentManagerName, false);
            HudInstance.Name = "HudInstance";
            TextContagem = new FlatRedBall.Graphics.Text();
            TextContagem.Name = "TextContagem";
            SpriteGramaChao = new FlatRedBall.Sprite();
            SpriteGramaChao.Name = "SpriteGramaChao";
            SpriteParedeEsquerda = new FlatRedBall.Sprite();
            SpriteParedeEsquerda.Name = "SpriteParedeEsquerda";
            SpriteParedeDireita = new FlatRedBall.Sprite();
            SpriteParedeDireita.Name = "SpriteParedeDireita";
            SpriteFundo = new FlatRedBall.Sprite();
            SpriteFundo.Name = "SpriteFundo";
            SpriteFade = new FlatRedBall.Sprite();
            SpriteFade.Name = "SpriteFade";
            
            
            PostInitialize();
            base.Initialize(addToManagers);
            if (addToManagers)
            {
                AddToManagers();
            }
        }
        public override void AddToManagers () 
        {
            FlatRedBall.Math.Geometry.ShapeManager.AddAxisAlignedRectangle(mChao);
            FlatRedBall.Math.Geometry.ShapeManager.AddAxisAlignedRectangle(mParedeDireita);
            FlatRedBall.Math.Geometry.ShapeManager.AddAxisAlignedRectangle(mParedeEsquerda);
            FlatRedBall.SpriteManager.AddPositionedObject(Spawn1);
            FlatRedBall.SpriteManager.AddPositionedObject(Spawn2);
            FlatRedBall.SpriteManager.AddSprite(SpriteChao);
            HudInstance.AddToManagers(mLayer);
            FlatRedBall.Graphics.TextManager.AddText(TextContagem); if(TextContagem.Font != null) TextContagem.SetPixelPerfectScale(SpriteManager.Camera);
            if (TextContagem.Font != null)
            {
                TextContagem.SetPixelPerfectScale(mLayer);
            }
            FlatRedBall.SpriteManager.AddSprite(SpriteGramaChao);
            FlatRedBall.SpriteManager.AddSprite(SpriteParedeEsquerda);
            FlatRedBall.SpriteManager.AddSprite(SpriteParedeDireita);
            FlatRedBall.SpriteManager.AddSprite(SpriteFundo);
            FlatRedBall.SpriteManager.AddSprite(SpriteFade);
            base.AddToManagers();
            AddToManagersBottomUp();
            CustomInitialize();
        }
        public override void Activity (bool firstTimeCalled) 
        {
            if (!IsPaused)
            {
                
                for (int i = ListaPersonagens.Count - 1; i > -1; i--)
                {
                    if (i < ListaPersonagens.Count)
                    {
                        // We do the extra if-check because activity could destroy any number of entities
                        ListaPersonagens[i].Activity();
                    }
                }
                HudInstance.Activity();
            }
            else
            {
            }
            base.Activity(firstTimeCalled);
            if (!IsActivityFinished)
            {
                CustomActivity(firstTimeCalled);
            }
        }
        public override void Destroy () 
        {
            base.Destroy();
            ground = null;
            Paredes = null;
            Grama = null;
            GramaMenor = null;
            Fundo = null;
            Bauhaus93_160 = null;
            Fade = null;
            
            ListaPersonagens.MakeOneWay();
            ListaParedes.MakeOneWay();
            ListaSpawnPoint.MakeOneWay();
            for (int i = ListaPersonagens.Count - 1; i > -1; i--)
            {
                ListaPersonagens[i].Destroy();
            }
            if (Chao != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.Remove(Chao);
            }
            for (int i = ListaParedes.Count - 1; i > -1; i--)
            {
                FlatRedBall.Math.Geometry.ShapeManager.Remove(ListaParedes[i]);
            }
            for (int i = ListaSpawnPoint.Count - 1; i > -1; i--)
            {
                FlatRedBall.SpriteManager.RemovePositionedObject(ListaSpawnPoint[i]);
            }
            if (SpriteChao != null)
            {
                FlatRedBall.SpriteManager.RemoveSprite(SpriteChao);
            }
            if (HudInstance != null)
            {
                HudInstance.Destroy();
                HudInstance.Detach();
            }
            if (TextContagem != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveText(TextContagem);
            }
            if (SpriteGramaChao != null)
            {
                FlatRedBall.SpriteManager.RemoveSprite(SpriteGramaChao);
            }
            if (SpriteParedeEsquerda != null)
            {
                FlatRedBall.SpriteManager.RemoveSprite(SpriteParedeEsquerda);
            }
            if (SpriteParedeDireita != null)
            {
                FlatRedBall.SpriteManager.RemoveSprite(SpriteParedeDireita);
            }
            if (SpriteFundo != null)
            {
                FlatRedBall.SpriteManager.RemoveSprite(SpriteFundo);
            }
            if (SpriteFade != null)
            {
                FlatRedBall.SpriteManager.RemoveSprite(SpriteFade);
            }
            ListaPersonagens.MakeTwoWay();
            ListaParedes.MakeTwoWay();
            ListaSpawnPoint.MakeTwoWay();
            FlatRedBall.Math.Collision.CollisionManager.Self.Relationships.Clear();
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            Chao.Width = 32f;
            Chao.Height = 100f;
            Chao.Visible = false;
            ListaParedes.Add(ParedeDireita);
            ParedeDireita.Width = 85f;
            ParedeDireita.Height = 32f;
            ParedeDireita.Visible = false;
            ListaParedes.Add(ParedeEsquerda);
            ParedeEsquerda.Width = 85f;
            ParedeEsquerda.Height = 32f;
            ParedeEsquerda.Visible = false;
            ListaSpawnPoint.Add(Spawn1);
            ListaSpawnPoint.Add(Spawn2);
            if (SpriteChao.Parent == null)
            {
                SpriteChao.Z = 2f;
            }
            else
            {
                SpriteChao.RelativeZ = 2f;
            }
            SpriteChao.Texture = ground;
            #if FRB_MDX
            SpriteChao.TextureAddressMode = Microsoft.DirectX.Direct3D.TextureAddress.Wrap;
            #else
            SpriteChao.TextureAddressMode = Microsoft.Xna.Framework.Graphics.TextureAddressMode.Wrap;
            #endif
            SpriteChao.TextureScale = 1f;
            SpriteChao.Width = 50f;
            SpriteChao.Height = 50f;
            TextContagem.DisplayText = "";
            TextContagem.Font = Bauhaus93_160;
            TextContagem.HorizontalAlignment = FlatRedBall.Graphics.HorizontalAlignment.Center;
            if (TextContagem.Parent == null)
            {
                TextContagem.Y = 0f;
            }
            else
            {
                TextContagem.RelativeY = 0f;
            }
            if (TextContagem.Parent == null)
            {
                TextContagem.Z = 16f;
            }
            else
            {
                TextContagem.RelativeZ = 16f;
            }
            TextContagem.TextureScale = 1f;
            #if FRB_MDX
            TextContagem.ColorOperation = Microsoft.DirectX.Direct3D.TextureOperation.ColorTextureAlpha;
            #else
            TextContagem.ColorOperation = FlatRedBall.Graphics.ColorOperation.ColorTextureAlpha;
            #endif
            TextContagem.Red = 1f;
            TextContagem.Green = 0.2f;
            TextContagem.Blue = 0.2f;
            TextContagem.BlendOperation = FlatRedBall.Graphics.BlendOperation.Regular;
            TextContagem.Alpha = 1f;
            if (SpriteGramaChao.Parent == null)
            {
                SpriteGramaChao.Z = 3f;
            }
            else
            {
                SpriteGramaChao.RelativeZ = 3f;
            }
            SpriteGramaChao.Texture = GramaMenor;
            #if FRB_MDX
            SpriteGramaChao.TextureAddressMode = Microsoft.DirectX.Direct3D.TextureAddress.Wrap;
            #else
            SpriteGramaChao.TextureAddressMode = Microsoft.Xna.Framework.Graphics.TextureAddressMode.Wrap;
            #endif
            SpriteGramaChao.TextureScale = 1f;
            if (SpriteParedeEsquerda.Parent == null)
            {
                SpriteParedeEsquerda.Z = 1f;
            }
            else
            {
                SpriteParedeEsquerda.RelativeZ = 1f;
            }
            SpriteParedeEsquerda.Texture = Paredes;
            SpriteParedeEsquerda.TextureScale = 1f;
            if (SpriteParedeDireita.Parent == null)
            {
                SpriteParedeDireita.Z = 1f;
            }
            else
            {
                SpriteParedeDireita.RelativeZ = 1f;
            }
            SpriteParedeDireita.Texture = Paredes;
            SpriteParedeDireita.FlipHorizontal = true;
            SpriteParedeDireita.TextureScale = 1f;
            SpriteFundo.Texture = Fundo;
            SpriteFundo.TextureScale = 1f;
            if (SpriteFade.Parent == null)
            {
                SpriteFade.Z = 20f;
            }
            else
            {
                SpriteFade.RelativeZ = 20f;
            }
            SpriteFade.Texture = Fade;
            SpriteFade.TextureScale = 1f;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp () 
        {
            CameraSetup.ResetCamera(SpriteManager.Camera);
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            for (int i = ListaPersonagens.Count - 1; i > -1; i--)
            {
                ListaPersonagens[i].Destroy();
            }
            if (Chao != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.RemoveOneWay(Chao);
            }
            for (int i = ListaParedes.Count - 1; i > -1; i--)
            {
                FlatRedBall.Math.Geometry.ShapeManager.Remove(ListaParedes[i]);
            }
            for (int i = ListaSpawnPoint.Count - 1; i > -1; i--)
            {
                FlatRedBall.SpriteManager.RemovePositionedObject(ListaSpawnPoint[i]);
            }
            if (SpriteChao != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(SpriteChao);
            }
            HudInstance.RemoveFromManagers();
            if (TextContagem != null)
            {
                FlatRedBall.Graphics.TextManager.RemoveTextOneWay(TextContagem);
            }
            if (SpriteGramaChao != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(SpriteGramaChao);
            }
            if (SpriteParedeEsquerda != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(SpriteParedeEsquerda);
            }
            if (SpriteParedeDireita != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(SpriteParedeDireita);
            }
            if (SpriteFundo != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(SpriteFundo);
            }
            if (SpriteFade != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(SpriteFade);
            }
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
                HudInstance.AssignCustomVariables(true);
            }
            Chao.Width = 32f;
            Chao.Height = 100f;
            Chao.Visible = false;
            ParedeDireita.Width = 85f;
            ParedeDireita.Height = 32f;
            ParedeDireita.Visible = false;
            ParedeEsquerda.Width = 85f;
            ParedeEsquerda.Height = 32f;
            ParedeEsquerda.Visible = false;
            if (SpriteChao.Parent == null)
            {
                SpriteChao.Z = 2f;
            }
            else
            {
                SpriteChao.RelativeZ = 2f;
            }
            SpriteChao.Texture = ground;
            #if FRB_MDX
            SpriteChao.TextureAddressMode = Microsoft.DirectX.Direct3D.TextureAddress.Wrap;
            #else
            SpriteChao.TextureAddressMode = Microsoft.Xna.Framework.Graphics.TextureAddressMode.Wrap;
            #endif
            SpriteChao.TextureScale = 1f;
            SpriteChao.Width = 50f;
            SpriteChao.Height = 50f;
            TextContagem.DisplayText = "";
            TextContagem.Font = Bauhaus93_160;
            TextContagem.HorizontalAlignment = FlatRedBall.Graphics.HorizontalAlignment.Center;
            if (TextContagem.Parent == null)
            {
                TextContagem.Y = 0f;
            }
            else
            {
                TextContagem.RelativeY = 0f;
            }
            if (TextContagem.Parent == null)
            {
                TextContagem.Z = 16f;
            }
            else
            {
                TextContagem.RelativeZ = 16f;
            }
            TextContagem.TextureScale = 1f;
            #if FRB_MDX
            TextContagem.ColorOperation = Microsoft.DirectX.Direct3D.TextureOperation.ColorTextureAlpha;
            #else
            TextContagem.ColorOperation = FlatRedBall.Graphics.ColorOperation.ColorTextureAlpha;
            #endif
            TextContagem.Red = 1f;
            TextContagem.Green = 0.2f;
            TextContagem.Blue = 0.2f;
            TextContagem.BlendOperation = FlatRedBall.Graphics.BlendOperation.Regular;
            TextContagem.Alpha = 1f;
            if (SpriteGramaChao.Parent == null)
            {
                SpriteGramaChao.Z = 3f;
            }
            else
            {
                SpriteGramaChao.RelativeZ = 3f;
            }
            SpriteGramaChao.Texture = GramaMenor;
            #if FRB_MDX
            SpriteGramaChao.TextureAddressMode = Microsoft.DirectX.Direct3D.TextureAddress.Wrap;
            #else
            SpriteGramaChao.TextureAddressMode = Microsoft.Xna.Framework.Graphics.TextureAddressMode.Wrap;
            #endif
            SpriteGramaChao.TextureScale = 1f;
            if (SpriteParedeEsquerda.Parent == null)
            {
                SpriteParedeEsquerda.Z = 1f;
            }
            else
            {
                SpriteParedeEsquerda.RelativeZ = 1f;
            }
            SpriteParedeEsquerda.Texture = Paredes;
            SpriteParedeEsquerda.TextureScale = 1f;
            if (SpriteParedeDireita.Parent == null)
            {
                SpriteParedeDireita.Z = 1f;
            }
            else
            {
                SpriteParedeDireita.RelativeZ = 1f;
            }
            SpriteParedeDireita.Texture = Paredes;
            SpriteParedeDireita.FlipHorizontal = true;
            SpriteParedeDireita.TextureScale = 1f;
            SpriteFundo.Texture = Fundo;
            SpriteFundo.TextureScale = 1f;
            if (SpriteFade.Parent == null)
            {
                SpriteFade.Z = 20f;
            }
            else
            {
                SpriteFade.RelativeZ = 20f;
            }
            SpriteFade.Texture = Fade;
            SpriteFade.TextureScale = 1f;
            TempoFading = 2f;
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            for (int i = 0; i < ListaPersonagens.Count; i++)
            {
                ListaPersonagens[i].ConvertToManuallyUpdated();
            }
            for (int i = 0; i < ListaSpawnPoint.Count; i++)
            {
                FlatRedBall.SpriteManager.ConvertToManuallyUpdated(ListaSpawnPoint[i]);
            }
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(SpriteChao);
            HudInstance.ConvertToManuallyUpdated();
            FlatRedBall.Graphics.TextManager.ConvertToManuallyUpdated(TextContagem);
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(SpriteGramaChao);
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(SpriteParedeEsquerda);
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(SpriteParedeDireita);
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(SpriteFundo);
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(SpriteFade);
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            ground = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/screens/telaluta/ground.png", contentManagerName);
            Paredes = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/screens/telaluta/paredes.png", contentManagerName);
            Grama = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/screens/telaluta/grama.png", contentManagerName);
            GramaMenor = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/screens/telaluta/gramamenor.png", contentManagerName);
            Fundo = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/screens/telaluta/fundo.png", contentManagerName);
            Bauhaus93_160 = FlatRedBall.FlatRedBallServices.Load<FlatRedBall.Graphics.BitmapFont>(@"content/screens/telaluta/bauhaus93_160.fnt", contentManagerName);
            Fade = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/screens/telaluta/fade.png", contentManagerName);
            ZoroscopoFighters.Entities.Hud.LoadStaticContent(contentManagerName);
            CustomLoadStaticContent(contentManagerName);
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            switch(memberName)
            {
                case  "ground":
                    return ground;
                case  "Paredes":
                    return Paredes;
                case  "Grama":
                    return Grama;
                case  "GramaMenor":
                    return GramaMenor;
                case  "Fundo":
                    return Fundo;
                case  "Bauhaus93_160":
                    return Bauhaus93_160;
                case  "Fade":
                    return Fade;
            }
            return null;
        }
        public static object GetFile (string memberName) 
        {
            switch(memberName)
            {
                case  "ground":
                    return ground;
                case  "Paredes":
                    return Paredes;
                case  "Grama":
                    return Grama;
                case  "GramaMenor":
                    return GramaMenor;
                case  "Fundo":
                    return Fundo;
                case  "Bauhaus93_160":
                    return Bauhaus93_160;
                case  "Fade":
                    return Fade;
            }
            return null;
        }
        object GetMember (string memberName) 
        {
            switch(memberName)
            {
                case  "ground":
                    return ground;
                case  "Paredes":
                    return Paredes;
                case  "Grama":
                    return Grama;
                case  "GramaMenor":
                    return GramaMenor;
                case  "Fundo":
                    return Fundo;
                case  "Bauhaus93_160":
                    return Bauhaus93_160;
                case  "Fade":
                    return Fade;
            }
            return null;
        }
    }
}
