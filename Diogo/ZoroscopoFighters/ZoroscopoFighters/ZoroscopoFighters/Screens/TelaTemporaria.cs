
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;

using FlatRedBall.Math.Geometry;
using FlatRedBall.Math.Splines;

using Cursor = FlatRedBall.Gui.Cursor;
using GuiManager = FlatRedBall.Gui.GuiManager;
using FlatRedBall.Localization;

using Keys = Microsoft.Xna.Framework.Input.Keys;
using Vector3 = Microsoft.Xna.Framework.Vector3;
using Texture2D = Microsoft.Xna.Framework.Graphics.Texture2D;
using ZoroscopoFighters.MinhasClasses.Organizadores;

namespace ZoroscopoFighters.Screens
{
	public partial class TelaTemporaria
	{

        private double mTempoInicio = 0;

		void CustomInitialize()
		{
            SpriteFundo.Width = GlobalData.ScreenWidth;
            SpriteFundo.Height = GlobalData.ScreenHeight;

            TextPressStart.X -= TextPressStart.Width / 2;

            mTempoInicio = TimeManager.CurrentTime;

            DefineControladores();

        }

		void CustomActivity(bool firstTimeCalled)
		{
            PiscarTexto();

            VerificarEnterPressionado();
		}

		void CustomDestroy()
		{


		}

        static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        private void PiscarTexto()
        {
            if ((int) (TimeManager.SecondsSince(mTempoInicio)*2) % 2 == 0)
            {
                TextPressStart.Visible = true;
            } else
            {
                TextPressStart.Visible = false;
            }
        }

        private void VerificarEnterPressionado()
        {
            if (InputManager.Keyboard.KeyDown(Keys.Enter))
            {
                this.MoveToScreen(typeof(TelaLuta).FullName);
            }
        }

        private void DefineControladores()
        {
            GlobalData.ControlePlayer1.BotaoSalto = Keys.Up;
            GlobalData.ControlePlayer1.BotaoAbaixar = Keys.Down;
            GlobalData.ControlePlayer1.BotaoDireita = Keys.Right;
            GlobalData.ControlePlayer1.BotaoEsquerda = Keys.Left;
            GlobalData.ControlePlayer1.BotaoSoco = Keys.RightControl;
            GlobalData.ControlePlayer1.BotaoChute = Keys.RightShift;

            GlobalData.ControlePlayer2.BotaoSalto = Keys.W;
            GlobalData.ControlePlayer2.BotaoAbaixar = Keys.S;
            GlobalData.ControlePlayer2.BotaoDireita = Keys.D;
            GlobalData.ControlePlayer2.BotaoEsquerda = Keys.A;
            GlobalData.ControlePlayer2.BotaoSoco = Keys.F;
            GlobalData.ControlePlayer2.BotaoChute = Keys.G;
        }


    }
}
