
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using RockBlaster.Data;

using FlatRedBall.Math.Geometry;
using FlatRedBall.Math.Splines;
using RockBlaster.Entities;

using Cursor = FlatRedBall.Gui.Cursor;
using GuiManager = FlatRedBall.Gui.GuiManager;
using FlatRedBall.Localization;

using Keys = Microsoft.Xna.Framework.Input.Keys;
using Vector3 = Microsoft.Xna.Framework.Vector3;
using Texture2D = Microsoft.Xna.Framework.Graphics.Texture2D;

namespace RockBlaster.Screens
{
	public partial class GameScreen
	{

		void CustomInitialize()
		{
            this.HudInstance.MainShipList = MainShipList;

        }

		void CustomActivity(bool firstTimeCalled)
		{
            CollisionActivity();

		}

		void CustomDestroy()
		{


		}

        static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        private void CollisionActivity()
        {
            BulletVsRockCollisionActivity();
            MainShipVsRockCollisionActivity();
            RockVsRockCollisionActivity();
        }

        private void BulletVsRockCollisionActivity()
        {
            // We need to reverse loop because we're going to call Destroy.
            for (int i = BulletList.Count - 1; i > -1; i--)
            {
                Bullet bullet = BulletList[i];
                for (int j = RockList.Count - 1; j > -1; j--)
                {
                    Rock rock = RockList[j];

                    if (rock.CircleInstance.CollideAgainst(bullet.CircleInstance))
                    {
                        rock.TakeHit();
                        bullet.Destroy();

                        GlobalData.PlayerData.Score += rock.PointsWorth;
                        this.HudInstance.Score = GlobalData.PlayerData.Score;

                        break;
                    }
                }
            }
        }

        private void MainShipVsRockCollisionActivity()
        {
            // We need to reverse loop because we're going to call Destroy.
            for (int i = RockList.Count - 1; i > -1; i--)
            {
                Rock rock = RockList[i];
                for (int j = MainShipList.Count - 1; j > -1; j--)
                {
                    MainShip mainShip = MainShipList[j];

                    if (mainShip.CircleInstance.CollideAgainst(rock.CircleInstance))
                    {
                        mainShip.Health--;
                        rock.TakeHit();
                        break;
                    }
                }
            }
        }

        private void RockVsRockCollisionActivity()
        {
            // Destroys aren't being called here so we can forward-loop
            for (int i = 0; i < RockList.Count; i++)
            {
                Rock firstRock = RockList[i];
                for (int j = i + 1; j < RockList.Count; j++)
                {
                    Rock secondRock = RockList[j];

                    float firstRockMass = 1;
                    float secondRockMass = 1;
                    float elasticity = .8f; // 1 would be a full bounce, we want to lose a little bit of energy

                    firstRock.CircleInstance.CollideAgainstBounce(
                        secondRock.CircleInstance, firstRockMass, secondRockMass, elasticity);
                }
            }
        }

    }
}
