#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using RockBlaster.Entities;
using RockBlaster.Factories;
using FlatRedBall;
using FlatRedBall.Screens;
using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall.Math;
namespace RockBlaster.Screens
{
    public partial class GameScreen : FlatRedBall.Screens.Screen
    {
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        
        private FlatRedBall.Math.PositionedObjectList<RockBlaster.Entities.Bullet> BulletList;
        private FlatRedBall.Math.PositionedObjectList<RockBlaster.Entities.Rock> RockList;
        private FlatRedBall.Math.PositionedObjectList<RockBlaster.Entities.MainShip> MainShipList;
        private RockBlaster.Entities.MainShip Player1Ship;
        private RockBlaster.Entities.EndGameUi EndGameUiInstance;
        private RockBlaster.Entities.Hud HudInstance;
        private RockBlaster.Entities.RockSpawner RockSpawnerInstance;
        public GameScreen () 
        	: base ("GameScreen")
        {
        }
        public override void Initialize (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            BulletList = new FlatRedBall.Math.PositionedObjectList<RockBlaster.Entities.Bullet>();
            BulletList.Name = "BulletList";
            RockList = new FlatRedBall.Math.PositionedObjectList<RockBlaster.Entities.Rock>();
            RockList.Name = "RockList";
            MainShipList = new FlatRedBall.Math.PositionedObjectList<RockBlaster.Entities.MainShip>();
            MainShipList.Name = "MainShipList";
            Player1Ship = new RockBlaster.Entities.MainShip(ContentManagerName, false);
            Player1Ship.Name = "Player1Ship";
            EndGameUiInstance = new RockBlaster.Entities.EndGameUi(ContentManagerName, false);
            EndGameUiInstance.Name = "EndGameUiInstance";
            HudInstance = new RockBlaster.Entities.Hud(ContentManagerName, false);
            HudInstance.Name = "HudInstance";
            RockSpawnerInstance = new RockBlaster.Entities.RockSpawner(ContentManagerName, false);
            RockSpawnerInstance.Name = "RockSpawnerInstance";
            
            
            PostInitialize();
            base.Initialize(addToManagers);
            if (addToManagers)
            {
                AddToManagers();
            }
        }
        public override void AddToManagers () 
        {
            Factories.BulletFactory.Initialize(ContentManagerName);
            Factories.RockFactory.Initialize(ContentManagerName);
            Factories.BulletFactory.AddList(BulletList);
            Factories.RockFactory.AddList(RockList);
            Player1Ship.AddToManagers(mLayer);
            EndGameUiInstance.AddToManagers(mLayer);
            HudInstance.AddToManagers(mLayer);
            RockSpawnerInstance.AddToManagers(mLayer);
            base.AddToManagers();
            AddToManagersBottomUp();
            CustomInitialize();
        }
        public override void Activity (bool firstTimeCalled) 
        {
            if (!IsPaused)
            {
                
                for (int i = BulletList.Count - 1; i > -1; i--)
                {
                    if (i < BulletList.Count)
                    {
                        // We do the extra if-check because activity could destroy any number of entities
                        BulletList[i].Activity();
                    }
                }
                for (int i = RockList.Count - 1; i > -1; i--)
                {
                    if (i < RockList.Count)
                    {
                        // We do the extra if-check because activity could destroy any number of entities
                        RockList[i].Activity();
                    }
                }
                for (int i = MainShipList.Count - 1; i > -1; i--)
                {
                    if (i < MainShipList.Count)
                    {
                        // We do the extra if-check because activity could destroy any number of entities
                        MainShipList[i].Activity();
                    }
                }
                EndGameUiInstance.Activity();
                HudInstance.Activity();
                RockSpawnerInstance.Activity();
            }
            else
            {
            }
            base.Activity(firstTimeCalled);
            if (!IsActivityFinished)
            {
                CustomActivity(firstTimeCalled);
            }
        }
        public override void Destroy () 
        {
            base.Destroy();
            Factories.BulletFactory.Destroy();
            Factories.RockFactory.Destroy();
            
            BulletList.MakeOneWay();
            RockList.MakeOneWay();
            MainShipList.MakeOneWay();
            for (int i = BulletList.Count - 1; i > -1; i--)
            {
                BulletList[i].Destroy();
            }
            for (int i = RockList.Count - 1; i > -1; i--)
            {
                RockList[i].Destroy();
            }
            for (int i = MainShipList.Count - 1; i > -1; i--)
            {
                MainShipList[i].Destroy();
            }
            if (EndGameUiInstance != null)
            {
                EndGameUiInstance.Destroy();
                EndGameUiInstance.Detach();
            }
            if (HudInstance != null)
            {
                HudInstance.Destroy();
                HudInstance.Detach();
            }
            if (RockSpawnerInstance != null)
            {
                RockSpawnerInstance.Destroy();
                RockSpawnerInstance.Detach();
            }
            BulletList.MakeTwoWay();
            RockList.MakeTwoWay();
            MainShipList.MakeTwoWay();
            FlatRedBall.Math.Collision.CollisionManager.Self.Relationships.Clear();
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            MainShipList.Add(Player1Ship);
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp () 
        {
            CameraSetup.ResetCamera(SpriteManager.Camera);
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            for (int i = BulletList.Count - 1; i > -1; i--)
            {
                BulletList[i].Destroy();
            }
            for (int i = RockList.Count - 1; i > -1; i--)
            {
                RockList[i].Destroy();
            }
            for (int i = MainShipList.Count - 1; i > -1; i--)
            {
                MainShipList[i].Destroy();
            }
            EndGameUiInstance.RemoveFromManagers();
            HudInstance.RemoveFromManagers();
            RockSpawnerInstance.RemoveFromManagers();
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
                Player1Ship.AssignCustomVariables(true);
                EndGameUiInstance.AssignCustomVariables(true);
                HudInstance.AssignCustomVariables(true);
                RockSpawnerInstance.AssignCustomVariables(true);
            }
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            for (int i = 0; i < BulletList.Count; i++)
            {
                BulletList[i].ConvertToManuallyUpdated();
            }
            for (int i = 0; i < RockList.Count; i++)
            {
                RockList[i].ConvertToManuallyUpdated();
            }
            for (int i = 0; i < MainShipList.Count; i++)
            {
                MainShipList[i].ConvertToManuallyUpdated();
            }
            EndGameUiInstance.ConvertToManuallyUpdated();
            HudInstance.ConvertToManuallyUpdated();
            RockSpawnerInstance.ConvertToManuallyUpdated();
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            RockBlaster.Entities.EndGameUi.LoadStaticContent(contentManagerName);
            RockBlaster.Entities.Hud.LoadStaticContent(contentManagerName);
            RockBlaster.Entities.RockSpawner.LoadStaticContent(contentManagerName);
            CustomLoadStaticContent(contentManagerName);
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            return null;
        }
        public static object GetFile (string memberName) 
        {
            return null;
        }
        object GetMember (string memberName) 
        {
            return null;
        }
    }
}
