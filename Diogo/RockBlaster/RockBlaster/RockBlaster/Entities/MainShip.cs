#region Usings

using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;

using FlatRedBall.Math.Geometry;
using FlatRedBall.Math.Splines;
using BitmapFont = FlatRedBall.Graphics.BitmapFont;
using Cursor = FlatRedBall.Gui.Cursor;
using GuiManager = FlatRedBall.Gui.GuiManager;
using RockBlaster.Factories;

#if FRB_XNA || SILVERLIGHT
using Keys = Microsoft.Xna.Framework.Input.Keys;
using Vector3 = Microsoft.Xna.Framework.Vector3;
using Texture2D = Microsoft.Xna.Framework.Graphics.Texture2D;

#endif
#endregion

namespace RockBlaster.Entities
{
	public partial class MainShip
	{

        Xbox360GamePad mGamePad;
        I2DInput mKeyboardInput { get; set; }
        IPressableInput mShootingButton { get; set; }

        int mHealth;
        public int Health
        {
            get
            {
                return mHealth;
            }
            set
            {
                mHealth = value;
                if (mHealth <= 0)
                {
                    Destroy();
                }
             }
        }

        /// <summary>
        /// Initialization logic which is execute only one time for this Entity (unless the Entity is pooled).
        /// This method is called when the Entity is added to managers. Entities which are instantiated but not
        /// added to managers will not have this method called.
        /// </summary>
		private void CustomInitialize()
		{
            SetupController();

            Health = StartingHealth;

        }

		private void CustomActivity()
		{
            MovementActivity();

            TurningActivity();

            ShootingActivity();

		}

		private void CustomDestroy()
		{


		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        private void MovementActivity()
        {
            this.Velocity = this.RotationMatrix.Up * this.MovementSpeed;
        }

        private void TurningActivity()
        {
            if (mKeyboardInput == null)
            {
                this.RotationZVelocity = -mGamePad.LeftStick.Position.X * TurningSpeed;
            }
            else
            {
                this.RotationZVelocity = -mKeyboardInput.X * TurningSpeed;
            }
        }

        private void ShootingActivity()
        {
            bool keyBoardUtilizado = mShootingButton != null;
            bool controleUtilizado = mGamePad!= null;
            bool confirmacaoDeTiro = false;

            if (keyBoardUtilizado)
            {
                confirmacaoDeTiro = mShootingButton.WasJustPressed;
            }

            if (controleUtilizado)
            {
                confirmacaoDeTiro = mGamePad.ButtonPushed(Xbox360GamePad.Button.A);
            }

            if (confirmacaoDeTiro)
            {

                // We'll create 2 bullets because it looks much cooler than 1
                Bullet firstBullet = BulletFactory.CreateNew();
                firstBullet.Position = this.Position;
                firstBullet.Position += this.RotationMatrix.Up * 12;
                // This is the bullet on the right side when the ship is facing up.
                // Adding along the Right vector will move it to the right relative to the ship
                firstBullet.Position += this.RotationMatrix.Right * 6;
                firstBullet.RotationZ = this.RotationZ;
                firstBullet.Velocity = this.RotationMatrix.Up * firstBullet.MovementSpeed;

                Bullet secondBullet = BulletFactory.CreateNew();
                secondBullet.Position = this.Position;
                secondBullet.Position += this.RotationMatrix.Up * 12;
                // This bullet is moved along the Right vector, but in the nevative
                // direction, making it the bullet on the left.
                secondBullet.Position -= this.RotationMatrix.Right * 6;
                secondBullet.RotationZ = this.RotationZ;
                secondBullet.Velocity = this.RotationMatrix.Up * secondBullet.MovementSpeed;
            }
        }

        private void SetupController()
        {
            mGamePad = null;
            //System.Console.Write(mGamePad);
            //System.Diagnostics.Debug.WriteLine("pad: " + mGamePad == null);
            if (InputManager.Xbox360GamePads[0].IsConnected)
            {
                mGamePad = InputManager.Xbox360GamePads[0];
            }
            else
            { 
                mKeyboardInput = InputManager.Keyboard.Get2DInput(Keys.A, Keys.D, Keys.W, Keys.S);
                mShootingButton = InputManager.Keyboard.GetKey(Keys.Space);
            }

        }

	}
}
