#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using RockBlaster.Screens;
using FlatRedBall.Graphics;
using FlatRedBall.Math;
using RockBlaster.Performance;
using RockBlaster.Entities;
using RockBlaster.Factories;
using FlatRedBall;
using FlatRedBall.Screens;
using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall.Math.Geometry;
namespace RockBlaster.Entities
{
    public partial class Rock : FlatRedBall.PositionedObject, FlatRedBall.Graphics.IDestroyable, FlatRedBall.Performance.IPoolable
    {
        // This is made static so that static lazy-loaded content can access it.
        public static string ContentManagerName { get; set; }
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        public enum VariableState
        {
            Uninitialized = 0, //This exists so that the first set call actually does something
            Unknown = 1, //This exists so that if the entity is actually a child entity and has set a child state, you will get this
            Size1 = 2, 
            Size2 = 3, 
            Size3 = 4, 
            Size4 = 5
        }
        protected int mCurrentState = 0;
        public Entities.Rock.VariableState CurrentState
        {
            get
            {
                if (mCurrentState >= 0 && mCurrentState <= 5)
                {
                    return (VariableState)mCurrentState;
                }
                else
                {
                    return VariableState.Unknown;
                }
            }
            set
            {
                mCurrentState = (int)value;
                switch(CurrentState)
                {
                    case  VariableState.Uninitialized:
                        break;
                    case  VariableState.Unknown:
                        break;
                    case  VariableState.Size1:
                        SpriteRockTexture = Rock1;
                        CircleInstanceRadius = 6f;
                        break;
                    case  VariableState.Size2:
                        SpriteRockTexture = Rock2;
                        CircleInstanceRadius = 12f;
                        break;
                    case  VariableState.Size3:
                        SpriteRockTexture = Rock3;
                        CircleInstanceRadius = 20f;
                        break;
                    case  VariableState.Size4:
                        SpriteRockTexture = Rock4;
                        CircleInstanceRadius = 30f;
                        break;
                }
            }
        }
        static object mLockObject = new object();
        static System.Collections.Generic.List<string> mRegisteredUnloads = new System.Collections.Generic.List<string>();
        static System.Collections.Generic.List<string> LoadedContentManagers = new System.Collections.Generic.List<string>();
        protected static Microsoft.Xna.Framework.Graphics.Texture2D Rock1;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D Rock2;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D Rock3;
        protected static Microsoft.Xna.Framework.Graphics.Texture2D Rock4;
        
        private FlatRedBall.Sprite SpriteRock;
        private FlatRedBall.Math.Geometry.Circle mCircleInstance;
        public FlatRedBall.Math.Geometry.Circle CircleInstance
        {
            get
            {
                return mCircleInstance;
            }
            private set
            {
                mCircleInstance = value;
            }
        }
        public Microsoft.Xna.Framework.Graphics.Texture2D SpriteRockTexture
        {
            get
            {
                return SpriteRock.Texture;
            }
            set
            {
                SpriteRock.Texture = value;
            }
        }
        public float CircleInstanceRadius
        {
            get
            {
                return CircleInstance.Radius;
            }
            set
            {
                CircleInstance.Radius = value;
            }
        }
        public int NumberOfRocksToBreakInto = 2;
        public float RandomSpeedOnBreak = 50f;
        public int PointsWorth = 10;
        public int Index { get; set; }
        public bool Used { get; set; }
        protected FlatRedBall.Graphics.Layer LayerProvidedByContainer = null;
        public Rock () 
        	: this(FlatRedBall.Screens.ScreenManager.CurrentScreen.ContentManagerName, true)
        {
        }
        public Rock (string contentManagerName) 
        	: this(contentManagerName, true)
        {
        }
        public Rock (string contentManagerName, bool addToManagers) 
        	: base()
        {
            ContentManagerName = contentManagerName;
            InitializeEntity(addToManagers);
        }
        protected virtual void InitializeEntity (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            SpriteRock = new FlatRedBall.Sprite();
            SpriteRock.Name = "SpriteRock";
            mCircleInstance = new FlatRedBall.Math.Geometry.Circle();
            mCircleInstance.Name = "mCircleInstance";
            
            PostInitialize();
            if (addToManagers)
            {
                AddToManagers(null);
            }
        }
        public virtual void ReAddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.SpriteManager.AddToLayer(SpriteRock, LayerProvidedByContainer);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mCircleInstance, LayerProvidedByContainer);
        }
        public virtual void AddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.SpriteManager.AddToLayer(SpriteRock, LayerProvidedByContainer);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mCircleInstance, LayerProvidedByContainer);
            AddToManagersBottomUp(layerToAddTo);
            CustomInitialize();
        }
        public virtual void Activity () 
        {
            
            CustomActivity();
        }
        public virtual void Destroy () 
        {
            if (Used)
            {
                Factories.RockFactory.MakeUnused(this, false);
            }
            FlatRedBall.SpriteManager.RemovePositionedObject(this);
            
            if (SpriteRock != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(SpriteRock);
            }
            if (CircleInstance != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.RemoveOneWay(CircleInstance);
            }
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            if (SpriteRock.Parent == null)
            {
                SpriteRock.CopyAbsoluteToRelative();
                SpriteRock.AttachTo(this, false);
            }
            SpriteRock.Texture = Rock1;
            SpriteRock.TextureScale = 1f;
            if (mCircleInstance.Parent == null)
            {
                mCircleInstance.CopyAbsoluteToRelative();
                mCircleInstance.AttachTo(this, false);
            }
            CircleInstance.Radius = 6f;
            CircleInstance.Visible = false;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            if (SpriteRock != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(SpriteRock);
            }
            if (CircleInstance != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.RemoveOneWay(CircleInstance);
            }
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
            }
            SpriteRock.Texture = Rock1;
            SpriteRock.TextureScale = 1f;
            CircleInstance.Radius = 6f;
            CircleInstance.Visible = false;
            CircleInstanceRadius = 6f;
            NumberOfRocksToBreakInto = 2;
            RandomSpeedOnBreak = 50f;
            PointsWorth = 10;
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            this.ForceUpdateDependenciesDeep();
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(SpriteRock);
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            ContentManagerName = contentManagerName;
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            bool registerUnload = false;
            if (LoadedContentManagers.Contains(contentManagerName) == false)
            {
                LoadedContentManagers.Add(contentManagerName);
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("RockStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/rock/rock1.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                Rock1 = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/rock/rock1.png", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/rock/rock2.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                Rock2 = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/rock/rock2.png", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/rock/rock3.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                Rock3 = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/rock/rock3.png", ContentManagerName);
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/rock/rock4.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                Rock4 = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/rock/rock4.png", ContentManagerName);
            }
            if (registerUnload && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("RockStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            CustomLoadStaticContent(contentManagerName);
        }
        public static void UnloadStaticContent () 
        {
            if (LoadedContentManagers.Count != 0)
            {
                LoadedContentManagers.RemoveAt(0);
                mRegisteredUnloads.RemoveAt(0);
            }
            if (LoadedContentManagers.Count == 0)
            {
                if (Rock1 != null)
                {
                    Rock1= null;
                }
                if (Rock2 != null)
                {
                    Rock2= null;
                }
                if (Rock3 != null)
                {
                    Rock3= null;
                }
                if (Rock4 != null)
                {
                    Rock4= null;
                }
            }
        }
        static VariableState mLoadingState = VariableState.Uninitialized;
        public static VariableState LoadingState
        {
            get
            {
                return mLoadingState;
            }
            set
            {
                mLoadingState = value;
            }
        }
        public FlatRedBall.Instructions.Instruction InterpolateToState (VariableState stateToInterpolateTo, double secondsToTake) 
        {
            switch(stateToInterpolateTo)
            {
                case  VariableState.Size1:
                    CircleInstance.RadiusVelocity = (6f - CircleInstance.Radius) / (float)secondsToTake;
                    break;
                case  VariableState.Size2:
                    CircleInstance.RadiusVelocity = (12f - CircleInstance.Radius) / (float)secondsToTake;
                    break;
                case  VariableState.Size3:
                    CircleInstance.RadiusVelocity = (20f - CircleInstance.Radius) / (float)secondsToTake;
                    break;
                case  VariableState.Size4:
                    CircleInstance.RadiusVelocity = (30f - CircleInstance.Radius) / (float)secondsToTake;
                    break;
            }
            var instruction = new FlatRedBall.Instructions.DelegateInstruction<VariableState>(StopStateInterpolation, stateToInterpolateTo);
            instruction.TimeToExecute = FlatRedBall.TimeManager.CurrentTime + secondsToTake;
            this.Instructions.Add(instruction);
            return instruction;
        }
        public void StopStateInterpolation (VariableState stateToStop) 
        {
            switch(stateToStop)
            {
                case  VariableState.Size1:
                    CircleInstance.RadiusVelocity =  0;
                    break;
                case  VariableState.Size2:
                    CircleInstance.RadiusVelocity =  0;
                    break;
                case  VariableState.Size3:
                    CircleInstance.RadiusVelocity =  0;
                    break;
                case  VariableState.Size4:
                    CircleInstance.RadiusVelocity =  0;
                    break;
            }
            CurrentState = stateToStop;
        }
        public void InterpolateBetween (VariableState firstState, VariableState secondState, float interpolationValue) 
        {
            #if DEBUG
            if (float.IsNaN(interpolationValue))
            {
                throw new System.Exception("interpolationValue cannot be NaN");
            }
            #endif
            bool setCircleInstanceRadius = true;
            float CircleInstanceRadiusFirstValue= 0;
            float CircleInstanceRadiusSecondValue= 0;
            switch(firstState)
            {
                case  VariableState.Size1:
                    if (interpolationValue < 1)
                    {
                        this.SpriteRockTexture = Rock1;
                    }
                    CircleInstanceRadiusFirstValue = 6f;
                    break;
                case  VariableState.Size2:
                    if (interpolationValue < 1)
                    {
                        this.SpriteRockTexture = Rock2;
                    }
                    CircleInstanceRadiusFirstValue = 12f;
                    break;
                case  VariableState.Size3:
                    if (interpolationValue < 1)
                    {
                        this.SpriteRockTexture = Rock3;
                    }
                    CircleInstanceRadiusFirstValue = 20f;
                    break;
                case  VariableState.Size4:
                    if (interpolationValue < 1)
                    {
                        this.SpriteRockTexture = Rock4;
                    }
                    CircleInstanceRadiusFirstValue = 30f;
                    break;
            }
            switch(secondState)
            {
                case  VariableState.Size1:
                    if (interpolationValue >= 1)
                    {
                        this.SpriteRockTexture = Rock1;
                    }
                    CircleInstanceRadiusSecondValue = 6f;
                    break;
                case  VariableState.Size2:
                    if (interpolationValue >= 1)
                    {
                        this.SpriteRockTexture = Rock2;
                    }
                    CircleInstanceRadiusSecondValue = 12f;
                    break;
                case  VariableState.Size3:
                    if (interpolationValue >= 1)
                    {
                        this.SpriteRockTexture = Rock3;
                    }
                    CircleInstanceRadiusSecondValue = 20f;
                    break;
                case  VariableState.Size4:
                    if (interpolationValue >= 1)
                    {
                        this.SpriteRockTexture = Rock4;
                    }
                    CircleInstanceRadiusSecondValue = 30f;
                    break;
            }
            if (setCircleInstanceRadius)
            {
                CircleInstanceRadius = CircleInstanceRadiusFirstValue * (1 - interpolationValue) + CircleInstanceRadiusSecondValue * interpolationValue;
            }
            if (interpolationValue < 1)
            {
                mCurrentState = (int)firstState;
            }
            else
            {
                mCurrentState = (int)secondState;
            }
        }
        public static void PreloadStateContent (VariableState state, string contentManagerName) 
        {
            ContentManagerName = contentManagerName;
            switch(state)
            {
                case  VariableState.Size1:
                    {
                        object throwaway = Rock1;
                    }
                    break;
                case  VariableState.Size2:
                    {
                        object throwaway = Rock2;
                    }
                    break;
                case  VariableState.Size3:
                    {
                        object throwaway = Rock3;
                    }
                    break;
                case  VariableState.Size4:
                    {
                        object throwaway = Rock4;
                    }
                    break;
            }
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            switch(memberName)
            {
                case  "Rock1":
                    return Rock1;
                case  "Rock2":
                    return Rock2;
                case  "Rock3":
                    return Rock3;
                case  "Rock4":
                    return Rock4;
            }
            return null;
        }
        public static object GetFile (string memberName) 
        {
            switch(memberName)
            {
                case  "Rock1":
                    return Rock1;
                case  "Rock2":
                    return Rock2;
                case  "Rock3":
                    return Rock3;
                case  "Rock4":
                    return Rock4;
            }
            return null;
        }
        object GetMember (string memberName) 
        {
            switch(memberName)
            {
                case  "Rock1":
                    return Rock1;
                case  "Rock2":
                    return Rock2;
                case  "Rock3":
                    return Rock3;
                case  "Rock4":
                    return Rock4;
            }
            return null;
        }
        protected bool mIsPaused;
        public override void Pause (FlatRedBall.Instructions.InstructionList instructions) 
        {
            base.Pause(instructions);
            mIsPaused = true;
        }
        public virtual void SetToIgnorePausing () 
        {
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(this);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(SpriteRock);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(CircleInstance);
        }
        public virtual void MoveToLayer (FlatRedBall.Graphics.Layer layerToMoveTo) 
        {
            var layerToRemoveFrom = LayerProvidedByContainer;
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(SpriteRock);
            }
            FlatRedBall.SpriteManager.AddToLayer(SpriteRock, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(CircleInstance);
            }
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(CircleInstance, layerToMoveTo);
            LayerProvidedByContainer = layerToMoveTo;
        }
    }
}
