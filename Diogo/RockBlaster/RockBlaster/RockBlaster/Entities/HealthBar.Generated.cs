#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using RockBlaster.Screens;
using FlatRedBall.Graphics;
using FlatRedBall.Math;
using RockBlaster.Entities;
using RockBlaster.Factories;
using FlatRedBall;
using FlatRedBall.Screens;
using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall.ManagedSpriteGroups;
namespace RockBlaster.Entities
{
    public partial class HealthBar : FlatRedBall.PositionedObject, FlatRedBall.Graphics.IDestroyable
    {
        // This is made static so that static lazy-loaded content can access it.
        public static string ContentManagerName { get; set; }
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        public enum VariableState
        {
            Uninitialized = 0, //This exists so that the first set call actually does something
            Unknown = 1, //This exists so that if the entity is actually a child entity and has set a child state, you will get this
            Full = 2, 
            Empty = 3
        }
        protected int mCurrentState = 0;
        public Entities.HealthBar.VariableState CurrentState
        {
            get
            {
                if (mCurrentState >= 0 && mCurrentState <= 3)
                {
                    return (VariableState)mCurrentState;
                }
                else
                {
                    return VariableState.Unknown;
                }
            }
            set
            {
                mCurrentState = (int)value;
                switch(CurrentState)
                {
                    case  VariableState.Uninitialized:
                        break;
                    case  VariableState.Unknown:
                        break;
                    case  VariableState.Full:
                        BarSpriteScaleX = 22f;
                        if (BarSprite.Parent == null)
                        {
                            BarSpriteX = 0f;
                        }
                        else
                        {
                            BarSprite.RelativeX = 0f;
                        }
                        break;
                    case  VariableState.Empty:
                        BarSpriteScaleX = 0f;
                        if (BarSprite.Parent == null)
                        {
                            BarSpriteX = -22f;
                        }
                        else
                        {
                            BarSprite.RelativeX = -22f;
                        }
                        break;
                }
            }
        }
        static object mLockObject = new object();
        static System.Collections.Generic.List<string> mRegisteredUnloads = new System.Collections.Generic.List<string>();
        static System.Collections.Generic.List<string> LoadedContentManagers = new System.Collections.Generic.List<string>();
        
        private FlatRedBall.ManagedSpriteGroups.SpriteFrame FrameSprite;
        private FlatRedBall.ManagedSpriteGroups.SpriteFrame BarSprite;
        public float FrameSpriteRed
        {
            get
            {
                return FrameSprite.Red;
            }
            set
            {
                FrameSprite.Red = value;
            }
        }
        public float FrameSpriteGreen
        {
            get
            {
                return FrameSprite.Green;
            }
            set
            {
                FrameSprite.Green = value;
            }
        }
        public float FrameSpriteBlue
        {
            get
            {
                return FrameSprite.Blue;
            }
            set
            {
                FrameSprite.Blue = value;
            }
        }
        public float BarSpriteRed
        {
            get
            {
                return BarSprite.Red;
            }
            set
            {
                BarSprite.Red = value;
            }
        }
        public float BarSpriteGreen
        {
            get
            {
                return BarSprite.Green;
            }
            set
            {
                BarSprite.Green = value;
            }
        }
        public float BarSpriteBlue
        {
            get
            {
                return BarSprite.Blue;
            }
            set
            {
                BarSprite.Blue = value;
            }
        }
        public float BarSpriteScaleX
        {
            get
            {
                return BarSprite.ScaleX;
            }
            set
            {
                BarSprite.ScaleX = value;
            }
        }
        public float BarSpriteX
        {
            get
            {
                if (BarSprite.Parent == null)
                {
                    return BarSprite.X;
                }
                else
                {
                    return BarSprite.RelativeX;
                }
            }
            set
            {
                if (BarSprite.Parent == null)
                {
                    BarSprite.X = value;
                }
                else
                {
                    BarSprite.RelativeX = value;
                }
            }
        }
        protected FlatRedBall.Graphics.Layer LayerProvidedByContainer = null;
        public HealthBar () 
        	: this(FlatRedBall.Screens.ScreenManager.CurrentScreen.ContentManagerName, true)
        {
        }
        public HealthBar (string contentManagerName) 
        	: this(contentManagerName, true)
        {
        }
        public HealthBar (string contentManagerName, bool addToManagers) 
        	: base()
        {
            ContentManagerName = contentManagerName;
            InitializeEntity(addToManagers);
        }
        protected virtual void InitializeEntity (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            FrameSprite = new FlatRedBall.ManagedSpriteGroups.SpriteFrame();
            FrameSprite.Name = "FrameSprite";
            BarSprite = new FlatRedBall.ManagedSpriteGroups.SpriteFrame();
            BarSprite.Name = "BarSprite";
            
            PostInitialize();
            if (addToManagers)
            {
                AddToManagers(null);
            }
        }
        public virtual void ReAddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.SpriteManager.AddToLayer(FrameSprite, LayerProvidedByContainer);
            FlatRedBall.SpriteManager.AddToLayer(BarSprite, LayerProvidedByContainer);
        }
        public virtual void AddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.SpriteManager.AddToLayer(FrameSprite, LayerProvidedByContainer);
            FlatRedBall.SpriteManager.AddToLayer(BarSprite, LayerProvidedByContainer);
            AddToManagersBottomUp(layerToAddTo);
            CustomInitialize();
        }
        public virtual void Activity () 
        {
            
            CustomActivity();
        }
        public virtual void Destroy () 
        {
            FlatRedBall.SpriteManager.RemovePositionedObject(this);
            
            if (FrameSprite != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteFrame(FrameSprite);
            }
            if (BarSprite != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteFrame(BarSprite);
            }
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            if (FrameSprite.Parent == null)
            {
                FrameSprite.CopyAbsoluteToRelative();
                FrameSprite.AttachTo(this, false);
            }
            FrameSprite.PixelSize = 0.5f;
            FrameSprite.ScaleX = 24f;
            FrameSprite.ScaleY = 6f;
            if (BarSprite.Parent == null)
            {
                BarSprite.CopyAbsoluteToRelative();
                BarSprite.AttachTo(this, false);
            }
            BarSprite.PixelSize = 0.5f;
            BarSprite.ScaleX = 22f;
            BarSprite.ScaleY = 4f;
            if (BarSprite.Parent == null)
            {
                BarSprite.Z = 1f;
            }
            else
            {
                BarSprite.RelativeZ = 1f;
            }
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            if (FrameSprite != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteFrame(FrameSprite);
            }
            if (BarSprite != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteFrame(BarSprite);
            }
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
            }
            FrameSprite.PixelSize = 0.5f;
            FrameSprite.ScaleX = 24f;
            FrameSprite.ScaleY = 6f;
            BarSprite.PixelSize = 0.5f;
            BarSprite.ScaleX = 22f;
            BarSprite.ScaleY = 4f;
            if (BarSprite.Parent == null)
            {
                BarSprite.Z = 1f;
            }
            else
            {
                BarSprite.RelativeZ = 1f;
            }
            FrameSpriteRed = 1f;
            FrameSpriteGreen = 1f;
            FrameSpriteBlue = 0f;
            BarSpriteRed = 0f;
            BarSpriteGreen = 1f;
            BarSpriteBlue = 0f;
            BarSpriteScaleX = 22f;
            BarSpriteX = 0f;
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            this.ForceUpdateDependenciesDeep();
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(FrameSprite);
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(BarSprite);
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            ContentManagerName = contentManagerName;
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            bool registerUnload = false;
            if (LoadedContentManagers.Contains(contentManagerName) == false)
            {
                LoadedContentManagers.Add(contentManagerName);
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("HealthBarStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            if (registerUnload && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("HealthBarStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            CustomLoadStaticContent(contentManagerName);
        }
        public static void UnloadStaticContent () 
        {
            if (LoadedContentManagers.Count != 0)
            {
                LoadedContentManagers.RemoveAt(0);
                mRegisteredUnloads.RemoveAt(0);
            }
            if (LoadedContentManagers.Count == 0)
            {
            }
        }
        static VariableState mLoadingState = VariableState.Uninitialized;
        public static VariableState LoadingState
        {
            get
            {
                return mLoadingState;
            }
            set
            {
                mLoadingState = value;
            }
        }
        public FlatRedBall.Instructions.Instruction InterpolateToState (VariableState stateToInterpolateTo, double secondsToTake) 
        {
            switch(stateToInterpolateTo)
            {
                case  VariableState.Full:
                    BarSprite.ScaleXVelocity = (22f - BarSprite.ScaleX) / (float)secondsToTake;
                    if (BarSprite.Parent != null)
                    {
                        BarSprite.RelativeXVelocity = (0f - BarSprite.RelativeX) / (float)secondsToTake;
                    }
                    else
                    {
                        BarSprite.XVelocity = (0f - BarSprite.X) / (float)secondsToTake;
                    }
                    break;
                case  VariableState.Empty:
                    BarSprite.ScaleXVelocity = (0f - BarSprite.ScaleX) / (float)secondsToTake;
                    if (BarSprite.Parent != null)
                    {
                        BarSprite.RelativeXVelocity = (-22f - BarSprite.RelativeX) / (float)secondsToTake;
                    }
                    else
                    {
                        BarSprite.XVelocity = (-22f - BarSprite.X) / (float)secondsToTake;
                    }
                    break;
            }
            var instruction = new FlatRedBall.Instructions.DelegateInstruction<VariableState>(StopStateInterpolation, stateToInterpolateTo);
            instruction.TimeToExecute = FlatRedBall.TimeManager.CurrentTime + secondsToTake;
            this.Instructions.Add(instruction);
            return instruction;
        }
        public void StopStateInterpolation (VariableState stateToStop) 
        {
            switch(stateToStop)
            {
                case  VariableState.Full:
                    BarSprite.ScaleXVelocity =  0;
                    if (BarSprite.Parent != null)
                    {
                        BarSprite.RelativeXVelocity =  0;
                    }
                    else
                    {
                        BarSprite.XVelocity =  0;
                    }
                    break;
                case  VariableState.Empty:
                    BarSprite.ScaleXVelocity =  0;
                    if (BarSprite.Parent != null)
                    {
                        BarSprite.RelativeXVelocity =  0;
                    }
                    else
                    {
                        BarSprite.XVelocity =  0;
                    }
                    break;
            }
            CurrentState = stateToStop;
        }
        public void InterpolateBetween (VariableState firstState, VariableState secondState, float interpolationValue) 
        {
            #if DEBUG
            if (float.IsNaN(interpolationValue))
            {
                throw new System.Exception("interpolationValue cannot be NaN");
            }
            #endif
            bool setBarSpriteScaleX = true;
            float BarSpriteScaleXFirstValue= 0;
            float BarSpriteScaleXSecondValue= 0;
            bool setBarSpriteX = true;
            float BarSpriteXFirstValue= 0;
            float BarSpriteXSecondValue= 0;
            switch(firstState)
            {
                case  VariableState.Full:
                    BarSpriteScaleXFirstValue = 22f;
                    BarSpriteXFirstValue = 0f;
                    break;
                case  VariableState.Empty:
                    BarSpriteScaleXFirstValue = 0f;
                    BarSpriteXFirstValue = -22f;
                    break;
            }
            switch(secondState)
            {
                case  VariableState.Full:
                    BarSpriteScaleXSecondValue = 22f;
                    BarSpriteXSecondValue = 0f;
                    break;
                case  VariableState.Empty:
                    BarSpriteScaleXSecondValue = 0f;
                    BarSpriteXSecondValue = -22f;
                    break;
            }
            if (setBarSpriteScaleX)
            {
                BarSpriteScaleX = BarSpriteScaleXFirstValue * (1 - interpolationValue) + BarSpriteScaleXSecondValue * interpolationValue;
            }
            if (setBarSpriteX)
            {
                if (BarSprite.Parent != null)
                {
                    BarSprite.RelativeX = BarSpriteXFirstValue * (1 - interpolationValue) + BarSpriteXSecondValue * interpolationValue;
                }
                else
                {
                    BarSpriteX = BarSpriteXFirstValue * (1 - interpolationValue) + BarSpriteXSecondValue * interpolationValue;
                }
                if (interpolationValue < 1)
                {
                    mCurrentState = (int)firstState;
                }
                else
                {
                    mCurrentState = (int)secondState;
                }
            }
        }
        public static void PreloadStateContent (VariableState state, string contentManagerName) 
        {
            ContentManagerName = contentManagerName;
            switch(state)
            {
                case  VariableState.Full:
                    break;
                case  VariableState.Empty:
                    break;
            }
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            return null;
        }
        public static object GetFile (string memberName) 
        {
            return null;
        }
        object GetMember (string memberName) 
        {
            return null;
        }
        protected bool mIsPaused;
        public override void Pause (FlatRedBall.Instructions.InstructionList instructions) 
        {
            base.Pause(instructions);
            mIsPaused = true;
        }
        public virtual void SetToIgnorePausing () 
        {
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(this);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(FrameSprite);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(BarSprite);
        }
        public virtual void MoveToLayer (FlatRedBall.Graphics.Layer layerToMoveTo) 
        {
            var layerToRemoveFrom = LayerProvidedByContainer;
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(FrameSprite);
            }
            FlatRedBall.SpriteManager.AddToLayer(FrameSprite, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(BarSprite);
            }
            FlatRedBall.SpriteManager.AddToLayer(BarSprite, layerToMoveTo);
            LayerProvidedByContainer = layerToMoveTo;
        }
    }
}
