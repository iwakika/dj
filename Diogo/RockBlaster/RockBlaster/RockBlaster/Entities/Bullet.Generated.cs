#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using RockBlaster.Screens;
using FlatRedBall.Graphics;
using FlatRedBall.Math;
using RockBlaster.Performance;
using RockBlaster.Entities;
using RockBlaster.Factories;
using FlatRedBall;
using FlatRedBall.Screens;
using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall.Math.Geometry;
namespace RockBlaster.Entities
{
    public partial class Bullet : FlatRedBall.PositionedObject, FlatRedBall.Graphics.IDestroyable, FlatRedBall.Performance.IPoolable
    {
        // This is made static so that static lazy-loaded content can access it.
        public static string ContentManagerName { get; set; }
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        static object mLockObject = new object();
        static System.Collections.Generic.List<string> mRegisteredUnloads = new System.Collections.Generic.List<string>();
        static System.Collections.Generic.List<string> LoadedContentManagers = new System.Collections.Generic.List<string>();
        protected static Microsoft.Xna.Framework.Graphics.Texture2D Bullet1;
        
        private FlatRedBall.Sprite SpriteBullet1;
        static float SpriteBullet1XReset;
        static float SpriteBullet1YReset;
        static float SpriteBullet1ZReset;
        static float SpriteBullet1XVelocityReset;
        static float SpriteBullet1YVelocityReset;
        static float SpriteBullet1ZVelocityReset;
        static float SpriteBullet1RotationXReset;
        static float SpriteBullet1RotationYReset;
        static float SpriteBullet1RotationZReset;
        static float SpriteBullet1RotationXVelocityReset;
        static float SpriteBullet1RotationYVelocityReset;
        static float SpriteBullet1RotationZVelocityReset;
        static float SpriteBullet1AlphaReset;
        static float SpriteBullet1AlphaRateReset;
        private FlatRedBall.Math.Geometry.Circle mCircleInstance;
        public FlatRedBall.Math.Geometry.Circle CircleInstance
        {
            get
            {
                return mCircleInstance;
            }
            private set
            {
                mCircleInstance = value;
            }
        }
        public float MovementSpeed = 300f;
        public int Index { get; set; }
        public bool Used { get; set; }
        protected FlatRedBall.Graphics.Layer LayerProvidedByContainer = null;
        public Bullet () 
        	: this(FlatRedBall.Screens.ScreenManager.CurrentScreen.ContentManagerName, true)
        {
        }
        public Bullet (string contentManagerName) 
        	: this(contentManagerName, true)
        {
        }
        public Bullet (string contentManagerName, bool addToManagers) 
        	: base()
        {
            ContentManagerName = contentManagerName;
            InitializeEntity(addToManagers);
        }
        protected virtual void InitializeEntity (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            SpriteBullet1 = new FlatRedBall.Sprite();
            SpriteBullet1.Name = "SpriteBullet1";
            mCircleInstance = new FlatRedBall.Math.Geometry.Circle();
            mCircleInstance.Name = "mCircleInstance";
            
            PostInitialize();
            if (SpriteBullet1.Parent == null)
            {
                SpriteBullet1XReset = SpriteBullet1.X;
            }
            else
            {
                SpriteBullet1XReset = SpriteBullet1.RelativeX;
            }
            if (SpriteBullet1.Parent == null)
            {
                SpriteBullet1YReset = SpriteBullet1.Y;
            }
            else
            {
                SpriteBullet1YReset = SpriteBullet1.RelativeY;
            }
            if (SpriteBullet1.Parent == null)
            {
                SpriteBullet1ZReset = SpriteBullet1.Z;
            }
            else
            {
                SpriteBullet1ZReset = SpriteBullet1.RelativeZ;
            }
            if (SpriteBullet1.Parent == null)
            {
                SpriteBullet1XVelocityReset = SpriteBullet1.XVelocity;
            }
            else
            {
                SpriteBullet1XVelocityReset = SpriteBullet1.RelativeXVelocity;
            }
            if (SpriteBullet1.Parent == null)
            {
                SpriteBullet1YVelocityReset = SpriteBullet1.YVelocity;
            }
            else
            {
                SpriteBullet1YVelocityReset = SpriteBullet1.RelativeYVelocity;
            }
            if (SpriteBullet1.Parent == null)
            {
                SpriteBullet1ZVelocityReset = SpriteBullet1.ZVelocity;
            }
            else
            {
                SpriteBullet1ZVelocityReset = SpriteBullet1.RelativeZVelocity;
            }
            if (SpriteBullet1.Parent == null)
            {
                SpriteBullet1RotationXReset = SpriteBullet1.RotationX;
            }
            else
            {
                SpriteBullet1RotationXReset = SpriteBullet1.RelativeRotationX;
            }
            if (SpriteBullet1.Parent == null)
            {
                SpriteBullet1RotationYReset = SpriteBullet1.RotationY;
            }
            else
            {
                SpriteBullet1RotationYReset = SpriteBullet1.RelativeRotationY;
            }
            if (SpriteBullet1.Parent == null)
            {
                SpriteBullet1RotationZReset = SpriteBullet1.RotationZ;
            }
            else
            {
                SpriteBullet1RotationZReset = SpriteBullet1.RelativeRotationZ;
            }
            if (SpriteBullet1.Parent == null)
            {
                SpriteBullet1RotationXVelocityReset = SpriteBullet1.RotationXVelocity;
            }
            else
            {
                SpriteBullet1RotationXVelocityReset = SpriteBullet1.RelativeRotationXVelocity;
            }
            if (SpriteBullet1.Parent == null)
            {
                SpriteBullet1RotationYVelocityReset = SpriteBullet1.RotationYVelocity;
            }
            else
            {
                SpriteBullet1RotationYVelocityReset = SpriteBullet1.RelativeRotationYVelocity;
            }
            if (SpriteBullet1.Parent == null)
            {
                SpriteBullet1RotationZVelocityReset = SpriteBullet1.RotationZVelocity;
            }
            else
            {
                SpriteBullet1RotationZVelocityReset = SpriteBullet1.RelativeRotationZVelocity;
            }
            SpriteBullet1AlphaReset = SpriteBullet1.Alpha;
            SpriteBullet1AlphaRateReset = SpriteBullet1.AlphaRate;
            if (addToManagers)
            {
                AddToManagers(null);
            }
        }
        public virtual void ReAddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.SpriteManager.AddToLayer(SpriteBullet1, LayerProvidedByContainer);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mCircleInstance, LayerProvidedByContainer);
        }
        public virtual void AddToManagers (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            LayerProvidedByContainer = layerToAddTo;
            FlatRedBall.SpriteManager.AddPositionedObject(this);
            FlatRedBall.SpriteManager.AddToLayer(SpriteBullet1, LayerProvidedByContainer);
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(mCircleInstance, LayerProvidedByContainer);
            AddToManagersBottomUp(layerToAddTo);
            CustomInitialize();
        }
        public virtual void Activity () 
        {
            
            CustomActivity();
        }
        public virtual void Destroy () 
        {
            if (Used)
            {
                Factories.BulletFactory.MakeUnused(this, false);
            }
            FlatRedBall.SpriteManager.RemovePositionedObject(this);
            
            if (SpriteBullet1 != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(SpriteBullet1);
            }
            if (CircleInstance != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.RemoveOneWay(CircleInstance);
            }
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            if (SpriteBullet1.Parent == null)
            {
                SpriteBullet1.CopyAbsoluteToRelative();
                SpriteBullet1.AttachTo(this, false);
            }
            SpriteBullet1.Texture = Bullet1;
            SpriteBullet1.TextureScale = 0.5f;
            if (mCircleInstance.Parent == null)
            {
                mCircleInstance.CopyAbsoluteToRelative();
                mCircleInstance.AttachTo(this, false);
            }
            if (CircleInstance.Parent == null)
            {
                CircleInstance.X = 0.5f;
            }
            else
            {
                CircleInstance.RelativeX = 0.5f;
            }
            if (CircleInstance.Parent == null)
            {
                CircleInstance.Y = 5f;
            }
            else
            {
                CircleInstance.RelativeY = 5f;
            }
            CircleInstance.Radius = 3f;
            CircleInstance.Visible = false;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp (FlatRedBall.Graphics.Layer layerToAddTo) 
        {
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            if (SpriteBullet1 != null)
            {
                FlatRedBall.SpriteManager.RemoveSpriteOneWay(SpriteBullet1);
            }
            if (CircleInstance != null)
            {
                FlatRedBall.Math.Geometry.ShapeManager.RemoveOneWay(CircleInstance);
            }
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
            }
            SpriteBullet1.Texture = Bullet1;
            SpriteBullet1.TextureScale = 0.5f;
            if (SpriteBullet1.Parent == null)
            {
                SpriteBullet1.X = SpriteBullet1XReset;
            }
            else
            {
                SpriteBullet1.RelativeX = SpriteBullet1XReset;
            }
            if (SpriteBullet1.Parent == null)
            {
                SpriteBullet1.Y = SpriteBullet1YReset;
            }
            else
            {
                SpriteBullet1.RelativeY = SpriteBullet1YReset;
            }
            if (SpriteBullet1.Parent == null)
            {
                SpriteBullet1.Z = SpriteBullet1ZReset;
            }
            else
            {
                SpriteBullet1.RelativeZ = SpriteBullet1ZReset;
            }
            if (SpriteBullet1.Parent == null)
            {
                SpriteBullet1.XVelocity = SpriteBullet1XVelocityReset;
            }
            else
            {
                SpriteBullet1.RelativeXVelocity = SpriteBullet1XVelocityReset;
            }
            if (SpriteBullet1.Parent == null)
            {
                SpriteBullet1.YVelocity = SpriteBullet1YVelocityReset;
            }
            else
            {
                SpriteBullet1.RelativeYVelocity = SpriteBullet1YVelocityReset;
            }
            if (SpriteBullet1.Parent == null)
            {
                SpriteBullet1.ZVelocity = SpriteBullet1ZVelocityReset;
            }
            else
            {
                SpriteBullet1.RelativeZVelocity = SpriteBullet1ZVelocityReset;
            }
            if (SpriteBullet1.Parent == null)
            {
                SpriteBullet1.RotationX = SpriteBullet1RotationXReset;
            }
            else
            {
                SpriteBullet1.RelativeRotationX = SpriteBullet1RotationXReset;
            }
            if (SpriteBullet1.Parent == null)
            {
                SpriteBullet1.RotationY = SpriteBullet1RotationYReset;
            }
            else
            {
                SpriteBullet1.RelativeRotationY = SpriteBullet1RotationYReset;
            }
            if (SpriteBullet1.Parent == null)
            {
                SpriteBullet1.RotationZ = SpriteBullet1RotationZReset;
            }
            else
            {
                SpriteBullet1.RelativeRotationZ = SpriteBullet1RotationZReset;
            }
            if (SpriteBullet1.Parent == null)
            {
                SpriteBullet1.RotationXVelocity = SpriteBullet1RotationXVelocityReset;
            }
            else
            {
                SpriteBullet1.RelativeRotationXVelocity = SpriteBullet1RotationXVelocityReset;
            }
            if (SpriteBullet1.Parent == null)
            {
                SpriteBullet1.RotationYVelocity = SpriteBullet1RotationYVelocityReset;
            }
            else
            {
                SpriteBullet1.RelativeRotationYVelocity = SpriteBullet1RotationYVelocityReset;
            }
            if (SpriteBullet1.Parent == null)
            {
                SpriteBullet1.RotationZVelocity = SpriteBullet1RotationZVelocityReset;
            }
            else
            {
                SpriteBullet1.RelativeRotationZVelocity = SpriteBullet1RotationZVelocityReset;
            }
            SpriteBullet1.Alpha = SpriteBullet1AlphaReset;
            SpriteBullet1.AlphaRate = SpriteBullet1AlphaRateReset;
            if (CircleInstance.Parent == null)
            {
                CircleInstance.X = 0.5f;
            }
            else
            {
                CircleInstance.RelativeX = 0.5f;
            }
            if (CircleInstance.Parent == null)
            {
                CircleInstance.Y = 5f;
            }
            else
            {
                CircleInstance.RelativeY = 5f;
            }
            CircleInstance.Radius = 3f;
            CircleInstance.Visible = false;
            MovementSpeed = 300f;
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            this.ForceUpdateDependenciesDeep();
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(this);
            FlatRedBall.SpriteManager.ConvertToManuallyUpdated(SpriteBullet1);
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            ContentManagerName = contentManagerName;
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            bool registerUnload = false;
            if (LoadedContentManagers.Contains(contentManagerName) == false)
            {
                LoadedContentManagers.Add(contentManagerName);
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("BulletStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
                if (!FlatRedBall.FlatRedBallServices.IsLoaded<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/bullet/bullet1.png", ContentManagerName))
                {
                    registerUnload = true;
                }
                Bullet1 = FlatRedBall.FlatRedBallServices.Load<Microsoft.Xna.Framework.Graphics.Texture2D>(@"content/entities/bullet/bullet1.png", ContentManagerName);
            }
            if (registerUnload && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                lock (mLockObject)
                {
                    if (!mRegisteredUnloads.Contains(ContentManagerName) && ContentManagerName != FlatRedBall.FlatRedBallServices.GlobalContentManager)
                    {
                        FlatRedBall.FlatRedBallServices.GetContentManagerByName(ContentManagerName).AddUnloadMethod("BulletStaticUnload", UnloadStaticContent);
                        mRegisteredUnloads.Add(ContentManagerName);
                    }
                }
            }
            CustomLoadStaticContent(contentManagerName);
        }
        public static void UnloadStaticContent () 
        {
            if (LoadedContentManagers.Count != 0)
            {
                LoadedContentManagers.RemoveAt(0);
                mRegisteredUnloads.RemoveAt(0);
            }
            if (LoadedContentManagers.Count == 0)
            {
                if (Bullet1 != null)
                {
                    Bullet1= null;
                }
            }
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            switch(memberName)
            {
                case  "Bullet1":
                    return Bullet1;
            }
            return null;
        }
        public static object GetFile (string memberName) 
        {
            switch(memberName)
            {
                case  "Bullet1":
                    return Bullet1;
            }
            return null;
        }
        object GetMember (string memberName) 
        {
            switch(memberName)
            {
                case  "Bullet1":
                    return Bullet1;
            }
            return null;
        }
        protected bool mIsPaused;
        public override void Pause (FlatRedBall.Instructions.InstructionList instructions) 
        {
            base.Pause(instructions);
            mIsPaused = true;
        }
        public virtual void SetToIgnorePausing () 
        {
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(this);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(SpriteBullet1);
            FlatRedBall.Instructions.InstructionManager.IgnorePausingFor(CircleInstance);
        }
        public virtual void MoveToLayer (FlatRedBall.Graphics.Layer layerToMoveTo) 
        {
            var layerToRemoveFrom = LayerProvidedByContainer;
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(SpriteBullet1);
            }
            FlatRedBall.SpriteManager.AddToLayer(SpriteBullet1, layerToMoveTo);
            if (layerToRemoveFrom != null)
            {
                layerToRemoveFrom.Remove(CircleInstance);
            }
            FlatRedBall.Math.Geometry.ShapeManager.AddToLayer(CircleInstance, layerToMoveTo);
            LayerProvidedByContainer = layerToMoveTo;
        }
    }
}
