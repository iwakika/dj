#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using RockBlaster.Entities;
using RockBlaster.Factories;
using FlatRedBall;
using FlatRedBall.Screens;
using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall.Math;
namespace RockBlaster.Screens
{
    public partial class GameScreen : FlatRedBall.Screens.Screen
    {
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        
        private FlatRedBall.Math.PositionedObjectList<RockBlaster.Entities.Bullet> BulletList;
        private FlatRedBall.Math.PositionedObjectList<RockBlaster.Entities.Rock> RockList;
        private FlatRedBall.Math.PositionedObjectList<RockBlaster.Entities.MainShip> MainShipList;
        private RockBlaster.Entities.MainShip MainShip1;
        private RockBlaster.Entities.Hud HudInstance;
        private RockBlaster.Entities.RockSpawner RockSpawnerInstance;
        private RockBlaster.Entities.EndGameUI EndGameUIInstance;
        private FlatRedBall.Math.PositionedObjectList<FlatRedBall.Math.Geometry.AxisAlignedRectangle> WallList;
        private FlatRedBall.Math.Geometry.AxisAlignedRectangle mWall1;
        public FlatRedBall.Math.Geometry.AxisAlignedRectangle Wall1
        {
            get
            {
                return mWall1;
            }
            private set
            {
                mWall1 = value;
            }
        }
        private FlatRedBall.Math.Geometry.AxisAlignedRectangle mWall2;
        public FlatRedBall.Math.Geometry.AxisAlignedRectangle Wall2
        {
            get
            {
                return mWall2;
            }
            private set
            {
                mWall2 = value;
            }
        }
        private FlatRedBall.Math.Geometry.AxisAlignedRectangle mWall3;
        public FlatRedBall.Math.Geometry.AxisAlignedRectangle Wall3
        {
            get
            {
                return mWall3;
            }
            private set
            {
                mWall3 = value;
            }
        }
        private FlatRedBall.Math.Geometry.AxisAlignedRectangle mWall4;
        public FlatRedBall.Math.Geometry.AxisAlignedRectangle Wall4
        {
            get
            {
                return mWall4;
            }
            private set
            {
                mWall4 = value;
            }
        }
        FlatRedBall.Gum.GumIdb gumIdb;
        public GameScreen () 
        	: base ("GameScreen")
        {
        }
        public override void Initialize (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            BulletList = new FlatRedBall.Math.PositionedObjectList<RockBlaster.Entities.Bullet>();
            BulletList.Name = "BulletList";
            RockList = new FlatRedBall.Math.PositionedObjectList<RockBlaster.Entities.Rock>();
            RockList.Name = "RockList";
            MainShipList = new FlatRedBall.Math.PositionedObjectList<RockBlaster.Entities.MainShip>();
            MainShipList.Name = "MainShipList";
            MainShip1 = new RockBlaster.Entities.MainShip(ContentManagerName, false);
            MainShip1.Name = "MainShip1";
            HudInstance = new RockBlaster.Entities.Hud(ContentManagerName, false);
            HudInstance.Name = "HudInstance";
            RockSpawnerInstance = new RockBlaster.Entities.RockSpawner(ContentManagerName, false);
            RockSpawnerInstance.Name = "RockSpawnerInstance";
            EndGameUIInstance = new RockBlaster.Entities.EndGameUI(ContentManagerName, false);
            EndGameUIInstance.Name = "EndGameUIInstance";
            WallList = new FlatRedBall.Math.PositionedObjectList<FlatRedBall.Math.Geometry.AxisAlignedRectangle>();
            WallList.Name = "WallList";
            mWall1 = new FlatRedBall.Math.Geometry.AxisAlignedRectangle();
            mWall1.Name = "mWall1";
            mWall2 = new FlatRedBall.Math.Geometry.AxisAlignedRectangle();
            mWall2.Name = "mWall2";
            mWall3 = new FlatRedBall.Math.Geometry.AxisAlignedRectangle();
            mWall3.Name = "mWall3";
            mWall4 = new FlatRedBall.Math.Geometry.AxisAlignedRectangle();
            mWall4.Name = "mWall4";
            gumIdb = new FlatRedBall.Gum.GumIdb();
            
            
            PostInitialize();
            base.Initialize(addToManagers);
            if (addToManagers)
            {
                AddToManagers();
            }
        }
        public override void AddToManagers () 
        {
            FlatRedBall.SpriteManager.AddDrawableBatch(gumIdb);
            Factories.BulletFactory.Initialize(ContentManagerName);
            Factories.RockFactory.Initialize(ContentManagerName);
            Factories.BulletFactory.AddList(BulletList);
            Factories.RockFactory.AddList(RockList);
            MainShip1.AddToManagers(mLayer);
            HudInstance.AddToManagers(mLayer);
            RockSpawnerInstance.AddToManagers(mLayer);
            EndGameUIInstance.AddToManagers(mLayer);
            FlatRedBall.Math.Geometry.ShapeManager.AddAxisAlignedRectangle(mWall1);
            FlatRedBall.Math.Geometry.ShapeManager.AddAxisAlignedRectangle(mWall2);
            FlatRedBall.Math.Geometry.ShapeManager.AddAxisAlignedRectangle(mWall3);
            FlatRedBall.Math.Geometry.ShapeManager.AddAxisAlignedRectangle(mWall4);
            base.AddToManagers();
            AddToManagersBottomUp();
            CustomInitialize();
        }
        public override void Activity (bool firstTimeCalled) 
        {
            if (!IsPaused)
            {
                
                for (int i = BulletList.Count - 1; i > -1; i--)
                {
                    if (i < BulletList.Count)
                    {
                        // We do the extra if-check because activity could destroy any number of entities
                        BulletList[i].Activity();
                    }
                }
                for (int i = RockList.Count - 1; i > -1; i--)
                {
                    if (i < RockList.Count)
                    {
                        // We do the extra if-check because activity could destroy any number of entities
                        RockList[i].Activity();
                    }
                }
                for (int i = MainShipList.Count - 1; i > -1; i--)
                {
                    if (i < MainShipList.Count)
                    {
                        // We do the extra if-check because activity could destroy any number of entities
                        MainShipList[i].Activity();
                    }
                }
                HudInstance.Activity();
                RockSpawnerInstance.Activity();
                EndGameUIInstance.Activity();
            }
            else
            {
            }
            base.Activity(firstTimeCalled);
            if (!IsActivityFinished)
            {
                CustomActivity(firstTimeCalled);
            }
        }
        public override void Destroy () 
        {
            FlatRedBall.SpriteManager.RemoveDrawableBatch(gumIdb);
            base.Destroy();
            Factories.BulletFactory.Destroy();
            Factories.RockFactory.Destroy();
            
            BulletList.MakeOneWay();
            RockList.MakeOneWay();
            MainShipList.MakeOneWay();
            WallList.MakeOneWay();
            for (int i = BulletList.Count - 1; i > -1; i--)
            {
                BulletList[i].Destroy();
            }
            for (int i = RockList.Count - 1; i > -1; i--)
            {
                RockList[i].Destroy();
            }
            for (int i = MainShipList.Count - 1; i > -1; i--)
            {
                MainShipList[i].Destroy();
            }
            if (HudInstance != null)
            {
                HudInstance.Destroy();
                HudInstance.Detach();
            }
            if (RockSpawnerInstance != null)
            {
                RockSpawnerInstance.Destroy();
                RockSpawnerInstance.Detach();
            }
            if (EndGameUIInstance != null)
            {
                EndGameUIInstance.Destroy();
                EndGameUIInstance.Detach();
            }
            for (int i = WallList.Count - 1; i > -1; i--)
            {
                FlatRedBall.Math.Geometry.ShapeManager.Remove(WallList[i]);
            }
            BulletList.MakeTwoWay();
            RockList.MakeTwoWay();
            MainShipList.MakeTwoWay();
            WallList.MakeTwoWay();
            FlatRedBall.Math.Collision.CollisionManager.Self.Relationships.Clear();
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            MainShipList.Add(MainShip1);
            if (MainShip1.Parent == null)
            {
                MainShip1.X = 0f;
            }
            else
            {
                MainShip1.RelativeX = 0f;
            }
            if (MainShip1.Parent == null)
            {
                MainShip1.Y = 0f;
            }
            else
            {
                MainShip1.RelativeY = 0f;
            }
            EndGameUIInstance.Visible = false;
            WallList.Add(Wall1);
            if (Wall1.Parent == null)
            {
                Wall1.X = 0f;
            }
            else
            {
                Wall1.RelativeX = 0f;
            }
            if (Wall1.Parent == null)
            {
                Wall1.Y = -300f;
            }
            else
            {
                Wall1.RelativeY = -300f;
            }
            Wall1.Width = 800f;
            Wall1.Height = 10f;
            WallList.Add(Wall2);
            if (Wall2.Parent == null)
            {
                Wall2.Y = 300f;
            }
            else
            {
                Wall2.RelativeY = 300f;
            }
            Wall2.Width = 800f;
            Wall2.Height = 10f;
            WallList.Add(Wall3);
            if (Wall3.Parent == null)
            {
                Wall3.X = -400f;
            }
            else
            {
                Wall3.RelativeX = -400f;
            }
            if (Wall3.Parent == null)
            {
                Wall3.Y = 0f;
            }
            else
            {
                Wall3.RelativeY = 0f;
            }
            Wall3.Width = 10f;
            Wall3.Height = 600f;
            WallList.Add(Wall4);
            if (Wall4.Parent == null)
            {
                Wall4.X = 400f;
            }
            else
            {
                Wall4.RelativeX = 400f;
            }
            Wall4.Width = 10f;
            Wall4.Height = 100f;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp () 
        {
            CameraSetup.ResetCamera(SpriteManager.Camera);
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            for (int i = BulletList.Count - 1; i > -1; i--)
            {
                BulletList[i].Destroy();
            }
            for (int i = RockList.Count - 1; i > -1; i--)
            {
                RockList[i].Destroy();
            }
            for (int i = MainShipList.Count - 1; i > -1; i--)
            {
                MainShipList[i].Destroy();
            }
            HudInstance.RemoveFromManagers();
            RockSpawnerInstance.RemoveFromManagers();
            EndGameUIInstance.RemoveFromManagers();
            for (int i = WallList.Count - 1; i > -1; i--)
            {
                FlatRedBall.Math.Geometry.ShapeManager.Remove(WallList[i]);
            }
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
                MainShip1.AssignCustomVariables(true);
                HudInstance.AssignCustomVariables(true);
                RockSpawnerInstance.AssignCustomVariables(true);
                EndGameUIInstance.AssignCustomVariables(true);
            }
            if (MainShip1.Parent == null)
            {
                MainShip1.X = 0f;
            }
            else
            {
                MainShip1.RelativeX = 0f;
            }
            if (MainShip1.Parent == null)
            {
                MainShip1.Y = 0f;
            }
            else
            {
                MainShip1.RelativeY = 0f;
            }
            EndGameUIInstance.Visible = false;
            if (Wall1.Parent == null)
            {
                Wall1.X = 0f;
            }
            else
            {
                Wall1.RelativeX = 0f;
            }
            if (Wall1.Parent == null)
            {
                Wall1.Y = -300f;
            }
            else
            {
                Wall1.RelativeY = -300f;
            }
            Wall1.Width = 800f;
            Wall1.Height = 10f;
            if (Wall2.Parent == null)
            {
                Wall2.Y = 300f;
            }
            else
            {
                Wall2.RelativeY = 300f;
            }
            Wall2.Width = 800f;
            Wall2.Height = 10f;
            if (Wall3.Parent == null)
            {
                Wall3.X = -400f;
            }
            else
            {
                Wall3.RelativeX = -400f;
            }
            if (Wall3.Parent == null)
            {
                Wall3.Y = 0f;
            }
            else
            {
                Wall3.RelativeY = 0f;
            }
            Wall3.Width = 10f;
            Wall3.Height = 600f;
            if (Wall4.Parent == null)
            {
                Wall4.X = 400f;
            }
            else
            {
                Wall4.RelativeX = 400f;
            }
            Wall4.Width = 10f;
            Wall4.Height = 100f;
        }
        public virtual void ConvertToManuallyUpdated () 
        {
            for (int i = 0; i < BulletList.Count; i++)
            {
                BulletList[i].ConvertToManuallyUpdated();
            }
            for (int i = 0; i < RockList.Count; i++)
            {
                RockList[i].ConvertToManuallyUpdated();
            }
            for (int i = 0; i < MainShipList.Count; i++)
            {
                MainShipList[i].ConvertToManuallyUpdated();
            }
            HudInstance.ConvertToManuallyUpdated();
            RockSpawnerInstance.ConvertToManuallyUpdated();
            EndGameUIInstance.ConvertToManuallyUpdated();
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            // Set the content manager for Gum
            var contentManagerWrapper = new FlatRedBall.Gum.ContentManagerWrapper();
            contentManagerWrapper.ContentManagerName = contentManagerName;
            RenderingLibrary.Content.LoaderManager.Self.ContentLoader = contentManagerWrapper;
            // Access the GumProject just in case it's async loaded
            var throwaway = GlobalContent.GumProject;
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            RockBlaster.Entities.Hud.LoadStaticContent(contentManagerName);
            RockBlaster.Entities.RockSpawner.LoadStaticContent(contentManagerName);
            RockBlaster.Entities.EndGameUI.LoadStaticContent(contentManagerName);
            CustomLoadStaticContent(contentManagerName);
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            return null;
        }
        public static object GetFile (string memberName) 
        {
            return null;
        }
        object GetMember (string memberName) 
        {
            return null;
        }
    }
}
