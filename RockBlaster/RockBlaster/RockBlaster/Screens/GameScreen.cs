using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;
using FlatRedBall.Localization;
using Microsoft.Xna.Framework.Input;
using RockBlaster.Entities;

namespace RockBlaster.Screens
{
	public partial class GameScreen
	{

		void CustomInitialize()
		{

            AssingInput();
            AddAdditionalShips();
            this.HudInstance.MainShipList = MainShipList;

        }

        private void AddAdditionalShips()
        {

            // We assume that the first player (player at index 0) 
            // is already part of the game.  Let's start with index 1:
            for (int i = 1; i < InputManager.Xbox360GamePads.Length; i++)
            {
                if (InputManager.Xbox360GamePads[i].IsConnected)
                {
                    MainShip mainShip = new MainShip(ContentManagerName);
                    mainShip.PlayerIndex = i;
                    MainShipList.Add(mainShip);
                    Console.WriteLine("Player conectado");
                }
            }

            const float spacingBetweenPlayers = 60;
            const float startingX = -90;
            // Reposition all players
            for (int i = 0; i < MainShipList.Count; i++)
            {
                MainShipList[i].X = -startingX + i * spacingBetweenPlayers;
            }

        }

        void CustomActivity(bool firstTimeCalled)
		{

            ColisionActivity();
            RemovalActivity();
            //FlatRedBall.Debugging.Debugger.TextCorner = FlatRedBall.Debugging.Debugger.Corner.BottomLeft;
            // FlatRedBall.Debugging.Debugger.Write(SpriteManager.ManagedPositionedObjects.Count);
            // FlatRedBall.SpriteManager.ManagedPositionedObjects;

            EndGameActivity();

        }

        private void RemovalActivity()
        {


            removalRock();
            removalBullets();    
            
        }

        private void EndGameActivity() {

            if (EndGameUIInstance.Visible == false && this.MainShipList.Count == 0)
            {
                EndGameUIInstance.Visible = true;
            }
        }

        private void removalRock()
        {

            ///destruir Rochas
            for (int i = RockList.Count - 1; i > -1; i--)
            {
                float absoluteX = Math.Abs(RockList[i].X);
                float absoluteY = Math.Abs(RockList[i].Y);

                const float removeBeyond = 600;
                if (absoluteX > removeBeyond || absoluteY > removeBeyond)
                {
                    RockList[i].Destroy();
                }
            }
        }

        private void removalBullets()
        {
            //destruir balas
            // inverter para detruir
            for (int i = BulletList.Count - 1; i > -1; i--)
            {
                float absoluteX = Math.Abs(BulletList[i].X);
                float absoluteY = Math.Abs(BulletList[i].Y);

                const float removeBeyond = 600;
                if (absoluteX > removeBeyond || absoluteY > removeBeyond)
                {
                    BulletList[i].Destroy();
                }


            }
        }
        void CustomDestroy()
		{


		}



        static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        private void AssingInput()
        {
            if (InputManager.Xbox360GamePads[0].IsConnected)
            {
            }
            else
            {
                KeyboardButtonMap();

            }

        }

        private void KeyboardButtonMap()
        {


            KeyboardButtonMap buttonMap = new KeyboardButtonMap();

            buttonMap.LeftAnalogLeft = Keys.Left;
            buttonMap.LeftAnalogRight = Keys.Right;
            buttonMap.LeftAnalogUp = Keys.Up;
            buttonMap.LeftAnalogDown = Keys.Down;

            buttonMap.A = Keys.A;
            buttonMap.B = Keys.S;
            buttonMap.X = Keys.Q;
            buttonMap.Y = Keys.W;

            InputManager.Xbox360GamePads[0].ButtonMap = buttonMap;
        }

        private void ColisionActivity()
        {

            BulletvsRockColition();
            MainShipVsRockCollisionActivity();
            RockVsRockCollisionActivity();
            MainShipVsWall();



        }

        private void MainShipVsWall()
        {

            for (int i = MainShipList.Count - 1; i >= 0; i--)            
            {
                MainShip mainShip = MainShipList[i];
                foreach (var wall in WallList)
                {
                    float shipMass = 0;
                    float wallMass = 1;

                    mainShip.CircleInstance.CollideAgainstMove(wall, shipMass, wallMass);


                }
            }

        }

        private void RockVsRockCollisionActivity()
        {
            //destroir n�o ser� chamado ent�o o loop pode ser natural
            //for (int i = RockList.Count - 1; i >= 0; i--)
             foreach(Rock rock in RockList)
             {
                // Rock rock = RockList[i];
                // for (int j = RockList.Count - 1; j >= 0; j--)
                foreach (Rock otherRock in RockList)
                {
                    //Rock otherRock = RockList[j];
                    if (rock != otherRock)
                    {
                        float rockMass = 1;
                        float otherRockMass = 1;
                        float elasticity = 0.8f;

                        rock.CircleInstance.CollideAgainstBounce(
                                                       otherRock.CircleInstance, rockMass, otherRockMass, elasticity);
                    }
                }
             }

      

            //throw new NotImplementedException();
        }

        private void MainShipVsRockCollisionActivity()
        {

            for (int i = MainShipList.Count - 1; i >= 0; i--)
            {
                MainShip mainShip = MainShipList[i];
                for (int j = RockList.Count - 1; j >= 0; j--)
                {
                    Rock rock = RockList[j];

                    if (rock.CircleInstance.CollideAgainst(mainShip.CircleInstance))
                    {
                        // rock.Destroy();
                        rock.TakeHit();
                        mainShip.Health--;
                        break;

                    }

                }
            }


        }

        private void BulletvsRockColition()
        {

            for(int i = BulletList.Count -1; i >= 0; i--)
            {
                Bullet bullet = BulletList[i];
                for (int j = RockList.Count - 1; j >= 0; j--)                   
                {
                    Rock rock = RockList[j];

                    if (rock.CircleInstance.CollideAgainst(bullet.CircleInstance))
                    {
                        rock.TakeHit();
                        bullet.Destroy();


                        Data.GlobalData.PlayerData.Score += rock.PointsWorth;
                        this.HudInstance.Score = Data.GlobalData.PlayerData.Score;
                        break;

                    }

                }
            } 
         
        }


    }
}
