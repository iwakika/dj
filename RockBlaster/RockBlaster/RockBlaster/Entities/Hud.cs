using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;

namespace RockBlaster.Entities
{
	public partial class Hud
	{
        
        FlatRedBall.Math.PositionedObjectList<MainShip> mMainShipList;
        public FlatRedBall.Math.PositionedObjectList<MainShip> MainShipList
        {
            set
            {
                mMainShipList = value;
                CreateHealthBarInstances();
            }

        }
    private void CreateHealthBarInstances()
        {
            // First we instantiate them
           
            for (int i = 0; i < mMainShipList.Count; i++)
            {
               
                HealthBar healthBar = new HealthBar(ContentManagerName, false);
                healthBar.AddToManagers(this.LayerProvidedByContainer);
                healthBar.AttachTo(this, false);
                this.HealthBarList.Add(healthBar);
                healthBar.MainShip = mMainShipList[i];
            }

            int divisions = mMainShipList.Count + 1;

            // Now let's position the HealthBars
            for (int i = 0; i < divisions - 1; i++)
            {
                HealthBarList[i].RelativeX = -SpriteManager.Camera.OrthogonalWidth / 2.0f +
                           SpriteManager.Camera.OrthogonalWidth * (i + 1) / (float)divisions;
                HealthBarList[i].RelativeY = HealthBarY;
            }
        }

        private void CustomInitialize()
		{


		}

		private void CustomActivity()
		{


		}

		private void CustomDestroy()
		{


		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }
	}
}
