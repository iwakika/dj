using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;
using RockBlaster.Factories;

namespace RockBlaster.Entities

{
    public partial class MainShip
    {

        //public I2DInput MovementInput { get; set; }
        //public IPressableInput Shoot { get; set; }
        
        Xbox360GamePad mGamePad = InputManager.Xbox360GamePads[0];

        int mPlayerIndex = 0;
        public int PlayerIndex
        {
            get { return mPlayerIndex; }
            set
            {
                mPlayerIndex = value;
                mGamePad = InputManager.Xbox360GamePads[mPlayerIndex];

                switch (mPlayerIndex)
                {
                    case 0:
                        this.Sprite.Texture = MainShip1;
                        break;
                    case 1:
                        this.Sprite.Texture = MainShip2;
                        break;
                    case 2:
                        this.Sprite.Texture = MainShip3;
                        break;
                    case 3:
                        this.Sprite.Texture = MainShip4;
                        break;
                }
            }
        }

        int mHealth;
                public int Health
                {
                    get
                    {
                        return mHealth;
                    }
                    set
                    {
                        mHealth = value;
                        if (mHealth <= 0)
                        {
                            Destroy();
                        }
                    }
                }



        /// <summary>
        /// Initialization logic which is execute only one time for this Entity (unless the Entity is pooled).
        /// This method is called when the Entity is added to managers. Entities which are instantiated but not
        /// added to managers will not have this method called.
        /// </summary>
        private void CustomInitialize()
        {
            Health = StartingHealth;

        }

        private void CustomActivity()
        {
            TurningActivity();
            MovementActivity();          
            ShootingActivity();





        }


        private void CustomDestroy()
        {


        }

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        private void MovementActivity()
        {
            //vai  para cima idemendente da posi��o atual da nave

            this.Velocity = this.RotationMatrix.Up * MovementSpeed;
        }

        private void TurningActivity()
        {

            this.RotationZVelocity = -mGamePad.LeftStick.Position.X * TurningSpeed;

        }

        public void ShootingActivity()
        {


            if (mGamePad.ButtonPushed(Xbox360GamePad.Button.A))
            {

                //riaremos 2 balas  porque � mais legal :D 

                Bullet firstBullet = BulletFactory.CreateNew();

                ///bala a direita

                firstBullet.Position = this.Position;
                firstBullet.Position += this.RotationMatrix.Up * 12;

               

                firstBullet.Position += this.RotationMatrix.Right * 6;
                firstBullet.RotationZ = this.RotationZ;
                firstBullet.Velocity = this.RotationMatrix.Up * firstBullet.MovementSpeed;

                // bala � esquerda
                Bullet secondBullet = BulletFactory.CreateNew();
                secondBullet.Position = this.Position;
                secondBullet.Position += this.RotationMatrix.Up * 12;
            
               
                secondBullet.Position -= this.RotationMatrix.Right * 6;
                secondBullet.RotationZ = this.RotationZ;
                secondBullet.Velocity = this.RotationMatrix.Up * secondBullet.MovementSpeed;


            }


        }
    }


}
