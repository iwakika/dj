using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;

namespace RockBlaster.Entities
{


	public partial class HealthBar
	{

        float mRatioFull;
        public float RatioFull
        {
            get { return mRatioFull; }
            set
            {
                mRatioFull = value;
                InterpolateBetween(VariableState.Empty, VariableState.Full, mRatioFull);
            }
        }

        public MainShip MainShip
        {
            get;
            set;
        }
        /// <summary>
        /// Initialization logic which is execute only one time for this Entity (unless the Entity is pooled).
        /// This method is called when the Entity is added to managers. Entities which are instantiated but not
        /// added to managers will not have this method called.
        /// </summary>
		private void CustomInitialize()
		{


		}

		private void CustomActivity()
		{
            if(MainShip!= null) { 
            RatioFull = MainShip.Health/ (float)MainShip.StartingHealth;
            }
            ChangeBarcolor();
        }

        private void ChangeBarcolor()
        {
           if (MainShip.Health == MainShip.StartingHealth / 2)
            {
                this.BarSpriteRed = 1;
                this.BarSpriteGreen = 0;
                this.BarSpriteBlue = 0;
            }
            
        }

        private void CustomDestroy()
		{


		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }
	}
}
