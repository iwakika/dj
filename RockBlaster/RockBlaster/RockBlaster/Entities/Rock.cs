using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;
using RockBlaster.Factories;

namespace RockBlaster.Entities
{
    public partial class Rock
    {
        /// <summary>
        /// Initialization logic which is execute only one time for this Entity (unless the Entity is pooled).
        /// This method is called when the Entity is added to managers. Entities which are instantiated but not
        /// added to managers will not have this method called.
        /// </summary>
		private void CustomInitialize()
        {


        }

        private void CustomActivity()
        {
            

        }

        private void RemovalActivity()
        {

        }


        private void CustomDestroy()
        {


        }

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        public void TakeHit()
        {
            switch (this.CurrentState)
            {

                case VariableState.Size1:
                    // faz nada;
                    break;
                case VariableState.Size2:
                    BreakIntoPieces(VariableState.Size1);
                    break;

                case VariableState.Size3:
                    BreakIntoPieces(VariableState.Size2);
                    break;
                case VariableState.Size4:
                    BreakIntoPieces(VariableState.Size3);
                    break;

            }

            this.Destroy();

        }

        private void BreakIntoPieces(VariableState newRockState)
        {

            for (int i = 0; i < NumberRocksToBreakInto; i++)
            {

                Rock newRock = RockFactory.CreateNew();
                newRock.Position = this.Position;

                newRock.Position.X += -1 + 2 * (float)(FlatRedBallServices.Random.NextDouble());
                newRock.Position.Y += -1 + 2 * (float)(FlatRedBallServices.Random.NextDouble());

                float randomAngle = (float)(FlatRedBallServices.Random.NextDouble() * System.Math.PI * 2);

                float
                speed = 0 + (float)(FlatRedBallServices.Random.NextDouble() * RandomSpeedOnBreak);
                newRock.Velocity = FlatRedBall.Math.MathFunctions.AngleToVector(randomAngle) * speed;
                //muda o estado atual do obbjeto
                newRock.CurrentState = newRockState;
            }
        }
    }
}
