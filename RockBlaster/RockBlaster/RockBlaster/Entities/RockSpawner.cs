using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;
using Microsoft.Xna.Framework;
using RockBlaster.Factories;
using RockBlaster.Entities;
using RockBlaster.Screens;

namespace RockBlaster.Entities
{
	public partial class RockSpawner
	{
        /// <summary>
        /// Initialization logic which is execute only one time for this Entity (unless the Entity is pooled).
        /// This method is called when the Entity is added to managers. Entities which are instantiated but not
        /// added to managers will not have this method called.
        /// </summary>
        /// 

        Double LastSpawnTime;
        Boolean IsTimeToSpawn
        {
            get
            {
                float spawFrequency = 1 / RockPerSecond;
                return FlatRedBall.Screens.ScreenManager.CurrentScreen.PauseAdjustedSecondsSince(LastSpawnTime)> spawFrequency;
            }
        }

        private void CustomInitialize()
		{


		}

		private void CustomActivity()
		{
           
            if (IsTimeToSpawn)
            {
                PerformSpawn();
            }

            this.RockPerSecond += TimeManager.SecondDifference * this.SpawRateIncrease;
		}

        private void PerformSpawn()
        {

            Vector3 position = GetRandomRockPosiotion();
            Vector3 velocity = GetRandomRockVelocity(position);
            
            Rock rock1 = RockFactory.CreateNew();
            rock1.CurrentState = Rock.VariableState.Size4;

            rock1.Position = position;
            rock1.Velocity = velocity;

            LastSpawnTime = FlatRedBall.Screens.ScreenManager.CurrentScreen.PauseAdjustedCurrentTime;
           
        }
        

        private static Vector3 GetRandomRockPosiotion()
        {


            //top, direita, baixo, esquerda
            //retorna 0,1,2, or 3
            int randomSide = FlatRedBallServices.Random.Next(4);

            //cordenadas absolutas da tela
            float topEdge = SpriteManager.Camera.AbsoluteTopYEdgeAt(0);
            float bottomEdge = SpriteManager.Camera.AbsoluteBottomYEdgeAt(0);
            float leftEdge = SpriteManager.Camera.AbsoluteLeftXEdgeAt(0);
            float rightEdge = SpriteManager.Camera.AbsoluteRightXEdgeAt(0);


            float minX = 0;
            float maxX = 0;
            float minY = 0;
            float maxY = 0;

            switch (randomSide)
            {
                case 0: // top
                    minX = leftEdge;
                    maxX = rightEdge;
                    minY = topEdge;
                    maxY = topEdge;
                    break;
                case 1: // right
                    minX = rightEdge;
                    maxX = rightEdge;
                    minY = bottomEdge;
                    maxY = topEdge;
                    break;
                case 2: // bottom
                    minX = leftEdge;
                    maxX = rightEdge;
                    minY = bottomEdge;
                    maxY = bottomEdge;
                    break;
                case 3: // left
                    minX = leftEdge;
                    maxX = leftEdge;
                    minY = bottomEdge;
                    maxY = topEdge;
                    break;
            }

            //pega um ponto randomico utilizando o minimo e o m�ximo valores

            float offScreenX = minX + (float)(FlatRedBallServices.Random.NextDouble() * (maxX - minX));
            float offScreenY = minY + (float)(FlatRedBallServices.Random.NextDouble() * (maxY - minY));
            
            //ParamArrayAttribute garantir que as rochas v�o aparecer apenas fora da tela;
            float amountToMoveBy = 64;

            switch (randomSide)
            {
                case 0: // top
                    offScreenY += amountToMoveBy;
                    break;
                case 1: // right
                    offScreenX += amountToMoveBy;
                    break;
                case 2: // bottom
                    offScreenY -= amountToMoveBy;
                    break;
                case 3: // left
                    offScreenX -= amountToMoveBy;
                    break;
            }

     


            return new Vector3(offScreenX, offScreenY, 0);
       
        }


        private Vector3 GetRandomRockVelocity(Vector3 position)
        {

           Vector3 centerOfGameScreen = new Vector3(SpriteManager.Camera.X, SpriteManager.Camera.Y, 0);
            
            // 2.  direciona o meteoro par ao centro da tela
            Vector3 directionToCenter = centerOfGameScreen - position ;
            // Console.WriteLine(directionToCenter);
            //adicionar margem aleat�ria
            directionToCenter.X += -1 +2 * (float)FlatRedBallServices.Random.NextDouble();
             directionToCenter.Y+= -1 +2 * (float)FlatRedBallServices.Random.NextDouble();
            //directionToCenter.X = FlatRedBallServices.Random.Next(-2,2);
            // directionToCenter.Y = FlatRedBallServices.Random.Next(-2,2);
            //Console.WriteLine(directionToCenter);




            directionToCenter.Normalize();
        

            float speed = MinVelocity +  (float)(FlatRedBallServices.Random.NextDouble() * (MaxVelocity - MinVelocity));
          

            
            return speed * directionToCenter;

        }
    

		private void CustomDestroy()
		{


		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }
	}
}
