<?xml version="1.0" encoding="UTF-8"?>
<tileset name="build_atlas" tilewidth="32" tileheight="32" tilecount="1024" columns="32">
 <image source="build_atlas.png" width="1024" height="1024"/>
 <tile id="64">
  <properties>
   <property name="Name" value="Wall7"/>
   <property name="SolidCollision" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="65">
  <properties>
   <property name="Name" value="Wall8"/>
   <property name="SolidCollision" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="66">
  <properties>
   <property name="Name" value="Wall9"/>
   <property name="SolidCollision" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="96">
  <properties>
   <property name="Name" value="Wall4"/>
   <property name="SolidCollision" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="97">
  <properties>
   <property name="Name" value="Wall5"/>
   <property name="SolidCollision" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="98">
  <properties>
   <property name="Name" value="Wall6"/>
   <property name="SolidCollision" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="99">
  <properties>
   <property name="Name" value="Door1"/>
  </properties>
 </tile>
 <tile id="128">
  <properties>
   <property name="Name" value="Wall1"/>
   <property name="SolidCollision" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="129">
  <properties>
   <property name="Name" value="Wall2"/>
   <property name="SolidCollision" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="130">
  <properties>
   <property name="Name" value="Wall3"/>
   <property name="SolidCollision" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="768">
  <properties>
   <property name="Name" value="WallGreen6"/>
   <property name="SolidCollision" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="769">
  <properties>
   <property name="Name" value="WallGreen5"/>
   <property name="SolidCollision" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="770">
  <properties>
   <property name="Name" value="WallGreen2"/>
   <property name="SolidCollision" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="771">
  <properties>
   <property name="Name" value=""/>
   <property name="SolidCollision" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="800">
  <properties>
   <property name="Name" value=""/>
   <property name="SolidCollision" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="801">
  <properties>
   <property name="Name" value=""/>
   <property name="SolidCollision" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="802">
  <properties>
   <property name="Name" value=""/>
   <property name="SolidCollision" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="803">
  <properties>
   <property name="Name" value=""/>
   <property name="SolidCollision" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="832">
  <properties>
   <property name="Name" value="WallGreen7"/>
   <property name="SolidCollision" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="833">
  <properties>
   <property name="Name" value=""/>
   <property name="SolidCollision" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="834">
  <properties>
   <property name="Name" value="WallGreen4"/>
   <property name="SolidCollision" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="835">
  <properties>
   <property name="Name" value=""/>
   <property name="SolidCollision" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="864">
  <properties>
   <property name="Name" value="WallGreen1"/>
   <property name="SolidCollision" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="865">
  <properties>
   <property name="Name" value=""/>
   <property name="SolidCollision" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="866">
  <properties>
   <property name="Name" value="WallGreen16"/>
   <property name="SolidCollision" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="867">
  <properties>
   <property name="Name" value=""/>
   <property name="SolidCollision" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="900">
  <properties>
   <property name="SolidCollision" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="932">
  <properties>
   <property name="SolidCollision" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="964">
  <properties>
   <property name="Name" value="WallGreen3"/>
   <property name="SolidCollision" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="996">
  <properties>
   <property name="SolidCollision" type="bool" value="false"/>
  </properties>
 </tile>
</tileset>
