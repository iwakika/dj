    namespace FlatRedBall.Gum
    {
        public  class GumIdbExtensions
        {
            public static void RegisterTypes () 
            {
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("Circle", typeof(GumTela.GumRuntimes.CircleRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("ColoredRectangle", typeof(GumTela.GumRuntimes.ColoredRectangleRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("Container", typeof(GumTela.GumRuntimes.ContainerRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("NineSlice", typeof(GumTela.GumRuntimes.NineSliceRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("Rectangle", typeof(GumTela.GumRuntimes.RectangleRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("Sprite", typeof(GumTela.GumRuntimes.SpriteRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("Text", typeof(GumTela.GumRuntimes.TextRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("btn", typeof(GumTela.GumRuntimes.btnRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("label", typeof(GumTela.GumRuntimes.labelRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("popUp", typeof(GumTela.GumRuntimes.popUpRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("Score", typeof(GumTela.GumRuntimes.ScoreRuntime));
                GumRuntime.ElementSaveExtensions.RegisterGueInstantiationType("MainMenuGum", typeof(GumTela.GumRuntimes.MainMenuGumRuntime));
            }
        }
    }
