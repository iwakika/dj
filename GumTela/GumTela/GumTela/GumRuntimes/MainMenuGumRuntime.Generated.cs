    using System.Linq;
    namespace GumTela.GumRuntimes
    {
        public partial class MainMenuGumRuntime : Gum.Wireframe.GraphicalUiElement
        {
            #region State Enums
            public enum VariableState
            {
                Default
            }
            #endregion
            #region State Fields
            VariableState mCurrentVariableState;
            #endregion
            #region State Properties
            public VariableState CurrentVariableState
            {
                get
                {
                    return mCurrentVariableState;
                }
                set
                {
                    mCurrentVariableState = value;
                    switch(mCurrentVariableState)
                    {
                        case  VariableState.Default:
                            btnInstance.X = 40f;
                            btnInstance.Y = 150f;
                            popUpInstance.ClipsChildren = true;
                            popUpInstance.CurrentVariableState = GumTela.GumRuntimes.popUpRuntime.VariableState.Default;
                            popUpInstance.WrapsChildren = true;
                            popUpInstance.X = 261f;
                            popUpInstance.Y = 21f;
                            ColoredRectangleInstance.X = 52f;
                            ColoredRectangleInstance.Y = 338f;
                            break;
                    }
                }
            }
            #endregion
            #region State Interpolation
            public void InterpolateBetween (VariableState firstState, VariableState secondState, float interpolationValue) 
            {
                #if DEBUG
                if (float.IsNaN(interpolationValue))
                {
                    throw new System.Exception("interpolationValue cannot be NaN");
                }
                #endif
                bool setbtnInstanceXFirstValue = false;
                bool setbtnInstanceXSecondValue = false;
                float btnInstanceXFirstValue= 0;
                float btnInstanceXSecondValue= 0;
                bool setbtnInstanceYFirstValue = false;
                bool setbtnInstanceYSecondValue = false;
                float btnInstanceYFirstValue= 0;
                float btnInstanceYSecondValue= 0;
                bool setColoredRectangleInstanceXFirstValue = false;
                bool setColoredRectangleInstanceXSecondValue = false;
                float ColoredRectangleInstanceXFirstValue= 0;
                float ColoredRectangleInstanceXSecondValue= 0;
                bool setColoredRectangleInstanceYFirstValue = false;
                bool setColoredRectangleInstanceYSecondValue = false;
                float ColoredRectangleInstanceYFirstValue= 0;
                float ColoredRectangleInstanceYSecondValue= 0;
                bool setpopUpInstanceCurrentVariableStateFirstValue = false;
                bool setpopUpInstanceCurrentVariableStateSecondValue = false;
                popUpRuntime.VariableState popUpInstanceCurrentVariableStateFirstValue= popUpRuntime.VariableState.Default;
                popUpRuntime.VariableState popUpInstanceCurrentVariableStateSecondValue= popUpRuntime.VariableState.Default;
                bool setpopUpInstanceXFirstValue = false;
                bool setpopUpInstanceXSecondValue = false;
                float popUpInstanceXFirstValue= 0;
                float popUpInstanceXSecondValue= 0;
                bool setpopUpInstanceYFirstValue = false;
                bool setpopUpInstanceYSecondValue = false;
                float popUpInstanceYFirstValue= 0;
                float popUpInstanceYSecondValue= 0;
                switch(firstState)
                {
                    case  VariableState.Default:
                        setbtnInstanceXFirstValue = true;
                        btnInstanceXFirstValue = 40f;
                        setbtnInstanceYFirstValue = true;
                        btnInstanceYFirstValue = 150f;
                        setColoredRectangleInstanceXFirstValue = true;
                        ColoredRectangleInstanceXFirstValue = 52f;
                        setColoredRectangleInstanceYFirstValue = true;
                        ColoredRectangleInstanceYFirstValue = 338f;
                        if (interpolationValue < 1)
                        {
                            this.popUpInstance.ClipsChildren = true;
                        }
                        setpopUpInstanceCurrentVariableStateFirstValue = true;
                        popUpInstanceCurrentVariableStateFirstValue = GumTela.GumRuntimes.popUpRuntime.VariableState.Default;
                        if (interpolationValue < 1)
                        {
                            this.popUpInstance.WrapsChildren = true;
                        }
                        setpopUpInstanceXFirstValue = true;
                        popUpInstanceXFirstValue = 261f;
                        setpopUpInstanceYFirstValue = true;
                        popUpInstanceYFirstValue = 21f;
                        break;
                }
                switch(secondState)
                {
                    case  VariableState.Default:
                        setbtnInstanceXSecondValue = true;
                        btnInstanceXSecondValue = 40f;
                        setbtnInstanceYSecondValue = true;
                        btnInstanceYSecondValue = 150f;
                        setColoredRectangleInstanceXSecondValue = true;
                        ColoredRectangleInstanceXSecondValue = 52f;
                        setColoredRectangleInstanceYSecondValue = true;
                        ColoredRectangleInstanceYSecondValue = 338f;
                        if (interpolationValue >= 1)
                        {
                            this.popUpInstance.ClipsChildren = true;
                        }
                        setpopUpInstanceCurrentVariableStateSecondValue = true;
                        popUpInstanceCurrentVariableStateSecondValue = GumTela.GumRuntimes.popUpRuntime.VariableState.Default;
                        if (interpolationValue >= 1)
                        {
                            this.popUpInstance.WrapsChildren = true;
                        }
                        setpopUpInstanceXSecondValue = true;
                        popUpInstanceXSecondValue = 261f;
                        setpopUpInstanceYSecondValue = true;
                        popUpInstanceYSecondValue = 21f;
                        break;
                }
                if (setbtnInstanceXFirstValue && setbtnInstanceXSecondValue)
                {
                    btnInstance.X = btnInstanceXFirstValue * (1 - interpolationValue) + btnInstanceXSecondValue * interpolationValue;
                }
                if (setbtnInstanceYFirstValue && setbtnInstanceYSecondValue)
                {
                    btnInstance.Y = btnInstanceYFirstValue * (1 - interpolationValue) + btnInstanceYSecondValue * interpolationValue;
                }
                if (setColoredRectangleInstanceXFirstValue && setColoredRectangleInstanceXSecondValue)
                {
                    ColoredRectangleInstance.X = ColoredRectangleInstanceXFirstValue * (1 - interpolationValue) + ColoredRectangleInstanceXSecondValue * interpolationValue;
                }
                if (setColoredRectangleInstanceYFirstValue && setColoredRectangleInstanceYSecondValue)
                {
                    ColoredRectangleInstance.Y = ColoredRectangleInstanceYFirstValue * (1 - interpolationValue) + ColoredRectangleInstanceYSecondValue * interpolationValue;
                }
                if (setpopUpInstanceCurrentVariableStateFirstValue && setpopUpInstanceCurrentVariableStateSecondValue)
                {
                    popUpInstance.InterpolateBetween(popUpInstanceCurrentVariableStateFirstValue, popUpInstanceCurrentVariableStateSecondValue, interpolationValue);
                }
                if (setpopUpInstanceXFirstValue && setpopUpInstanceXSecondValue)
                {
                    popUpInstance.X = popUpInstanceXFirstValue * (1 - interpolationValue) + popUpInstanceXSecondValue * interpolationValue;
                }
                if (setpopUpInstanceYFirstValue && setpopUpInstanceYSecondValue)
                {
                    popUpInstance.Y = popUpInstanceYFirstValue * (1 - interpolationValue) + popUpInstanceYSecondValue * interpolationValue;
                }
                if (interpolationValue < 1)
                {
                    mCurrentVariableState = firstState;
                }
                else
                {
                    mCurrentVariableState = secondState;
                }
            }
            #endregion
            #region State Interpolate To
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (GumTela.GumRuntimes.MainMenuGumRuntime.VariableState fromState,GumTela.GumRuntimes.MainMenuGumRuntime.VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null) 
            {
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from:0, to:1, duration:(float)secondsToTake, type:interpolationType, easing:easing );
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(fromState, toState, newPosition);
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = this.ElementSave.States.First(item => item.Name == toState.ToString());
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateToRelative (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = AddToCurrentValuesWithState(toState);
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            #endregion
            #region State Animations
            #endregion
            public override void StopAnimations () 
            {
                base.StopAnimations();
                btnInstance.StopAnimations();
                popUpInstance.StopAnimations();
                ScoreInstance.StopAnimations();
            }
            #region Get Current Values on State
            private Gum.DataTypes.Variables.StateSave GetCurrentValuesOnState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "btnInstance.X",
                            Type = "float",
                            Value = btnInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "btnInstance.Y",
                            Type = "float",
                            Value = btnInstance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "popUpInstance.Clips Children",
                            Type = "bool",
                            Value = popUpInstance.ClipsChildren
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "popUpInstance.State",
                            Type = "State",
                            Value = popUpInstance.CurrentVariableState
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "popUpInstance.Wraps Children",
                            Type = "bool",
                            Value = popUpInstance.WrapsChildren
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "popUpInstance.X",
                            Type = "float",
                            Value = popUpInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "popUpInstance.Y",
                            Type = "float",
                            Value = popUpInstance.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ColoredRectangleInstance.X",
                            Type = "float",
                            Value = ColoredRectangleInstance.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ColoredRectangleInstance.Y",
                            Type = "float",
                            Value = ColoredRectangleInstance.Y
                        }
                        );
                        break;
                }
                return newState;
            }
            private Gum.DataTypes.Variables.StateSave AddToCurrentValuesWithState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "btnInstance.X",
                            Type = "float",
                            Value = btnInstance.X + 40f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "btnInstance.Y",
                            Type = "float",
                            Value = btnInstance.Y + 150f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "popUpInstance.Clips Children",
                            Type = "bool",
                            Value = popUpInstance.ClipsChildren
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "popUpInstance.State",
                            Type = "State",
                            Value = popUpInstance.CurrentVariableState
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "popUpInstance.Wraps Children",
                            Type = "bool",
                            Value = popUpInstance.WrapsChildren
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "popUpInstance.X",
                            Type = "float",
                            Value = popUpInstance.X + 261f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "popUpInstance.Y",
                            Type = "float",
                            Value = popUpInstance.Y + 21f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ColoredRectangleInstance.X",
                            Type = "float",
                            Value = ColoredRectangleInstance.X + 52f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "ColoredRectangleInstance.Y",
                            Type = "float",
                            Value = ColoredRectangleInstance.Y + 338f
                        }
                        );
                        break;
                }
                return newState;
            }
            #endregion
            public override void ApplyState (Gum.DataTypes.Variables.StateSave state) 
            {
                bool matches = this.ElementSave.AllStates.Contains(state);
                if (matches)
                {
                    var category = this.ElementSave.Categories.FirstOrDefault(item => item.States.Contains(state));
                    if (category == null)
                    {
                        if (state.Name == "Default") this.mCurrentVariableState = VariableState.Default;
                    }
                }
                base.ApplyState(state);
            }
            private GumTela.GumRuntimes.btnRuntime btnInstance { get; set; }
            private GumTela.GumRuntimes.popUpRuntime popUpInstance { get; set; }
            private GumTela.GumRuntimes.ScoreRuntime ScoreInstance { get; set; }
            private GumTela.GumRuntimes.ColoredRectangleRuntime ColoredRectangleInstance { get; set; }
            public MainMenuGumRuntime (bool fullInstantiation = true) 
            {
                if (fullInstantiation)
                {
                    Gum.DataTypes.ElementSave elementSave = Gum.Managers.ObjectFinder.Self.GumProjectSave.Screens.First(item => item.Name == "MainMenuGum");
                    this.ElementSave = elementSave;
                    string oldDirectory = FlatRedBall.IO.FileManager.RelativeDirectory;
                    FlatRedBall.IO.FileManager.RelativeDirectory = FlatRedBall.IO.FileManager.GetDirectory(Gum.Managers.ObjectFinder.Self.GumProjectSave.FullFileName);
                    GumRuntime.ElementSaveExtensions.SetGraphicalUiElement(elementSave, this, RenderingLibrary.SystemManagers.Default);
                    FlatRedBall.IO.FileManager.RelativeDirectory = oldDirectory;
                }
            }
            public override void SetInitialState () 
            {
                base.SetInitialState();
                this.CurrentVariableState = VariableState.Default;
                CallCustomInitialize();
            }
            public override void CreateChildrenRecursively (Gum.DataTypes.ElementSave elementSave, RenderingLibrary.SystemManagers systemManagers) 
            {
                base.CreateChildrenRecursively(elementSave, systemManagers);
                this.AssignReferences();
            }
            private void AssignReferences () 
            {
                btnInstance = this.GetGraphicalUiElementByName("btnInstance") as GumTela.GumRuntimes.btnRuntime;
                popUpInstance = this.GetGraphicalUiElementByName("popUpInstance") as GumTela.GumRuntimes.popUpRuntime;
                ScoreInstance = this.GetGraphicalUiElementByName("ScoreInstance") as GumTela.GumRuntimes.ScoreRuntime;
                ColoredRectangleInstance = this.GetGraphicalUiElementByName("ColoredRectangleInstance") as GumTela.GumRuntimes.ColoredRectangleRuntime;
            }
            public override void AddToManagers (RenderingLibrary.SystemManagers managers, RenderingLibrary.Graphics.Layer layer) 
            {
                base.AddToManagers(managers, layer);
            }
            private void CallCustomInitialize () 
            {
                CustomInitialize();
            }
            partial void CustomInitialize();
        }
    }
