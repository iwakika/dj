    using System.Linq;
    namespace GumTela.GumRuntimes
    {
        public partial class ScoreRuntime : GumTela.GumRuntimes.ContainerRuntime
        {
            #region State Enums
            public enum VariableState
            {
                Default
            }
            #endregion
            #region State Fields
            VariableState mCurrentVariableState;
            #endregion
            #region State Properties
            public VariableState CurrentVariableState
            {
                get
                {
                    return mCurrentVariableState;
                }
                set
                {
                    mCurrentVariableState = value;
                    switch(mCurrentVariableState)
                    {
                        case  VariableState.Default:
                            Height = 0f;
                            HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            Width = 0f;
                            WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                            PLayer1.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                            PLayer1.Text = "Player1";
                            PLayer1.VerticalAlignment = RenderingLibrary.Graphics.VerticalAlignment.Center;
                            PLayer1.Width = 0f;
                            PLayer1.WidthUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                            PLayer1.X = 0f;
                            PLayer1.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Left;
                            PLayer1.XUnits = Gum.Converters.GeneralUnitType.PixelsFromSmall;
                            PLayer1.Y = 0f;
                            PLayer1.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Top;
                            PLayer1.YUnits = Gum.Converters.GeneralUnitType.PixelsFromSmall;
                            PLayer2.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                            PLayer2.Text = "PLayer2";
                            PLayer2.VerticalAlignment = RenderingLibrary.Graphics.VerticalAlignment.Center;
                            PLayer2.Width = 0f;
                            PLayer2.WidthUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                            PLayer2.X = 0f;
                            PLayer2.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Right;
                            PLayer2.XUnits = Gum.Converters.GeneralUnitType.PixelsFromLarge;
                            PLayer2.Y = 0f;
                            PLayer2.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Top;
                            PLayer2.YUnits = Gum.Converters.GeneralUnitType.PixelsFromSmall;
                            break;
                    }
                }
            }
            #endregion
            #region State Interpolation
            public void InterpolateBetween (VariableState firstState, VariableState secondState, float interpolationValue) 
            {
                #if DEBUG
                if (float.IsNaN(interpolationValue))
                {
                    throw new System.Exception("interpolationValue cannot be NaN");
                }
                #endif
                bool setHeightFirstValue = false;
                bool setHeightSecondValue = false;
                float HeightFirstValue= 0;
                float HeightSecondValue= 0;
                bool setPLayer1WidthFirstValue = false;
                bool setPLayer1WidthSecondValue = false;
                float PLayer1WidthFirstValue= 0;
                float PLayer1WidthSecondValue= 0;
                bool setPLayer1XFirstValue = false;
                bool setPLayer1XSecondValue = false;
                float PLayer1XFirstValue= 0;
                float PLayer1XSecondValue= 0;
                bool setPLayer1YFirstValue = false;
                bool setPLayer1YSecondValue = false;
                float PLayer1YFirstValue= 0;
                float PLayer1YSecondValue= 0;
                bool setPLayer2WidthFirstValue = false;
                bool setPLayer2WidthSecondValue = false;
                float PLayer2WidthFirstValue= 0;
                float PLayer2WidthSecondValue= 0;
                bool setPLayer2XFirstValue = false;
                bool setPLayer2XSecondValue = false;
                float PLayer2XFirstValue= 0;
                float PLayer2XSecondValue= 0;
                bool setPLayer2YFirstValue = false;
                bool setPLayer2YSecondValue = false;
                float PLayer2YFirstValue= 0;
                float PLayer2YSecondValue= 0;
                bool setWidthFirstValue = false;
                bool setWidthSecondValue = false;
                float WidthFirstValue= 0;
                float WidthSecondValue= 0;
                switch(firstState)
                {
                    case  VariableState.Default:
                        setHeightFirstValue = true;
                        HeightFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue < 1)
                        {
                            this.PLayer1.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        if (interpolationValue < 1)
                        {
                            this.PLayer1.Text = "Player1";
                        }
                        if (interpolationValue < 1)
                        {
                            this.PLayer1.VerticalAlignment = RenderingLibrary.Graphics.VerticalAlignment.Center;
                        }
                        setPLayer1WidthFirstValue = true;
                        PLayer1WidthFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.PLayer1.WidthUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        setPLayer1XFirstValue = true;
                        PLayer1XFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.PLayer1.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Left;
                        }
                        if (interpolationValue < 1)
                        {
                            this.PLayer1.XUnits = Gum.Converters.GeneralUnitType.PixelsFromSmall;
                        }
                        setPLayer1YFirstValue = true;
                        PLayer1YFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.PLayer1.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Top;
                        }
                        if (interpolationValue < 1)
                        {
                            this.PLayer1.YUnits = Gum.Converters.GeneralUnitType.PixelsFromSmall;
                        }
                        if (interpolationValue < 1)
                        {
                            this.PLayer2.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        if (interpolationValue < 1)
                        {
                            this.PLayer2.Text = "PLayer2";
                        }
                        if (interpolationValue < 1)
                        {
                            this.PLayer2.VerticalAlignment = RenderingLibrary.Graphics.VerticalAlignment.Center;
                        }
                        setPLayer2WidthFirstValue = true;
                        PLayer2WidthFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.PLayer2.WidthUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        setPLayer2XFirstValue = true;
                        PLayer2XFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.PLayer2.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Right;
                        }
                        if (interpolationValue < 1)
                        {
                            this.PLayer2.XUnits = Gum.Converters.GeneralUnitType.PixelsFromLarge;
                        }
                        setPLayer2YFirstValue = true;
                        PLayer2YFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.PLayer2.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Top;
                        }
                        if (interpolationValue < 1)
                        {
                            this.PLayer2.YUnits = Gum.Converters.GeneralUnitType.PixelsFromSmall;
                        }
                        setWidthFirstValue = true;
                        WidthFirstValue = 0f;
                        if (interpolationValue < 1)
                        {
                            this.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        break;
                }
                switch(secondState)
                {
                    case  VariableState.Default:
                        setHeightSecondValue = true;
                        HeightSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.HeightUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.PLayer1.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.PLayer1.Text = "Player1";
                        }
                        if (interpolationValue >= 1)
                        {
                            this.PLayer1.VerticalAlignment = RenderingLibrary.Graphics.VerticalAlignment.Center;
                        }
                        setPLayer1WidthSecondValue = true;
                        PLayer1WidthSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.PLayer1.WidthUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        setPLayer1XSecondValue = true;
                        PLayer1XSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.PLayer1.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Left;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.PLayer1.XUnits = Gum.Converters.GeneralUnitType.PixelsFromSmall;
                        }
                        setPLayer1YSecondValue = true;
                        PLayer1YSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.PLayer1.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Top;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.PLayer1.YUnits = Gum.Converters.GeneralUnitType.PixelsFromSmall;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.PLayer2.HorizontalAlignment = RenderingLibrary.Graphics.HorizontalAlignment.Center;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.PLayer2.Text = "PLayer2";
                        }
                        if (interpolationValue >= 1)
                        {
                            this.PLayer2.VerticalAlignment = RenderingLibrary.Graphics.VerticalAlignment.Center;
                        }
                        setPLayer2WidthSecondValue = true;
                        PLayer2WidthSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.PLayer2.WidthUnits = Gum.DataTypes.DimensionUnitType.Percentage;
                        }
                        setPLayer2XSecondValue = true;
                        PLayer2XSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.PLayer2.XOrigin = RenderingLibrary.Graphics.HorizontalAlignment.Right;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.PLayer2.XUnits = Gum.Converters.GeneralUnitType.PixelsFromLarge;
                        }
                        setPLayer2YSecondValue = true;
                        PLayer2YSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.PLayer2.YOrigin = RenderingLibrary.Graphics.VerticalAlignment.Top;
                        }
                        if (interpolationValue >= 1)
                        {
                            this.PLayer2.YUnits = Gum.Converters.GeneralUnitType.PixelsFromSmall;
                        }
                        setWidthSecondValue = true;
                        WidthSecondValue = 0f;
                        if (interpolationValue >= 1)
                        {
                            this.WidthUnits = Gum.DataTypes.DimensionUnitType.RelativeToContainer;
                        }
                        break;
                }
                if (setHeightFirstValue && setHeightSecondValue)
                {
                    Height = HeightFirstValue * (1 - interpolationValue) + HeightSecondValue * interpolationValue;
                }
                if (setPLayer1WidthFirstValue && setPLayer1WidthSecondValue)
                {
                    PLayer1.Width = PLayer1WidthFirstValue * (1 - interpolationValue) + PLayer1WidthSecondValue * interpolationValue;
                }
                if (setPLayer1XFirstValue && setPLayer1XSecondValue)
                {
                    PLayer1.X = PLayer1XFirstValue * (1 - interpolationValue) + PLayer1XSecondValue * interpolationValue;
                }
                if (setPLayer1YFirstValue && setPLayer1YSecondValue)
                {
                    PLayer1.Y = PLayer1YFirstValue * (1 - interpolationValue) + PLayer1YSecondValue * interpolationValue;
                }
                if (setPLayer2WidthFirstValue && setPLayer2WidthSecondValue)
                {
                    PLayer2.Width = PLayer2WidthFirstValue * (1 - interpolationValue) + PLayer2WidthSecondValue * interpolationValue;
                }
                if (setPLayer2XFirstValue && setPLayer2XSecondValue)
                {
                    PLayer2.X = PLayer2XFirstValue * (1 - interpolationValue) + PLayer2XSecondValue * interpolationValue;
                }
                if (setPLayer2YFirstValue && setPLayer2YSecondValue)
                {
                    PLayer2.Y = PLayer2YFirstValue * (1 - interpolationValue) + PLayer2YSecondValue * interpolationValue;
                }
                if (setWidthFirstValue && setWidthSecondValue)
                {
                    Width = WidthFirstValue * (1 - interpolationValue) + WidthSecondValue * interpolationValue;
                }
                if (interpolationValue < 1)
                {
                    mCurrentVariableState = firstState;
                }
                else
                {
                    mCurrentVariableState = secondState;
                }
            }
            #endregion
            #region State Interpolate To
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (GumTela.GumRuntimes.ScoreRuntime.VariableState fromState,GumTela.GumRuntimes.ScoreRuntime.VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null) 
            {
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from:0, to:1, duration:(float)secondsToTake, type:interpolationType, easing:easing );
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(fromState, toState, newPosition);
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateTo (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = this.ElementSave.States.First(item => item.Name == toState.ToString());
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            public FlatRedBall.Glue.StateInterpolation.Tweener InterpolateToRelative (VariableState toState, double secondsToTake, FlatRedBall.Glue.StateInterpolation.InterpolationType interpolationType, FlatRedBall.Glue.StateInterpolation.Easing easing, object owner = null ) 
            {
                Gum.DataTypes.Variables.StateSave current = GetCurrentValuesOnState(toState);
                Gum.DataTypes.Variables.StateSave toAsStateSave = AddToCurrentValuesWithState(toState);
                FlatRedBall.Glue.StateInterpolation.Tweener tweener = new FlatRedBall.Glue.StateInterpolation.Tweener(from: 0, to: 1, duration: (float)secondsToTake, type: interpolationType, easing: easing);
                if (owner == null)
                {
                    tweener.Owner = this;
                }
                else
                {
                    tweener.Owner = owner;
                }
                tweener.PositionChanged = newPosition => this.InterpolateBetween(current, toAsStateSave, newPosition);
                tweener.Ended += ()=> this.CurrentVariableState = toState;
                tweener.Start();
                StateInterpolationPlugin.TweenerManager.Self.Add(tweener);
                return tweener;
            }
            #endregion
            #region State Animations
            #endregion
            public override void StopAnimations () 
            {
                base.StopAnimations();
            }
            #region Get Current Values on State
            private Gum.DataTypes.Variables.StateSave GetCurrentValuesOnState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Height",
                            Type = "float",
                            Value = Height
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Height Units",
                            Type = "DimensionUnitType",
                            Value = HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Width",
                            Type = "float",
                            Value = Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Width Units",
                            Type = "DimensionUnitType",
                            Value = WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer1.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = PLayer1.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer1.Text",
                            Type = "string",
                            Value = PLayer1.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer1.VerticalAlignment",
                            Type = "VerticalAlignment",
                            Value = PLayer1.VerticalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer1.Width",
                            Type = "float",
                            Value = PLayer1.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer1.Width Units",
                            Type = "DimensionUnitType",
                            Value = PLayer1.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer1.X",
                            Type = "float",
                            Value = PLayer1.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer1.X Origin",
                            Type = "HorizontalAlignment",
                            Value = PLayer1.XOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer1.X Units",
                            Type = "PositionUnitType",
                            Value = PLayer1.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer1.Y",
                            Type = "float",
                            Value = PLayer1.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer1.Y Origin",
                            Type = "VerticalAlignment",
                            Value = PLayer1.YOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer1.Y Units",
                            Type = "PositionUnitType",
                            Value = PLayer1.YUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer2.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = PLayer2.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer2.Text",
                            Type = "string",
                            Value = PLayer2.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer2.VerticalAlignment",
                            Type = "VerticalAlignment",
                            Value = PLayer2.VerticalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer2.Width",
                            Type = "float",
                            Value = PLayer2.Width
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer2.Width Units",
                            Type = "DimensionUnitType",
                            Value = PLayer2.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer2.X",
                            Type = "float",
                            Value = PLayer2.X
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer2.X Origin",
                            Type = "HorizontalAlignment",
                            Value = PLayer2.XOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer2.X Units",
                            Type = "PositionUnitType",
                            Value = PLayer2.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer2.Y",
                            Type = "float",
                            Value = PLayer2.Y
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer2.Y Origin",
                            Type = "VerticalAlignment",
                            Value = PLayer2.YOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer2.Y Units",
                            Type = "PositionUnitType",
                            Value = PLayer2.YUnits
                        }
                        );
                        break;
                }
                return newState;
            }
            private Gum.DataTypes.Variables.StateSave AddToCurrentValuesWithState (VariableState state) 
            {
                Gum.DataTypes.Variables.StateSave newState = new Gum.DataTypes.Variables.StateSave();
                switch(state)
                {
                    case  VariableState.Default:
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Height",
                            Type = "float",
                            Value = Height + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Height Units",
                            Type = "DimensionUnitType",
                            Value = HeightUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Width",
                            Type = "float",
                            Value = Width + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "Width Units",
                            Type = "DimensionUnitType",
                            Value = WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer1.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = PLayer1.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer1.Text",
                            Type = "string",
                            Value = PLayer1.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer1.VerticalAlignment",
                            Type = "VerticalAlignment",
                            Value = PLayer1.VerticalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer1.Width",
                            Type = "float",
                            Value = PLayer1.Width + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer1.Width Units",
                            Type = "DimensionUnitType",
                            Value = PLayer1.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer1.X",
                            Type = "float",
                            Value = PLayer1.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer1.X Origin",
                            Type = "HorizontalAlignment",
                            Value = PLayer1.XOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer1.X Units",
                            Type = "PositionUnitType",
                            Value = PLayer1.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer1.Y",
                            Type = "float",
                            Value = PLayer1.Y + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer1.Y Origin",
                            Type = "VerticalAlignment",
                            Value = PLayer1.YOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer1.Y Units",
                            Type = "PositionUnitType",
                            Value = PLayer1.YUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer2.HorizontalAlignment",
                            Type = "HorizontalAlignment",
                            Value = PLayer2.HorizontalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer2.Text",
                            Type = "string",
                            Value = PLayer2.Text
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer2.VerticalAlignment",
                            Type = "VerticalAlignment",
                            Value = PLayer2.VerticalAlignment
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer2.Width",
                            Type = "float",
                            Value = PLayer2.Width + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer2.Width Units",
                            Type = "DimensionUnitType",
                            Value = PLayer2.WidthUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer2.X",
                            Type = "float",
                            Value = PLayer2.X + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer2.X Origin",
                            Type = "HorizontalAlignment",
                            Value = PLayer2.XOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer2.X Units",
                            Type = "PositionUnitType",
                            Value = PLayer2.XUnits
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer2.Y",
                            Type = "float",
                            Value = PLayer2.Y + 0f
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer2.Y Origin",
                            Type = "VerticalAlignment",
                            Value = PLayer2.YOrigin
                        }
                        );
                        newState.Variables.Add(new Gum.DataTypes.Variables.VariableSave()
                        {
                            SetsValue = true,
                            Name = "PLayer2.Y Units",
                            Type = "PositionUnitType",
                            Value = PLayer2.YUnits
                        }
                        );
                        break;
                }
                return newState;
            }
            #endregion
            public override void ApplyState (Gum.DataTypes.Variables.StateSave state) 
            {
                bool matches = this.ElementSave.AllStates.Contains(state);
                if (matches)
                {
                    var category = this.ElementSave.Categories.FirstOrDefault(item => item.States.Contains(state));
                    if (category == null)
                    {
                        if (state.Name == "Default") this.mCurrentVariableState = VariableState.Default;
                    }
                }
                base.ApplyState(state);
            }
            private GumTela.GumRuntimes.TextRuntime PLayer1 { get; set; }
            private GumTela.GumRuntimes.TextRuntime PLayer2 { get; set; }
            public string PLayer1Text
            {
                get
                {
                    return PLayer1.Text;
                }
                set
                {
                    if (PLayer1.Text != value)
                    {
                        PLayer1.Text = value;
                        PLayer1TextChanged?.Invoke(this, null);
                    }
                }
            }
            public string PLayer2Text
            {
                get
                {
                    return PLayer2.Text;
                }
                set
                {
                    if (PLayer2.Text != value)
                    {
                        PLayer2.Text = value;
                        PLayer2TextChanged?.Invoke(this, null);
                    }
                }
            }
            public event System.EventHandler PLayer1TextChanged;
            public event System.EventHandler PLayer2TextChanged;
            public ScoreRuntime (bool fullInstantiation = true) 
            	: base(false)
            {
                this.HasEvents = false;
                this.ExposeChildrenEvents = false;
                if (fullInstantiation)
                {
                    Gum.DataTypes.ElementSave elementSave = Gum.Managers.ObjectFinder.Self.GumProjectSave.Components.First(item => item.Name == "Score");
                    this.ElementSave = elementSave;
                    string oldDirectory = FlatRedBall.IO.FileManager.RelativeDirectory;
                    FlatRedBall.IO.FileManager.RelativeDirectory = FlatRedBall.IO.FileManager.GetDirectory(Gum.Managers.ObjectFinder.Self.GumProjectSave.FullFileName);
                    GumRuntime.ElementSaveExtensions.SetGraphicalUiElement(elementSave, this, RenderingLibrary.SystemManagers.Default);
                    FlatRedBall.IO.FileManager.RelativeDirectory = oldDirectory;
                }
            }
            public override void SetInitialState () 
            {
                base.SetInitialState();
                this.CurrentVariableState = VariableState.Default;
                CallCustomInitialize();
            }
            public override void CreateChildrenRecursively (Gum.DataTypes.ElementSave elementSave, RenderingLibrary.SystemManagers systemManagers) 
            {
                base.CreateChildrenRecursively(elementSave, systemManagers);
                this.AssignReferences();
            }
            private void AssignReferences () 
            {
                PLayer1 = this.GetGraphicalUiElementByName("PLayer1") as GumTela.GumRuntimes.TextRuntime;
                PLayer2 = this.GetGraphicalUiElementByName("PLayer2") as GumTela.GumRuntimes.TextRuntime;
            }
            public override void AddToManagers (RenderingLibrary.SystemManagers managers, RenderingLibrary.Graphics.Layer layer) 
            {
                base.AddToManagers(managers, layer);
            }
            private void CallCustomInitialize () 
            {
                CustomInitialize();
            }
            partial void CustomInitialize();
        }
    }
