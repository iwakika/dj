#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using FlatRedBall;
using FlatRedBall.Screens;
using System;
using System.Collections.Generic;
using System.Text;
namespace GumTela.Screens
{
    public partial class Game : FlatRedBall.Screens.Screen
    {
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        
        public bool UsesTmxLevelFiles = true;
        FlatRedBall.Gum.GumIdb gumIdb;
        public Game () 
        	: base ("Game")
        {
        }
        public override void Initialize (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            gumIdb = new FlatRedBall.Gum.GumIdb();
            
            
            PostInitialize();
            base.Initialize(addToManagers);
            if (addToManagers)
            {
                AddToManagers();
            }
        }
        public override void AddToManagers () 
        {
            FlatRedBall.SpriteManager.AddDrawableBatch(gumIdb);
            base.AddToManagers();
            AddToManagersBottomUp();
            CustomInitialize();
        }
        public override void Activity (bool firstTimeCalled) 
        {
            if (!IsPaused)
            {
                
                if (CurrentTileMap != null)
                {
                    CurrentTileMap.AnimateSelf();
                }
                if (CurrentTileMap != null)
                {
                    CurrentTileMap.AnimateSelf();
                }
            }
            else
            {
            }
            base.Activity(firstTimeCalled);
            if (!IsActivityFinished)
            {
                CustomActivity(firstTimeCalled);
            }
        }
        public override void Destroy () 
        {
            FlatRedBall.SpriteManager.RemoveDrawableBatch(gumIdb);
            base.Destroy();
            
            FlatRedBall.Math.Collision.CollisionManager.Self.Relationships.Clear();
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp () 
        {
            CameraSetup.ResetCamera(SpriteManager.Camera);
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
            }
            UsesTmxLevelFiles = true;
        }
        public virtual void ConvertToManuallyUpdated () 
        {
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            // Set the content manager for Gum
            var contentManagerWrapper = new FlatRedBall.Gum.ContentManagerWrapper();
            contentManagerWrapper.ContentManagerName = contentManagerName;
            RenderingLibrary.Content.LoaderManager.Self.ContentLoader = contentManagerWrapper;
            // Access the GumProject just in case it's async loaded
            var throwaway = GlobalContent.GumProject;
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            CustomLoadStaticContent(contentManagerName);
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            return null;
        }
        public static object GetFile (string memberName) 
        {
            return null;
        }
        object GetMember (string memberName) 
        {
            return null;
        }
        FlatRedBall.TileGraphics.LayeredTileMap CurrentTileMap;
        void InitializeLevel (string levelName) 
        {
            CurrentTileMap = GetFile(levelName) as FlatRedBall.TileGraphics.LayeredTileMap;
            CurrentTileMap.AddToManagers();
            // This sets the min and max values for the Camera so it can't view beyond the edge of the map
            // If you don't like it, you an add this to your CustomInitialize:
            // FlatRedBall.Camera.Main.ClearBorders();
            FlatRedBall.Camera.Main.SetBordersAtZ(0, -CurrentTileMap.Height, CurrentTileMap.Width, 0, 0);
            FlatRedBall.TileCollisions.TileShapeCollectionLayeredTileMapExtensions.AddCollisionFrom(SolidCollisions, CurrentTileMap);
            FlatRedBall.TileEntities.TileEntityInstantiator.CreateEntitiesFrom(CurrentTileMap);
        }
        FlatRedBall.TileGraphics.LayeredTileMap CurrentTileMap;
        void InitializeLevel (string levelName) 
        {
            CurrentTileMap = GetFile(levelName) as FlatRedBall.TileGraphics.LayeredTileMap;
            CurrentTileMap.AddToManagers();
            var levelInfoObject = GetFile(levelName + "Info");
            // This sets the min and max values for the Camera so it can't view beyond the edge of the map
            // If you don't like it, you an add this to your CustomInitialize:
            // FlatRedBall.Camera.Main.ClearBorders();
            FlatRedBall.Camera.Main.SetBordersAtZ(0, -CurrentTileMap.Height, CurrentTileMap.Width, 0, 0);
            if (levelInfoObject is System.Collections.Generic.List<DataTypes.TileMapInfo>)
            {
                FlatRedBall.TileCollisions.TileShapeCollectionLayeredTileMapExtensions.AddCollisionFrom(SolidCollisions, CurrentTileMap, levelInfoObject as System.Collections.Generic.List<DataTypes.TileMapInfo>);
                FlatRedBall.TileEntities.TileEntityInstantiator.CreateEntitiesFrom(CurrentTileMap, levelInfoObject as System.Collections.Generic.List<DataTypes.TileMapInfo>);
            }
            else
            {
                FlatRedBall.TileCollisions.TileShapeCollectionLayeredTileMapExtensions.AddCollisionFrom(SolidCollisions, CurrentTileMap, levelInfoObject as System.Collections.Generic.Dictionary<string, DataTypes.TileMapInfo>);
                FlatRedBall.TileEntities.TileEntityInstantiator.CreateEntitiesFrom(CurrentTileMap, levelInfoObject as System.Collections.Generic.Dictionary<string, DataTypes.TileMapInfo>);
            }
            // initialize the animations:
            Dictionary<string, FlatRedBall.Graphics.Animation.AnimationChain> animationDictionary = new Dictionary<string, FlatRedBall.Graphics.Animation.AnimationChain>();
            foreach (var item in levelInfoObject as System.Collections.Generic.List<DataTypes.TileMapInfo>)
            {
                if (item.EmbeddedAnimation != null && item.EmbeddedAnimation.Count != 0)
                {
                    FlatRedBall.Graphics.Animation.AnimationChain newChain = new FlatRedBall.Graphics.Animation.AnimationChain();
                    newChain.Name = item.Name + "Animation";
                    animationDictionary.Add(item.Name, newChain);
                    foreach (var frameSave in item.EmbeddedAnimation)
                    {
                        var frame = new FlatRedBall.Graphics.Animation.AnimationFrame();
                        int index = 0;
                        int.TryParse(frameSave.TextureName, out index);
                        frame.Texture = CurrentTileMap.MapLayers[index].Texture;
                        frame.FrameLength = frameSave.FrameLength;
                        // We use pixel coords in the Save:
                        frame.LeftCoordinate = frameSave.LeftCoordinate / frame.Texture.Width;
                        frame.RightCoordinate = frameSave.RightCoordinate / frame.Texture.Width;
                        frame.TopCoordinate = frameSave.TopCoordinate / frame.Texture.Height;
                        frame.BottomCoordinate = frameSave.BottomCoordinate / frame.Texture.Height;
                        // finish others
                        newChain.Add(frame);
                    }
                }
            }
            CurrentTileMap.Animation = new FlatRedBall.TileGraphics.LayeredTileMapAnimation(animationDictionary);
        }
    }
}
