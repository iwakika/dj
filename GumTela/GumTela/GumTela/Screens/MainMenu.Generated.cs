#if ANDROID || IOS || DESKTOP_GL
#define REQUIRES_PRIMARY_THREAD_LOADING
#endif
using Color = Microsoft.Xna.Framework.Color;
using FlatRedBall;
using FlatRedBall.Screens;
using System;
using System.Collections.Generic;
using System.Text;
namespace GumTela.Screens
{
    public partial class MainMenu : FlatRedBall.Screens.Screen
    {
        #if DEBUG
        static bool HasBeenLoadedWithGlobalContentManager = false;
        #endif
        protected static FlatRedBall.Gum.GumIdb MainMenuGum;
        
        private GumTela.GumRuntimes.btnRuntime btnInstance;
        private GumTela.GumRuntimes.popUpRuntime popUpInstance;
        private GumTela.GumRuntimes.ScoreRuntime ScoreInstance;
        private GumTela.GumRuntimes.ColoredRectangleRuntime ColoredRectangleInstance;
        public event FlatRedBall.Gui.WindowEvent btnInstanceClick;
        public event FlatRedBall.Gui.WindowEvent popUpInstancebtnCancelClick;
        public event FlatRedBall.Gui.WindowEvent popUpInstancebtnOkClick;
        public MainMenu () 
        	: base ("MainMenu")
        {
        }
        public override void Initialize (bool addToManagers) 
        {
            LoadStaticContent(ContentManagerName);
            btnInstance = MainMenuGum.GetGraphicalUiElementByName("btnInstance") as GumTela.GumRuntimes.btnRuntime;
            popUpInstance = MainMenuGum.GetGraphicalUiElementByName("popUpInstance") as GumTela.GumRuntimes.popUpRuntime;
            ScoreInstance = MainMenuGum.GetGraphicalUiElementByName("ScoreInstance") as GumTela.GumRuntimes.ScoreRuntime;
            ColoredRectangleInstance = MainMenuGum.GetGraphicalUiElementByName("ColoredRectangleInstance") as GumTela.GumRuntimes.ColoredRectangleRuntime;
            
            
            PostInitialize();
            base.Initialize(addToManagers);
            if (addToManagers)
            {
                AddToManagers();
            }
        }
        public override void AddToManagers () 
        {
            MainMenuGum.InstanceInitialize(); FlatRedBall.FlatRedBallServices.GraphicsOptions.SizeOrOrientationChanged += MainMenuGum.HandleResolutionChanged;
            base.AddToManagers();
            AddToManagersBottomUp();
            CustomInitialize();
        }
        public override void Activity (bool firstTimeCalled) 
        {
            if (!IsPaused)
            {
                
            }
            else
            {
            }
            base.Activity(firstTimeCalled);
            if (!IsActivityFinished)
            {
                CustomActivity(firstTimeCalled);
            }
        }
        public override void Destroy () 
        {
            base.Destroy();
            FlatRedBall.SpriteManager.RemoveDrawableBatch(MainMenuGum); FlatRedBall.FlatRedBallServices.GraphicsOptions.SizeOrOrientationChanged -= MainMenuGum.HandleResolutionChanged;
            MainMenuGum = null;
            
            if (btnInstance != null)
            {
                btnInstance.RemoveFromManagers();
            }
            if (popUpInstance != null)
            {
                popUpInstance.RemoveFromManagers();
            }
            if (ScoreInstance != null)
            {
                ScoreInstance.RemoveFromManagers();
            }
            if (ColoredRectangleInstance != null)
            {
                ColoredRectangleInstance.RemoveFromManagers();
            }
            FlatRedBall.Math.Collision.CollisionManager.Self.Relationships.Clear();
            CustomDestroy();
        }
        public virtual void PostInitialize () 
        {
            bool oldShapeManagerSuppressAdd = FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = true;
            btnInstance.Click += OnbtnInstanceClick;
            btnInstance.Click += OnbtnInstanceClickTunnel;
            popUpInstance.btnCancelClick += OnpopUpInstancebtnCancelClick;
            popUpInstance.btnCancelClick += OnpopUpInstancebtnCancelClickTunnel;
            popUpInstance.btnOkClick += OnpopUpInstancebtnOkClick;
            popUpInstance.btnOkClick += OnpopUpInstancebtnOkClickTunnel;
            FlatRedBall.Math.Geometry.ShapeManager.SuppressAddingOnVisibilityTrue = oldShapeManagerSuppressAdd;
        }
        public virtual void AddToManagersBottomUp () 
        {
            CameraSetup.ResetCamera(SpriteManager.Camera);
            AssignCustomVariables(false);
        }
        public virtual void RemoveFromManagers () 
        {
            if (btnInstance != null)
            {
                btnInstance.RemoveFromManagers();
            }
            if (popUpInstance != null)
            {
                popUpInstance.RemoveFromManagers();
            }
            if (ScoreInstance != null)
            {
                ScoreInstance.RemoveFromManagers();
            }
            if (ColoredRectangleInstance != null)
            {
                ColoredRectangleInstance.RemoveFromManagers();
            }
        }
        public virtual void AssignCustomVariables (bool callOnContainedElements) 
        {
            if (callOnContainedElements)
            {
            }
        }
        public virtual void ConvertToManuallyUpdated () 
        {
        }
        public static void LoadStaticContent (string contentManagerName) 
        {
            if (string.IsNullOrEmpty(contentManagerName))
            {
                throw new System.ArgumentException("contentManagerName cannot be empty or null");
            }
            // Set the content manager for Gum
            var contentManagerWrapper = new FlatRedBall.Gum.ContentManagerWrapper();
            contentManagerWrapper.ContentManagerName = contentManagerName;
            RenderingLibrary.Content.LoaderManager.Self.ContentLoader = contentManagerWrapper;
            // Access the GumProject just in case it's async loaded
            var throwaway = GlobalContent.GumProject;
            #if DEBUG
            if (contentManagerName == FlatRedBall.FlatRedBallServices.GlobalContentManager)
            {
                HasBeenLoadedWithGlobalContentManager = true;
            }
            else if (HasBeenLoadedWithGlobalContentManager)
            {
                throw new System.Exception("This type has been loaded with a Global content manager, then loaded with a non-global.  This can lead to a lot of bugs");
            }
            #endif
            Gum.Wireframe.GraphicalUiElement.IsAllLayoutSuspended = true;  MainMenuGum = new FlatRedBall.Gum.GumIdb();  MainMenuGum.LoadFromFile("content/gumproject/screens/mainmenugum.gusx");  MainMenuGum.AssignReferences();Gum.Wireframe.GraphicalUiElement.IsAllLayoutSuspended = false; MainMenuGum.Element.UpdateLayout(); MainMenuGum.Element.UpdateLayout();
            CustomLoadStaticContent(contentManagerName);
        }
        [System.Obsolete("Use GetFile instead")]
        public static object GetStaticMember (string memberName) 
        {
            switch(memberName)
            {
                case  "MainMenuGum":
                    return MainMenuGum;
            }
            return null;
        }
        public static object GetFile (string memberName) 
        {
            switch(memberName)
            {
                case  "MainMenuGum":
                    return MainMenuGum;
            }
            return null;
        }
        object GetMember (string memberName) 
        {
            switch(memberName)
            {
                case  "MainMenuGum":
                    return MainMenuGum;
            }
            return null;
        }
    }
}
