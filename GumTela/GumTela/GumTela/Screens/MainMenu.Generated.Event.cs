using System;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Specialized;
using FlatRedBall.Audio;
using FlatRedBall.Screens;
using GumTela.Screens;
namespace GumTela.Screens
{
    public partial class MainMenu
    {
        void OnbtnInstanceClickTunnel (FlatRedBall.Gui.IWindow window) 
        {
            if (this.btnInstanceClick != null)
            {
                btnInstanceClick(window);
            }
        }
        void OnpopUpInstancebtnCancelClickTunnel (FlatRedBall.Gui.IWindow window) 
        {
            if (this.popUpInstancebtnCancelClick != null)
            {
                popUpInstancebtnCancelClick(window);
            }
        }
        void OnpopUpInstancebtnOkClickTunnel (FlatRedBall.Gui.IWindow window) 
        {
            if (this.popUpInstancebtnOkClick != null)
            {
                popUpInstancebtnOkClick(window);
            }
        }
    }
}
